#! /usr/bin/python

import sys

infile = open(sys.argv[1], "r").read() ## this should be a csv
osum = sys.argv[1] + '.sum'

#Iterate over lines and store correct/covered counts
correct_dict = {}
covered_dict = {}
methods = []
for line in infile.split("\n")[1:]:
    if not line.strip():
        continue
    query, pid, spec, called_spec, method, call_made, call, call_final = line.split(",")
    if method not in correct_dict:
        correct_dict[method] = [0,0]
        covered_dict[method] = [0,0]
        methods.append(method)
    if call_made == "Y":
        covered_dict[method][0] += 1
        covered_dict[method][1] += 1
        if call == "Y":
            correct_dict[method][0] += 1
            correct_dict[method][1] += 1
        else:
            correct_dict[method][0] += 0
            correct_dict[method][1] += 1
    else:
        covered_dict[method][0] += 0
        covered_dict[method][1] += 1

#Write to output CSV file
outfile = open(osum, "w")
outfile.write("method,covered,correct\n")
for method in methods:
    correct = int(float(correct_dict[method][0]) / float(correct_dict[method][1]) * 100)
    covered = int(float(covered_dict[method][0]) / float(covered_dict[method][1]) * 100)
    outfile.write("%s,%i,%i\n" % (method, covered, correct))
