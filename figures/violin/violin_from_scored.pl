#!/bin/env perl

use strict;
use warnings;
use Cwd 'abs_path';

## usage:       perl violin_from_scored.pl scored.csv

## Setup paths
my @p = split(/\//, abs_path($0));
my $script_dir = join '/', @p[0..$#p-1];
my $vp = $script_dir . '/violin_plot.py';
my $gs = $script_dir . '/generate_summary_csv.py';
my $scored = shift;
my $sf = $scored . '.cf';
my $sum = $sf . '.sum';
my $cm = $sf . '.cm';

## Generate call made file
open my $cfh, '>', $cm or die $!;
open my $ffh, '>', $sf or die $!;
open my $sfh, '<', $scored or die $!;
while(<$sfh>){
	chomp;
	if($_ =~ m/^query/){
		print $ffh join(',', $_, 'call_final') . "\n";
		$_ =~ s/,call$/,correct/;
		print $cfh join(',', $_, 'call_final') . "\n";
	}else{
		my ($q, $pid, $spec, $called_spec, $meth, $call_made, $call) = split(/,/, $_);
		$meth =~ s/forced_prediCAT/prediCAT_Forced/;
		my $call_fin = 'X';
		if($call_made eq 'Y'){
			$call_fin = $call;
			print $cfh join(',', $q, $pid, $spec, $called_spec, $meth, $call_made, $call, $call_fin) . "\n";
		}
		print $ffh join(',', $q, $pid, $spec, $called_spec, $meth, $call_made, $call, $call_fin) . "\n";
	}
}
close $sfh;
close $ffh;
close $cfh;

## Generate summary file
system("python $gs $sf");

## Draw violin
system("python $vp $sf $cm $sum");
