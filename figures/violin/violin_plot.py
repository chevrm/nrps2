#! /usr/bin/python

import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import sys

## usage: python violin_plot scored.csv scored.csv.cm scored.csv.sum

pref = sys.argv[1] + '.vp'
ord = ['prediCAT_SNN50', 'prediCAT', 'prediCAT_Forced', 'nrps2_svm', 'minowa', 'pHMM', 'seqlnrps', 'bach_rav', 'nrpssp', 'nrps2_stach']

#Draw violin plot for correct per method by % ID
figure = plt.figure()
figure.set_size_inches(10, 3.5)
nrpsdata = pd.read_csv(sys.argv[2])
sns.set(style="whitegrid", palette="pastel", color_codes=True)
sns.violinplot(x="method", y="pid", hue="correct", data=nrpsdata.sort("method"), split=True, scale="count", 
               inner="quart", palette={"Y": "g", "N": "r"}, order=ord, bw=.3)
sns.despine(left=True)
plt.ylim((25,100))
apm = pref + '.acc_per_method.svg'
figure.savefig(apm)

#Draw violin plot for covered per method by % ID
figure = plt.figure()
figure.set_size_inches(10, 3.5)
nrpsdata2 = pd.read_csv(sys.argv[1])
sns.set(style="whitegrid", palette="pastel", color_codes=True)
sns.violinplot(x="method", y="pid", hue="call_made", data=nrpsdata2.sort("method"), split=True, scale="count", 
               inner="quart", palette={"Y": "g", "N": "r"}, order=ord, bw=.3)
sns.despine(left=True)
plt.ylim((0,100))
cpm = pref + '.cov_per_method.svg'
figure.savefig(cpm)

#Draw bar plot with correct per method
nrps_summary = pd.read_csv(sys.argv[3])
figure = plt.figure()
figure.set_size_inches(10, 2)
sns.barplot(x="method", y="correct", data=nrps_summary.sort("method"), palette="Set3")
plt.ylim((0,100))
ba = pref + '.baracc.svg'
figure.savefig(ba)

#Draw bar plot with cov per method
figure = plt.figure()
figure.set_size_inches(10, 2)
sns.barplot(x="method", y="covered", data=nrps_summary.sort("method"), palette="Set3")
plt.ylim((0,100))
bc = pref + '.barcov.svg'
figure.savefig(bc)
