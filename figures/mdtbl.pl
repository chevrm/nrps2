#!/bin/env perl

use strict;
use warnings;

while(<>){
	chomp;
	my @ln = split(/\t/, $_);
	unless($ln[0] =~ m/Spec/ || $ln[0] =~ m/Subst/){
	    foreach my $i (1..scalar(@ln)-1){
		unless($ln[$i] eq 'NA'){
		    $ln[$i] = sprintf("%.3f", $ln[$i]);
		}
	    }
	}
	print '|' . join('|', @ln) . "|\n";
}
