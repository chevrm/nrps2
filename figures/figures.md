# NRPS Figures and Tables Draft

## Figure 1

![Image Alt](./fig1_draft2.png)

**_Figure 1:_**  _Assembly and diversity of NRPS-derived molecules. C = condensation domain, A = adenylation domain, PCP = peptidyl carrier protein, AA = amino acid.  (a) A specific AA substrate is adenylated by the A-domain (step 1) and loaded onto the PCP (step 2).  A peptide bond is then formed between the AA of the current and upstream modules at the C-domain acceptor site (step 3). (b) The growing peptide binds at the C-domain donor site of the downstream module (step 1). (c) Structural diversity of NRPS-derived compounds._

## Figure 2

![Image Alt](./fig2_draft.png)
![Image Alt](./fig2f_draft.png)

**_Figure 2:_**  _Overview of prediCAT. Structural similarities of (a) ornithine (orn) and (b) 2,4-diaminobutyric acid (dab). Examples of (c) monophyly prediCAT classification, (d) nearest neighbor (NN) prediCAT classification, and (e) an ambiguous tree for which no prediCAT call is made. (f) Accuracy of prediCAT across SNN scores (bins of size 0.5)._

## Figure 3

![Image Alt](./fig3_draft2.png)

**_Figure 3:_**  _Dataset (a) source, (b) phylum distribution of the total dataset, and (c) phylum distribution of the train and test datasets._

## Figure 4

![Image Alt](./nfig4a_draft.png)

![Image Alt](./nfig4b_draft.png)

![Image Alt](./nfig4c_draft.png)

**_Figure 4:_**  _Comparison of current NRPS prediction algoritms. (a) Accuracy and (b) coverage distributions of NRPS prediction methods at different sequence percent identity to the training set. Solid and dashed lines indicate means and quartiles, respectively. Training data for prediCAT methods from Khayatt dataset.  All other algorithms were used as released.  Test data was comprised of A-domains from MIBiG (post-Khayatt) and manual sources. PID was calculated as protein sequence identity of test queries to the Khayatt dataset. (c) Pairwise accuracy ratios of shared coverage. Ratio calculated as (fraction correct method A) / (fraction correct method B)._

## Table 1

| Method A | Method B | Shared Coverage Count | McNemar p-value |
|:---:|:---:|:---:|:---:|:---:|
|NRPSPredictor2 - ASM|Minowa|336|2.41e-13
|NRPSPredictor2 - ASM|Khayatt|429|2.39e-09
|NRPSPredictor2 - SVM|Minowa|262|5.39e-09
|NRPSPredictor2 - ASM|SEQL-NRPS|429|4.59e-08
|NRPSPredictor2 - ASM|prediCAT - Monophyly & NN|410|9.56e-07
|NRPSPredictor2 - ASM|NRPSSP|429|1.21e-05
|NRPSPredictor2 - SVM|Khayatt|303|1.81e-05
|NRPSSP|Minowa|337|5.23e-05
|NRPSPredictor2 - ASM|Bachmann-Ravel|304|5.23e-05
|NRPSPredictor2 - SVM|SEQL-NRPS|303|1.78e-04
|NRPSPredictor2 - SVM|prediCAT - Monophyly & NN|290|2.00e-04
|Khayatt|Minowa|337|1.43e-03
|prediCAT - Monophyly & NN|Minowa|321|3.57e-03
|Bachmann-Ravel|Minowa|272|3.57e-03
|prediCAT - SNN Score >= 0.5|Minowa|58|4.43e-03
|NRPSPredictor2 - SVM|Bachmann-Ravel|247|1.04e-02
|prediCAT - SNN Score >= 0.5|SEQL-NRPS|63|1.33e-02
|Bachmann-Ravel|SEQL-NRPS|306|1.61e-02
|prediCAT - SNN Score >= 0.5|NRPSSP|63|4.12e-02
|NRPSPredictor2 - SVM|NRPSSP|303|4.25e-02



**_Table 1:_**  _McNemar p-values < 0.05 for shared coverage A-domains denote significant differences between correct and incorrect classifications between algorithms.  Absent pairings were not significant. Training and test data used as defined in Figure 4._

## Figure 5

![Image Alt](./taxlev2.png)

![Image Alt](./prf2.png)

**_Figure 5:_**  _Jackknife validation. 10 independent random shuffles were performed to segregate the full dataset into tenths (92 or 93 sequences).  Each 1/10 was used to test methods trained on the remaining 9/10.  (a) Accuracy of methods across different taxonomic groups. (b) Weighted means of precision, recall, and F-score  across individual and ensemble methods._

## Table 2
|Individual Method|Shared JackKnife Queries|Individual Accuracy|SANDPUMA Accuracy|McNemar p-value|
|:-----------:|:----------------:|:---------:|:------:|:--------:|
|ASM|7255|0.905|0.922|6.59e-07|
|SVM|7201|0.824|0.908|1.95e-90|
|prediCAT Monophyly|4846|0.899|0.945|4.25e-36|
|prediCAT SNN >= 0.5|5116|0.879|0.939|1.27e-52|
|pHMM|7935|0.727|0.899|2.03e-246|

**_Table 2:_**  _Comparison of SANDPUMA to individual methods.  Training and test data used as defined in Figure 5._


## Table 3
| Substrate | # Jackknife Queries | Precision | Recall | F-score |
|:-----------:|:----------------:|:---------:|:------:|:--------:|
|3oh-3me-pro|20|1.000|0.850|0.919|
|4oh-pro|20|1.000|0.900|0.947|
|aad|110|1.000|1.000|1.000|
|abu|20|1.000|0.500|0.667|
|aile|10|0.769|1.000|0.870|
|ala|545|0.863|0.783|0.821|
|alpha-me-ser|10|1.000|0.700|0.824|
|aoh-gly|10|0.435|1.000|0.606|
|arg|90|0.673|0.367|0.475|
|asn|297|0.993|0.953|0.973|
|asp|250|0.942|0.916|0.929|
|athr|30|0.667|0.333|0.444|
|b-ala|40|0.682|0.375|0.484|
|bht|95|0.989|0.926|0.957|
|bmt|29|0.476|0.345|0.400|
|cys|250|1.000|0.864|0.927|
|dab|128|0.847|0.906|0.875|
|d-ala|40|0.833|0.250|0.385|
|d-asn|10|0.714|1.000|0.833|
|d-athr|20|0.619|0.650|0.634|
|d-cyclo-horn|10|0.200|0.200|0.200|
|d-dab|10|1.000|0.800|0.889|
|deoxy-thr|20|1.000|0.900|0.947|
|d-gln|30|1.000|0.400|0.571|
|dhabu|40|0.719|0.575|0.639|
|dhb|10|1.000|1.000|1.000|
|d-leu|50|0.634|0.520|0.571|
|dpg|150|1.000|1.000|1.000|
|d-pro|19|0.783|0.947|0.857|
|d-ser|30|0.897|0.867|0.881|
|d-trp|10|0.833|0.500|0.625|
|d-val|80|1.000|0.675|0.806|
|gln|200|0.925|0.620|0.743|
|glu|210|0.864|0.881|0.873|
|gly|380|1.000|0.887|0.940|
|goh-val|10|0.889|0.800|0.842|
|his|30|1.000|0.267|0.421|
|horn|110|0.875|0.636|0.737|
|hpg|88|1.000|0.989|0.994|
|ile|210|0.925|0.824|0.872|
|kyn|20|1.000|0.800|0.889|
|leu|760|0.910|0.854|0.881|
|lys|190|0.812|0.589|0.683|
|me-asp|40|0.976|1.000|0.988|
|met|10|0.286|0.800|0.421|
|nme-fhorn|20|0.150|0.300|0.200|
|noh-ala|10|1.000|0.800|0.889|
|noh-val|20|0.950|0.950|0.950|
|orn|210|0.774|0.867|0.818|
|phe|209|0.904|0.722|0.803|
|phe-ac|30|0.903|0.933|0.918|
|pip|80|0.982|0.700|0.818|
|pro|300|0.860|0.837|0.848|
|ser|630|0.979|0.897|0.936|
|thr|524|0.905|0.889|0.897|
|trp|200|0.882|0.485|0.626|
|tyr|260|0.942|0.746|0.833|
|val|725|0.920|0.830|0.873|

**_Table 3:_**  _Classification statistics of SANDPUMA ensemble (max depth 40, minimum leaf support 10) on jackknifed dataset by substrate specificity. Training and test data used as defined in Figure 5._

## Figure 6

![Image Alt](./tree.png)

![Image Alt](./shannon.png)

# Supplemental

## Table S1
| Method | Predictor Type | Year of Training | Reference |
|:-:|:-:|:-:|
|Bachmann-Ravel|Active-site Motifs|2009|Bachmann and Ravel, 2009|
|Khayatt|Profile Hidden Markov Models|2013|Khayatt et al., 2013|
|prediCAT|Tree-based|2016|Described here|
|Minowa|Profile Hidden Markov Models|2007|Minowa et al., 2007|
|NRPSPredictor2 ASM|Active-site Motifs|2011|Rottig et al., 2011|
|NRPSPredictor2 SVM|Support Vector Machines|2011|Rottig et al., 2011|
|NRPSSP|Profile Hidden Markov Models|2012|Prieto et al., 2012|
|SEQL-NRPS|Greedy Coordinate-descent|2015|Knudsen et al., 2015|

**_Table S1:_**  _A-domain prediction methods used._

## Table S2
|Substrate|ASM|prediCAT_SNN|pHMM|prediCAT_MP|SANDPUMA|SVM|
|:-:|:-:|:-:|:-:|:-:|:-:|
|3oh-3me-pro|0.947|0.947|NA|0.947|0.919|NA|
|3oh-leu|1.000|0.923|NA|0.947|NA|NA|
|4oh-pro|NA|NA|NA|NA|0.947|NA|
|aad|1.000|0.682|0.900|0.830|1.000|0.887|
|abu|NA|NA|NA|NA|0.667|NA|
|aeo|1.000|0.909|NA|NA|NA|NA|
|aile|NA|NA|NA|NA|0.870|NA|
|ala|0.774|0.435|0.667|0.627|0.821|0.731|
|alpha-me-ser|NA|NA|NA|NA|0.824|NA|
|aoh-gly|NA|NA|NA|NA|0.606|NA|
|arg|0.857|0.361|0.459|0.357|0.475|0.562|
|asn|0.948|0.625|0.843|0.762|0.973|0.963|
|asp|0.920|0.652|0.777|0.802|0.929|0.930|
|athr|NA|NA|NA|NA|0.444|NA|
|b-ala|0.750|NA|NA|NA|0.484|NA|
|bht|1.000|0.988|0.876|0.734|0.957|0.959|
|blys|1.000|NA|0.154|NA|NA|NA|
|bmt|NA|NA|NA|NA|0.400|NA|
|cys|0.948|0.345|0.892|0.804|0.927|0.947|
|dab|0.932|0.706|0.843|0.817|0.875|NA|
|d-ala|NA|NA|NA|NA|0.385|NA|
|d-asn|NA|NA|NA|NA|0.833|NA|
|d-athr|NA|0.433|NA|NA|0.634|NA|
|d-cyclo-horn|NA|NA|NA|NA|0.200|NA|
|d-dab|NA|NA|NA|NA|0.889|NA|
|deoxy-thr|NA|0.690|NA|NA|0.947|NA|
|d-gln|NA|0.286|NA|NA|0.571|NA|
|dhabu|NA|0.629|0.475|0.196|0.639|NA|
|dhb|NA|NA|NA|NA|1.000|1.000|
|d-leu|NA|0.020|NA|NA|0.571|NA|
|dpg|1.000|1.000|1.000|0.976|1.000|NA|
|d-pro|NA|0.667|NA|NA|0.857|NA|
|d-ser|NA|0.952|0.714|0.667|0.881|NA|
|d-trp|NA|NA|NA|NA|0.625|NA|
|d-val|NA|0.546|0.727|0.508|0.806|NA|
|gln|0.737|0.372|0.299|0.421|0.743|0.640|
|glu|0.909|0.517|0.665|0.514|0.873|0.873|
|gly|0.951|0.389|0.650|0.786|0.940|0.930|
|goh-val|NA|NA|NA|NA|0.842|NA|
|his|NA|NA|NA|NA|0.421|NA|
|horn|0.735|0.467|0.593|0.589|0.737|NA|
|hpg|1.000|0.928|0.722|0.379|0.994|0.942|
|hse|0.947|0.947|NA|NA|NA|NA|
|ile|0.848|0.644|0.542|0.581|0.872|0.844|
|kyn|1.000|0.909|NA|1.000|0.889|NA|
|leu|0.885|0.604|0.703|0.681|0.881|0.813|
|lys|0.680|0.531|0.534|0.565|0.683|0.413|
|me-asp|1.000|0.988|1.000|0.857|0.988|NA|
|met|NA|NA|NA|NA|0.421|NA|
|nme-fhorn|NA|NA|NA|NA|0.200|NA|
|noh-ala|NA|NA|NA|NA|0.889|NA|
|noh-val|0.952|0.952|NA|1.000|0.950|NA|
|orn|0.833|0.676|0.721|0.772|0.818|0.554|
|phe|0.626|0.168|0.407|0.449|0.803|0.760|
|phe-ac|0.696|0.829|0.710|0.667|0.918|NA|
|pip|0.580|0.521|0.640|0.457|0.818|0.855|
|pro|0.896|0.544|0.796|0.633|0.848|0.838|
|ser|0.893|0.638|0.707|0.796|0.936|0.880|
|thr|0.376|0.510|0.763|0.548|0.897|0.799|
|trp|0.669|0.448|0.367|0.604|0.626|0.605|
|tyr|0.870|0.630|0.660|0.647|0.833|0.796|
|val|0.776|0.595|0.750|0.683|0.873|0.742|

**_Table S2:_**  _Comparison across methods of substrate specificity F-scores._

## Figure S1
![Image Alt](./fig_s1.png)
**_Figure S1:_**  _Schematic of jackknife resampling analysis._

## Figure S2

![Image Alt](./fig_s2.png)

**_Figure S2:_**  _Frequency of nearest neighbor distance across jackknife analysis. Emperical cutoff of 2.5 shown as dotted line._

## Figure S3

![Image Alt](./ml_supfig.png)

**_Figure S3:_**  _Supervised Machine Learning accuracies across max depths and minimum leaf supports._

## Figure S4

![Image Alt](./mlpaths.png)

**_Figure S4:_**  _Accuracy of SML decision paths._

## Figure S5

![Image Alt](./ml_correction_acc2.png)

![Image Alt](./ml_correction_cov2.png)

**_Figure S5:_**  _Accuracy (a) and coverage (b) comparisons of JackKnives._

## Figure S6

![Image Alt](./venndraft2.png)

![Image Alt](./jkresults2.png)

![Image Alt](./jk_coverage2.png)

**_Figure S6:_**  _Jackknife shared coverage for individual methods (a), fraction correct (b), and coverage (c)._

## Figure S7

![Image Alt](./taxlev1.png)

**_Figure S7:_**  _Jackknife accuracy by taxon level 1._

## Figure S8

![Image Alt](./tanimoto.png)

**_Figure S8:_**  _Tanimoto distances of incorrect jackknife calls._
