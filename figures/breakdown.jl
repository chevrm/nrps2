#!/bin/julia

using Vega
using DataFrames


tax = readtable("tax.tsv", header=true, separator='\t')
sou = readtable("source.tsv", header=true, separator='\t')
tax2 = readtable("tax2.tsv", header=true, separator='\t')

t = piechart(x=tax[:Taxon], y=tax[:Count], holesize = 125)
legend!(t, title="Taxon")

s = piechart(x=sou[:Source], y=sou[:Count], holesize = 125)
legend!(s, title="Data Source")

t2 = piechart(x=tax2[:Taxon], y=tax2[:Count], holesize = 125)
legend!(t2, title="Taxon")
