#!/bin/julia

using DataFrames

dt = readtable("../tax/fullsettax.tsv", header=false, separator='\t')
tax2ct = Dict()
for tax in dt[:x2]
	level = split(tax, '_')
	if length(level) < 2
		if haskey(tax2ct, "Unclassified")
			tax2ct["Unclassified"] += 1
		else
			tax2ct["Unclassified"] = 1
		end
	else
		if haskey(tax2ct, level[2])
			tax2ct[level[2]] += 1
		else
			tax2ct[level[2]] = 1
		end
	end
end
println("Taxon\tCount")
for (t, c) in tax2ct
	println("$t\t$c")
end
