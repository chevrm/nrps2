#!/bin/env perl

use strict;
use warnings;

print join(',', 'shuffle', 'jackknife', 'query', 'spec', 'called_spec', 'method', 'call_made', 'call', 'methshuf') . "\n";
while(<>){
	next if($_ =~ m/^shuf/);
	chomp;
	my ($shuf, $jk, $query, $meth, $spec) = split(/\t/, $_);
	my @q = split(/_+/, $query);
	my @s = split(/\|/, $q[-1]);
	my $cov = 'Y';
	$cov = 'N' if($spec eq 'N/A');
	my $call = 'N';
	foreach my $true (@s){
		$call = 'Y' if($true eq $spec);
	}
	print join(',', $shuf, $jk, $query, $q[-1], $spec, $meth, $cov, $call, $meth . '_' . $shuf) . "\n";
}
