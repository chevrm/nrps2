#!/bin/env perl

use strict;
use warnings;

my $jdir = '~/git/nrps2/benchmarks/jackknife_fullset';
system("mkdir jk") unless(-d 'jk');
foreach my $k ( glob("$jdir/jk*/*knife*.faa") ){
	next if($k =~ m/stach/);
	my @p = split(/\//, $k);
	$p[-1] =~ s/.+knife(\d+\.faa)/k$1/;
	my $fn = join('_', $p[-2], $p[-1]);
	system("cp $k jk/$fn");
}
