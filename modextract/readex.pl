#!/bin/env perl

use strict;
use warnings;
use JSON;
use Data::Dumper;

my $jstring = '';
open my $fh, '<', 'example.json' or die $!;
while(<$fh>){
	$jstring .= $_;
}
close $fh;
my $ps = decode_json($jstring);
print Dumper(\$ps) . "\nEND\n";
