#!/bin/env perl

use strict;
use warnings;

my %cov = ();
my %tru = ();
open my $pfh, '<', '/home/mchevrette/git/nrps2/benchmarks/jackknife_fullsetsmile/pc.scored.csv' or die $!;
while(<$pfh>){
    next if($_ =~ m/^shuffle/);
    chomp;
    my($shuffle, $jk, $query, $pid, $spec, $called_spec, $method, $call_made, $call, $nnd, $nns, $snn, $methshuf) = split(/,/, $_);
    $call_made = 'N' if($method eq 'forced_prediCAT' && $snn < 0.5);
    $cov{$shuffle}{$jk}{$query}{$method} = $call_made;
    $tru{$query} = $spec unless(exists $tru{$query});
}
close $pfh;
open my $nfh, '<', '/home/mchevrette/git/nrps2/benchmarks/jackknife_fullsetsmile/nonpc.scored.csv' or die $!;
while(<$nfh>){
    next if($_ =~ m/^shuffle/);
    my($shuffle, $jk, $query, $pid, $spec, $called_spec, $method, $call_made, $call, $methshuf) = split(/,/, $_);
    if($method ne 'pHMM'){
	$cov{$shuffle}{$jk}{$query}{$method} = $call_made;
    }
}
close $nfh;
my @tochk = ('asm', 'svm', 'forced_prediCAT', 'prediCAT');
open my $efh, '<', '/home/mchevrette/git/nrps2/ml/all_ml.tsv' or die $!;
my $chg = 0;
while(<$efh>){
    chomp;
    my ($shuffle, $jk, $query, $pid, $spec, $called_spec, $method, $call_made, $call, $methshuf, $uname, $bin) = split(/\t/, $_);
    my $newcallmade = 'N';
    foreach my $c (@tochk){
	if(exists $cov{$shuffle}{$jk}{$query}{$c} && $cov{$shuffle}{$jk}{$query}{$c} eq 'Y'){
	    $newcallmade = 'Y';
	    last;
	}
    }
    if($newcallmade eq 'Y'){
	print join("\t", $shuffle, $jk, $query, $pid, $spec, $called_spec, $method.'_nc', $newcallmade, $call, $method.'_nc_'.$shuffle, $uname, $bin)."\n";
    }else{
	$chg += 1;	
	print join("\t", $shuffle, $jk, $query, $pid, $spec, 'no_call', $method.'_nc', $newcallmade, 'N', $method.'_nc_'.$shuffle, $uname, $bin)."\n";
    }
}
close $efh;
print STDERR "$chg changes made.\n";
