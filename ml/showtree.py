from sklearn import tree
import csv
import sys
from sklearn.externals.six import StringIO
from IPython.display import Image 
import pydotplus

## Define params
md = int(sys.argv[1])	## sklearn default='None'
msl = int(sys.argv[2])	## sklearn default=1

## Read in features
features = []
with open('full_features.tsv','rb') as tsvin:
	tsvin = csv.reader(tsvin, delimiter='\t')
	for row in tsvin:
		features.append(row)
## Read in labels
labels = []
with open('full_labels.tsv','rb') as tsvin:
	tsvin = csv.reader(tsvin, delimiter='\t')
	for row in tsvin:
		labels.append(row[0])
## Train the decision tree
clf = tree.DecisionTreeClassifier(min_samples_leaf=msl, max_depth=md)
clf = clf.fit(features, labels)

## Read in feature names
featnames = []
with open('full_mat.tsv','rb') as tsvin:
	tsvin = csv.reader(tsvin, delimiter='\t')
	for row in tsvin:
		for entry in row:
			featnames.append(entry)
## Show the tree
dot_data = StringIO()  
tree.export_graphviz(clf, out_file=dot_data,
	feature_names=featnames,
	filled=True, rounded=True,
	special_characters=True)
graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
graph.write_pdf("out.pdf")
