#!/bin/env perl

use strict;
use warnings;

## Read in params
my $dfile = shift;
my $md = shift;
my $msl = shift;

## Read in the data
my %da = ();
my %allspec = ();
my %jk = ();
open my $dfh, '<', $dfile or die $!;
while(<$dfh>){
	chomp;
	next if ($_ =~ m/^shuffle/);
	my ($shuf, $jk, $query, $pid, $truespec, $called_spec, $method, $covered, $correct, $methshuf, $uname, $bin) = split(/\t/, $_);
	unless(exists $da{$uname}){
		$da{$uname}{'true'} = $truespec;
		$da{$uname}{'pid'} = $pid;
		$da{$uname}{'shuf'} = $shuf;
		$da{$uname}{'jk'} = $jk;
		$da{$uname}{'query'} = $query;
		$da{$uname}{'bin'} = $bin;
	}
	$called_spec = 'no_call' if($covered eq 'N');
	$da{$uname}{'method'}{$method} = $called_spec;
	$allspec{$truespec} = -1;
	$allspec{$called_spec} = -1;
	$jk{$shuf}{$jk}{$uname} = 1;
}
close $dfh;

my $fullmat = 'pid';
## Map specificities to integers
my $id = 0;
my %i2s = ();
open my $sfh, '>', 'full_specmap.tsv' or die $!;
foreach my $spec (sort keys %allspec){
	$allspec{$spec} = $id;
	print $sfh join("\t", $id, $spec) . "\n";
	$i2s{$id} = $spec;
	$id += 1;
}
close $sfh;

## Create feature and label files for each jackknife
my @m = ('prediCAT', 'forced_prediCAT_snn50', 'svm', 'stach', 'pHMM');
foreach my $meth (@m){
	foreach my $spec (sort keys %allspec){
		$fullmat .= "\t" . join('_', $meth, $spec);
	}
}
open my $fh, '>', 'full_mat.tsv' or die $!;
print $fh "$fullmat\n";
close $fh;
## FEATURE ORDER: pid', 'prediCAT', 'forced_prediCAT_snn50', 'svm', 'stach', 'forced_prediCAT', 'pHMM')
open my $ffh, '>', 'full_features.tsv' or die $!;
open my $lfh, '>', 'full_labels.tsv' or die $!;
foreach my $uname (keys %da){
	foreach(@m){
		$da{$uname}{'method'}{$_} = 'no_call' unless(exists $da{$uname}{'method'}{$_});
	}
	print $lfh $allspec{$da{$uname}{'true'}} . "\n";
	print $ffh join("\t", $da{$uname}{'pid'},
		getmatrix($da{$uname}{'method'}{'prediCAT'}, "\t"),
		getmatrix($da{$uname}{'method'}{'forced_prediCAT_snn50'}, "\t"),
		getmatrix($da{$uname}{'method'}{'svm'}, "\t"),
		getmatrix($da{$uname}{'method'}{'stach'}, "\t"),
		getmatrix($da{$uname}{'method'}{'pHMM'}, "\t")
	) . "\n";
}
close $ffh;
close $lfh;

sub getmatrix{
	my ($spec, $del) = @_;
	my $mat = '';
	for(my $i=0;$i<scalar(keys %i2s);$i+=1){
		if($mat eq ''){
			if($spec eq $i2s{$i}){
				$mat .= '1';
			}else{
				$mat .= '0';
			}
		}else{
			if($spec eq $i2s{$i}){
				$mat .= $del . '1';
			}else{
				$mat .= $del . '0';
			}
		}
	}
	return $mat;
}
