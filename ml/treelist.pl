#!/bin/env perl

use strict;
use warnings;
use CAM::PDF;

my $doc=CAM::PDF->new(shift) or die 'CAM::PDF error'."\n";
my $txt = $doc->getPageText(1);
#print $txt;

my %pivot = (
    'svm' => 1,
    'forced_prediCAT_snn50' => 1,
    'prediCAT' => 1,
    'pHMM' => 1,
    'pid' => 1,
    'stach' => 1
    );
my $i = 0;
my %node = ();
my $seeng = 0;
foreach my $ln (split(/\n/, $txt)){
    my $done = 0;
    next if($ln eq 'True' || $ln eq 'False');
    my ($dec, @rest) = split(/\s+/, $ln);
    my @d = split(/_/, $dec);
    my $meth = join('_', @d[0..$#d-1]);
    my $st = $ln;
    if($ln =~ m/_/){
	$st =~ s/no_call//;
	$st =~ s/(\S+)_.+/$1/;
    }else{
	$st = $dec;
    }
    if(exists $pivot{$st}){
	$node{$i} = {
	    'decision' => $dec,
	    'thresh' => $rest[1]
	};
	$done = 1;
	$seeng = 0;
    }
    if($ln =~ m/gini/){
	$seeng += 1;
	if($seeng > 1){
	    unless(exists $node{$i}{'decision'}){
		$node{$i} = {
		    'decision' => 'LEAF_NODE',
		    'thresh' => 'NA'
		};
		$done = 1;
	    }
	}
    }
    $i += 1 if($done==1);
}
my @stack = ();
my $verbose = 0;
my %proc = ();
foreach my $n (sort { $a <=> $b } keys %node){
    print "\n\nProcessiong $n\n" if($verbose == 1);
    print "STACK\t".join("-", @stack)."\n" if($verbose == 1);
    if($n == 0){
	$node{$n}{'parent'} = 'NA';
	$node{$n}{'parent_call'} = 'NA';
    }
    my ($childT, $childF) = ($n+2, $n+3);
    if(exists $node{$n}{'parent_call'} && $node{$n}{'parent_call'} ne 'T'){ ## Not a true call
	($childT, $childF) = ($n+1, $n+2);
    }
    if($node{$n}{'decision'} ne 'LEAF_NODE'){ ## no leaf
	if(exists $node{$n}{'parent_call'}){
	    if($node{$n}{'parent_call'} eq 'F' && $node{$n-1}{'decision'} ne 'LEAF_NODE'){
		push @stack, $n unless(exists $proc{$n});
	    }
	}
	if(exists $node{$childT} && exists $node{$childF}){
	    unless(exists $node{$childT}{'parent'}){
		$node{$childT}{'parent'} = $n;
		$node{$childT}{'parent_call'} = 'T';
		print "\tadded parent\t$childT\t$n\n" if($verbose == 1);
	    }
	    unless(exists $node{$childF}{'parent'}){
		$node{$childF}{'parent'} = $n;
		$node{$childF}{'parent_call'} = 'F';
		print "\tadded parent\t$childF\t$n\n" if($verbose == 1);
	    }
	}
	if($node{$childT}{'decision'} eq 'LEAF_NODE' && $node{$childF}{'decision'} eq 'LEAF_NODE'){ ## Dead end
	    if(exists $node{$childT+2} && exists $node{$childF+2}){
		unless(exists $node{$childT+2}{'parent'} && exists $node{$childF+2}{'parent'}){
		    if(exists $node{$n+1}{'parent_call'} && $node{$n+1}{'decision'} ne 'LEAF_NODE' && $node{$n+1}{'parent_call'} eq 'F'){
			push @stack, $n+1 unless(exists $proc{$n+1});
			print "\tforward added $n+1 to the stack\n" if($verbose==1);
		    }
		    my $v = $childT+2;
		    print "\trecstack: $v\n" if($verbose == 1);
		    recstack($childT+2);
		}
	    }
	    #next;
	}
    }
    unless(exists $node{$n}{'parent'}){ ## no parent
	print "\tbottomrecstack: $n\n" if($verbose == 1);
	recstack($n);
    }
}

print join("\t", '#Index', 'Parent', 'Parent_Call', 'Decision', 'Threshold')."\n";
foreach my $n (sort { $a <=> $b } keys %node){
    print join("\t", $n, $node{$n}{'parent'}, $node{$n}{'parent_call'}, $node{$n}{'decision'}, $node{$n}{'thresh'})."\n"; 
}

sub recstack{
    my $i = shift;
    my $j = $i+1;
    if(exists $node{$i} && exists $node{$j}){
	my $par = pop @stack;
	$node{$i}{'parent'} = $par;
	$node{$i}{'parent_call'} = 'T';
	$node{$j}{'parent'} = $par;
	$node{$j}{'parent_call'} = 'F';
	print "\tadded rec parent\t$i\t$par\n" if($verbose == 1);
	print "\tadded rec parent\t$j\t$par\n" if($verbose == 1);
	$proc{$par} = 1;
    }
}
