#!/bin/env perl

use strict;
use warnings;

my $data = '../benchmarks/jackknife_fullsetsmile/alldata.tsv';
my @md = (20, 30, 40, 50);
my @msl = (2, 5, 10, 15, 20);

foreach my $d (@md){
	foreach my $s (@msl){
		## Make all decision tree diagrams
		print "Making md=$d msl=$s trees...";
		my $mfn = 'ml_md'.$d.'_msl'.$s.'.pdf';
		system("perl fullclass.pl $data $d $s");
		system("python showtree.py $d $s");
		system("mv out.pdf $mfn");
		print "DONE!\n";
		## Make all queries
		#print "Running md=$d msl=$s ensemble predictor...";
		#my $pfn = 'ml_md'.$d.'_msl'.$s.'.tsv';
		#system("perl mlclassifierp.pl $data $d $s > $pfn");
		#print "DONE!\n";
	}
}
