#!/bin/env perl

use strict;
use warnings;

open my $nfh, '<', 'nodemap.tsv' or die $!;
my %node = ();
while(<$nfh>){
    next if($_ =~ m/^#/);
    chomp;
    my($i, $parent, $parent_call, $dec, $thresh) = split(/\t/, $_);
    $node{$i} = {
	'parent' => $parent,
	'decision' => $dec,
	'parent_call' => $parent_call,
	'thresh' => $thresh
    };
}
close $nfh;
my @path = ();
foreach my $n (sort {$a <=> $b} keys %node){
    if($node{$n}{'decision'} eq 'LEAF_NODE'){
	my $p = $node{$n}{'parent'};
	my $traceback = $node{$p}{'decision'}.'%'.$node{$p}{'thresh'}.'-'.$node{$n}{'parent_call'}.'&LEAF_NODE-'.$n;
	while($p != 0){
	    my $t = '';
	    ($p, $t) = getparent($p);
	    $traceback = $t.'&'.$traceback;
	}
	push @path, $traceback;
    }
}

my %p = ();
open my $afh, '<', '/home/mchevrette/git/nrps2/benchmarks/jackknife_fullsetsmile/alldata.tsv' or die $!;
while(<$afh>){
    chomp;
    next if($_ =~ m/^shuffle/);
    my($shuf, $jk, $query, $pid, $spec, $called_spec, $method, $covered, $correct, $methshuf, $uname, $bin)=split(/\t/, $_);
    $p{$shuf}{$jk}{$query}{'pid'} = $pid;
    $called_spec = cleannc($called_spec);
    $p{$shuf}{$jk}{$query}{$method} = $called_spec;
}
close $afh;
my $totpassed = 0;
foreach my $s (keys %p){
    foreach my $j (keys %{$p{$s}}){
	foreach my $q (keys %{$p{$s}{$j}}){
	    foreach my $pa (@path){
		my $pass = 1;
		my @dec = split(/&/, $pa);
		foreach my $d (@dec){
		    last if($d =~ m/LEAF_NODE/);
		    my($decision, $threshchoice) = split(/%/, $d);
		    my($thresh, $choice) = split(/-/, $threshchoice);
		    if($decision eq 'pid'){
			if($choice eq 'T'){ ## need greater than thresh to pass
			    if($thresh <= $p{$s}{$j}{$q}{'pid'}){
				$pass = 0;
				last;
			    }
			}else{ ## need less than or eq thresh to pass
			    if($thresh > $p{$s}{$j}{$q}{'pid'}){
				$pass = 0;
				last;
			    }
			}
		    }else{ ## not pid
			$decision = cleannc($decision);
			my @a = split(/_/, $decision);
			my $m = join('_', @a[0..$#a-1]);
			my $sp = $a[-1];
			if($choice eq 'T'){ ## less than 0.5, so NOT sp
			    die join("\t", $s, $j, $q, $m)."\n" unless(exists $p{$s}{$j}{$q}{$m});
			    if($p{$s}{$j}{$q}{$m} eq $sp){
				$pass = 0;
				last;
			    }
			}else{ ## matches sp
			    if($p{$s}{$j}{$q}{$m} ne $sp){
				$pass = 0;
				last;
			    }
			}
		    }
		}
		$p{$s}{$j}{$q}{'pass'} = $pass;
		$p{$s}{$j}{$q}{'path'} = $pa if($pass==1);
		$totpassed += $pass;
	    }
	}
    }
}
#print "$totpassed passed total\n";

my $cond = 'ml_md40_msl10';
my %e = ();
open my $efh, '<', 'all_ml.tsv' or die $!;
while(<$efh>){
    chomp;
    my($shuf, $jk, $query, $pid, $spec, $called_spec, $method, $covered, $correct, $methshuf, $uname, $bin)=split(/\t/, $_);
    if($method eq $cond){
	my $path = $p{$shuf}{$jk}{$query}{'path'};
	if($correct eq 'Y'){
	    $e{$path}{'correct'} += 1;
	}else{
	    $e{$path}{'incorrect'} += 1;
	}
    }
}
close $efh;

foreach my $p (keys %e){
    unless(exists $e{$p}{'correct'}){
	$e{$p}{'correct'} = 0;
    }
    unless(exists $e{$p}{'incorrect'}){
	$e{$p}{'incorrect'} = 0;
    }
    my $tot = $e{$p}{'correct'} + $e{$p}{'incorrect'};    
    my $pct = sprintf("%.3f", $e{$p}{'correct'} / $tot);
    print join("\t", $pct, $tot, $p)."\n";
}

sub getparent{
    my $n = shift;
    my $p = $node{$n}{'parent'};
    #my $c = $node{$n}{'parent_call'};
    my $c = $node{$p}{'thresh'}.'-'.$node{$n}{'parent_call'};# if($node{$n}{'decision'} eq 'pid');
    return ($p, $node{$p}{'decision'}.'%'.$c);
}
sub cleannc{
    my $s = shift;
    $s =~ s/_result//;
    $s =~ s/no_confident/nocall/;
    $s =~ s/N\/A/nocall/;
    $s =~ s/no_call/nocall/;
    return($s);
}
