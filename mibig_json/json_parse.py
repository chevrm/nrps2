#!/bin/env python

import json
import glob
from datetime import date
import os
import re

## Note, this will break on files of size 0 so make sure to run
##	$ find mibigdir/ -size  0 -print0 |xargs -0 rm
## on future downloads of the mibig json db

khayatt = date(2013,03,19)
dayssince = (date.today() - khayatt).days
for j in glob.glob('all_json_120615/*.json'):
	with open(j) as data_file:    
		data = json.load(data_file)
		for c in data['general_params']['biosyn_class']:
			if (c == 'NRP') and ('publications' in data['general_params']):
				waszero = 0
				for pub in [data['general_params']['publications']]:
					for p in str(pub).split(','):
						if( not (re.search('\.', str(p) )) ): ## ignore non uid entries
							q = re.sub(r'\D', "", p)
							#print "Looking up " + q
							cmd = 'esearch -db pubmed -query "' + str(q) + '[uid]" | efilter -days ' + str(dayssince) + ' > tmp'
							os.system(cmd)
							with open('tmp') as tfh:
								content = tfh.readlines()
								count = re.search(r'<Count>(\d+)', str(content)).group(1)
								if(int(count) == 0):
									waszero = 1
				if(waszero == 0):
					print j
