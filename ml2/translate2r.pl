#!/bin/env perl

use strict;
use warnings;

my %p = ();
open my $pfh, '<', 'pid.tsv' or die $!;
while(<$pfh>){
    chomp;
    my ($s, $j, $q, $pid) = split(/\t/, $_);
    $p{$s}{$j}{$q} = $pid;
}
close $pfh;

while(<>){
    chomp;
    my ($shuf, $jk, $query, $method, $called_spec) = split(/\t/, $_);
    my @a = split(/_/, $query);
    my $spec = $a[-1];
    if($method eq 'ENS'){
	$method = 'secjk';
    }elsif($method eq 'SANDPUMA'){
	$method = 'secjktr';
    }
    my $call_made = 'Y';
    $call_made = 'N' if($called_spec eq 'no_call');
    my $call = 'N';
    
    my @ca = ();
    if($called_spec =~ m/\|/){
	@ca = split(/\|/, $called_spec);
    }else{
	$ca[0] = $called_spec;
    }
    my @true = ();
    if($spec =~ m/\|/){
	@true = split(/\|/, $spec);
    }else{
	$true[0] = $spec;
    }
    foreach my $t (@true){
	foreach my $c (@ca){
	    $call = 'Y' if($t eq $c);
	}
    }
    my $methshuf = join('_', $method, $shuf);
    my $uname = join('_', $shuf, $jk, $query);
    print join(',', $shuf, $jk, $query, $p{$shuf}{$jk}{$query}, $spec, $called_spec, $method, $call_made, $call, $methshuf, $uname)."\n";
}
