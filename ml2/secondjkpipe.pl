#!/bin/env perl

use strict;
use warnings;

my $data = '~/git/nrps2/benchmarks/jackknife_fullsetsmile/alldata.tsv';
my $d = shift;
my @msl = (10, 2, 5, 15, 20);

foreach my $s (@msl){
	print "Running md=$d msl=$s ensemble predictor...";
	my $pfn = 'ml_md'.$d.'_msl'.$s.'.tsv';
	system("perl mlclassifierp.pl $data $d $s > $pfn");
	print "DONE!\n";
}
