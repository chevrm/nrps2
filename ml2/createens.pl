#!/bin/env perl

use strict;
use warnings;

my %t = (
    'prediCAT' => 'prediCAT_MP',
    'forced_prediCAT' => 'prediCAT_SNN',
    'asm' => 'ASM',
    'svm' => 'SVM',
    'pHMM' => 'pHMM',
    );

my %m = ();
my %p = ();
open my $dfh, '<', '/home/mchevrette/git/nrps2/benchmarks/secondjk/alldata.tsv' or die $!;
while(<$dfh>){
    next if($_ =~ m/^shuffle/);
    chomp;
    my($shuf, $jk, $q, $pid, $spec, $called_spec, $meth, $covered, $correct, $methshuf, $uname, $bin) = split(/\t/, $_);
    $m{$shuf}{$jk}{$q}{$meth} = $called_spec;
    $p{$shuf}{$jk}{$q} = $pid;
}
close $dfh;

my $max_depth = 40;
my $min_leaf_support = 10;
open my $efh, '>', 'ens.tsv' or die $!;
open my $ifh, '>', 'ind.tsv' or die $!;
open my $pfh, '>', 'pid.tsv' or die $!;
foreach my $s (keys %m){
    foreach my $jk (keys %{$m{$s}}){
	foreach my $q (keys %{$m{$s}{$jk}}){
	    my $ens = ensemble(
		$m{$s}{$jk}{$q}{'prediCAT'},
		$m{$s}{$jk}{$q}{'forced_prediCAT_snn50'},
		$m{$s}{$jk}{$q}{'asm'},
		$m{$s}{$jk}{$q}{'svm'},
		$m{$s}{$jk}{$q}{'pHMM'},
		$max_depth,
		$min_leaf_support,
		'/home/mchevrette/git/nrps2/benchmarks/jackknife_fullsetsmile/alldata.tsv',
		$p{$s}{$jk}{$q}
		);
	    print $efh join("\t", $s, $jk, $q, 'ENS', $ens)."\n";
	    print $pfh join("\t", $s, $jk, $q, $p{$s}{$jk}{$q})."\n";
	    print $ifh join("\n",
			    join("\t", $s, $jk, $q, 'prediCAT_MP', $m{$s}{$jk}{$q}{'prediCAT'}),
			    join("\t", $s, $jk, $q, 'prediCAT_SNN', $m{$s}{$jk}{$q}{'forced_prediCAT_snn50'}),
			    join("\t", $s, $jk, $q, 'ASM', $m{$s}{$jk}{$q}{'asm'}),
			    join("\t", $s, $jk, $q, 'SVM', $m{$s}{$jk}{$q}{'svm'}),
			    join("\t", $s, $jk, $q, 'pHMM', $m{$s}{$jk}{$q}{'pHMM'})
		)."\n";
	}
    }
}
close $efh;
close $ifh;
close $pfh;
system("rm etmp*");

sub ensemble{
    my ($pc, $pcf, $asm, $svm, $phmm, $md, $msl, $dfile, $pid) = @_;
    unless(-e 'etmp.specmap.tsv' && -e 'etmp.features.tsv' && -e 'etmp.labels.tsv'){
	## Read in the data
	my %da = ();
	my %allspec = ();
	open my $dfh, '<', $dfile or die $!;
	while(<$dfh>){
	    chomp;
	    next if ($_ =~ m/^shuffle/);
	    my ($shuf, $jk, $query, $pid, $truespec, $called_spec, $method, $covered, $correct, $methshuf, $uname, $bin) = split(/\t+/, $_);
	    unless(exists $da{$uname}){
		$da{$uname}{'true'} = $truespec;
		$da{$uname}{'pid'} = $pid;
		$da{$uname}{'shuf'} = $shuf;
		$da{$uname}{'jk'} = $jk;
		$da{$uname}{'query'} = $query;
		$da{$uname}{'bin'} = $bin;
	    }
	    $called_spec = 'no_call' if($covered eq 'N');
	    $da{$uname}{'method'}{$method} = $called_spec;
	    $allspec{$truespec} = -1;
	    $allspec{$called_spec} = -1;
	}
	close $dfh;
	## Map specificities to integers
	my $id = 0;
	my %i2s = ();
	open my $sfh, '>', 'etmp.specmap.tsv' or die $!;
	foreach my $spec (sort keys %allspec){
	    $allspec{$spec} = $id;
	    print $sfh join("\t", $id, $spec) . "\n";
	    $i2s{$id} = $spec;
	    $id += 1;
	}
	close $sfh;
	## Create feature and label files
	my @m = ('prediCAT', 'forced_prediCAT_snn50', 'svm', 'asm', 'phmm');
	## FEATURE ORDER: pid', 'prediCAT', 'forced_prediCAT_snn50', 'svm', 'stach', 'phmm')
	open my $ffh, '>', 'etmp.features.tsv' or die $!;
	open my $lfh, '>', 'etmp.labels.tsv' or die $!;
	foreach my $uname (keys %da){
	    foreach(@m){
		$da{$uname}{'method'}{$_} = 'no_call' unless(exists $da{$uname}{'method'}{$_});
	    }
	    print $lfh $allspec{$da{$uname}{'true'}} . "\n";
	    print $ffh join("\t", $da{$uname}{'pid'},
			    getmatrix($da{$uname}{'method'}{'prediCAT'}, "\t"),
			    getmatrix($da{$uname}{'method'}{'forced_prediCAT_snn50'}, "\t"),
			    getmatrix($da{$uname}{'method'}{'svm'}, "\t"),
			    getmatrix($da{$uname}{'method'}{'asm'}, "\t"),
			    getmatrix($da{$uname}{'method'}{'pHMM'}, "\t")
		) . "\n";
	}
	close $ffh;
	close $lfh;
    }
    my %i = ();
    open my $ifh, '<', 'etmp.specmap.tsv' or die $!;
    while(<$ifh>){
	chomp;
	my ($n, $sp) = split(/\t/, $_);
	$i{$n}=$sp;
    }
    close $ifh;
    my $qmat = join(' ', $pid,
		    getmatrix($pc, ' '),
		    getmatrix($pcf, ' '),
		    getmatrix($svm, ' '),
		    getmatrix($asm, ' '),
		    getmatrix($phmm, ' ')
	);
    chomp(my $ml = `python classifyp.py $qmat $md $msl`);
    return $i{$ml};
}                                                    

sub getmatrix{
    my ($spec, $del) = @_;
    my $mat = '';
    my %i2s = ();
    open my $sfh, '<', 'etmp.specmap.tsv' or die $!;
    while(<$sfh>){
	chomp;
	my ($id, $sp) = split(/\t/, $_);
	$i2s{$id} = $sp;
    }
    close $sfh;
    for(my $i=0;$i<scalar(keys %i2s);$i+=1){
	if($mat eq ''){
	    if($spec eq $i2s{$i}){
		$mat .= '1';
	    }else{
		$mat .= '0';
	    }
	}else{
	    if($spec eq $i2s{$i}){
		$mat .= $del . '1';
	    }else{
		$mat .= $del . '0';
	    }
	}
    }
    return $mat;
}     
