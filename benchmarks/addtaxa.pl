#!/bin/env perl

use strict;
use warnings;

my $taxfi = '../tax/taxa.tsv';
my %taxof = ();
open my $tfh, '<', $taxfi or die $!;
while(<$tfh>){
	my ($h, $t) = split(/\t/, $_);
	$taxof{$h} = $t;
}
close $tfh;
while(<>){
	chomp;
	if($_ =~ m/^shuffle/){
		print join(',', $_, 'taxlevel1', 'taxlevel2', 'taxlevel3') . "\n";
	}else{
		my ($l1, $l2, $l3) = ('','','');
		my ($shuf, $jk, $q, @rest) = split(/,/, $_);
		if(exists $taxof{$q}){
			my @lev = split(/_/, $taxof{$q});
			$l1 = $lev[0];
			$l1 =~ s/\s//g;
			$l2 = $lev[1] if(defined $lev[1]);
			$l3 = $lev[2] if(defined $lev[2]);
		}
		print join(',', $_, $l1, $l2, $l3) . "\n";
	}
}
