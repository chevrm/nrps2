#!/bin/env perl

use strict;
use warnings;

my %d = ();
my $o = 1;
while(<>){
	unless($_ =~ m/^query/){
		chomp;
		my ($q, $pid, $true, $method, $call_made, $call) = split(/,/, $_);
		if(exists $d{$q}){
			$d{$q}{'method'}{$method} = {
				'made'	=> $call_made,
				'call'	=> $call
			};
		}else{
			$d{$q} = {
				'pid'		=> $pid,
				'true'		=> $true,
				'order'		=> $o,
				'method'	=> {
					$method	=> {
						'made'	=> $call_made,
						'call'	=> $call
					}
				}
			};
		}
		$o +=1;
	}
}
my %aoverb = ();
my @tochk = ('atree', 'bachmann-ravel', 'nrpspred2_stach', 'nrpspred2_singaa', 'khayatt_hmm', 'nrpssp');
foreach my $q (keys %d){
	foreach my $o (keys %{$d{$q}{'method'}}){ # outside method
		if($d{$q}{'method'}{$o}{'made'} eq 'Y'){ # call made for outside
			foreach my $i (keys %{$d{$q}{'method'}}){ # inside method
				unless($i eq $o){ # not the same
					if($d{$q}{'method'}{$i}{'made'} eq 'Y'){ # call made for inside method
						if($d{$q}{'method'}{$o}{'call'} eq 'Y' && $d{$q}{'method'}{$i}{'call'} eq 'N'){
							if($d{$q}{'pid'} >= 60){
								$aoverb{$o}{$i}{'hi'} += 1;
							}elsif($d{$q}{'pid'} >= 40){
								$aoverb{$o}{$i}{'med'} += 1;
							}else{
								$aoverb{$o}{$i}{'lo'} += 1;
							}
						}
					}
				}
			}
		}
	}
}
my @h = ('hi', 'med', 'lo');
foreach my $o (@tochk){
	foreach my $i (@tochk){
		foreach my $h (@h){
			print join("\t", $o, $i, $h) . "\t";
			if(exists $aoverb{$o}{$i}{$h}){
				print $aoverb{$o}{$i}{$h};
			}else{
				print '0';
			}
			print "\n";
		}
	}
}
