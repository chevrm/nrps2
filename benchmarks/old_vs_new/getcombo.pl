#!/bin/env perl

use strict;
use warnings;

my %d = ();
my $o = 1;
while(<>){
	unless($_ =~ m/^query/){
		chomp;
		my ($q, $pid, $call, $method, $call_made, $corr) = split(/,/, $_);
		if(exists $d{$q}){
			$d{$q}{$method} = {
				'made'	=> $call_made,
				'corr'	=> $corr,
				'call'	=> $call
			};
		}else{
			$d{$q} = {
				'pid'	=> $pid,
				'order'	=> $o,
				$method	=> {
					'made'	=> $call_made,
					'call'	=> $call,
					'corr'	=> $corr
				}
			};
		}
		$o +=1;
	}
}
foreach my $q (sort {$d{$a}{'order'} <=> $d{$b}{'order'} } keys %d){
	## Get combination desc tree call
	my @order = ();
	if($d{$q}{'pid'} >= 60){
		@order = ('atree', 'nrpspred2_stach', 'nrpspred2_singaa', 'khayatt_hmm');
	}elsif($d{$q}{'pid'} >= 40){
		@order = ('nrpspred2_singaa', 'atree', 'nrpspred2_stach', 'khayatt_hmm');
	}else{
		@order = ('nrpspred2_stach', 'atree', 'khayatt_hmm');
	}
	my $correct = 'N';
	my $meth = 'na';
	my $call = 'na';
	foreach my $m (@order){
		die join("\t", $q, $m) . "\n" unless(exists $d{$q}{$m}{'made'});
		if($d{$q}{$m}{'made'} eq 'Y'){
			$correct = $d{$q}{$m}{'corr'};
			$call = $d{$q}{$m}{'call'};
			$meth = $m;
			last;
		}
	}
	## Get consensus
	my %concount = (
		'true'  => 'unk'
	);
	foreach my $m (keys %{$d{$q}}){
		next if($m eq 'pid' || $m eq 'order');
		my $thiscall = $d{$q}{$m}{'call'};
		$thiscall =~ s/(\w+)_.+/$1/;
		$concount{'calls'}{$thiscall} += 1;
		$concount{'true'} = $thiscall if($d{$q}{$m}{'corr'} eq 'Y');
	}
	my ($curr, $best) = (0,0);
	my ($concall, $concov, $concorr) = ('no_call', 'N', 'N');
	foreach my $c ( sort { $concount{'calls'}{$b} <=> $concount{'calls'}{$a} } keys %{$concount{'calls'}}){
		$curr += 1;
		if($curr == 1){
			$best = $concount{'calls'}{$c};
			$concov = 'Y';
			$concall = $c;
			if($c eq $concount{'true'}){
				$concorr = 'Y';
			}
		}elsif($curr == 2){
			if($best == $concount{'calls'}{$c}){
				($concall, $concov, $concorr) = ('no_call', 'N', 'N');
			}
			last;
		}else{
			last;
		}
	} 
	## Correct SVM in 40-60pid
	if($d{$q}{'pid'} >= 40 && $d{$q}{'pid'} < 60){
		if($meth eq 'nrpspred2_singaa' && $call =~ m/tyr|orn|asn/){
			if($d{$q}{'nrpspred2_stach'}{'made'} eq 'Y'){
				$correct = $d{$q}{'nrpspred2_stach'}{'corr'};
				$call = $d{$q}{'nrpspred2_stach'}{'call'};
				$meth = 'nrpspred2_stach_changed';
			}
		}
	}
	## Print it out
	print join("\t", $q, $d{$q}{'pid'}, $concall, 'consensus', $concov, $concorr) . "\n";
	print join("\t", $q, $d{$q}{'pid'}, $call, 'combo', 'Y', $correct, $meth) . "\n";
}
my %top3 = (
	'nrpspred2_stach'	=> 1,
	'nrpspred2_singaa'	=> 1,
	'khayatt_hmm'		=> 1
);
my %res = ();
foreach my $q (keys %d){
	my ($tot, $t3tot, $t4tot, $t3, $t4, $all) = (0, );
	foreach my $m (keys %{$d{$q}}){
		next if($m eq 'pid' || $m eq 'order');
		$tot +=1;
		if($d{$q}{$m}{'made'} eq 'Y'){
			$all += 1;
			if(exists $top3{$m}){
				$t3 += 1;
				$t3tot += 1;
				$t4 += 1;
				$t4tot += 1;
			}elsif($m eq 'atree'){
				$t4 += 1;
				$t4tot += 1;
			}
		}else{
			if(exists $top3{$m}){
				$t3tot += 1;
				$t4tot += 1;
			}elsif($m eq 'atree'){
				$t4tot += 1;
			}
		}
	}
	if($tot == $all){
		$res{$q}{'all'} = 1;
	}else{
		$res{$q}{'all'} = 0;
	}
	if($t3tot == $t3){
		$res{$q}{'top3'} = 1;
	}else{
		$res{$q}{'top3'} = 0;
	}
	if($t4tot == $t4){
		$res{$q}{'top4'} = 1;
	}else{
		$res{$q}{'top4'} = 0;
	}
}
#print STDERR join("\t", 'query', 'pid', 'spec', 'method', 'call_made', 'call', 'cov_all', 'cov_top3', 'cov_top4') . "\n";
foreach my $q (keys %d){
	foreach my $m (keys %{$d{$q}}){
		next if($m eq 'pid' || $m eq 'order');
		#print STDERR join("\t", $q, $d{$q}{'pid'}, $d{$q}{$m}{'call'}, $m, $d{$q}{$m}{'made'}, $d{$q}{$m}{'corr'}, $res{$q}{'all'}, $res{$q}{'top3'}, $res{$q}{'top4'}) . "\n";
	}
}
