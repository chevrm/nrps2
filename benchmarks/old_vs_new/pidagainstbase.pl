#!/bin/env perl

use strict;
use warnings;

my $qfi = shift;
my $qpref = $qfi;
$qpref =~ s/\.faa//;
my $base = '../trees/K_full_san.faa';
my @b = split(/\//, $base);
my $bpref = $b[-1];
$bpref =~ s/\.faa//;
system("cp $base $b[-1]");
system("makeblastdb -in $b[-1] -out $bpref.db -dbtype prot > /dev/null 2>&1");
system("blastp -query $qfi -db $bpref.db -evalue 1e-3 -max_target_seqs 1 -outfmt 6 -out $bpref.$qpref.e3.m1.bp -num_threads 8");
open my $bfh, '<', "$bpref.$qpref.e3.m1.bp" or die $!;
while(<$bfh>){
	chomp;
	my ($query, $bhit, $pctid, $alen, $mismatch, $gapopen, $qstart, $qend, $sstart, $send, $evalue, $bitscore) = split(/\t/, $_);
	print join("\t", $query, $bhit, $pctid) . "\n";
}
close $bfh;
