#!/bin/env perl

use strict;
use warnings;

print join("\t", '#seqid', 'nearest_stachelhaus', 'singe_AA') . "\n";
while(<>){
	chomp;
	next if($_ =~ m/^#/);
	#sequence-id			8A-signature			stachelhaus-code		3class-pred
	#large-class-pred		small-class-pred		single-class-pred		nearest stachelhaus code
	#NRPS1pred-large-class-pred	NRPS2pred-large-class-pred	outside applicability domain?	coords
	#pfam-score
	my($seqid, $eightAsig, $stach, $cp3, $lcp, $scp, $singcp, $nearstach, $n1lcp, $n2lcp, $oad, $coords, $pfamscore) = split(/\t/, $_);
	print join("\t", $seqid, $nearstach, $singcp) . "\n";
}
