#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

## usage:
##	perl jackqueryfaa.pl > out.tsv

my $sn = shift;
print join("\t", 'Shuffle', 'Jackknife', 'Query_ID', 'prediCAT_Call', 'Forced_prediCAT_Call', 'prediCAT_NN_Dist', 'prediCAT_NN_Score', 'prediCAT_SNN_Score') . "\n";
my $wildcard = 'UNK';
my @q = glob("jk*/*knife*.faa");
foreach my $query (@q){
	next if($query =~ m/stach/);
	my @dir = split(/\//, $query);
	next unless ($dir[0] eq 'jk' . $sn);
	my $d = $dir[-1];
	$d =~ s/.+knife(\d+)\.faa/$1/;
	my $kaq = 'k' . $d . '_as_query';
	next unless($d == 9);
	until(-e "$dir[0]/$kaq/$kaq".'_train.faa'){
		print STDERR "Still waiting on $dir[0]/$kaq training faa\n";
		sleep 60;
	}
	my ($trainfaa, @junk) = glob("$dir[0]/$kaq/*.faa");
	my $fa = new Bio::SeqIO(-file=> $query, -format=> 'fasta');
	while(my $seq = $fa->next_seq){
		open my $tf, '>', $sn.'.tmpt.fa' or die $!;
		print $tf '>' . $seq->id . '_' . $wildcard . "\n" . $seq->seq . "\n";
		close $tf;
		my ($at, $atf, $nndist, $nnscore, $snnscore) = treescanner($trainfaa, $sn.'.tmpt.fa', $wildcard);
		print join("\t", $dir[0], $kaq, $seq->id, $at, $atf, $nndist, $nnscore, $snnscore) . "\n";
		system("rm $sn.*");
	}
}

sub treescanner{
	my ($basefaa, $q, $wc) = @_;
	my $pl = '../../trees/treeparserscore.pl';
	#my $basefaa = './trees/khayatt_goset_mibig/khayatt_goset_mibig_trim985.faa'; # full new db
	#$basefaa = '../../trees/K_full_san/K_full_san_trim766.faa'; # training seq only
	## Grab first seq
	my $bfa = new Bio::SeqIO(-file=>$basefaa, -format=>'fasta');
	my $seq = $bfa->next_seq;
	open my $ttf, '>', $sn.'.tmp.trim.faa' or die $!;
	print $ttf '>' . $seq->id . "\n" . $seq->seq . "\n";
	close $ttf;
	system("cat $q >> $sn.tmp.trim.faa");
	## Align query to first seq
	system("mafft --quiet --namelength 60 --op 5 $sn.tmp.trim.faa > $sn.tmp.trim.afa");
	## Trim query
	my $pfa = new Bio::SeqIO(-format=>'fasta', -file=>$sn.'.tmp.trim.afa');
	my ($head, $tail) = (undef, undef);
	my ($id, $s) = (undef, undef);
	while(my $qseq = $pfa->next_seq){
		if($qseq->id =~ m/$wc/){
			($id, $s) = ($qseq->id, $qseq->seq);
		}else{
			($head, $tail) = ($qseq->seq, $qseq->seq);
			if($head =~ m/^-/){
				$head =~ s/^(-+).+/$1/;
			}else{
				$head = '';
			}
			if($tail =~ m/-$/){
				$tail =~ s/\w(-+)$/$1/;
			}else{
				$tail = '';
			}
		}
	}
	$head = length($head);
	$tail = length($tail);
	substr($s, -1, $tail) = '';
	substr($s, 0, $head) = '';
	$s =~ s/-//g;
	## Compile all seqs
	open my $newfaa, '>', $sn.'.tmp.tree.faa' or die $!;
	print $newfaa '>' . $id . "\n" . $s . "\n";
	close $newfaa;
	system("cat $basefaa >> $sn.tmp.tree.faa");
	system("perl -pi -e 's/[\(\)]/-/g' $sn.tmp.tree.faa");
	## Align all seqs, make tree
	#system("mafft --clustalout --quiet --namelength 60 tmp.tree.faa > tmp.tree.aln");
	system("mafft --quiet --namelength 60 $sn.tmp.tree.faa > $sn.tmp.tree.aln");
	#system("clustalw -TREE -INFILE=tmp.tree.aln -QUIET 1> /dev/null");
	system("FastTree -quiet < $sn.tmp.tree.aln > $sn.tmp.tree.ph 2>/dev/null");
	## Make calls
	chomp(my $cwd = `pwd -P`);
	system("perl $pl $cwd/$sn.tmp.tree.ph > $sn.tmp.tp.tsv");
	## Parse calls
	open my $tp, '<', $sn.'.tmp.tp.tsv' or die $!;
	while(<$tp>){
		chomp;
		my ($i, $c, $fc, $nn, $nnsc, $snnsc) = split(/\t/, $_);
		if(defined $c && defined $fc){
			close $tp;
			return ($c, $fc, $nn, $nnsc, $snnsc);
		}
	}
	close $tp;
	
}
