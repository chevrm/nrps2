#!/bin/env perl

use strict;
use warnings;

print join("\t", 'shuffle', 'jackknife', 'query', 'phmm_call') . "\n";
foreach my $j ( glob("jk*/*phmm.tsv") ){
	my @p = split(/\//, $j);
	open my $jfh, '<', $j or die $!;
	while(<$jfh>){
		next if ($_ =~ m/^Jack/);
		chomp;
		my ($kaq, $q, $phmm) = split(/\t/, $_);
		print join("\t", $p[0], $kaq, $q, $phmm) . "\n";
	}
	close $jfh;
}
