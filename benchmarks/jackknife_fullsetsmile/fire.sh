#!/bin/sh

rm -r jk* 2> /dev/null
perl createnewjk.pl
for N in `seq 1 1 9`
do
cd jk$N
cp ../jacktrees.pl ./
echo "jacktrees $N"
perl jacktrees.pl fullset_smiles > /dev/null
rm jacktrees.pl
cp ../jackhmmbuild.pl ./
echo "jackhmmbuild $N"
perl jackhmmbuild.pl
rm jackhmmbuild.pl
cp ../jackphmm.pl ./
echo "jackphmm $N"
perl jackphmm.pl > jq$N.phmm.tsv
rm jackphmm.pl
cd ..
done
echo "predicatjack ALL"
perl predicatjack.pl > snn.tsv
