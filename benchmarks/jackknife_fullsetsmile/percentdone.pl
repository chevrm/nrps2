#!/bin/env perl

use strict;
use warnings;

my $tdone = 0;
my $last = 0;
while($tdone < 1){
	my $tot = 9280;
	my $done = 0;
	foreach my $a (1..10){
		chomp(my $w = `wc -l snn$a.tsv`);
		my ($n, @rest) = split(/\s+/, $w);
		$done += $n;
	}
	$tdone = $done/$tot;
	print sprintf("%.3f", $tdone) . "\n" unless($last == $tdone);
	$last = $tdone;
	sleep 60;
}
