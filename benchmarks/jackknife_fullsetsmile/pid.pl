#!/bin/env perl

use strict;
use warnings;

my %pid = ();
foreach my $shuf (1..10){
	foreach my $kaq (1..10){
		my $query = 'jk'.$shuf.'/fullset_smiles_knife'.$kaq.'.faa';
		my $db = 'jk'.$shuf.'/k'.$kaq.'_as_query/k'.$kaq.'_as_query_train.faa';
		system("makeblastdb -dbtype prot -in $db -out $db.db 1> /dev/null 2>/dev/null");
		system("blastp -query $query -db $db.db -outfmt 6 -out pid.bp -evalue 1e-10 -max_target_seqs 100000000 -num_threads 10");
		system("rm $db.db.*");
		open my $bfh, '<', 'pid.bp' or die $!;
		while(<$bfh>){
			chomp;
			my ($bquery, $hit, $pctid, $alen, $mismatch, $gapopen, $qstart, $qend, $sstart, $send, $evalue, $bitscore) = split(/\t/, $_);
			if(exists $pid{$shuf}{$kaq}{$bquery}{'score'}){
				if($pid{$shuf}{$kaq}{$bquery}{'score'} < $bitscore){
					$pid{$shuf}{$kaq}{$bquery}{'pid'} = $pctid;
					$pid{$shuf}{$kaq}{$bquery}{'score'} = $bitscore;
				}
			}else{
				$pid{$shuf}{$kaq}{$bquery}{'pid'} = $pctid;
				$pid{$shuf}{$kaq}{$bquery}{'score'} = $bitscore;
			}
		}
		close $bfh;
		system("rm pid.bp");
	}
}
foreach my $s (keys %pid){
	foreach my $k (keys %{$pid{$s}}){
		foreach my $q (keys %{$pid{$s}{$k}}){
			print join("\t", 'jk'.$s, 'k'.$k.'_as_query', $q, $pid{$s}{$k}{$q}{'pid'}) . "\n";
		}
	}
}
