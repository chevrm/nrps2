#!/bin/env perl

use strict;
use warnings;

my %j = ();
## phmm
open my $phmm, '<', 'phmm.tsv' or die $!;
while(<$phmm>){
	next if($_ =~ m/^shuffle/);
	chomp;
	my ($shuf, $kaq, $q, $call) = split(/\t/, $_);
	$call =~ s/_\d+$//;
	$j{$shuf}{$kaq}{$q}{'pHMM'}{'call'} = $call;
	if($call eq 'no_call'){
		$j{$shuf}{$kaq}{$q}{'pHMM'}{'cov'} = 'N';
	}else{
		$j{$shuf}{$kaq}{$q}{'pHMM'}{'cov'} = 'Y';
	}
}
close $phmm;
## pid
my %p = ();
my %n = ();
open my $pfh, '<', 'pid.tsv' or die $!;
while(<$pfh>){
	chomp;
	my ($s, $k, $q, $pid) = split(/\t/, $_);
	$p{$s}{$k}{$q} = $pid;
	my @qnm = split(/_/, $q);
	my $pref = join '_', @qnm[0..$#qnm-1];
	$n{$pref} = $q;
}
close $pfh;
## asm
open my $asm, '<', 'stach.csv' or die $!;
while(<$asm>){
	next if($_ =~ m/^shuffle/);
	chomp;
	my ($s, $k, $q, $call) = split(/,/, $_);
	$j{$s}{$k}{$q}{'asm'}{'call'} = $call;
	if($call eq 'no_call'){
		$j{$s}{$k}{$q}{'asm'}{'cov'} = 'N';
	}else{
		$j{$s}{$k}{$q}{'asm'}{'cov'} = 'Y';
	}
}
close $asm;
## svm
open my $svm, '<', '../../svm/svmcalls.tsv' or die $!;
while(<$svm>){
	next if($_ =~ m/^shuffle/);
	chomp;
	my ($s, $k, $qfull, $m, $call) = split(/\t/, $_);
	my @qnm = split(/_/, $qfull);
	my $pref = join '_', @qnm[0..$#qnm-1];
	my $q = $n{$pref};
	$j{$s}{$k}{$q}{'svm'}{'call'} = $call;
	if($call eq 'N/A'){
		$j{$s}{$k}{$q}{'svm'}{'cov'} = 'N';
	}else{
		$j{$s}{$k}{$q}{'svm'}{'cov'} = 'Y';
	}
}
close $svm;
## score and print
print join(',', 'shuffle', 'jackknife', 'query', 'pid', 'spec', 'called_spec', 'method', 'call_made', 'call', 'methshuf') . "\n";
foreach my $shuf (keys %j){
	foreach my $kasq (keys %{$j{$shuf}}){
		foreach my $q (keys %{$j{$shuf}{$kasq}}){
			my @id = split(/_+/, $q);
			foreach my $meth (keys %{$j{$shuf}{$kasq}{$q}}){
				my $call = 'N';
				if($j{$shuf}{$kasq}{$q}{$meth}{'cov'} eq 'Y'){
					my @true = ();
					my @ca = ();
					if($id[-1] =~ m/\|/){
						@true = split(/\|/, $id[-1]);
					}else{
						$true[0] = $id[-1];
					}
					my $ct = $j{$shuf}{$kasq}{$q}{$meth}{'call'};
					$ct =~ s/(\S+)_.+/$1/;
					## Error correct
					if($ct eq 'abu-iva' || $ct eq 'athr-thr' || $ct eq 'dhab-dht' || $ct eq 'dhb-sal' || $ct eq 'dpg-dhpg' || $ct eq 'hpg-hpg2cl' || $ct eq ''){
						$ct =~ s/-/\|/g;
					}elsif($ct eq 'pro-me-pro'){
						$ct = 'pro|me-pro';
					}
					if($ct =~ m/\|/){
						@ca = split(/\|/, $ct);
					}else{
						$ca[0] = $ct;
					}
					$j{$shuf}{$kasq}{$q}{$meth}{'call'} = $ct;
					foreach my $t (@true){
						foreach my $c (@ca){
							$call = 'Y' if($t eq $c);
						}
					}
				}
				print join(',', $shuf, $kasq, $q, $p{$shuf}{$kasq}{$q}, $id[-1], $j{$shuf}{$kasq}{$q}{$meth}{'call'}, $meth, $j{$shuf}{$kasq}{$q}{$meth}{'cov'}, $call, $meth . '_' . $shuf) . "\n";
			}
		}
	}
}
