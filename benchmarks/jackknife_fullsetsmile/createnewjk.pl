#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

my %smile = ();
my $sfa = new Bio::SeqIO(-file=>'../../consolseqs/fullset_smiles.faa', -format=>'fasta');
while(my $seq = $sfa->next_seq){
	my @i = split(/_/, $seq->id);
	my $n = join '_', @i[0..$#i-1];
	$smile{$n}{'id'} = $seq->id;
}
foreach my $shuf (1..10){
	system("mkdir jk$shuf") unless(-d "jk$shuf");
	foreach my $k (1..10){
		my $kfn = '../jackknife_fullset/jk' . $shuf . '/fullset20151130_post_knife' . $k . '.faa';
		my $nfn = 'jk' . $shuf . '/fullset20151130_post_knife' . $k . '.faa';
		my $ofn = 'jk' . $shuf . '/fullset_smiles_knife' . $k . '.faa';
		system("cp $kfn $nfn");
		open my $ofh, '>', $ofn or die $!;
		my $nfa = new Bio::SeqIO(-file=>$nfn, -format=>'fasta');
		while(my $seq = $nfa->next_seq){
			my @i = split(/_/, $seq->id);
			my $n = join '_', @i[0..$#i-1];
			print $ofh '>' . $smile{$n}{'id'} . "\n" . $seq->seq . "\n";
		}
		close $ofh;
		system("rm $nfn");
	}
}
