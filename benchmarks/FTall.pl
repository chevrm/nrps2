#!/bin/env perl

use strict;
use warnings;

foreach my $j (shift..shift){
	chdir("./jackknife$j");
	print "Processing jackknife$j...";
	system("perl jackqueryfaaFT.pl > jackquery_resultsFT.tsv");
	print "DONE\n";
	chdir('..');
}
