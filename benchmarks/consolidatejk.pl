#!/bin/env perl

use strict;
use warnings;

print join(',', 'shuffle', 'jackknife', 'query', 'pid', 'spec', 'called_spec', 'method', 'call_made', 'call', 'methshuf') . "\n";
my %t = ();
open my $tfh, '<', 'jackknife1/truth.tsv' or die $!;
while(<$tfh>){
	chomp;
	my ($q, $tru) = split(/\t/, $_);
	$t{$q} = $tru;
}
close $tfh;
my %jkq = ();
foreach my $res ( glob("jackknife*/jackquery_results.tsv") ){
	my ($js, $f) = split(/\//, $res);
	open my $pfh, '<', "$js/pid.tsv" or die $!;
	my %kqp = ();
	while(<$pfh>){
		chomp;
		my ($pkaq, $pq, $ph, $pp) = split(/\t/, $_);
		if(exists $kqp{$pkaq}{$pq}){
			$kqp{$pkaq}{$pq} = $pp if($pp > $kqp{$pkaq}{$pq});
		}else{
			$kqp{$pkaq}{$pq} = $pp;
		}
	}
	close $pfh;
	open my $rfh, '<', $res or die $!;
	while(<$rfh>){
		unless($_ =~ m/^Jack/){
			chomp;
			my ($jk, $q, $h, $a, $fa) = split(/\t/, $_);
			$h =~ s/(.+)_\d+/$1/ unless($h =~ m/^no_/);
			my ($ch, $ca, $cfa, $covh, $cova, $covfa) = ('N', 'N', 'N', 'N', 'N', 'N');
			$ch = 'Y' if(lc($h) eq lc($t{$q}));
			$ca = 'Y' if(lc($a) eq lc($t{$q}));
			$cfa = 'Y' if(lc($fa) eq lc($t{$q}));
			$covh = 'Y' unless($h =~ m/^no_conf/);
			$cova = 'Y' unless($a =~ m/^no_conf/);
			$covfa = 'Y' unless($fa =~ m/^no_conf/);
			$jkq{$js}{$jk}{$q} = {
				'hmm'	=> {
					'call'	=> $h,
					'cor'	=> $ch,
					'cov'	=> $covh,
					'meth'	=> 'pHMM',
					'pid'	=> $kqp{$jk}{$q}
				},
				'at'	=> {
					'call'	=> $a,
					'cor'	=> $ca,
					'cov'	=> $cova,
					'meth'	=> 'atree',
					'pid'	=> $kqp{$jk}{$q}
				},
				#'atf'	=> {
				#	'call'	=> $fa,
				#	'cor'	=> $cfa,
				#	'cov'	=> $covfa,
				#	'meth'	=> 'atree_forced',
				#	'pid'	=> $kqp{$jk}{$q}
				#}
			};
		}
	}
	close $rfh;
}
foreach my $s (sort keys %jkq){
	foreach my $k (sort keys %{$jkq{$s}} ){
		foreach my $q (sort keys %{$jkq{$s}{$k}} ){
			foreach my $m (sort keys %{$jkq{$s}{$k}{$q}}){
				print join(',', $s, $k, $q, $jkq{$s}{$k}{$q}{$m}{'pid'}, $t{$q}, $jkq{$s}{$k}{$q}{$m}{'call'}, $jkq{$s}{$k}{$q}{$m}{'meth'}, $jkq{$s}{$k}{$q}{$m}{'cov'}, $jkq{$s}{$k}{$q}{$m}{'cor'}, join('_', $jkq{$s}{$k}{$q}{$m}{'meth'}, $s) ) . "\n";
			}
		}
	}
}
