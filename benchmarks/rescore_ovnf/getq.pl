#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

my %train = ();
open my $tfh, '<', '../../consolseqs/K_only_cl.faa' or die $!;
while(<$tfh>){
	chomp;
	if($_ =~ m/^>(.+)/){
		$train{$1} = 1;
	}
}
close $tfh;
my $fa = new Bio::SeqIO(-file=>'../../consolseqs/fullset_smiles.faa', -format=>'fasta');
while(my $seq = $fa->next_seq){
	unless(exists $train{$seq->id}){
		print '>' . $seq->id . "\n" . $seq->seq . "\n";
	}
}
