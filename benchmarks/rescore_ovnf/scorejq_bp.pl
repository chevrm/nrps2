#!/bin/env perl

use strict;
use warnings;

my $jqf = shift;
my %j = ();
open my $jfh, '<', $jqf or die $!;
while(<$jfh>){
	chomp;
	next if($_ =~ m/^Query/);
	my ($query, $pc, $fpc, $nnd, $nns, $snn) = split(/\t/, $_);
	$fpc = $pc if($fpc eq 'no_force_needed');
	$j{$query}{'prediCAT'}{'call'} = $pc;
	if($pc eq 'no_confident_result'){
		$j{$query}{'prediCAT'}{'cov'} = 'N';
	}else{
		$j{$query}{'prediCAT'}{'cov'} = 'Y';
	}
	$j{$query}{'forced_prediCAT'}{'cov'} = 'Y';
	if($fpc eq 'no_force_needed'){
		$j{$query}{'forced_prediCAT'}{'call'} = $pc;
	}else{
		$j{$query}{'forced_prediCAT'}{'call'} = $fpc;
	}
	$j{$query}{'nnd'} = $nnd;
	$j{$query}{'nns'} = $nns;
	$j{$query}{'snn'} = $snn;
}
close $jfh;

my %p = ();
open my $pfh, '<', 'pid.tsv' or die $!;
while(<$pfh>){
	chomp;
	my ($g, $pid) = split(/\t/, $_);
	$p{$g} = $pid;
}
close $pfh;


print join(',', 'query', 'pid', 'spec', 'called_spec', 'method', 'call_made', 'call', 'nnd', 'nns', 'snn') . "\n";
foreach my $q (keys %j){
	my @id = split(/_+/, $q);
	foreach my $meth ('prediCAT', 'forced_prediCAT'){
		my $call = 'N';
		if($j{$q}{$meth}{'cov'} eq 'Y'){
			my @true = ();
			my @ca = ();
			if($id[-1] =~ m/\|/){
				@true = split(/\|/, $id[-1]);
			}else{
				$true[0] = $id[-1];
			}
			my $ct = $j{$q}{$meth}{'call'};
			$ct =~ s/(\S+)_.+/$1/;
			## Error correct
			if($ct eq 'abu-iva' || $ct eq 'athr-thr' || $ct eq 'dhab-dht' || $ct eq 'dhb-sal' || $ct eq 'dpg-dhpg' || $ct eq 'hpg-hpg2cl' || $ct eq ''){
				$ct =~ s/-/\|/g;
			}elsif($ct eq 'pro-me-pro'){
				$ct = 'pro|me-pro';
			}
			if($ct =~ m/\|/){
				@ca = split(/\|/, $ct);
			}else{
				$ca[0] = $ct;
			}
			$j{$q}{$meth}{'call'} = $ct;
			foreach my $t (@true){
				foreach my $c (@ca){
					$call = 'Y' if($t eq $c);
				}
			}
		}
		print join(',', $q, $p{$q}, $id[-1], $j{$q}{$meth}{'call'}, $meth, $j{$q}{$meth}{'cov'}, $call, $j{$q}{'nnd'}, $j{$q}{'nns'}, $j{$q}{'snn'}) . "\n";
	}
}
