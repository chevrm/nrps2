#!/bin/env perl

use strict;
use warnings;

my $builder = '../../../trees/treehmmbuilder.pl';
foreach my $f (glob("*/*.faa")){
	my @d = split(/\//, $f);
	my ($tree, @junk) = glob($d[0] . '/*/*.ph');
	system("perl $builder $tree $f");
}
