#!/bin/env perl

use strict;
use warnings;

print join("\t", 'Shuffle','Jackknife','Query_ID','prediCAT_Call','Forced_prediCAT_Call','prediCAT_NN_Dist','prediCAT_NN_Score','prediCAT_SNN_Score') . "\n";
foreach my $s ( glob("snn*tsv") ){
	open my $sfh, '<', $s or die $!;
	while(<$sfh>){
		next if($_ =~ m/^Shuffle/);
		print $_;
	}
	close $sfh;
}
