#!/bin/env perl

use strict;
use warnings;

my $tot = 9280;
chomp(my $wc = `wc -l snn*.tsv`);
my $done = 0;
foreach (split(/\n/, $wc)){
    if($_ =~ m/^\s*(\d+)\s+snn/){
	$done += $1-1;
    }
}
print "$done of $tot processed\n(".sprintf("%.2f", 100*$done/$tot)."% complete)\n";
