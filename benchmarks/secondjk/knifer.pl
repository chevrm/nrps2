#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;
use List::Util qw/shuffle/;

my $f = shift;
knife($f);
my $p = passknife();
my $loops = 1;
while($p != 1){
    knife($f);
    $p = passknife();
    $loops += 1;
}
print "Completed after $loops loops\n";

sub knife{
    my $fin = shift;
    for my $jk (1..10){
	system("rm -r jk$jk") if(-d 'jk'.$jk);
	mkdir 'jk'.$jk;
	my %seq = ();
	my $fa = new Bio::SeqIO(-file=>$fin, -format=>'fasta');
	while(my $s = $fa->next_seq){
	    $seq{$s->id}{'seq'} = $s->seq;
	}
	my ($cur, $top) = (1, 10);
	foreach my $id (shuffle(keys %seq)){
	    $cur = 1 if($cur > $top);
	    $seq{$id}{'bin'} = $cur;
	    $cur+=1;
	}
	foreach my $i (1..$top){
	    my $kout = $fin;
	    $kout =~ s/\.faa/_knife$i\.faa/;
	    $kout = 'jk'.$jk.'/'.$kout;
	open my $ofh, '>', $kout or die $!;
	    foreach my $id (keys %seq){
		print $ofh '>' . "$id\n" . $seq{$id}{'seq'} . "\n" if($seq{$id}{'bin'} == $i);
	    }
	    close $ofh;
	}
    }
}
sub passknife{
    my @n = ('Q70AZ7_A3', 'Q8KLL5_A3');
    my $issues = 0;
    foreach my $a ( glob("jk*/*knife*.faa")){
	my ($dir, $faa) = split(/\//, $a);
	for my $j (1..10){
	    next unless($dir =~ m/^jk$j$/);
	    my $k = $faa;
	    $k =~ s/\w+knife(\d+)\.faa/$1/;
	    my %seen = ();
	    foreach my $t (1..10){
		next if($k == $t);
		open my $ffh, '<', $dir.'/fullset_smiles_knife'.$t.'.faa' or die $!;
		while(<$ffh>){
		    chomp;
		    if($_ =~ m/^>(.+)/){
			my $r = $1;
			foreach my $n (@n){
			    if($r =~ m/^$n/){
				$seen{$n} += 1;
			    }
			}
		    }
		}
		close $ffh;
	    }
	    unless(scalar(keys %seen) == 2){
		$issues += 1;
	    }
	}
    }
    if($issues > 0){
	return 0;
    }else{
	return 1;
    }
}
