#!/bin/env perl

use strict;
use warnings;

my @n = ('Q70AZ7_A3', 'Q8KLL5_A3');
foreach my $a ( glob("*knife*.faa")){
	my $k = $a;
	$k =~ s/\w+knife(\d+)\.faa/$1/;
	my %seen = ();
	foreach my $t (1..10){
		next if($k == $t);
		open my $ffh, '<', 'fullset_smiles_knife'.$t.'.faa' or die $!;
		while(<$ffh>){
			chomp;
			if($_ =~ m/^>(.+)/){
				my $r = $1;
				foreach my $n (@n){
					if($r =~ m/^$n/){
						$seen{$n} += 1;
					}
				}
			}
		}
		close $ffh;
	}
	if(scalar(keys %seen) == 2){
		print "All clear with $a\n";
	}else{
		print "Issue with $a\n";
	}
}
