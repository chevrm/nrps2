#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

## usage:
##	perl jackqueryfaa.pl > out.tsv

print join("\t", 'Jackknife', 'Query_ID', 'pHMM_Call') . "\n";
my $wildcard = 'UNK';
foreach my $query (glob("*knife*.faa")){
	next if($query =~ m/stach/);
	my $d = $query;
	$d =~ s/.+knife(\d+)\.faa/$1/;
	my $kaq = 'k' . $d . '_as_query';
	my ($trainfaa, @junk) = glob("$kaq/*.faa");
	my ($hmmdb, @morejunk) = glob("$kaq/*.hmmdb");;
	my $fa = new Bio::SeqIO(-file=> $query, -format=> 'fasta');
	while(my $seq = $fa->next_seq){
		open my $tf, '>', "tmpt.fa" or die $!;
		print $tf '>' . $seq->id . '_' . $wildcard . "\n" . $seq->seq . "\n";
		close $tf;
		print join("\t", $kaq, $seq->id, hmmscanner($hmmdb, 'tmpt.fa')) . "\n";
		system("rm tmp*");
	}
}

sub hmmscanner{
	my ($db, $q) = @_;
	#my $db = './trees/khayatt_goset_mibig/khayatt_goset_mibig_nrpsA.hmmdb'; # full new db
	#my $db = './trees/K_full_san/K_full_san_nrpsA.hmmdb'; # training seq only
	system("hmmscan -o tmp.hmmscan.out --tblout tmp.hmmtbl.out --noali $db $q");
	return "no_call" unless(-e 'tmp.hmmscan.out');
	open my $sfh, '<', "tmp.hmmtbl.out" or die "Died in hmmscanner: $!";
	while(<$sfh>){
		chomp;
		if($_ =~ m/^(\w+\S*)\s/){
			close $sfh;
			return $1;
		}
	}
	close $sfh;
	return "no_call";
}
