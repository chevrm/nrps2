#!/bin/env perl

use strict;
use warnings;


my $c2s = '../../stachelhaus/code2spec.pl';
my $st = '../../stachelhaus/stachextract.pl';
foreach my $j (1..10){
	foreach my $k (1..10){
		my $train = 'jk' . $j . '/k' . $k . '_as_query/k' . $k . '_as_query_train.faa';
		my $tst = 'jk' . $j . '/k' . $k . '_as_query/k' . $k . '_as_query_train.stach2.faa';
		print "Stachelhaus extract $train\n";
		system("perl $st $train > $tst 2> /dev/null");
		my $query = 'jk' . $j . '/fullset20151130_post_knife' . $k . '.faa';
		my $qst = 'jk' . $j . '/fullset20151130_post_knife' . $k . '.stach2.faa';
		print "Stachelhaus extract $query\n";
		system("perl $st $query > $qst 2> /dev/null");
		my $out = 'jk' . $j . '/k' . $k . '_as_query.stachbest2.tsv';
		print "Stachelhaus code to specificity $tst $qst\n";
		system("perl $c2s $tst $qst > $out");
	}
}
