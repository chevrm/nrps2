#!/bin/env perl

use strict;
use warnings;

my ($query, $db) = (shift, shift);
system("makeblastdb -dbtype prot -in $db -out $db.db 1> /dev/null 2>/dev/null");
system("blastp -query $query -db $db.db -outfmt 6 -out pid.bp -evalue 1e-10 -max_target_seqs 100000000 -num_threads 6");
system("rm $db.db.*");
my %pid = ();
open my $bfh, '<', 'pid.bp' or die $!;
while(<$bfh>){
	chomp;
	my ($query, $hit, $pctid, $alen, $mismatch, $gapopen, $qstart, $qend, $sstart, $send, $evalue, $bitscore) = split(/\t/, $_);
	if(exists $pid{$query}{'score'}){
		if($pid{$query}{'score'} < $bitscore){
			$pid{$query}{'pid'} = $pctid;
			$pid{$query}{'score'} = $bitscore;
		}
	}else{
		$pid{$query}{'pid'} = $pctid;
		$pid{$query}{'score'} = $bitscore;
	}
}
close $bfh;
system("rm pid.bp");
foreach my $q (keys %pid){
	print join("\t", $q, $pid{$q}{'pid'}) . "\n";
}
