#!/bin/env perl

use strict;
use warnings;

my $pref = shift;
my @knife = glob( $pref . '_knife*.faa');
foreach my $k (@knife){
	my @train = ();
	foreach(@knife){
		push @train, $_ if($k ne $_);
	}
	my $i = $k;
	$i =~ s/.+knife(\d+)\.faa/$1/;
	my $d = 'k' . $i . '_as_query';
	system("mkdir $d") unless(-d $d);
	my $trainset = "$d/$d" . '_train.faa';
	my $allt = join(' ', @train);
	system("cat $allt > $trainset");
	system("perl ../../../trees/generate_tree.pl $trainset");
}
