#!/bin/env perl

use strict;
use warnings;

print join(",", 'shuffle', 'jackknife', 'query', 'called_spec') . "\n";
foreach my $j (1..10){
	foreach my $k (1..10){
		my $st = 'jk' . $j . '/k' . $k . '_as_query.stachbest.tsv';
		open my $sfh, '<', $st or die "$!\n$st";
		while(<$sfh>){
			unless($_ =~ m/^Query/){
				chomp;
				my ($q, $c) = split(/\t/, $_);
				print join(",", 'jk' . $j, 'k' . $k . '_as_query', $q, $c) . "\n";
			}
		}
		close $sfh;
	}
}
