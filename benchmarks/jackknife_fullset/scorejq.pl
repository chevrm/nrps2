#!/bin/env perl

use strict;
use warnings;

my %j = ();
foreach my $jq (glob("jk*/jq*.tsv")){
	my ($jkfn, $jqfn) = split(/\//, $jq);
	open my $jfh, '<', $jq or die $!;
	while(<$jfh>){
		unless($_ =~ m/^Jack/){
			chomp;
			my ($kasq, $query, $pHMM, $at, $atf, $ft, $ftf) = split(/\t/, $_);
			my @q = split(/_+/, $query);
			#my @true = ();
			#if($q[-1] =~ m/\|/){
			#	@true = split(/\|/, $q[-1]);
			#}else{
			#	$true[0] = $q[-1];
			#}
			$j{$jkfn}{$kasq}{$query}{'pHMM'}{'call'} = $pHMM;
			if($pHMM eq 'no_call'){
				$j{$jkfn}{$kasq}{$query}{'pHMM'}{'cov'} = 'N';
			}else{
				$j{$jkfn}{$kasq}{$query}{'pHMM'}{'cov'} = 'Y';
			}
			$j{$jkfn}{$kasq}{$query}{'atree'}{'call'} = $at;
			if($at eq 'no_confident_result'){
				$j{$jkfn}{$kasq}{$query}{'atree'}{'cov'} = 'N';
			}else{
				$j{$jkfn}{$kasq}{$query}{'atree'}{'cov'} = 'Y';
			}
			$j{$jkfn}{$kasq}{$query}{'atreeFT'}{'call'} = $ft;
			if($ft eq 'no_confident_result'){
				$j{$jkfn}{$kasq}{$query}{'atreeFT'}{'cov'} = 'N';
			}else{
				$j{$jkfn}{$kasq}{$query}{'atreeFT'}{'cov'} = 'Y';
			}
		}
	}
	close $jfh;
}
my %p = ();
foreach my $pidf (glob("jk*/k*as_query/pid.tsv")){
	my ($jk, $kasq, $p) = split(/\//, $pidf);
	open my $pfh, '<', $pidf or die $!;
	while(<$pfh>){
		chomp;
		my ($g, $pid) = split(/\t/, $_);
		$p{$jk}{$kasq}{$g} = $pid;
	}
	close $pfh;
}
print join(',', 'shuffle', 'jackknife', 'query', 'pid', 'spec', 'called_spec', 'method', 'call_made', 'call', 'methshuf') . "\n";
foreach my $shuf (keys %j){
	foreach my $kasq (keys %{$j{$shuf}}){
		foreach my $q (keys %{$j{$shuf}{$kasq}}){
			my @id = split(/_+/, $q);
			foreach my $meth (keys %{$j{$shuf}{$kasq}{$q}}){
				my $call = 'N';
				if($j{$shuf}{$kasq}{$q}{$meth}{'cov'} eq 'Y'){
					my @true = ();
					my @ca = ();
					if($id[-1] =~ m/\|/){
						@true = split(/\|/, $id[-1]);
					}else{
						$true[0] = $id[-1];
					}
					my $ct = $j{$shuf}{$kasq}{$q}{$meth}{'call'};
					$ct =~ s/(\S+)_.+/$1/;
					## Error correct
					if($ct eq 'abu-iva' || $ct eq 'athr-thr' || $ct eq 'dhab-dht' || $ct eq 'dhb-sal' || $ct eq 'dpg-dhpg' || $ct eq 'hpg-hpg2cl' || $ct eq ''){
						$ct =~ s/-/\|/g;
					}elsif($ct eq 'pro-me-pro'){
						$ct = 'pro|me-pro';
					}
					if($ct =~ m/\|/){
						@ca = split(/\|/, $ct);
					}else{
						$ca[0] = $ct;
					}
					$j{$shuf}{$kasq}{$q}{$meth}{'call'} = $ct;
					foreach my $t (@true){
						foreach my $c (@ca){
							$call = 'Y' if($t eq $c);
						}
					}
				}
				print join(',', $shuf, $kasq, $q, $p{$shuf}{$kasq}{$q}, $id[-1], $j{$shuf}{$kasq}{$q}{$meth}{'call'}, $meth, $j{$shuf}{$kasq}{$q}{$meth}{'cov'}, $call, $meth . '_' . $shuf) . "\n";
			}
		}
	}
}
