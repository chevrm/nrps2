#!/bin/env perl

use strict;
use warnings;

my $as = shift;
my %p = ();
foreach my $pidf (glob("jk*/k*as_query/pid.tsv")){
	my ($jk, $kasq, $p) = split(/\//, $pidf);
	open my $pfh, '<', $pidf or die $!;
	while(<$pfh>){
		chomp;
		my ($g, $pid) = split(/\t/, $_);
		$p{$jk}{$kasq}{$g} = $pid;
	}
	close $pfh;
}
#print join(',', 'shuffle', 'jackknife', 'query', 'pid', 'spec', 'called_spec', 'method', 'call_made', 'call', 'methshuf') . "\n";
open my $sfh, '<', $as or die $!;
while(<$sfh>){
	next if($_ =~ m/^shuffle/);
	chomp;
	my ($shuf, $kasq, $q, $spec) = split(/,/, $_);
	my $cov = 'Y';
	$cov = 'N' if($spec eq 'no_call');
	my @id = split(/_+/, $q);
	my $call = 'N';
	if($cov eq 'Y'){
		my @true = ();
		my @ca = ();
		if($id[-1] =~ m/\|/){
			@true = split(/\|/, $id[-1]);
		}else{
			$true[0] = $id[-1];
		}
		## Error correct
		if($spec eq 'abu-iva' || $spec eq 'athr-thr' || $spec eq 'dhab-dht' || $spec eq 'dhb-sal' || $spec eq 'dpg-dhpg' || $spec eq 'hpg-hpg2cl' || $spec eq ''){
			$spec =~ s/-/\|/g;
		}elsif($spec eq 'pro-me-pro'){
			$spec = 'pro|me-pro';
		}
		if($spec =~ m/\|/){
			@ca = split(/\|/, $spec);
		}else{
			$ca[0] = $spec;
		}
		if(scalar(@ca) < 4){
			foreach my $t (@true){
				foreach my $c (@ca){
					$call = 'Y' if($t eq $c);
				}
			}
		}else{
			$cov = 'N';
		}
		print join(',', $shuf, $kasq, $q, $p{$shuf}{$kasq}{$q}, $id[-1], $spec, 'stach', $cov, $call, 'stach_' . $shuf) . "\n";
	}
}
close $sfh;
