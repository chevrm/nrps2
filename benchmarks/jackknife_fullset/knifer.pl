#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;
use List::Util qw/shuffle/;

my %seq = ();
my $fin = shift;
my $fa = new Bio::SeqIO(-file=>$fin, -format=>'fasta');
while(my $s = $fa->next_seq){
	$seq{$s->id}{'seq'} = $s->seq;
}
my ($cur, $top) = (1, 10);
foreach my $id (shuffle(keys %seq)){
	$cur = 1 if($cur > $top);
	$seq{$id}{'bin'} = $cur;
	$cur+=1;
}
foreach my $i (1..$top){
	my $kout = $fin;
	$kout =~ s/\.faa/_knife$i\.faa/;
	open my $ofh, '>', $kout or die $!;
	foreach my $id (keys %seq){
		print $ofh '>' . "$id\n" . $seq{$id}{'seq'} . "\n" if($seq{$id}{'bin'} == $i);
	}
	close $ofh;
}
