#!/bin/env perl

use strict;
use warnings;

foreach my $jk (1..10){
	print STDERR 'jk' . "$jk\n";
	chdir('jk' . $jk);
	foreach my $k (1..10){
		print STDERR "\tk" . $k . "_as_query\n";
		chdir('k' . $k . '_as_query');
		my $q = '../fullset20151130_post_knife' . $k . '.faa';
		my $db = 'k' . $k . '_as_query_train.faa';
		system("perl ../../pid.pl $q $db > pid.tsv");
		chdir("..");
	}
	chdir("..");
}
