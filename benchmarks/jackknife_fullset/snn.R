## Load data
setwd("~/git/nrps2/benchmarks/jackknife_fullset/")
data <- read.table("jq_branchpipe_scored.tsv", header=TRUE, sep=",")

## Break it up
fpc <- data[data["method"]=="forced_prediCAT",]
pc <- data[data["method"]=="prediCAT",]

## Set ssn bins
pc$snn_bin = cut(pc$snn, breaks = seq(0,7.2,.4), labels=seq(0,6.8,.4))
library(plyr)
pcn <- ddply(pc, c("snn_bin"), summarise, ct=sum(!is.na(snn_bin)), pctacc=sum(call=="Y")/ct, cov=sum(call_made=="Y"), covpctacc=sum(call=="Y")/cov)
pcn[,"method"] = "prediCAT"

## Same for fpc
fpc$snn_bin = cut(fpc$snn, breaks = seq(0,7.2,.4), labels=seq(0,6.8,.4))
fpcn <- ddply(fpc, c("snn_bin"), summarise, ct=sum(!is.na(snn_bin)), pctacc=sum(call=="Y")/ct, cov=sum(call_made=="Y"), covpctacc=sum(call=="Y")/cov)
fpcn[,"method"] = "forced_prediCAT"

## join
bysnn <- na.omit(rbind(pcn, fpcn))

## plot
library(ggplot2)
ggplot(data=bysnn, aes(x=snn_bin, y=pctacc, group=method, color=method)) + geom_line(size=1.5) + ggtitle("SNN All") + theme_bw() + xlab("SNN") + ylab("Fraction Correct")
#ggsave("snn_line_all.png")
ggplot(data=bysnn, aes(x=snn_bin, y=covpctacc, group=method, color=method)) + geom_line(size=1.5) + ggtitle("SNN Covered") + theme_bw() + xlab("SNN") + ylab("Fraction Correct")
#ggsave("snn_line_covonly.png")
ggplot(data=bysnn, aes(snn_bin, cov, group=method)) + geom_bar(stat="identity", position="dodge", aes(fill=method)) + ggtitle("Coverage") + theme_bw() + xlab("SNN") + ylab("A-Domains Covered")
#ggsave("snn_bar_coverage.png")
