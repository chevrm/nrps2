#!/bin/sh

for N in `seq 3 1 4`
do
if [ -d "jk$N" ]; then
rm -r jk$N
fi
mkdir jk$N
cd jk$N
cp ../fullset20151130_post.faa ./
perl ../knifer.pl fullset20151130_post.faa
rm fullset20151130_post.faa
cp ../jacktrees.pl ./
perl jacktrees.pl fullset20151130_post
rm jacktrees.pl
cp ../jackhmmbuild.pl ./
perl jackhmmbuild.pl
rm jackhmmbuild.pl
cp ../jackqueryfaaALL.pl ./
perl jackqueryfaaALL.pl > jq$N.tsv
rm jackqueryfaaALL.pl
cd ..
done
