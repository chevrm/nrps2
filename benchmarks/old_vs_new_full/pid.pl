#!/bin/env perl

use strict;
use warnings;

my %pid = ();
open my $bfh, '<', shift or die $!;
while(<$bfh>){
	chomp;
	my ($query, $hit, $pctid, $alen, $mismatch, $gapopen, $qstart, $qend, $sstart, $send, $evalue, $bitscore) = split(/\t/, $_);
	if(exists $pid{$query}{'score'}){
		if($pid{$query}{'score'} < $bitscore){
			$pid{$query}{'pid'} = $pctid;
			$pid{$query}{'score'} = $bitscore;
		}
	}else{
		$pid{$query}{'pid'} = $pctid;
		$pid{$query}{'score'} = $bitscore;
	}
}
close $bfh;
foreach my $q (keys %pid){
	print join("\t", $q, $pid{$q}{'pid'}) . "\n";
}
