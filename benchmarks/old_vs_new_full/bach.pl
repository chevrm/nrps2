#!/bin/env perl

use strict;
use warnings;
use LWP::UserAgent;
use Bio::SeqIO;

open my $ofh, '>', 'br.tsv' or die $!;
print $ofh join("\t", "Query", "BR_result") . "\n";
my $fa = new Bio::SeqIO(-file=>shift, -format=>'fasta');
while(my $seq=$fa->next_seq){
	my $spec = getspec($seq->id, $seq->seq);
	print $ofh join("\t", $seq->id, $spec) . "\n";
}

sub getspec{
	my ($id, $seq) = @_;
	my $bsite = 'http://nrps.igs.umaryland.edu/cgi/nrps_parse.cgi';
	my $browser = new LWP::UserAgent;
	my $response = $browser->post(
		$bsite,
		[
			'SEQUENCE' => '>' . $id . "\n" . $seq,
			'submit' => 'Analyze PKS/NRPS',
		],
	);
	die "$!: " . $response->status_line . "\n" unless ($response->is_success);
	my $r = $response->content;
	if($r =~ m/ght="20" align="absmiddle"><\/A><\/td><td width="200" align="right"> (.+)</){
		my @p = split(/</, $1);
		return $p[0];
	}else{
		return "ERROR";
	}
}
