#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

my $query = shift;
print join("\t", 'Query_ID', 'minowa_call') . "\n";
my $hmmdb = '../../minowa/minowa.hmmdb';
my $fa = new Bio::SeqIO(-file=> $query, -format=> 'fasta');
while(my $seq = $fa->next_seq){
	open my $tf, '>', "tmpt.fa" or die $!;
	print $tf '>' . $seq->id . "\n" . $seq->seq . "\n";
	close $tf;
	print join("\t", $seq->id, hmmscanner($hmmdb, 'tmpt.fa')) . "\n";
	#system("rm tmp*");
}

sub hmmscanner{
	my ($db, $q) = (shift, shift);
	system("hmmscan -o tmp.hmmscan.out --tblout tmp.hmmtbl.out --noali $db $q");
	open my $sfh, '<', "tmp.hmmtbl.out" or die "Died in hmmscanner: $!";
	while(<$sfh>){
		chomp;
		if($_ =~ m/^(\w+\S*)\s/){
			close $sfh;
			return $1;
		}
	}
	close $sfh;
	return "no_call";
}
