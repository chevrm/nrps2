#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

my %seen = ();
my $fa1 = new Bio::SeqIO(-file=>shift, -format=>'fasta');
while(my $seq = $fa1->next_seq){
	$seen{$seq->id} = 1;
}
my $fa2 = new Bio::SeqIO(-file=>shift, -format=>'fasta');
while(my $seq = $fa2->next_seq){
	die $seq->id . " not in FA1\n" unless(exists $seen{$seq->id});
}
