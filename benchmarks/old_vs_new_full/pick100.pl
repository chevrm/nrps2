#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

my %s= ();
open my $bp, '<', shift or die $!;
while(<$bp>){
	chomp;
	my ($q, $s, $p, @rest) = split(/\t/, $_);
	if($p == 100){
		$s{$q} = 1;
	}
}
close $bp;
my $fa = new Bio::SeqIO(-file=>shift, -format=>'fasta');
while(my $seq=$fa->next_seq){
	print '>' . $seq->id . "\n" . $seq->seq . "\n" if(exists $s{$seq->id});
}
