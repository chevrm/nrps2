#!/bin/env perl

use strict;
use warnings;

while(<>){
	chomp;
	my ($q, $s, $p, @rest) = split(/\t/, $_);
	if($p == 100){
		my @q = split(/_+/, $q);
		my @s = split(/_+/, $s);
		print join("\t", $q[-1], $s[-1]) . "\n";
	}
}
