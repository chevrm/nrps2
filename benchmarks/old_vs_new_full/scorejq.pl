#!/bin/env perl

use strict;
use warnings;

my @method = ('pHMM', 'atree', 'atreeFT', 'bach_rav', 'nrps2_stach', 'nrps2_svm', 'nrpssp', 'seqlnrps', 'minowa');
my %j = ();
my $jq = 'jq_ovn.tsv';
## Parse CAT and pHMM
open my $jfh, '<', $jq or die $!;
while(<$jfh>){
	unless($_ =~ m/^Query/){
		chomp;
		my ($query, $pHMM, $at, $atf, $ft, $ftf) = split(/\t/, $_);
		my @q = split(/_+/, $query);
		my @p = split(/_/, $pHMM);
		my $p = join '_', @p[0..$#p-1];
		$j{$query}{'pHMM'}{'call'} = $p;
		if($pHMM eq 'no_call'){
			$j{$query}{'pHMM'}{'cov'} = 'N';
		}else{
			$j{$query}{'pHMM'}{'cov'} = 'Y';
		}
		$j{$query}{'atree'}{'call'} = $at;
		if($at eq 'no_confident_result'){
			$j{$query}{'atree'}{'cov'} = 'N';
		}else{
			$j{$query}{'atree'}{'cov'} = 'Y';
		}
		$j{$query}{'atreeFT'}{'call'} = $ft;
		if($ft eq 'no_confident_result'){
			$j{$query}{'atreeFT'}{'cov'} = 'N';
		}else{
			$j{$query}{'atreeFT'}{'cov'} = 'Y';
		}
	}
}
close $jfh;
## Parse PID
my %p = ();
my $pidf = 'pid.tsv';
my ($jk, $kasq, $p) = split(/\//, $pidf);
open my $pfh, '<', $pidf or die $!;
while(<$pfh>){
	chomp;
	my ($g, $pid) = split(/\t/, $_);
	$p{$g} = $pid;
}
close $pfh;
## Parse Bachmann-Ravel
my $brf = 'br.csv';
open my $bfh, '<', $brf or die $!;
while(<$bfh>){
	unless($_ =~ m/^Query/){
		chomp;
		my ($q, $ufr, $fr) = split(/,/, $_);
		$j{$q}{'bach_rav'}{'call'} = $fr;
		if($fr =~ m/^no_/){
			$j{$q}{'bach_rav'}{'cov'} = 'N';
		}else{
			$j{$q}{'bach_rav'}{'cov'} = 'Y';
		}
	}
}
close $bfh;
## Parse Minowa
my $mf = 'minowa.tsv';
open my $mfh, '<', $mf or die $!;
while(<$mfh>){
	unless($_ =~ m/^Query_ID/){
		chomp;
		my ($q, $c) = split(/\t/, $_);
		$j{$q}{'minowa'}{'call'} = lc($c);
		if($c eq 'no_call'){
			$j{$q}{'minowa'}{'cov'} = 'N';
		}else{
			$j{$q}{'minowa'}{'cov'} = 'Y';
		}
	}
}
close $mfh;
## Parse NRPSPredictor2
my $nrpsp2 = 'nrpspred2.tsv';
open my $n2fh, '<', $nrpsp2 or die $!;
while(<$n2fh>){
	unless($_ =~ m/^#/){
		chomp;
		my ($q, $sta, $svm) = split(/\t/, $_);
		$sta =~ s/,/-/g;
		$q =~ s/_m1//;
		$q =~ s/comple$/complex/;
		$j{$q}{'nrps2_stach'}{'call'} = $sta;
		$j{$q}{'nrps2_svm'}{'call'} = $svm;
		$j{$q}{'nrps2_stach'}{'cov'} = 'Y';
		$j{$q}{'nrps2_svm'}{'cov'} = 'Y';
		$j{$q}{'nrps2_stach'}{'cov'} = 'N' if($sta eq 'N/A');
		$j{$q}{'nrps2_svm'}{'cov'} = 'N' if($svm eq 'N/A');
	}
}
close $n2fh;
## Parse NRPSSP
my $nsp = 'nrpssp.csv';
my %sof = ();
open my $nfh, '<', $nsp or die $!;
while(<$nfh>){
	chomp;
	my ($q, $s1, $s2, $score, $abbr1, $abbr3, $spec, $s3) = split(/;/, $_);
	$abbr3 = $abbr1 if($abbr3 eq '');
	if(exists $sof{$q}){
		if($sof{$q} < $score){
			$j{$q}{'nrpssp'}{'call'} = lc($abbr3);
			$j{$q}{'nrpssp'}{'cov'} = 'Y';
			$sof{$q} = $score;
		}
	}else{
		$j{$q}{'nrpssp'}{'call'} = lc($abbr3);
		$j{$q}{'nrpssp'}{'cov'} = 'Y';
		$sof{$q} = $score;
	}
}
close $nfh;
## Parse SEQL-NRPS
my $sn = 'seqlnrps.csv';
open my $snfh, '<', $sn or die $!;
while(<$snfh>){
	my ($id, $seq, $pred, @rest) = split(/,/, $_);
	$j{$id}{'seqlnrps'}{'call'} = lc($pred);
	$j{$id}{'seqlnrps'}{'cov'} = 'Y';
}
close $snfh;
## Score everything; Dump everything
print join(',', 'query', 'pid', 'spec', 'called_spec', 'method', 'call_made', 'call') . "\n";
foreach my $q (keys %j){
	my @id = split(/_+/, $q);
	foreach my $meth (@method){
		my $call = 'N';
		if(exists $j{$q}{$meth}){
			if($j{$q}{$meth}{'cov'} eq 'Y'){
				my @true = ();
				my @ca = ();
				if($id[-1] =~ m/\|/){
					@true = split(/\|/, $id[-1]);
				}else{
					$true[0] = $id[-1];
				}
				my $ct = $j{$q}{$meth}{'call'};
				$ct =~ s/^(\S+)_.+/$1/;
				if($ct =~ m/\|/){
					@ca = split(/\|/, $ct);
				}else{
					$ca[0] = $ct;
				}
				foreach my $t (@true){
					foreach my $c (@ca){
						$call = 'Y' if($t eq $c);
					}
				}
			}
		}else{
			$j{$q}{$meth}{'call'} = 'no_call';
			$j{$q}{$meth}{'cov'} = 'N';
		}
		print join(',', $q, $p{$q}, $id[-1], $j{$q}{$meth}{'call'}, $meth, $j{$q}{$meth}{'cov'}, $call) . "\n";
	}
}
