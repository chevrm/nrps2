#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

## usage:
##	perl jackqueryfaa.pl query treedir > out.tsv

my ($query, $treedir) = (shift, shift);
print join("\t", 'Query_ID', 'pHMM_Call', 'Atree_Call', 'Forced_Atree_Call', 'FT_Call', 'Forced_FT_Call') . "\n";
my $wildcard = 'UNK';
my ($trainfaa, @junk) = glob("$treedir/*.faa");
my ($hmmdb, @morejunk) = glob("$treedir/*.hmmdb");;
my $fa = new Bio::SeqIO(-file=> $query, -format=> 'fasta');
while(my $seq = $fa->next_seq){
	open my $tf, '>', "tmpt.fa" or die $!;
	print $tf '>' . $seq->id . '_' . $wildcard . "\n" . $seq->seq . "\n";
	close $tf;
	my ($at, $atf, $ft, $ftf) = treescanner($trainfaa, 'tmpt.fa', $wildcard);
	print join("\t", $seq->id, hmmscanner($hmmdb, 'tmpt.fa'), $at, $atf, $ft, $ftf) . "\n";
	system("rm tmp*");
}

sub hmmscanner{
	my ($db, $q) = @_;
	system("hmmscan -o tmp.hmmscan.out --tblout tmp.hmmtbl.out --noali $db $q");
	open my $sfh, '<', "tmp.hmmtbl.out" or die "Died in hmmscanner: $!";
	while(<$sfh>){
		chomp;
		if($_ =~ m/^(\w+\S*)\s/){
			close $sfh;
			return $1;
		}
	}
	close $sfh;
	return "no_call";
}

sub treescanner{
	my ($basefaa, $q, $wc) = @_;
	my $pl = '../../trees/treeparser.pl';
	## Grab first seq
	my $bfa = new Bio::SeqIO(-file=>$basefaa, -format=>'fasta');
	my $seq = $bfa->next_seq;
	open my $ttf, '>', 'tmp.trim.faa' or die $!;
	print $ttf '>' . $seq->id . "\n" . $seq->seq . "\n";
	close $ttf;
	system("cat $q >> tmp.trim.faa");
	## Align query to first seq
	system("mafft --quiet --namelength 60 --op 5 tmp.trim.faa > tmp.trim.afa");
	## Trim query
	my $pfa = new Bio::SeqIO(-format=>'fasta', -file=>'tmp.trim.afa');
	my ($head, $tail) = (undef, undef);
	my ($id, $s) = (undef, undef);
	while(my $qseq = $pfa->next_seq){
		if($qseq->id =~ m/$wc/){
			($id, $s) = ($qseq->id, $qseq->seq);
		}else{
			($head, $tail) = ($qseq->seq, $qseq->seq);
			if($head =~ m/^-/){
				$head =~ s/^(-+).+/$1/;
			}else{
				$head = '';
			}
			if($tail =~ m/-$/){
				$tail =~ s/\w(-+)$/$1/;
			}else{
				$tail = '';
			}
		}
	}
	$head = length($head);
	$tail = length($tail);
	substr($s, -1, $tail) = '';
	substr($s, 0, $head) = '';
	$s =~ s/-//g;
	## Compile all seqs
	open my $newfaa, '>', 'tmp.tree.faa' or die $!;
	print $newfaa '>' . $id . "\n" . $s . "\n";
	close $newfaa;
	system("cat $basefaa >> tmp.tree.faa");
	system("perl -pi -e 's/[\(\)]/-/g' tmp.tree.faa");
	## Align all seqs, make tree
	#system("mafft --clustalout --quiet --namelength 60 tmp.tree.faa > tmp.tree.aln");
	system("mafft --quiet --namelength 60 tmp.tree.faa > tmp.tree.aln");
	system("clustalw -TREE -INFILE=tmp.tree.aln -QUIET 1> /dev/null");
	system("FastTree -quiet < tmp.tree.aln > tmp.tree.ft 2>/dev/null");
	## Make calls
	chomp(my $cwd = `pwd -P`);
	system("perl $pl $cwd/tmp.tree.ph > tmp.tp.tsv");
	system("perl $pl $cwd/tmp.tree.ft > tmp.ftp.tsv");
	## Parse calls
	open my $tp, '<', 'tmp.tp.tsv' or die $!;
	my ($a, $af, $ft, $ftf) = (undef, undef, undef, undef);
	while(<$tp>){
		chomp;
		my ($i, $c, $fc) = split(/\t/, $_);
		if(defined $c && defined $fc){
			($a, $af) = ($c, $fc);
		}
	}
	close $tp;
	open my $ftp, '<', 'tmp.ftp.tsv' or die $!;
	while(<$ftp>){
		chomp;
		my ($i, $c, $fc) = split(/\t/, $_);
		if(defined $c && defined $fc){
			($ft, $ftf) = ($c, $fc);
		}
	}
	close $ftp;
	return ($a, $af, $ft, $ftf);
}
