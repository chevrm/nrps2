#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

## usage:
##	perl queryfaa.pl *.faa > file.tsv

my $wildcard = 'UNK';
foreach my $faa (@ARGV){
	my $fa = new Bio::SeqIO(-file=> $faa, -format=> 'fasta');
	while(my $seq = $fa->next_seq){
		open my $tf, '>', "tmpt.fa" or die $!;
		print $tf '>' . $seq->id . '_' . $wildcard . "\n" . $seq->seq . "\n";
		close $tf;
		print join("\t", $seq->id, hmmscanner('tmpt.fa'), treescanner('tmpt.fa', $wildcard)) . "\n";
		system("rm tmp*");
	}
}

sub hmmscanner{
	my $q = $_[0];
	my $db = './trees/K_full_san/K_full_san_nrpsA.hmmdb';
	system("hmmscan -o tmp.hmmscan.out --tblout tmp.hmmtbl.out --noali $db $q");
	open my $sfh, '<', "tmp.hmmtbl.out" or die "Died in hmmscanner: $!";
	while(<$sfh>){
		chomp;
		if($_ =~ m/^(\w+\S*)\s/){
			close $sfh;
			return $1;
		}
	}
	close $sfh;
	return "no_call";
}

sub treescanner{
	my ($q, $wc) = @_;
	my $pl = './trees/treeparser.pl';
	my $basefaa = './trees/K_full_san/K_full_san_trim766.faa';
	## Grab first seq
	my $bfa = new Bio::SeqIO(-file=>$basefaa, -format=>'fasta');
	my $seq = $bfa->next_seq;
	open my $ttf, '>', 'tmp.trim.faa' or die $!;
	print $ttf '>' . $seq->id . "\n" . $seq->seq . "\n";
	close $ttf;
	system("cat $q >> tmp.trim.faa");
	## Align query to first seq
	system("mafft --quiet --namelength 60 --op 5 tmp.trim.faa > tmp.trim.afa");
	## Trim query
	my $pfa = new Bio::SeqIO(-format=>'fasta', -file=>'tmp.trim.afa');
	my ($head, $tail) = (undef, undef);
	my ($id, $s) = (undef, undef);
	while(my $qseq = $pfa->next_seq){
		if($qseq->id =~ m/$wc/){
			($id, $s) = ($qseq->id, $qseq->seq);
		}else{
			($head, $tail) = ($qseq->seq, $qseq->seq);
			$head =~ s/^(-+).+/$1/;
			$tail =~ s/\w(-+)$/$1/;
		}
	}
	$head = length($head);
	$tail = length($tail);
	substr($s, -1, $tail) = '';
	substr($s, 0, $head) = '';
	$s =~ s/-//g;
	## Compile all seqs
	open my $newfaa, '>', 'tmp.tree.faa' or die $!;
	print $newfaa '>' . $id . "\n" . $s . "\n";
	close $newfaa;
	system("cat $basefaa >> tmp.tree.faa");
	## Align all seqs, make tree
	system("mafft --clustalout --quiet --namelength 60 tmp.tree.faa > tmp.tree.aln");
	system("clustalw -TREE -INFILE=tmp.tree.aln &> /dev/null");
	## Make calls
	system("perl $pl tmp.tree.ph > tmp.tp.tsv");
	## Parse calls
	open my $tp, '<', 'tmp.tp.tsv' or die $!;
	while(<$tp>){
		chomp;
		my ($i, $c) = split(/\t/, $_);
		if(defined $c){
			close $tp;
			return $c;
		}
	}
	close $tp;
}
