CLUSTAL W multiple sequence alignment


Q01886v2_A2_ala                          AELLLCSPATSRMGALQNISTQMGTEFKIVELEPEFIRSLPLPPKPNHQPMVGLNDDLYV
Q01886v1_A2_ala                          AELLLCSPATSRMGALQNISTQMGTEFKIVELEPEFIRSLPLPPKPNHQPMVGLNDDLYV
                                         ************************************************************

Q01886v2_A2_ala                          VFTSGSTGVPKGAVATHQAYATGIYEHAVACGMTSLGAPPRSLQFASYSFDASIGDIFTT
Q01886v1_A2_ala                          VFTSGSTGVPKGAVATHQAYATGIYEHAVACGMTSLGAPPRSLQFASYSFDASIGDIFTT
                                         ************************************************************

Q01886v2_A2_ala                          LAVGGCLCIPREEDRNPAGITTFINRYGVTWAGITPSLALHLDPDAVPTLKALCVAGEPL
Q01886v1_A2_ala                          LAVGGCLCIPREEDRNPAGITTFINRYGVTWAGITPSLALHLDPDAVPTLKALCVAGEPL
                                         ************************************************************

Q01886v2_A2_ala                          SMSVVTVWSKRLNLINMYGPTEATVACIANQVTCTTTTVSDIGRGYRATTWVVQPDNHNS
Q01886v1_A2_ala                          SMSVVTVWSKRLNLINMYGPTEATVACIANQVTCTTTTVSDIGRGYRATTWVVQPDNHNS
                                         ************************************************************

Q01886v2_A2_ala                          LVPIGAVGELIIEGSILCRGYLNDPERTAEVFIRSPSWLHDLRPNSTLYKTGDLVRYSAD
Q01886v1_A2_ala                          LVPIGAVGELIIEGSILCRGYLNDPERTAEVFIRSPSWLHDLRPNSTLYKTGDLVRYSAD
                                         ************************************************************

Q01886v2_A2_ala                          GKIIFIGRKDTQVKMNGQRFELGEVEHALQLQLDPSDGPIIVDLLKRTQSGEPDLLIAFL
Q01886v1_A2_ala                          GKIIFIGRKDTQVKMNGQRFELGEVEHALQLQLDPSDGPIIVDLLKRTQSGEPDLLIAFL
                                         ************************************************************

Q01886v2_A2_ala                          FVGRANTGTG
Q01886v1_A2_ala                          FVGRANTGTG
                                         **********
