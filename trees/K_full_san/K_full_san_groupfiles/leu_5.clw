CLUSTAL W multiple sequence alignment


Q7WRQ4_A1_8__leu                         VSILLTQQSLVDSLEAN----SAEVVCLDRDWHIIANYSQHNPVKLVKAENLAYVIYTSG
Q8G982_A1_8__leu                         VSILLTQQSLVTNLREDLDTLKIESFCLDSDWLILENYSRENPSSSVQSENLAYLIYTSG
                                         *********** .*. :    . * .*** ** *: ***:.** . *::*****:*****

Q7WRQ4_A1_8__leu                         STGKPKGVMNIHKGICNNLLRTIDTYPLIAGDCILHIGVLSFDVSVWEIFWSLTSGTTLV
Q8G982_A1_8__leu                         STGKPKGVMNLHQGICNNILRTKDSYPTTNRDRLLQISSLAFDASVLDIFWSLSSGMALI
                                         **********:*:*****:*** *:**    * :*:*. *:**.** :*****:** :*:

Q7WRQ4_A1_8__leu                         VAKPEGHKDIAYLINLIAQQQVTQVFFVPSMLRIFLQQPNLESCRCLKRVFSGAETLSYE
Q8G982_A1_8__leu                         IPKPEGTKDLAYLIQLMIEKKVSQVFFVPSLLRLLLQQPNLENCRYLKRVFCGGEALSSE
                                         :.**** **:****:*: :::*:*******:**::*******.** *****.*.*:** *

Q7WRQ4_A1_8__leu                         LTQRFFERLDCELHNLYGPTEAAVDATCWQCQPQSNYQVIPIGRPIANTQTYILDQYLQP
Q8G982_A1_8__leu                         LMQQFFQHFNCELHNLYGPTETSVDATCWQCPPRTDDPAIAIGRPIANTQIYILDRHLQP
                                         * *:**::::***********::******** *:::  .*.********* ****::***

Q7WRQ4_A1_8__leu                         VPIGIAGELHIGGVQLARGYLNQPELTNERFISNPFGEGKLYKTGDKARYLSDGNIEYLG
Q8G982_A1_8__leu                         VPVGIVGELHIGGIPLARGYLNQLELTAEKFIPNPFGQGKLYKTGDLVRYLADGNIEYLG
                                         **:**.*******: ******** *** *:**.****:******** .***:********

Q7WRQ4_A1_8__leu                         RIDHQVKLRGLRIELGEIEFLLDTHPQVEQTVVVLQADTSENQRLVAYVVRKNSSL
Q8G982_A1_8__leu                         RIDNQVKLRGLRIELGEIQTILDSHPQINQSVVIIQTDSEDNQRLVAYVDSQNQAL
                                         ***:**************: :**:***::*:**::*:*:.:********  :*.:*
