CLUSTAL W multiple sequence alignment


Q9RLP6v2_A3_ala                          PVAALTTADLRSRLDQYDLAVIDMADPAIDRRPSDALSGPRPDDLAYMTYTSGTTGVPKA
Q9RLP6v1_A3_ala                          PVAALTTADLRSRLDQYDLAVIDMADPAIDRRPSDALSGPRPDDLAYMTYTSGTTGVPKA
                                         ************************************************************

Q9RLP6v2_A3_ala                          VAVTHHNVTQLVSALHADLPSGPGQVWSQWHSLVFDVSVWEIWGALLHGGRLVVVPESVG
Q9RLP6v1_A3_ala                          VAVTHHNVTQLVSALHADLPSGPGQVWSQWHSLVFDVSVWEIWGALLHGGRLVVVPESVG
                                         ************************************************************

Q9RLP6v2_A3_ala                          SSPDDLHNLLITEKVSVLCQTPSAAGMLSPEGLESTTLIVAGEACPTELVDRWAPGRVMI
Q9RLP6v1_A3_ala                          SSPDDLHNLLITEKVSVLCQTPSAAGMLSPEGLESTTLIVAGEACPTELVDRWAPGRVMI
                                         ************************************************************

Q9RLP6v2_A3_ala                          NAYGPTEATIYAAMSEPLTAGTGVAPIGAPVPGAALFVLDKWLRPAPEGVVGELYVAGHG
Q9RLP6v1_A3_ala                          NAYGPTEATIYAAMSEPLTAGTGVAPIGAPVPGAALFVLDKWLRPAPEGVVGELYVAGHG
                                         ************************************************************

Q9RLP6v2_A3_ala                          VATGYIGRPDLTASRFVACPFGETGERMYRTGDLVRWGSDGQLEYLGRADEQVKIRGYRI
Q9RLP6v1_A3_ala                          VATGYIGRPDLTASRFVACPFGETGERMYRTGDLVRWGSDGQLEYLGRADEQVKIRGYRI
                                         ************************************************************

Q9RLP6v2_A3_ala                          ELGEIQAALAKLDGVDQAVVIAREDRPGDKRLVGYITGTADPA
Q9RLP6v1_A3_ala                          ELGEIQAALAKLDGVDQAVVIAREDRPGDKRLVGYITGTADPA
                                         *******************************************
