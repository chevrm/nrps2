CLUSTAL W multiple sequence alignment


Q50E73_A2_7__asp                         GLVVVGEGLSHVVGDFPGGEVFEFSRVVRESCLVELVAADGVEVRNVTDGERASRLLPGH
Q50E73_A4_9__asp                         GLVVVGEGLSHVVGDFPGGEVFEFSRVVRESCLVELVAADGVEVRNVTDGERASRLLPGH
                                         ************************************************************

Q50E73_A2_7__asp                         PLYVVYTSGSTGRPKGVVVTHASVGGYLARGRDVYAGAVGGVGFVHSSLAFDLTVTVLFT
Q50E73_A4_9__asp                         PLYVVYTSGSTGRPKGVVVTHASVGGYLARGRDVYAGAVGGVGFVHSSLAFDLTVTVLFT
                                         ************************************************************

Q50E73_A2_7__asp                         PLVSGGCVVLGELDESAQGVGASFVKVTPSHLGLLGELEGVVAGNGMLLVGGEALSGGAL
Q50E73_A4_9__asp                         PLVSGGCVVLGELDESAQGVGASFVKVTPSHLGLLGELEGVVAGNGMLLVGGEALSGGAL
                                         ************************************************************

Q50E73_A2_7__asp                         REWRERNPGVVVVNAYGPTELTVNCAEFLIAPGEEVPDGPVPIGRPFAGQRMFVLDAALR
Q50E73_A4_9__asp                         REWRERNPGVVVVNAYGPTELTVNCAEFLIAPGEEVPDGPVPIGRPFAGQRMFVLDAALR
                                         ************************************************************

Q50E73_A2_7__asp                         VVPVGVVGELYVAGVGLARGYLGRAGLTAERFVACPFGAPGERMYRTGDLVRWRVDGALE
Q50E73_A4_9__asp                         VVPVGVVGELYVAGVGLARGYLGRVGLTAERFVACPFGVPGERMYRTGDLVRWRVDGALE
                                         ************************.*************.*********************

Q50E73_A2_7__asp                         FVGRADDQVKVRGFRVELGEVEGAVAAHPDVVRAVVVVREDRPGDHRLVAYVTGVDTGG-
Q50E73_A4_9__asp                         FVGRADDQVKVRGFRVELGEVEGAVAAHPDVVRAVVVVREDRPGDHRLVAYVTAGGVGGD
                                         *****************************************************. ..** 

Q50E73_A2_7__asp                         -
Q50E73_A4_9__asp                         G
                                          
