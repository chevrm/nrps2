CLUSTAL W multiple sequence alignment


Q5DIS7_A2_dab                            PALVVGEG--HQALARELLEELPEESRPALLDW-AAQQGQGKCEQRPGIVAPTNGLAYVI
Q9I157_A3_4__dab                         LVLVCTQACREQALA--LFDELGCVDRPRLLVWDEIQQGEG-AEHDPQVYSGPQNLAYVI
                                          .**  :.  .****  *::**   .** ** *   ***:* .*: * : : .:.*****

Q5DIS7_A2_dab                            YTSGSTGHPKGVMVEQAGMLNNQLSKVPLLALDENDVIAQTASQSFDISVWQFLAAPLFG
Q9I157_A3_4__dab                         YTSGSTGLPKGVMVEQAGMLNNQLSKVPYLELDENDVIAQTASQSFDISVWQFLAAPLFG
                                         ******* ******************** * *****************************

Q5DIS7_A2_dab                            AQVDILPNDIAHDPLALSRRVRERGITVLELVPSLIQEVLDDPQETLPGLRWMLSTGEAL
Q9I157_A3_4__dab                         ARVAIVPNAVAHDPQGLLAHVGEQGITVLESVPSLIQGMLAEERQALDGLRWMLPTGEAM
                                         *:* *:** :**** .*  :* *:****** ****** :* : :::* ******.****:

Q5DIS7_A2_dab                            SPELARRWLTRYPQVGLMNAYGPAECSDDVSFFRVDTQSTGGTYLPIGQATDNNHLQVL-
Q9I157_A3_4__dab                         PPELARQWLKRYPRIGLVNAYGPAECSDDVAFFRVDLASTESTYLPIGSPTDNNRLYLLG
                                         .*****:**.***::**:************:*****  ** .******..****:* :* 

Q5DIS7_A2_dab                            ---DDDLLPVPLGGIGELYVSGTGVGRGYLADPGRTALAFLPDPYAQVPGSRIYRTGDLA
Q9I157_A3_4__dab                         AGADDAFELVPLGAVGELCVAGTGVGRGYVGDPLRTAQAFVPHPFG-APGERLYRTGDLA
                                            ** :  ****.:*** *:********:.** *** **:*.*:. .**.*:*******

Q5DIS7_A2_dab                            CRGKGDQLEYVGRIDHQVKVRGFRIELGEIESRLLELPIVREAVVLAQDGPTGKSLAAYL
Q9I157_A3_4__dab                         RRRADGVLEYVGRIDHQVKIRGFRIELGEIEARLHERADVREAAVAVQEGANGKYLVGYL
                                          *  .. ************:***********:** * . ****.* .*:*..** *..**

Q5DIS7_A2_dab                            VPAE-----SDTP
Q9I157_A3_4__dab                         VPGETPRSSADSP
                                         **.*     :*:*
