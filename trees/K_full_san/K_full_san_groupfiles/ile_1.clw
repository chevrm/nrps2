CLUSTAL W multiple sequence alignment


Q45563_A1_10__ile                        AAILIVQKGLEPNTAFAGTFISADAEAMIEEHTKPLEIVTGPDDLAYIMYTSGSTGRPKG
P94460_A1_ile                            AKMLIVQKGLEQNAAFSGTCIISDAQGLMEENDIPINISSSPDDLAYIMYTSGSTGRPKG
                                         * :******** *:**:** * :**:.::**:  *::* :.*******************

Q45563_A1_10__ile                        VMITNRNVVSLVCNSNYTSASVNDRFILTGSISFDAVTFEMFGALLKGATLHIIDKSTML
P94460_A1_ile                            VMITNRNVVSLVRNSNYTSASGDDRFIMTGSISFDAVTFEMFGALLNGASLHIIDKSTML
                                         ************ ******** :****:******************:**:**********

Q45563_A1_10__ile                        TPDRFGAYLIENNITVLFLTTALFNQLAQAQADMFHRLHTLYVGGEALSPELINAVRRAC
P94460_A1_ile                            TPDRFGAYLLENDITVLFLTTALFNQLAQVRADMFRGLHTLYVGGEALSPALMNAVRHAC
                                         *********:**:****************.:****: ************* *:****:**

Q45563_A1_10__ile                        PNLSLYNIYGPTENTTFSTFFEIKRDYATPIPIGKPISNCTAFILDAKGCLLPIGVPGEL
P94460_A1_ile                            PDLALHNIYGPTENTTFSTFFEMKRDYAGPIPIGKPISNSTAYILDTKGRLLPIGVPGEL
                                         *:*:*:****************:***** **********.**:***:** **********

Q45563_A1_10__ile                        CVGGDGVAKGYLNRDDVTAAVFSPDPFIPGERIYRTGDLARWLPDGNLEYISRIDRQIKI
P94460_A1_ile                            CVGGDGVAKGYLNRVDLTNAVFSPHPFLPGERIYRTGDLARWLPDGNLEYISRIDRQMKI
                                         ************** *:* *****.**:*****************************:**

Q45563_A1_10__ile                        RGKRIEPAEIEARLLEIEGVREAAVTLLETDGEVQLYTHYVSDESRNEKE
P94460_A1_ile                            RGKRIEPAEIEARLLEMEGVQEAAVTLREKDGEAHVYTHYVGDHKKTDTD
                                         ****************:***:****** *.***.::*****.*..:.:.:
