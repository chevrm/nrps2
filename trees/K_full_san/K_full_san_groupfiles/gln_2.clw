CLUSTAL W multiple sequence alignment


Q65NK5_A1_gln                            AKAVITDSGLTFETAETVQFSEALSESRENGYPSSAAGAGHLAYIIYTSGTTGRPKGVMI
O66069_A1_gln                            VKVVITDSGLTFETAETVRFSEALSESLENGHPSSEAGAGHLAYIIYTSGTTGRPKGVMI
Q45295_A1_gln                            VKVVITDSGLTFETTETVRFSEALSESLENGHPSSEAGAGHLAYIIYTSGTTGRPKGVMI
                                         .*.***********:***:******** ***:*** ************************

Q65NK5_A1_gln                            EHRQVHHLVRGLQQAVGTYDQDDLKLALLAPFHFDASVQQIFTSLLLGQTLYIVPKKTVS
O66069_A1_gln                            EHRQVHHLVRGLQQAVGAYDQDDLKLALLAPFHFDASVQQIFTSLLLGQTLYIVPKKTVS
Q45295_A1_gln                            EHRQVHHLVRGLQQAVGAYDQDDLKLALLAPFHFDASVQQIFTSLLLGQTLYIVPKKTVS
                                         *****************:******************************************

Q65NK5_A1_gln                            DGRALSDYYRRHQIDVTDGTPAHLQLLAAADDLSGVKLRHMLVGGEALSRVATERLLQLF
O66069_A1_gln                            DGRALSDYYRRHQIDVTDGTPAHLQLLSAADDLSGVKLRHMLVGGEALSRVATERLLQLF
Q45295_A1_gln                            DGRALSDYYRRHQIDVTDGTPAHLQLLSAADDLSGVKLRHMLVGGEALSRVATERLLQLF
                                         ***************************:********************************

Q65NK5_A1_gln                            AETAESVPAVTNVYGPTETCVDASSFTITNRTDLQYDTAYVPIGRPIGNNRFYILDENGA
O66069_A1_gln                            AETAESVPDVTNVYGPTETCVDASSFTMTNHADLQGDTAYVPIGRPIGNNRFYILDENGA
Q45295_A1_gln                            AETAESVPDVTNVYGPTETCVDASSFTMTNHADLQADTAYVPIGRPIGNNRFYILDEGGA
                                         ******** ******************:**::*** *********************.**

Q65NK5_A1_gln                            LLPDGVEGELYIAGDGVGRGYLNLPDMTRDRFLKDPFVSGGLMYRTGDTARWLPDGTVDF
O66069_A1_gln                            LLPDGVEGELYIAGDGVGRGYLNLPDMTADKFLEDPFVPGGFMYRTGDAVRWLPDGTVDF
Q45295_A1_gln                            LLPDGVEGELYIAGDGVGRGYLNLPDMTADKFLEDPFVPGGFMYRTGDAVRWLPDGTVDF
                                         **************************** *:**:****.**:******:.**********

Q65NK5_A1_gln                            IGRRDDQVKIRGFRIELGEIESVLQGAPAVEKAVVLARHETGGSLEVCAYVVPKQGGKIH
O66069_A1_gln                            IGRKDDQVKIRGYRIELGEIESVLQGAPAVGKAVVLARPETGGSLEVCAYVVPKQSGEIH
Q45295_A1_gln                            IGRKDDQVKIRGYRIELGEIESVLQGAPAVGKAVVLARPETGGSLEVCAYVVPKQSGEIH
                                         ***:********:***************** ******* ****************.*:**

Q65NK5_A1_gln                            I
O66069_A1_gln                            L
Q45295_A1_gln                            L
                                         :
