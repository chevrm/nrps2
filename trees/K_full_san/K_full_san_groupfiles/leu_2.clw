CLUSTAL W multiple sequence alignment


P0C063_A4_leu                            AALLLTQSHVL-------NKLPVDIEWLDLTDEQNYVEDGTNLPFMNQSTDLAYIIYTSG
O33743_A6_10__leu                        AALLLSQAHLLPLLAQVSSELP---ECLDLNAELDAGLSGSNLPAVNQPTDLAYVIYTSG
P0C064_A4_5__leu                         ATLLLTQSHVL-------NKLPVDIEWLDLTDEQNYVEDGTNLPFMNQSTDLAYIIYTSG
                                         *:***:*:*:*       .:**   * ***. * :   .*:*** :**.*****:*****

P0C063_A4_leu                            TTGKPKGVMIEHQSIINCLQWRKEEYEFGPGDTALQVFSFAFDGFVASLFAPILAGATSV
O33743_A6_10__leu                        TTGKPKGVMIPHQGIVNCLQWRRDEYGFGPSDKALQVFSFAFDGFVASLFAPLLGGATCV
P0C064_A4_5__leu                         TTGKPKGVMIEHQSIINCLQWRKEEYEFGPGDTALQVFSFAFDGFVASLFAPILAGATSV
                                         ********** **.*:******::** ***.*.*******************:*.***.*

P0C063_A4_leu                            LPKEEEAKDPVALKKLIASEEITHYYGVPSLFSAILDVSSSKDLQNLRCVTLGGEKLPAQ
O33743_A6_10__leu                        LPQEAAAKDPVALKKLMAATEVTHYYGVPSLFQAILDCSTTTDFNQLRCVTLGGEKLPVQ
P0C064_A4_5__leu                         LPKEEEAKDPVALKKLIASEEITHYYGVPSLFSAILDVSSSKDLQNLRCVTLGGEKLPAQ
                                         **:*  **********:*: *:**********.**** *::.*:::************.*

P0C063_A4_leu                            IVKKIKEKNKEIEVNNEYGPTENSVVTTIMRDIQVEQEITIGRPLSNVDVYIVNCNHQLQ
O33743_A6_10__leu                        LVQKTKEKHPAIEINNEYGPTENSVVTTISRSIEAGQAITIGRPLANVQVYIVDEQHHLQ
P0C064_A4_5__leu                         IVKKIKEKNKEIEVNNEYGPTENSVVTTIMRDIQVEQEITIGCPLSNVDVYIVNCNHQLQ
                                         :*:* ***:  **:*************** *.*:. * **** **:**:****: :*:**

P0C063_A4_leu                            PVGVVGELCIGGQGLARGYLNKPELTADKFVVNPFVPGERMYKTGDLAKWRSDGMIEYVG
O33743_A6_10__leu                        PIGVVGELCIGGAGLARGYLNKPELTAEKFVANPFRPGERMYKTGDLVKWRTDGTIEYIG
P0C064_A4_5__leu                         PVGVVGELCIGGQGLARGYLNKPELTADKFVVNPFVPGERMYKTGDLAKWRSDGMIEYVG
                                         *:********** **************:***.*** ***********.***:** ***:*

P0C063_A4_leu                            RVDEQVKVRGYRIELGEIESAILEYEKIKEAVVMVSEHTASE-QMLCAYIVGEEDVLTLD
O33743_A6_10__leu                        RADEQVKVRGYRIEIGEIESAVLAYQGIDQAVVVARDDDATAGSYLCAYFVAATAVSVSG
P0C064_A4_5__leu                         RVDEQVKVRGYRIELGEIESAILEYEKIKEAVVIVSEHTASE-QMLCAYIVGEEDVLTLD
                                         *.************:******:* *: *.:***:. :. *:  . ****:*.   * . .
