CLUSTAL W multiple sequence alignment


Q6WZ98_A1_lys-beta                       ---LGRAESLGELPGLGIPVIP---AEPPGDLAGGAPPATRADAEPPLPDDLAYVMLTSG
O33743_A_lys-beta                        REAVATAET-GWQPPVETVIRPRWTQTPVTTVLQGTLPESGAR-----PEDPAYMLFTSG
                                            :. **: *  * :   : *     *   :  *: * : *      *:* **:::***

Q6WZ98_A1_lys-beta                       TTGTPKAVLVPHRAVTRAARSLVPLFGVTSTDRVLHWTSLIWDTSGEEIYPALLGGAALV
O33743_A_lys-beta                        STGEPKGVVVPHRAPAAVVPLLRDLYGIGPEETVLHFHGAGGDTSLEEILPALTGGATLV
                                         :** **.*:***** : ..  *  *:*: . : ***: .   *** *** *** ***:**

Q6WZ98_A1_lys-beta                       VDGRVETRSVPALLAAV-REHRVTVVDLPTAMWNELAHYLALGGEELPPALRLVVIGGEA
O33743_A_lys-beta                        ID-----DAAPEGFARVAEEQQVSVAILPTGFWSSLTGDLLHQGARLPESLRTVVIGGEA
                                         :*      :.*  :* * .*::*:*. ***.:*..*:  *   * .** :** *******

Q6WZ98_A1_lys-beta                       AHARTVRLWNERVP--DRVRLLNTYGQTETVMVTHAAELGGPAGRAL-RDGDPVPIGRPL
O33743_A_lys-beta                        VRADMLERW-RRLPGTDGVRLLNTYGSTETALVTHAAQLAGPGAPALPGTGLDLPIGHPL
                                         .:*  :. * .*:*  * ********.***.:*****:*.**.. **   *  :***:**

Q6WZ98_A1_lys-beta                       PHIRQVLVPSDDGPDELWTGGPGLAWGYADRPALTAAAFGPAPGAGGRFYRTGDLVRTLP
O33743_A_lys-beta                        PHVRQRV----DDRGELSIAGPGLALGYHGNPGATSARFRERDGT--RWYRTGDLVTEAP
                                         **:** :    *. .**  .***** ** ..*. *:* *    *:  *:*******   *

Q6WZ98_A1_lys-beta                       DGSLVHAGRADRRLKVRGVRVEPAEVERAMTTCPGVVAAAVFPVGDDPEHLRLYGAFVPS
O33743_A_lys-beta                        GGILVFRGRSDHQVKIRGFRVDLLDVEALIGRCEGVAAVAAARV-DRTEHGVLAAFFV--
                                         .* **. **:*:::*:**.**:  :**  :  * **.*.*.  * * .**  * . **  

Q6WZ98_A1_lys-beta                       KSGPATE---
O33743_A_lys-beta                        -ALPHREPDE
                                          : *  *   
