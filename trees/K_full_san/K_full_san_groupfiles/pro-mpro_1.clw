CLUSTAL W multiple sequence alignment


Q84BC7_A3_6__pro-mpro                    VKVLLTQRSLLDRLPQCEKAGGQGAGSRGESPSTRDRASTKGKEEVLSLPASYQTQLVCL
Q9RAH4_A3_pro-mpro                       VSVLLTQQSILDRLPQ------------------------------------HQANQVCL
                                         *.*****:*:******                                    :*:: ***

Q84BC7_A3_6__pro-mpro                    DTDAELISQCSQDNLITGVQANNLGYIIYTSGSTGQPKGIAMNQLALCNLILWHPDNLKI
Q9RAH4_A3_pro-mpro                       DTDAQLISQCSQDNLISDVQANNLAYIIYTSGSTGQPKGIAMNQLALSNLILWHRENLKI
                                         ****:***********:.******.**********************.****** :****

Q84BC7_A3_6__pro-mpro                    ARGAKTLQFASINFDVSFQEIFTTWCSGGTLFLITKELRHDTSNLLRVIQEKAIQRMFLP
Q9RAH4_A3_pro-mpro                       PRGAKTLQFASINFDVSFQEIFTTWCSGGTLFLIGEELRRDTSALLGFLQQKAIERMFLP
                                         .********************************* :***:*** ** .:*:***:*****

Q84BC7_A3_6__pro-mpro                    VVGLQQLAEFAVGSELVNTHLREIITAGEQLQITPAISKWLSQLSDCTLHNHYGPSESHV
Q9RAH4_A3_pro-mpro                       FVALQQLAEVAIGGELVNSHLREIITAGEQLQITPAISQWLSKLTDCTLHNHYGPSESHL
                                         .*.******.*:*.****:*******************:***:*:**************:

Q84BC7_A3_6__pro-mpro                    ATSFTLPNLVNTWPLLPPIGRPISNTQIYILDKYLQPVPIGVPGEVYIAGVLLARGYLNR
Q9RAH4_A3_pro-mpro                       ATSFTLTNSVETWPLLPPVGRPIANAQIYILDRFLQPVPVGVPGELYIAGVLLSQGYFNR
                                         ******.* *:*******:****:*:******::*****:*****:*******::**:**

Q84BC7_A3_6__pro-mpro                    PELTQEKFIQNPFGGSRGAGEQGSRGAEEQSFPSAPHSLCPSASSERLYKTGDLARYLPD
Q9RAH4_A3_pro-mpro                       PELTLEKFIPNPFKRSRGAGEQGSRG---ETF-----------NCDRLYKTGDLARYLSD
                                         **** **** ***  ***********   ::*           ..:************.*

Q84BC7_A3_6__pro-mpro                    GNIEYLGRIDNQVKIRGFRIELGEIEAVLSQHINVQASCA-------VVREDTPGDKRLV
Q9RAH4_A3_pro-mpro                       GNIEYLGRIDNQVKIRGFRIELGEIEAVLSQ-LDVQASCAMATPAAGIAREDIPGNKRLV
                                         ******************************* ::******       :.*** **:****

Q84BC7_A3_6__pro-mpro                    AYIVPQPEQRVSV
Q9RAH4_A3_pro-mpro                       AYIVPQKEQKLTV
                                         ****** **:::*
