CLUSTAL W multiple sequence alignment


O07944_A2_phe                            PALVV----------DAIP----DDTTLAAYADSRLTDADRSAPLLPAHPAYVIYTSGST
O05647_A2_phe                            PALVLTHTSVAAGLPGGVPQLLVDQVGLDDVPGHDLTDAERTTPLHPLHPAYVIYTSGST
                                         ****:          ..:*    *:. *   ..  ****:*::** * ************

O07944_A2_phe                            GAPKGVVVAHRSLAATVPAQAAAFGLGTHSRVLNFASISFDAAVWELTSALFTGAGLVLA
O05647_A2_phe                            GLPKGVPVPHRSVASVLVPLIEEFGLGPGSRVLQFASISFDAALWEITLALLSGATLVVA
                                         * **** *.***:*:.: .    ****. ****:*********:**:* **::** **:*

O07944_A2_phe                            DADDLLPGPSLARLVHDRHITLIALPPSALPALPDGALPPGTDLIVAGDATAPDQAARFA
O05647_A2_phe                            PAEQLQPGPALAELVARTGTTFLTLPPTALAVLADDALPAGVDLVVAGEATSPDQVGRWS
                                          *::* ***:**.**     *:::***:**..*.*.***.*.**:***:**:***..*::

O07944_A2_phe                            PGRRMVNAYGLTETTVCATMSEPATGDGAPPIGRPVAHARVYVLDERLRPVPPGVTGEMY
O05647_A2_phe                            TGRRMTNAYGPTEAAVCTTISAPLTGAVVPPIGRPVPNARAYVLDALLQPVPPGVVGELY
                                         .****.**** **::**:*:* * **  .*******.:**.****  *:******.**:*

O07944_A2_phe                            VSGAGVARGYLHRPALTAQRFVPDPYALLFGETGTRMYRTGDLARLDADGRLHFAGRADQ
O05647_A2_phe                            LAGGGLARGYRNRPGLTAERFVADP----FGTPGARMYRTGDLARWRPDGELEFAGRTDH
                                         ::*.*:**** :**.***:***.**    ** .*:**********  .**.*.****:*:

O07944_A2_phe                            QVKIRGFRIEPGEIETVLTAHPAVAAGAVIA--REDTPGDKQLVAYL-------
O05647_A2_phe                            QVKIRGFRIEPGEVEAALATHPAVERAAVIAARHED---DRRLVAYLVPAGAGT
                                         *************:*:.*::****  .****  :**   *::*****       
