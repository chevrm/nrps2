CLUSTAL W multiple sequence alignment


Q45R83_A4_11__asn                        PVLVLAHTATQDALPEG-----AGPVVRLDAPAIEAALAGLDGGDCTDADR-RAPATHHD
Q939Z1_A3_asn                            PALVIT-TAVLSASPIGDVLAARSRTVVLDEPAAAGQLAGRDRAPVTDTDRARALDPRH-
                                         *.**:: **. .* * *      . .* ** **  . *** * .  **:** **  .:* 

Q45R83_A4_11__asn                        PAYVVYTSGSTGTPKGVVVEQRSLAAFLVRSAARYRGAAGTALLHGSPAFDLTVTTLFTP
Q939Z1_A3_asn                            PAYLIYTSGSTGRPKAVVVTHRNLTNYLLHCGRMYPGLRGRSVLHSSIAFDLTVTATFTP
                                         ***::******* **.*** :*.*: :*::..  * *  * ::**.* *******: ***

Q45R83_A4_11__asn                        LIAGGCIVVADLDAPERDAPARPDL-LKVTPSHLALLDTIASWATPAADLVVGGEQLTAS
Q939Z1_A3_asn                            LIVGGEIHVGALEDLIGVVEAAPSIFLKATPSHLLTLDTASRGSAGSGDLLLGGEQLPAD
                                         **.** * *. *:     . * *.: **.*****  *** :  :: :.**::*****.*.

Q45R83_A4_11__asn                        RLARLRRAHPDMRVFNDYGPTEATVSCADFVLEPG-DAPPTDTVPIGRPLAGHRLFVLDD
Q939Z1_A3_asn                            TVVQWRRKYPNIVVVNEYGPTEATVGCVEYRLEPGQECPPGGVVPIGTPLANMRAFVLDS
                                          :.: ** :*:: *.*:********.*.:: **** :.** ..**** ***. * ****.

Q45R83_A4_11__asn                        RLRPVPANVPGELYVSGVGVARGYLGRPGMTAERFVACPFGEPGERMYRTGDLARRRADG
Q939Z1_A3_asn                            WLRLVPPGAVGELYVAGAGLARGYLGRAGLTATRFVADPFGS-GERMYRTGDLVQWNPDG
                                          ** **... *****:*.*:*******.*:** **** ***. **********.: ..**

Q45R83_A4_11__asn                        NLEYLGRRDGQVKVRGFRVETGEIETALLDRPEIGQAAVVLR----GERLLAYVAAPPER
Q939Z1_A3_asn                            QLVFAGRVDDQVKVRGFRIEPGEIEAALVAQESVGQAVVVARDSEIGTRLIGYVTAAGES
                                         :* : ** *.********:*.****:**: : .:***.** *    * **:.**:*. * 

Q45R83_A4_11__asn                        F---
Q939Z1_A3_asn                            GVDE
                                             
