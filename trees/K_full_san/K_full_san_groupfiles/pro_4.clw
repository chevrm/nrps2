CLUSTAL W multiple sequence alignment


O05647_A1_pro                            PALVLTE----------------DD--VDEDLSGIPDGNLTDAERTAPLTPAHPAYVIYT
Q9L8H4_A1_4__pro                         PTLVLTTTETEAKLPDRHPGLLLDDPAVLADLSGRP-----AHDPVVELHPDHPAYVIYT
                                         *:****                 **  *  **** *       : .. * * ********

O05647_A1_pro                            SGSTGRPKAVVMPGAAVVNLLAWHRREIPAGAGTTVAQFASLSFDVAAQEILSTLLYGAT
Q9L8H4_A1_4__pro                         SGSTGVPKGVVMPAGGLLNLLQWHHRAVGDEPGTRTAQFTAISFDVSAQEVLSSVAFGKT
                                         ***** **.****...::*** **:* :   .** .***:::****:***:**:: :* *

O05647_A1_pro                            LAVPTDAVRRDADAFAAWLEEYRVNELYAPNLVVEALAEAAAEQGRTLPDLRHIAQAGEA
Q9L8H4_A1_4__pro                         LVIPDEEVRRDAARFAGWLDDRQVDELFAPNLVLEALAEAAVETGRTLPQLRTVAQAGEA
                                         *.:* : *****  **.**:: :*:**:*****:*******.* *****:** :******

O05647_A1_pro                            LTAGPRVRDFCAALPGRRLHNHYGPAETHVMTGIELPVDPGGWPERVPIGGPVDNARLYV
Q9L8H4_A1_4__pro                         LTLSRTVRAFHRSAPGRRLHNHYGPTETHVVTAHALGDDPEDWRLPAPIGRPIDNTHAYV
                                         ** .  ** *  : ***********:****:*.  *  ** .*   .*** *:**:: **

O05647_A1_pro                            LDGFLRPVPPGVVGELYLAGAGVARGYLNRPGLTAERFVADPFG-GPGTRMYRTGDLARW
Q9L8H4_A1_4__pro                         RTRAVRLVEPGVVGELYIAGAGLARGYLGRPALTAERFVADPYGLEPGGRMYRTGDLVRR
                                             :* * ********:****:*****.**.**********:*  ** ********.* 

O05647_A1_pro                            AGSGVLEFAGRADHQVKVRGFRIEPGEVESVLAAQPGVARAVVLAREDRPGERRLVAYLV
Q9L8H4_A1_4__pro                         NPDGELEFCGRADHQVKVRGFRIEPGEIEKVLTDHPDIAQAAVVTRPHRPGDTRLVAYVV
                                           .* ***.******************:*.**: :*.:*:*.*::* .***: *****:*

O05647_A1_pro                            AVPGSVPDPG
Q9L8H4_A1_4__pro                         GREALRPE--
                                         .  .  *:  
