CLUSTAL W multiple sequence alignment


Q79JZ1_A1_cys                            INVILINESDSKNSPSNDLFFFLDWQTAIKSEPMRSPQDVAPSQPAYIIYTSGSTGTPKG
P48633_A1_cys                            VRLVLICQHDA--SAGSDDIPVLAWQQAIEAEPIVNPVVRAPTQPAYIIYTSGSTGTPKG
O85739_A1_cys                            VCLAITEEDDPQALP-----PRLDVQRLLRGPALAAPVPLAPQASAYVIYTSGSTGVPKG
                                         : : :  : *.   .       *  *  :.. .:  *   **  .**:********.***

Q79JZ1_A1_cys                            VVISHQGALNTCIAINRRYQIGKNDRVLALSALHFDLSVYDIFGLLSAGGTIVLVSELER
P48633_A1_cys                            VVISHRGALNTCCDINTRYQVGPHDRVLALSALHFDLSVYDIFGVLRAGGALVMVMENQR
O85739_A1_cys                            VEVSHAAAINTIDALLDLLRVNASDRLLAVSALDFDLSVFDLFGGLGAGASLVLPAQEQA
                                         * :** .*:**   :    ::.  **:**:***.*****:*:** * **.::*:  : : 

Q79JZ1_A1_cys                            RDPIAWCQAIEEHNVTMWNSVPALFDMLLTYATCFNSIAPSKLRLTMLSGDWIGLDLPQR
P48633_A1_cys                            RDPHAWCELIQRHQVTLWNSVPALFDMLLTWCEGFADATPENLRAVMLSGDWIGLDLPAR
O85739_A1_cys                            RDAAAWAEAIQRHAVSLWNSAPALLEMALSLPASQADY--RSLRAVLLSGDWVALDLPGR
                                         **. **.: *:.* *::***.***::* *:      .    .** .:*****:.**** *

Q79JZ1_A1_cys                            YRNYRVDG-QFIAMGGATEASIWSNVFDVEKVPMEWRSIPYGYPLPRQQYRVVDDLGRDC
P48633_A1_cys                            YRAFRPQG-QFIAMGGATEASIWSNACEIHDVPAHWRSIPYGFPLTNQRYRVVDERGRDC
O85739_A1_cys                            LRPRCAEGCRLHVLGGATEAGIWSNLQSVDTVPPHWRSIPYGRPLPGQAYRVVDTHGRDV
                                          *    :* :: .:******.****  .:. ** .******* **. * *****  *** 

Q79JZ1_A1_cys                            PDWVAGELWIGGDGIALGYFDDELKTQAQFLHIDGHAWYRTGDMGCYWPDGTLEFLGRRD
P48633_A1_cys                            PDWVSGELWIGGIGVAEGYFNDSLRSEQQFLTLPDERWYRTGDLGCYWPDGTIEFLGRRD
O85739_A1_cys                            PDLVVGELWIGGASLARGYRNDPELSARRFVHDAQGRWYRTGDRGRYWGDGTLEFLGRVD
                                         ** * ******* .:* ** :*   :  :*:      ****** * ** ***:***** *

Q79JZ1_A1_cys                            KQVKVGGYRIELGEIEVALNNIPGVQRAVAIAVGNKDKTLAAFIVMDSEQAPI
P48633_A1_cys                            KQVKVGGYRIELGEIESALSQLAGVKQATVLAIGEKEKTLAAYVVPQSE----
O85739_A1_cys                            QQVKVRGQRIELGEVEAALCAQAGVESACAAVLGGGVASLGAVLVPRLA--P-
                                         :**** * ******:* **   .**: * . .:*    :*.* :*        
