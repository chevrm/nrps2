CLUSTAL W multiple sequence alignment


Q6YK40_A4_glu                            AKVLLAQTHLQDRVSFAGEILLLNDERMNSGDSSNLVTAAGPDHLAYVIYTSGTTGKPKG
Q70JZ9_A4_glu                            AKVLLAQPHLQDRVSFAGEILLLNDERMNSGDGSNLVTAAGPDHLAYVIYTSGTTGKPKG
                                         *******.************************.***************************

Q6YK40_A4_glu                            TLIEHRQVLHLMEGLRGQVYGAYDSGLRVSLLAPYYFDASVKQIFASLLGGHALYIVPKA
Q70JZ9_A4_glu                            TLIEHRQVLHLMEGLRGQVYGAYDSGLRVSLLAPYYFDASVKQIFAALLGGHALYIVPKA
                                         **********************************************:*************

Q6YK40_A4_glu                            SVSDGYALSNYYRTHRIDVTDGTPSHLQLLIAADSLHGVTIRHMLIGGEALPQATVAQLL
Q70JZ9_A4_glu                            SVSDGYALSNYYRTHRIDVTDGTPSHLQLLIAADSLHGVTIRHMLIGGEALPQATVAQLL
                                         ************************************************************

Q6YK40_A4_glu                            ELFASNGSSMPLITNVYGPTETCVDASVFHIVPETLASADDGGYVPIGKPLGNNRVYIVD
Q70JZ9_A4_glu                            ELFASNGSSMPLITNVYGPTETCVDASVFHIVPETLASADDGGYVPIGKPLGNNRVYIVD
                                         ************************************************************

Q6YK40_A4_glu                            SHDRMLPIGVKGELCIAGDGVGRGYLNLPELTGEKFVADPFVIGERMYKTGDLARYLPDG
Q70JZ9_A4_glu                            SHDRMLPIGVKGELCIAGDGVGRGYLNLPELTGVKFVADPFVIGERMYKTGDLARYLPDG
                                         ********************************* **************************

Q6YK40_A4_glu                            NIEYAGRKDHQVKIRGYRIELGEVEAALLNIEHVQEAVILARENAEGQSDLYAYFTGEKS
Q70JZ9_A4_glu                            NIEYAGRKDHQVKIRGYRIELGEVEAALLNIEHVQEAVILARENAEGQSDLYAYFTGEKS
                                         ************************************************************

Q6YK40_A4_glu                            LPINQ
Q70JZ9_A4_glu                            LPINQ
                                         *****
