CLUSTAL W multiple sequence alignment


Q2XNF8_A2_3__ala                         -AIALVDRVGRASLGADAAGARDLLDLDAADPAWSDRSAADPEPAGLSARNLAYVIYTSG
Q2XNF8_A1_2__ala                         PTLVLSDAAGRRALGENALLAHAVVDLDERRPVWAGASTADPRPAGLSPRHLACAIYPAG
                                          ::.* * .** :** :*  *: ::***   *.*:. *:***.*****.*:** .**.:*

Q2XNF8_A2_3__ala                         STGTPKGVQNEHRGVVNRLAWMPEEYRLGAGDTVLQKTSFGFDVSVWEFFWPLLHGATLA
Q2XNF8_A1_2__ala                         ATAAPAGAQNEHRALVDRLRWMQDAYRLGAGETVVQQTSLAADAALWELFWTWSNGATVV
                                         :*.:* *.*****.:*:** ** : ******:**:*:**:. *.::**:**.  :***:.

Q2XNF8_A2_3__ala                         LAPPDAHKDPAALIELIVRHRVTTVHFVPSMLAVFLQADGVERCAGLRRVICSGEALPGA
Q2XNF8_A1_2__ala                         LAPAGAQSTPAALVELFVRHDIATAHFAPAALAAFLRADGVERCVGLRRLICSGEALSGA
                                         ***..*:. ****:**:*** ::*.**.*: **.**:*******.****:*******.**

Q2XNF8_A2_3__ala                         SVRLLHKRLPQTAIHNLYGPTEAAIEATAWTCPRDFAGDTVPIGRPIANARIYLLDPRRQ
Q2XNF8_A1_2__ala                         SLRLAQQRLPWAAIHRLYGPAETAMDATAWTCPPDFAGDRAPIGRPIANTRVYLLDRHRR
                                         *:** ::*** :***.****:*:*::******* ***** .********:*:**** :*:

Q2XNF8_A2_3__ala                         PVPLGAVGELYIGGVGVARGYLNRPELTDERFLPDPFAADPDARMYRSGDLARHLAGGDI
Q2XNF8_A1_2__ala                         PLPPGAVGELHIGGAGIGRGYLNRPELTAERFVPDPFAADPAARMYRSGERARYRPDGEL
                                         *:* ******:***.*:.********** ***:******** *******: **: ..*::

Q2XNF8_A2_3__ala                         EFLGRNDHQVKLRGFRIELGEIETRLAAHAEVRETVVLALGEGSLMRLVAYVVA--AQRG
Q2XNF8_A1_2__ala                         ECLGRSDRQLRLRGLRIEPGDIEARLAEHPAVQRAVVLAPGDGERKRLVAYLVVRPGHAG
                                         * ***.*:*::***:*** *:**:*** *. *:.:**** *:*.  *****:*.  .: *

Q2XNF8_A2_3__ala                         DER
Q2XNF8_A1_2__ala                         DL-
                                         *  
