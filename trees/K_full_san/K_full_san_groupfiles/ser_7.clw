CLUSTAL W multiple sequence alignment


Q6WZB2_A1_2__ser                         ARAVLLRQELRDRLPEDLRDGTGQVAVVPVGAESGAGTSTRRPVTPVEQEPRPERLAYIV
Q6WZB2_A2_3__ser                         ARVLLTQRRQTASLP--------KLPGVTVLVVDDHEALSRFPATVPKPVPRPQDLAYVI
                                         **.:* ::.    **        ::. *.* . ..  : :* *.*  :  ***: ***::

Q6WZB2_A1_2__ser                         YTSGSTGLPKGVMVEHRGIVSYLLGMLEHFPMGPRDRMLQVTSLSFDVSVYEIFLPLLTG
Q6WZB2_A2_3__ser                         YTSGSTGRPKGVMVEHHSVVNYLTTLQEKFRLTSDDRLLLKSPLSFDVSVREVFWALSTG
                                         ******* ********:.:*.**  : *:* : . **:*  :.******* *:* .* **

Q6WZB2_A1_2__ser                         GATVLPRSGSHTDAAYLSGLIAEHGVTSFHMVPSLLRTFVDGLD-PRQCAGLRRIFVSGE
Q6WZB2_A2_3__ser                         ATLVVAEAGRHADPDYLVEAIERERVTVVHFVPSMLHVLLETLDGPGRCPTLRQVMTSGE
                                         .: *:..:* *:*. **   * .. ** .*:***:*:.::: ** * :*. **:::.***

Q6WZB2_A1_2__ser                         ALDTTLVVDVHDRLPCDVVNLYGATEVSVDST-----WWTAPRDLPDAPVLVGRPMAGAT
Q6WZB2_A2_3__ser                         TLPVQTARRCLELLGAELRNMYGPTETTVEMTDCEVRGRTDTERLP-----IGRPFPNTR
                                         :* .  .    : * .:: *:**.**.:*: *       * .. **     :***:..: 

Q6WZB2_A1_2__ser                         AYVLDDEMRRLAPNEVGEVYLGGASVTRGYHGRAALTAQRFLPDPYGPPGSRLYRTGDLG
Q6WZB2_A2_3__ser                         VYVLDDELRLVPRGTVGELYVSGAPVARGYLGRPALTADRFLPDPYGPPGSRMYRTGDLG
                                         .******:* :. . ***:*:.**.*:*** **.****:*************:*******

Q6WZB2_A1_2__ser                         RVEDNGELRLLGRIDHQVKLHGRRIEPGEIEAAMTAHPHVSLAAAV---PAGAGAGATLT
Q6WZB2_A2_3__ser                         RFTGEGLLDFQGRGDFQVQLRGHRIEPGEIETVLCEQPGVTAAVAVVRRPDSPEA-AHLV
                                         *. .:* * : ** *.**:*:*:********:.:  :* *: *.**   * .. * * *.

Q6WZB2_A1_2__ser                         GFFTGAEADA----
Q6WZB2_A2_3__ser                         AYAVRAEEPHGTDQ
                                         .: . **       
