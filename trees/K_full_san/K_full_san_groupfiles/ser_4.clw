CLUSTAL W multiple sequence alignment


Q2N3S9_A1_9__ser                         VAVLLTQSSFSGVLAGFTGTRLCLDTEASRLSSYSA------DSPRVEVRPEHLAYVIYT
Q9Z4X6_A1_ser                            AMLVLTTRDTAERLPGDGTPRLLLDEPAAAGTTAAGAPAPPGTLPRALPAPGHPAYVIYT
Q50E73_A6_11__ser                        PVAVLTRGDV--ELPG-SVPRIGLDDTEIRATLATA----PGTNPGTPVTEAHPAYMIYT
                                            :**  .    *.*   .*: **      :  :.        * .     * **:***

Q2N3S9_A1_9__ser                         SGSTGMPKGCMLSHRAICNRLHWMQEAYALTPLDRVLQKTPFTFDVSVWEFFWPLITGAR
Q9Z4X6_A1_ser                            SGSTGRPKGVVISHRAIVNRLAWMQDTYGLEPSDRVLQKTPSGFDVSVWEFFWPLVQGAT
Q50E73_A6_11__ser                        SGSTGRPKGVVVSHGAIVNRLAWMQAEYRLDATDRVLQKTPAGFDVSVWEFFWPLLEGAV
                                         ***** *** ::** ** *** ***  * * . ********  ************: ** 

Q2N3S9_A1_9__ser                         IVLAAPGAERDPAALAGLIQEHGVTACHFVPSMLRLFLDEPRAAGCAS---LRHVFFSGE
Q9Z4X6_A1_ser                            LVVARPGGHTDPAYLAGTVRREGVTTLHFVPSMLDVFLREPAAAALGGATPVRRVFCSGE
Q50E73_A6_11__ser                        LVFARPGGHRDAAYLAGLIERERITTAHFVPSMLRVFLEEPGAALCTG---LRRVICSGE
                                         :*.* **.. *.* *** :... :*: ******* :** ** **   .   :*:*: ***

Q2N3S9_A1_9__ser                         ALPYPLMERALSTFSAQLHNLYGPTEAAVDVSFWKCN-LRDDRKVPIGRPIANIRLYILD
Q9Z4X6_A1_ser                            ALPAELRARFRAVSDVPLHNLYGPTEAAVDVTYWPCAEDTGDGPVPIGRPVWNTRMYVLD
Q50E73_A6_11__ser                        ALGTDLAVDFRAKLPVPLHNLYGPTEAAVDVTHHAYEPATGTATVPIGRPIWNIRTYVLD
                                         **   *     :   . **************:.       .   ******: * * *:**

Q2N3S9_A1_9__ser                         EQQRPVRPDQTGELYIAGVGLARGYLNRPELTAERFVRDPFDPSPGARMYKTGDRAAWLP
Q9Z4X6_A1_ser                            AALRPVPAGVPGELYIAGVQLARGYLGRPALSAERFTADPHG-APGSRMYRTGDLARWNH
Q50E73_A6_11__ser                        AALRPVPPGVPGELYLAGAGLARGYHGRPALTAERFVACPFG-VPGERMYRTGDLVRWRV
                                            *** .. .****:**. ***** .** *:****.  *..  ** ***:*** . *  

Q2N3S9_A1_9__ser                         DGNIDFLGRMDGQIKLRGLRIELGEIEAALLGHEAVREAAVAVRDADSGDPRLVAYVV-L
Q9Z4X6_A1_ser                            DGSLDYLGRADHQVKLRGFRIELGEIEAALVRQPEIAQAAVVLREDRPGDQRLVAYTVPA
Q50E73_A6_11__ser                        DGTLEFVGRADDQVKVRGFRVELGEVEGAVAAHPDVVRAVVVVREDRPGDHRLVAYVT--
                                         **.::::** * *:*:**:*:****:*.*:  :  : .*.*.:*:  .** *****..  

Q2N3S9_A1_9__ser                         RD---------------GASFSPQ
Q9Z4X6_A1_ser                            RDADTLTGPPAEAGTHPGPGAAPD
Q50E73_A6_11__ser                        ------------------------
                                                                 
