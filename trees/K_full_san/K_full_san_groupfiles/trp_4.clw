CLUSTAL W multiple sequence alignment


Q5V8A8_A2_trp                            PCLLITTTN---SLISNPKLNIPKLQLDSFPWEASLNVTSEKQDYGLQANIEDNRGTQPL
Q93N89_A2_2__trp                         PAAVLCTAQTR------PMVPEG---FTGLVVTLDEDLPEGDFDDGRV-------LTAP-
Q45R85_A1_trp                            PALLVGGTGTEPSAADCPRVPAEEL-LDAGACRAEADVPPP--------------GSLP-
Q93N89_A2_trp                            PAAVLCTAQTR------PMVPEG---FTGLVVTLDEDLPEGDFDDGRV-------LTAP-
                                         *. ::  :         * :      : .     . ::.                 : * 

Q5V8A8_A2_trp                            HVSDPAYIIYTSGSTGKPKGVVVTHAGISSMVATQIKYFEVTPESRILQFSSLSFDGVVW
Q93N89_A2_2__trp                         DADHIAYVIHTSGSTGTPKGAAVTWGGLRNLVADRIERYGIGTDTRLVQLVSPSFDVSMA
Q45R85_A1_trp                            -VDLPAYVVHTSGSTGRPKGVVVTHAGIAALAAEQIERYRLGPGSRVAQLAALGFDVAVA
Q93N89_A2_trp                            DADHIAYVIHTSGSTGTPKGAAVTWGGLRNLVADRIERYGIGTDTRLVQLVSPSFDVSMA
                                          ..  **:::****** ***..** .*:  :.* :*: : : . :*: *: : .**  : 

Q5V8A8_A2_trp                            ELCSALLTGATLVMAPSERVQPGPELIQLIGDYHVTHAVLPPAVLMVLSPDNIPSLTHLI
Q93N89_A2_2__trp                         DIWPTLCAGGRLVLAPPGGHTTGDELGDLLADHRITHAVMPAVQLTHLPDRELPALRVLV
Q45R85_A1_trp                            ELVMALASGSCLVLPPHGLA--GDELASFLRDRRITTALAPAAVLATLPPGDLPDLTDLV
Q93N89_A2_trp                            DIWPTLCAGGRLVLAPPGGHTTGDELGDLLADHRITHAVMPAVQLTHLPDRELPALRVLV
                                         ::  :* :*. **:.*      * ** .:: * ::* *: *.. *  *.  ::* *  *:

Q5V8A8_A2_trp                            VSGEAASGELVKRWSVGRCLINGYGPTETTVCATLS-SPLSGNGIPPIGRAVINVQCYVL
Q93N89_A2_2__trp                         SGGDALPADTRRRWVARCDLHNEYGVTEATVVSTVT-APLDDAGPLTIGGPIARAGVHVL
Q45R85_A1_trp                            TGGEQPPPALIARWAPGRRMFNVYGPTEATVQATSGRCAADGDRSPDIGNPEAGVDAYVL
Q93N89_A2_trp                            SGGDALPADTRRRWVARCDLHNEYGVTEATVVSTVT-APLDDAGPLTIGGPIARAGVHVL
                                          .*:  .     **     : * ** **:** :*   .. ..     ** .   .  :**

Q5V8A8_A2_trp                            DDQLQLLPPGAIGELYISGPGLARGYLNQPQLTAERFLANPFRDIGSRMYRTGDLVRWRN
Q93N89_A2_2__trp                         DGFLRPVPPGVTGELYVTGTGVARGYLNRPTLTAHRFIADP-RTRGGRMYRTGDLVRHTH
Q45R85_A1_trp                            DAALRPVPDGVTGELYLRGRGLARGYLGRPGLTAGRFVADPHTGTGERMYRTGDLVR-RV
Q93N89_A2_trp                            DGFLRPVPPGVTGELYVTGTGVARGYLNRPTLTAHRFIADP-RTRGGRMYRTGDLVRHTH
                                         *  *: :* *. ****: * *:*****.:* *** **:*:*    * **********   

Q5V8A8_A2_trp                            HGE----LEFVGRADNQVKIRGFRVELGEVETALTNCPPVSDALAMVREDRPGEKFLVAY
Q93N89_A2_2__trp                         GGE----LVFVGRADEQVKLRGHRIELGEVEAALADHPDVEQAVAAIHDAR-----LIGY
Q45R85_A1_trp                            PGDGRTVLRFVGRADDQVKIRGFRVEPGEVEAALAELDGVEQALVTVREERPGDRRLVGY
Q93N89_A2_trp                            GGE----LVFVGRADEQVKLRGHRIELGEVEAALADHPDVEQAVAAIHDAR-----LIGY
                                          *:    * ******:***:**.*:* ****:**::   *.:*:. ::: *     *:.*

Q5V8A8_A2_trp                            VV----GQDSMDTDA
Q93N89_A2_2__trp                         VVPADGRVPDPS---
Q45R85_A1_trp                            LTPAPGHRGSLDVE-
Q93N89_A2_trp                            VVPADGRVPDPS---
                                         :.       . .   
