CLUSTAL W multiple sequence alignment


Q8RTG4_A1_arg                            VSLVLTQESLGDFLPQTGAELLCLDRDWEKIATYSPENPFNLTTP---ENLAYVIYTSGS
Q9S1A7_A1_10__arg                        ISLLLTQQSLVQFLPENQAEILCLDTDWSRIANYSQE---NLTSPVKPENLAYVIYTSGS
Q9RNA9_A1_arg                            ISLLLTQQSLVQFLPENQAEILCLDTDWSRIANYSQE---NLTSPVKTENLAYVIYTSGS
                                         :**:***:** :***:. **:**** **.:**.** *   ***:*   ************

Q8RTG4_A1_arg                            TGKPKGVMNIHRGICNTLTYAIGHYNINSEDRILQITSLSFDVSVWEVFLSLISGASLVV
Q9S1A7_A1_10__arg                        TGKPKGVMNIHQGICNTLKYNIDNYNLNSEERILQITPFSFDVSVWEIFLSLTSGATLVV
Q9RNA9_A1_arg                            TGKPKGVMNIHQGICNTLKYNIDNYNLNSEDRILQITPFSFDVSVWEVFSSLTSGATLVV
                                         ***********:******.* *.:**:***:******.:********:* ** ***:***

Q8RTG4_A1_arg                            AKPDGYKDIDYLIDLIVQEQVTCFTCVPSILRVFLQHPKSKDCHCLKRVIVGGEALSYEL
Q9S1A7_A1_10__arg                        AKPDGYKDIDYLIDLIVQEQVTCFTCVPSILRVFLQHSKSKDCHCLKRVIVGGEALSYEL
Q9RNA9_A1_arg                            TKPDGYKDIDYLIDLIVQEQVTCFTCVPSILRVFLQHPKSKDCHCLKRVIVGGEALSYEL
                                         :************************************.**********************

Q8RTG4_A1_arg                            NQRFFQQLNCELYNAYGPTEVAVETTIWCCQPNSQ-ISIGTPIANAQVYILDSYLQPVPI
Q9S1A7_A1_10__arg                        NQRFFQQLNCELYNAYGPTEVAVETTIWCCQPNSQ-ISIGTPIANAQVYILDSYLQPVPI
Q9RNA9_A1_arg                            NQRFFQQLNCELYNAYGPTEVAVDATVWCCQPNSQLISIGRPIANVQVYILDSYLQPVPI
                                         ***********************::*:******** **** ****.**************

Q8RTG4_A1_arg                            GVAGELHIGGMGLARGYLNQPELTAEKFIPHPFAQGKLYKTGDLARYLPEGNIEYLGRID
Q9S1A7_A1_10__arg                        GVAGELHIGGMGLARGYLNQPELTAEKFIPHPFAQGKLYKTGDLARYLPDGNIEYLGRID
Q9RNA9_A1_arg                            GVAGELHIGGMGLARGYLNQAELTAEKFIPHPFAEGKLYKTGDLARYLPDGNIEYLGRID
                                         ********************.*************:**************:**********

Q8RTG4_A1_arg                            NQVKLRGLRIELGEIQTVLETHPNVEQTVVIPRGSA------------GNSLL
Q9S1A7_A1_10__arg                        NQVKLRGLRIELGEIQTVLETHPNVEQTVVIMREDTLYNQRLVAYVIRKDTLL
Q9RNA9_A1_arg                            NQVKLRGFRIELGEIQTVLETHPNVEQTVVIMREDTLYNQRLVAYVMRKEPLL
                                         *******:*********************** * .:             :.**
