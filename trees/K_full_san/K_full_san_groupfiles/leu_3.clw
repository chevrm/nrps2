CLUSTAL W multiple sequence alignment


Q09164_A2_leu                            EKLVLLGAGEPSPEGQSPEVSIVRI----ADATSP--AGHASLR-DGKSKPTAGSLAYVI
Q09164_A3_leu                            SMLVLVGAETPIPEGMA-EAETIRITEILADAKTDDINGLAA------SQPTAASLAYVI
Q09164_A8_leu                            KRLVLLGSGIDMPQSDRMDVETARIQDILTNTKVE--------RSDPMSRPSATSLAYVI
Q09164_A10_leu                           EKFVLLGAGVPIPDNKTADVRMVFISDIVA-SKTD--KSYSP-----GTRPSASSLAYVI
                                         . :**:*:    *:.   :.    *    : :.               ::*:* ******

Q09164_A2_leu                            FTSGSTGKPKGVMIEHRGVLRLVKQTNILSSLPPAQTFRMAHMSNLAFDASIWEVFTALL
Q09164_A3_leu                            FTSGSTGRPKGVMVEHRGIVRLTKQTNITSKLP--ESFHMAHISNLAFDASVWEVFTTLL
Q09164_A8_leu                            FTSGSTGRPKGVMIEHRNILRLVKQSNVTSQLP--QDLRMAHISNLAFDASIWEIFTAIL
Q09164_A10_leu                           FTSGSTGRPKGVMVEHRGVISLVKQN--ASRIP--QSLRMAHVSNLAFDASVWEIFTTLL
                                         *******:*****:***.:: *.**.   * :*  : ::***:********:**:**::*

Q09164_A2_leu                            NGGSLVCIDRFTILDAQALEALFLREHINIALFPPALLKQCLTDAAATIKSLDLLYVGGD
Q09164_A3_leu                            NGGTLVCIDYFTLLESTALEKVFFDQRVNVALLPPALLKQCLDNSPALVKTLSVLYIGGD
Q09164_A8_leu                            NGGALICIDYFTLLDSQALRTTFEKARVNATLFAPALLKECLNHAPTLFEDLKVLYIGGD
Q09164_A10_leu                           NGGTLFCISYFTVLDSKALSAAFSDHRINITLLPPALLKQCLADAPSVLSSLESLYIGGD
                                         ***:*.**. **:*:: **   *   ::* :*:.*****:** .:.: .. *. **:***

Q09164_A2_leu                            RLDTADAALAKALVKSEVYNAYGPTENTVMSTLYSIADTERFVNGVPIGRAVS-NSGVYV
Q09164_A3_leu                            RLDASDAAKARGLVQTQAFNAYGPTENTVMSTIYPIAE-DPFINGVPIGHAVS-NSGAFV
Q09164_A8_leu                            RLDATDAAKIQALVKGTVYNAYGPTENTVMSTIYRLTDGESYANGVPIGNAVS-SSGAYI
Q09164_A10_leu                           RLDGADATKVKDLVKGKAYNAYGPTENSVMSTIYTIEH-ETFANGVPIGTSLGPKSKAYI
                                         *** :**:  : **:  .:********:****:* : . : : ****** ::. .* .::

Q09164_A2_leu                            MDQNQQLVPLGVMGELVVTGDGLARGYTNPALDSDRFVDVIARGQLLRAYRTGDRARYRP
Q09164_A3_leu                            MDQNQQITPPGAMGELIVTGDGLARGYTTSSLNTGRFINVDIDGEQVRAYRTGDRVRYRP
Q09164_A8_leu                            MDQKQRLVPPGVMGELVVSGDGLARGYTNSTLNADRFVDIVINDQKARAYRTGDRTRYRP
Q09164_A10_leu                           MDQDQQLVPAGVMGELVVAGDGLARGYTDPSLNTGRFIHITIDGKQVQAYRTGDRVRYRP
                                         ***.*::.* *.****:*:********* .:*::.**:.:   .:  :*******.****

Q09164_A2_leu                            KDGQVEFFGRMDHQVKVRGHRIELAEVEHALLSSAGVHDAVVVSNSQEDNQGVEMVAFIT
Q09164_A3_leu                            KDLQIEFFGRIDHQVKIRGHRIEPAEVEYALLSHDLVTDAAVVTHSQE-NQDLEMVGFVA
Q09164_A8_leu                            KDGSIEFFGRMDQQVKIRGHRVEPAEVEQAMLGNKAIHDAAVVVQAVD-GQETEMIGFVS
Q09164_A10_leu                           RDYQIEFFGRLDQQIKIRGHRIEPAEVEQALLSDSSINDAVVVS-AQN-KEGLEMVGYIT
                                         :* .:*****:*:*:*:****:* **** *:*.   : **.**  : :  :  **:.:::

Q09164_A2_leu                            AQDNETLQE
Q09164_A3_leu                            ARVADVRED
Q09164_A8_leu                            MASDRFSEG
Q09164_A10_leu                           TQAAQSVDK
                                                : 
