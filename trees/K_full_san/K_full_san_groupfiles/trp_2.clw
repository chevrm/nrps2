CLUSTAL W multiple sequence alignment


Q9Z4X6_A3_trp                            PTLIVTTSALAASLPDTGTPVLLLDTPETAATLAALPGHDVTDADRPVPLRPEHPAYMIY
Q8CJX2_A2_11__trp                        PVALLTTAAVAAGLPDTDVPRLLLD--EEPAAGGGEDAADLTDADRLAPLLPGHPAYVIY
                                         *. ::**:*:**.****..* ****  * .*: ..  . *:***** .** * ****:**

Q9Z4X6_A3_trp                            TSGTTGRPKGVVVTHTGLPGLLDIFTRDCAAGPGSRILQHLSPSFDASFWELAMGLLTGA
Q8CJX2_A2_11__trp                        TSGTTGRPKGVTVTHSGLPALLDIFTSQLDVVPGSRVLHHLSPAFDGGFWELAMGLLTGA
                                         ***********.***:***.****** :  . ****:*:****:**..************

Q9Z4X6_A3_trp                            TLVVAPPETTPGPELAELATRHAATHLSLTTSVLGLLPPDSLPDGLTLVVGAEAIPPELV
Q8CJX2_A2_11__trp                        ALVVVEPGTVPGPALAALAVRHRVTHAAITPAVLQLIPEGALPAGTTLVVAAETCPPELV
                                         :***. * *.*** ** **.** .** ::*.:** *:* .:** * ****.**: *****

Q9Z4X6_A3_trp                            ERWSPGRTMLNSYGPTETTVCSTMSGPLSGPAVPPIGSPVANSAVYVLDAALRPVPPGVP
Q8CJX2_A2_11__trp                        ARWSAGRLMRNSYGPTETTVCATMSAPLAGAAVPPIGRPIADTAGYVLDDALQPVPPGVP
                                          ***.** * ***********:***.**:*.****** *:*::* **** **:*******

Q9Z4X6_A3_trp                            GELYAAGAHLARGYHDRRALTAERFVANPFGEPGSRLYRTGDLVRWRPDGQLEYLGRADT
Q8CJX2_A2_11__trp                        GELYVRGPGLARGYLGRPSLTAGRFVACPFGPAGGVMYRTGDLVRHRADGDLEYLGRTDT
                                         ****. *. ***** .* :*** **** *** .*. :******** *.**:******:**

Q9Z4X6_A3_trp                            QVKIRGLRIEPTEIEAVITERPHLARAAVIVREDRPGDRRLVAYVVPEPGATVDTA
Q8CJX2_A2_11__trp                        QVKLRGMRVEPAEIEAVTAGLPGVAQAAVLVREDTPGDRRLVGYVVPDAGASVDPG
                                         ***:**:*:**:***** :  * :*:***:**** *******.****:.**:**..
