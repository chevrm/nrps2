CLUSTAL W multiple sequence alignment


Q9RLP6/5869932_A1_phe                    PVAVVTTAELRSRFDGHDLAVIDIDDPAIASLPATAVSDPRPDDIAYVIYTSGTTGTPKG
Q9RLP6_A1_phe                            PVAVVTTAELRSRFDGHDLAVIDIDDPAIASLPATAVSDPRPDDIAYVIYTSGTTGTPKG
                                         ************************************************************

Q9RLP6/5869932_A1_phe                    VAVTHKNLTHLIAVLEERLPKPGVWPLCHSLAFDASVWEISNALLRGGRLVVVPEAVAGS
Q9RLP6_A1_phe                            VAVTHKNLTHLIAVLEERLPKPGVWPLCHSLAFDASVWEISNALLRGGRLVVVPEAVAGS
                                         ************************************************************

Q9RLP6/5869932_A1_phe                    PEDFHDLLVAEQVTFLTQTPSAVAMLSPDGLESMTLAVVGEACPPALVDRWATNRTMINA
Q9RLP6_A1_phe                            PEDFHDLLVAEQVTFLTQTPSAVAMLSPDGLESMTLAVVGEACPPALVDRWATNRTMINA
                                         ************************************************************

Q9RLP6/5869932_A1_phe                    YGPTETTICVTSSSPLEPGSVVVPIGSALPRTALFVLDPWLRPVPTGVAGELYVAGDGVT
Q9RLP6_A1_phe                            YGPTETTICVTSSSPLEPGSVVVPIGSALPRTALFVLDPWLRPVPTGVAGELYVAGDGVT
                                         ************************************************************

Q9RLP6/5869932_A1_phe                    CGYIGRSGLTASRFVPCPFGEPGARMYRTGDLVRWGRDGQLEYLGRADEQVKIRGYRIEL
Q9RLP6_A1_phe                            CGYIGRSGLTASRFVPCPFGEPGARMYRTGDLVRWGRDGQLEYLGRADEQVKIRGYRIEL
                                         ************************************************************

Q9RLP6/5869932_A1_phe                    GEVQSALAALDGVESAAAIMREDRPGDRRLVGYITGTADPV
Q9RLP6_A1_phe                            GEVQSALAALDGVESAAAIMREDRPGDRRLVGYITGTADPV
                                         *****************************************
