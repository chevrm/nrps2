CLUSTAL W multiple sequence alignment


Q5V8A8_A1_val                            AQTLLTISNSKNLDKIKNIQTTIR---VDVHLKKEDPDSPAIDTQPDDLAHIMYTSGSTG
Q9L8H4_A3_6__val                         -TKLLITDHTTDLD-----TTTTQFNPADTPHDGEDPGNPNHTTHPDDAAYIMYTSGSTG
removed_A2_3__val                        -TKLLITDHTTDLD-----TTTTQFNPADTPHDGEDPGNPNHTTHPDDAAYIMYTSGSTG
                                           .**  .::.:**      ** :   .*.  . ***..*   *:*** *:*********

Q5V8A8_A1_val                            HPKGVAITHRAILEFVADRCWKNELQERVLFHSSLGFDISNYELWVPLLRSGQLVMAPSG
Q9L8H4_A3_6__val                         RPKGVIATHRNITALALDPRFDPTAHRRVLLHSPTAFDASTYEIWVPLLNGNTVVLAPTG
removed_A2_3__val                        RPKGVIATHRNITALALDPRFDPTAHRRVLLHSPTAFDASTYEIWVPLLNGNTVVLAPTG
                                         :****  *** *  :. *  :.   :.***:**. .** *.**:*****... :*:**:*

Q5V8A8_A1_val                            PLDVSTLKQVIQKQSITSLFLTTSLFNLVTEENPRCLAGVQQVWIGGEQASVAAIQRMWD
Q9L8H4_A3_6__val                         DLDVHTYHRVITDQQITALWLTSWVFNLLTEQSPETFTRVRQIWTGGEAVSGATVTRLQQ
removed_A2_3__val                        DLDVHTYHRVITDQQITAVFLTTALFNLLTEHDPACLAGVREVWTGGEAVSAFSVRRVQE
                                          *** * ::** .*.**:::**: :***:**..*  :: *:::* *** .*  :: *: :

Q5V8A8_A1_val                            ACPEITVVNGYGPTETTTYVTLYTIESRPQGNR-VPIGRPMENTQVYVLDEELKPVPSGT
Q9L8H4_A3_6__val                         ACPDTTVVDGYGPTETTTFATHHPVPTPYTGSAVVPIGRPMATMHTYVLDDSLQPVAPGV
removed_A2_3__val                        ACPSVVVVDVYGPTETTTFATHNPVPTPYTGPAVVAIGRPMATMHAYVLDDALQPVAPGV
                                         ***. .**: ********:.*  .: :   *   *.***** . :.****: *:**..*.

Q5V8A8_A1_val                            PGEIYLAGTGLARGYFGQPSLTSTRFLANPFGLPGSRMYRTGDLGVWIDSDQLVCLGRSD
Q9L8H4_A3_6__val                         TGELYLAGSGLARGYLDRPALTAERFVANPYAAPGERMYRTGDLARWNPDDHLEYAGRAD
removed_A2_3__val                        VGELYLGGAGLARGYLDRPALTAERFVANPH-RPGERMYRTGDLARWSADAQLEFVGRAD
                                          **:**.*:******:.:*:**: **:***.  **.********. *  . :*   **:*

Q5V8A8_A1_val                            RQVKIRGIRIEPSEIEAELRNHPEIGEAVVTVREDSPDEKRLIAYLIA---------
Q9L8H4_A3_6__val                         HQVKVRGFRIEPGEIENVLTDHPAVAQAAVHLNRDQPGNPRLVAYVVAD-TSAPSSD
removed_A2_3__val                        QQVKVRGFRIEPGEIENVLTGHPAVAQAAILVREDQPGRPRLVAYVVADGGTAPDG-
                                         :***:**:****.***  * .** :.:*.: :..*.*.. **:**::*         
