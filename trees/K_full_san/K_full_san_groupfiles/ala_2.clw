CLUSTAL W multiple sequence alignment


Q01886_A3_ala                            AKLVLTLPESA--NALATLSGLTKV-IPVSLSELVQQITDNTTKKDEYCKSGDTDP----
Q01886v2_A2_ala                          AELLLCSPATSRMGALQNIS--TQMGTEFKIVEL----------EPEFIRSLPLPPKPNH
Q01886/62867029_A3_ala                   AKLVLTLPESA--NALATLSGLTKV-IPVSLSELVQQITDNTTKKDEYCKSGDTDP----
Q01886v1_A2_ala                          AELLLCSPATSRMGALQNIS--TQMGTEFKIVEL----------EPEFIRSLPLPPKPNH
                                         *:*:*  * ::  .** .:*  *::   ..: **          : *: :*    *    

Q01886_A3_ala                            ------SSPAYLLYTSGTSGKPKGVVMEHRAWSLGFTCHAEYMGFNSC---TRILQFSSL
Q01886v2_A2_ala                          QPMVGLNDDLYVVFTSGSTGVPKGAVATHQAYATGIYEHAVACGMTSLGAPPRSLQFASY
Q01886/62867029_A3_ala                   ------SSPAYLLYTSGTSGKPKGVVMEHRAWSLGFTCHAEYMGFNSC---TRILQFSSL
Q01886v1_A2_ala                          QPMVGLNDDLYVVFTSGSTGVPKGAVATHQAYATGIYEHAVACGMTSLGAPPRSLQFASY
                                               ..  *:::***::* ***.*  *:*:: *:  **   *:.*    .* ***:* 

Q01886_A3_ala                            MFDLSILEIWAVLYAGGCLFIPSDKERVNNLQDFTRINDINTVF--LTPSIGKLLNPKDL
Q01886v2_A2_ala                          SFDASIGDIFTTLAVGGCLCIPREEDR-NPAGITTFINRYGVTWAGITPSLALHLDPDAV
Q01886/62867029_A3_ala                   MFDLSILEIWAVLYAGGCLFIPSDKERVNNLQDFTRINDINTVF--LTPSIGKLLNPKDL
Q01886v1_A2_ala                          SFDASIGDIFTTLAVGGCLCIPREEDR-NPAGITTFINRYGVTWAGITPSLALHLDPDAV
                                          ** ** :*::.* .**** ** :::* *     * **  ...:  :***:.  *:*. :

Q01886_A3_ala                            PNISFAGFIGEPMTRSLIDAWTLPGRR--LVNSYGPTEACVLVTAREISPTAPHDKPSSN
Q01886v2_A2_ala                          PTLKALCVAGEPLSMSVVTVWS---KRLNLINMYGPTEATVACIANQVTCTT---TTVSD
Q01886/62867029_A3_ala                   PNISFAGFIGEPMTRSLIDAWTLPGRR--LVNSYGPTEACVLVTAREISPTAPHDKPSSN
Q01886v1_A2_ala                          PTLKALCVAGEPLSMSVVTVWS---KRLNLINMYGPTEATVACIANQVTCTT---TTVSD
                                         *.:.   . ***:: *:: .*:   :*  *:* ****** *   *.::: *:   .. *:

Q01886_A3_ala                            IGHALGANIWVVEP-QRTALVPIGAVGELCIEAPSLARCYLANPERTEYSF---PSTVLD
Q01886v2_A2_ala                          IGRGYRATTWVVQPDNHNSLVPIGAVGELIIEGSILCRGYLNDPERTAEVFIRSPSWLHD
Q01886/62867029_A3_ala                   IGHALGANIWVVEP-QRTALVPIGAVGELCIEAPSLARCYLANPERTEYSF---PSTVLD
Q01886v1_A2_ala                          IGRGYRATTWVVQPDNHNSLVPIGAVGELIIEGSILCRGYLNDPERTAEVFIRSPSWLHD
                                         **:.  *. ***:* ::.:********** **.. *.* ** :****   *   ** : *

Q01886_A3_ala                            NWQTKKGTRVYRTGDLVRYASDGTLDFLGRKDGQIKLRGQRIELGEIEHHIRRLMSDDPR
Q01886v2_A2_ala                          ---LRPNSTLYKTGDLVRYSADGKIIFIGRKDTQVKMNGQRFELGEVEHALQ--------
Q01886/62867029_A3_ala                   NWQTKKGTRVYRTGDLVRYASDGTLDFLGRKDGQIKLRGQRIELGEIEHHIRRLMSDDPR
Q01886v1_A2_ala                          ---LRPNSTLYKTGDLVRYSADGKIIFIGRKDTQVKMNGQRFELGEVEHALQ--------
                                             : .: :*:*******::**.: *:**** *:*:.***:****:** ::        

Q01886_A3_ala                            FHEASVQLYNPATDP-DRDATVDV----QMREPYLAGLLVLDLVS--------
Q01886v2_A2_ala                          -----LQL-----DPSDGPIIVDLLKRTQSGEP---DLLIAFLFVGRANTGTG
Q01886/62867029_A3_ala                   FHEASVQLYNPATDP-DRDATVDV----QMREPYLAGLLVLDLVFTDEVMGIP
Q01886v1_A2_ala                          -----LQL-----DPSDGPIIVDLLKRTQSGEP---DLLIAFLFVGRANTGTG
                                              :**     ** *    **:    *  **   .**:  *.         
