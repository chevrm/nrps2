CLUSTAL W multiple sequence alignment


Q939Z1_A1_leu                            AKLVVCSAASRGAVPAGVESLEPAAAAEEGASDAPAATVRPGDPAYVMYTSGSTGTPKGV
O52819_A1_leu                            VSLTVCSAATRDGVPEGIESI---VITDEDASDTSVATVRPGDLAYVMYTSGSTGTPKGV
                                         ..*.*****:*..** *:**:   . ::*.***:..******* ****************

Q939Z1_A1_leu                            TISQGCVAELTMDAGWAMEPGEAVLMHSPHAFDASLFELWMPLASGVRVVLAEPGSVDAR
O52819_A1_leu                            AITHGTIAELAEDPGWVMEPGEAVLMHSPHTFDASLFEVWTPLSLGARVVIAEPGSVDVR
                                         :*::* :***: *.**.*************:*******:* **: *.***:*******.*

Q939Z1_A1_leu                            RLREAAAAGVTRVYLTAGSLRAVAEEAPESFAEFREVLTGGDVVPAHAVERVRTAAPRAR
O52819_A1_leu                            RLREAAAAGVTRVYLTAGSFRAVAEESPESFAAFREVLTGGDVVPAHAVERVREACPGAR
                                         *******************:******:***** ******************** *.* **

Q939Z1_A1_leu                            FRNMYGPTEATMCATWHLLQPGDVVGPVVPIGRPLTGRRVQVLDASLRPVGPGVVGDLYL
O52819_A1_leu                            VRNMYGPTEATMCATWHLLQPGDVMGPVMPIGRPLAGRRIQVLDESLRPVEPGVVGDLYL
                                         .***********************:***:******:***:**** ***** *********

Q939Z1_A1_leu                            SGALAEGYFNRAALTAERFVADPSAPGQRMYWTGDLAQWTADGELVFAGRADDQVKIRGF
O52819_A1_leu                            SGGLAEGYFNRAGLTAERFVADPSAPGQRMYWTGDLAQWTADGELLFAGRADHQVKVRGF
                                         **.*********.********************************:******.***:***

Q939Z1_A1_leu                            RIEPGEIEAALIAQPDVHDAVVAAVDGRLIGYVVTEGDADPR
O52819_A1_leu                            RIEPGEIEAALIALPDVQDAVVAAIDGRLVGYVVADGDVDPA
                                         ************* ***:******:****:****::**.** 
