CLUSTAL W multiple sequence alignment


O52819_A3_asn                            PALVIT-TAALSASPVGEVLAARSTTMVIDDPSAAGEVAGRDRAPVTDTDRTRRLDPRHP
Q45R83_A4_11__asn                        PVLVLAHTATQDALPEGAGPVVR-----LDAPAIEAALAGLDGGDCTDADRRAPATHHDP
Q939Z1_A3_asn                            PALVIT-TAVLSASPIGDVLAARSRTVVLDEPAAAGQLAGRDRAPVTDTDRARALDPRHP
                                         *.**:: **. .* * *   ..*     :* *:  . :** * .  **:**      :.*

O52819_A3_asn                            AYLIYTSGSTGRPKAVVITHRNLTNYLFHCGRMYPGLRGRSVMHSSIAFDLTITAMFTPL
Q45R83_A4_11__asn                        AYVVYTSGSTGTPKGVVVEQRSLAAFLVRSAARYRGAAGTALLHGSPAFDLTVTTLFTPL
Q939Z1_A3_asn                            AYLIYTSGSTGRPKAVVVTHRNLTNYLLHCGRMYPGLRGRSVLHSSIAFDLTVTATFTPL
                                         **::******* **.**: :*.*: :*.:..  * *  * :::*.* *****:*: ****

O52819_A3_asn                            TVGGTVHVGALEAVIGAVDSAPSIFLKATPSHLRTLDTGSRESAVSGDLLLGGEQLPVDT
Q45R83_A4_11__asn                        IAGGCIVVADLDAPERDAPARPDL-LKVTPSHLALLDTIASWATPAADLVVGGEQLTASR
Q939Z1_A3_asn                            IVGGEIHVGALEDLIGVVEAAPSIFLKATPSHLLTLDTASRGSAGSGDLLLGGEQLPADT
                                          .** : *. *:     . : *.: **.*****  *** :  :: :.**::*****... 

O52819_A3_asn                            IVQWRRTYPNTVVVNEYGPTEATVGCVEYRLEPGQECPPGGVVPIGTPLTNMRAFVLDSW
Q45R83_A4_11__asn                        LARLRRAHPDMRVFNDYGPTEATVSCADFVLEPG-DAPPTDTVPIGRPLAGHRLFVLDDR
Q939Z1_A3_asn                            VVQWRRKYPNIVVVNEYGPTEATVGCVEYRLEPGQECPPGGVVPIGTPLANMRAFVLDSW
                                         :.: ** :*:  *.*:********.*.:: **** :.** ..**** **:. * ****. 

O52819_A3_asn                            LRLVPPGAVGELYVSGVGVARGYLGRAGLTASRFVADPFGS-GERMYRTGDLVRWNPDGQ
Q45R83_A4_11__asn                        LRPVPANVPGELYVSGVGVARGYLGRPGMTAERFVACPFGEPGERMYRTGDLARRRADGN
Q939Z1_A3_asn                            LRLVPPGAVGELYVAGAGLARGYLGRAGLTATRFVADPFGS-GERMYRTGDLVQWNPDGQ
                                         ** **... *****:*.*:*******.*:** **** ***. **********.: ..**:

O52819_A3_asn                            LVFAGRVDDQVKVRGFRIEPGEIEAALVAQESVGQAVVVAHDSDVGKRLIAYVTAAGQTG
Q45R83_A4_11__asn                        LEYLGRRDGQVKVRGFRVETGEIETALLDRPEIGQAAVVLR----GERLLAYVAAPPERF
Q939Z1_A3_asn                            LVFAGRVDDQVKVRGFRIEPGEIEAALVAQESVGQAVVVARDSEIGTRLIGYVTAAGESG
                                         * : ** *.********:*.****:**: : .:***.** :    * **:.**:*. :  

O52819_A3_asn                            VDT
Q45R83_A4_11__asn                        ---
Q939Z1_A3_asn                            VDE
                                            
