CLUSTAL W multiple sequence alignment


Q9FB27_A1_asn                            TRTLDDAGVTPVALDADRRRIA---------AHPAGPTGIATTPDAPAYVVYTSGTTGKP
Q9FB23_A2_asn                            ARLL----VLPAGLDTPLRACGLPVVAPDDLGAPIAP--VSVHPEQLAAVMATSGSTGTP
                                         :* *    * *..**:  *  .         . * .*  ::. *:  * *: ***:**.*

Q9FB27_A1_asn                            NGVRVPHRGLTNYLTWCTGAYGLDGGTGTLVHTSISFDLTLTTLFGPLLAGGQVVMLSET
Q9FB23_A2_asn                            KTIGVPQRALAGYLRWAIGHYRLDEETVSPVHSSLGFDLTVTALLAPLAAGGQ-ARLTDS
                                         : : **:*.*:.** *. * * **  * : **:*:.****:*:*:.** **** . *:::

Q9FB27_A1_asn                            AGVTGLIAALRSRRDLTLVKLTPTHL-DVVNQLLTPDELRGAVRTLVVGGEAVRAESLEP
Q9FB23_A2_asn                            GDPGALGAALAAGHH-TLLKITPAHLAALAHQLGAP----TALRTVVAGGEPLHAGHVRA
                                         ..  .* *** : :. **:*:**:**  :.:** :*     *:**:*.***.::*  :..

Q9FB27_A1_asn                            FR--ASGTRVVNEYGPSETVVGSVAHVVDAATPRTGPVPIGRPIANTTVHLLDQRRRPVP
Q9FB23_A2_asn                            LRAFAPGARLVNEYGPTETTVGCCAHDV-APDPGEAPIPVGTPIAGLSACVVDD-ALPAP
                                         :*  *.*:*:******:**.**. ** * *. *  .*:*:* ***. :. ::*:   *.*

Q9FB27_A1_asn                            DGVVGELWIGGAGVADGYLGRPELTGERFLPSDYPPDGGRVYRTGDLARRRADGTLEYLG
Q9FB23_A2_asn                            PGVRGELYIGGTGVTRGYLGRPAATAAAYVPDPAAP-GARRYRTGDLARRLPDGTLLLAG
                                          ** ***:***:**: ******  *.  ::*.  .* *.* ********* .****   *

Q9FB27_A1_asn                            RTDAQVKIRGVRVEPAETEAVLASHPGVGQAVVVARLDEDPGRSSPLAGELTLTGYVVPA
Q9FB23_A2_asn                            RADRQVKIRGHRVEPGEVEQVLGGHPGVREAAVVAHPAPGGGRR--------LVAYWVPA
                                         *:* ****** ****.*.* **..**** :*.***:   . **         *..* ***

Q9FB27_A1_asn                            RGAQAPPH
Q9FB23_A2_asn                            EPARPPSA
                                         . *:.*. 
