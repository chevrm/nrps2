CLUSTAL W multiple sequence alignment


Q93N89_A2_2__trp                         PAAVLCTAQTRPMVPEGFTGLVVTLDEDLPEGDFDDGRVLTAPDADHIAYVIHTSGSTGT
Q93N89_A2_trp                            PAAVLCTAQTRPMVPEGFTGLVVTLDEDLPEGDFDDGRVLTAPDADHIAYVIHTSGSTGT
                                         ************************************************************

Q93N89_A2_2__trp                         PKGAAVTWGGLRNLVADRIERYGIGTDTRLVQLVSPSFDVSMADIWPTLCAGGRLVLAPP
Q93N89_A2_trp                            PKGAAVTWGGLRNLVADRIERYGIGTDTRLVQLVSPSFDVSMADIWPTLCAGGRLVLAPP
                                         ************************************************************

Q93N89_A2_2__trp                         GGHTTGDELGDLLADHRITHAVMPAVQLTHLPDRELPALRVLVSGGDALPADTRRRWVAR
Q93N89_A2_trp                            GGHTTGDELGDLLADHRITHAVMPAVQLTHLPDRELPALRVLVSGGDALPADTRRRWVAR
                                         ************************************************************

Q93N89_A2_2__trp                         CDLHNEYGVTEATVVSTVTAPLDDAGPLTIGGPIARAGVHVLDGFLRPVPPGVTGELYVT
Q93N89_A2_trp                            CDLHNEYGVTEATVVSTVTAPLDDAGPLTIGGPIARAGVHVLDGFLRPVPPGVTGELYVT
                                         ************************************************************

Q93N89_A2_2__trp                         GTGVARGYLNRPTLTAHRFIADPRTRGGRMYRTGDLVRHTHGGELVFVGRADEQVKLRGH
Q93N89_A2_trp                            GTGVARGYLNRPTLTAHRFIADPRTRGGRMYRTGDLVRHTHGGELVFVGRADEQVKLRGH
                                         ************************************************************

Q93N89_A2_2__trp                         RIELGEVEAALADHPDVEQAVAAIHDARLIGYVVPADGRVPDPS
Q93N89_A2_trp                            RIELGEVEAALADHPDVEQAVAAIHDARLIGYVVPADGRVPDPS
                                         ********************************************
