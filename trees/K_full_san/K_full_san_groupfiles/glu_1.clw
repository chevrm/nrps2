CLUSTAL W multiple sequence alignment


P27206_A1_glu                            ADFYLTESKVAAPEADAELIDLDQAIE-EGAEESLNADVNARNLAYIIYTSGTTGRPKGV
Q70KJ7_A1_glu                            AKAVLTEAGIQAPAADAERIDFDEAVQYETAADGVS--TQSDRLAYIIYTSGTTGRPKGV
                                         *.  ***: : ** **** **:*:*:: * * :.:.  .:: .*****************

P27206_A1_glu                            MIEHRQVHHLVESLQQTIYQSPTQTLPMAFLPPFHFDASVKQIFASLLLGQTLYIVPKKT
Q70KJ7_A1_glu                            MIEHRQVHHLVQSLQQEIYQCGEQTLRMALLAPFHFDASVKQIFASLLLGQTLYIVPKTT
                                         ***********:**** ***.  *** **:*.**************************.*

P27206_A1_glu                            VTNGAALTAYYRKNSIEATDGTPAHLQMLAAAGDFEGLKLKHMLIGGEGLSSVVADKLLK
Q70KJ7_A1_glu                            VTNGSALLDYYRQNRIEATDGTPAHLQMMVAAGDVSGIELRHMLIGGEGLSAAVAEQLMA
                                         ****:**  ***:* *************:.****..*::*:**********:.**::*: 

P27206_A1_glu                            LFKEAGTAPRLTNVYGPTETCVDASVHPVIPENAV--QSAYVPIGKALGNNRLYILDQKG
Q70KJ7_A1_glu                            LFHQSGRTPRLTNVYGPTETCVDASVHQVSADNGMNQQAAYVPIGKPLGNARLYILDKHQ
                                         **:::* :******************* * .:*.:  *:*******.*** ******:: 

P27206_A1_glu                            RLQPEGVAGELYIAGDGVGRGYLHLPELTEEKFLQDPFVPGDRMYRTGDVVRWLPDGTIE
Q70KJ7_A1_glu                            RLQPDGTAGELYIAGDGVGRGYLNLPDLTAEKFLQDPFNGSGRMYRTGDMARWLPDGTIE
                                         ****:*.****************:**:** ********  ..*******:.*********

P27206_A1_glu                            YLGREDDQVKVRGYRIELGEIEAVIQQAPDVAKAVVLARPDEQGNLEVCAYVVQKPGSEF
Q70KJ7_A1_glu                            YIGREDDQVKVRGYRIELGEIETVLRKAPGAAQAVVLARPDQQGSLDVCAYIVQEKGTEF
                                         *:********************:*:::**..*:********:**.*:****:**: *:**

P27206_A1_glu                            AP
Q70KJ7_A1_glu                            HP
                                          *
