CLUSTAL W multiple sequence alignment


Q884E4_A1_thr                            IELLLTQTQLLGQLPIPAHVQTLDLADALDGYSTENPINQTSPDNLAYVIYTSGSTGKPK
Q884E5_A2_thr                            IELLLSQTQLLGQLPIPAHVQTLDLADALDGYSTENPINQTSPDNLAYVIYTSGSTGKPK
                                         *****:******************************************************

Q884E4_A1_thr                            GTLLAHHNLMRLFAATDDWFKFNEKDVWTLFHSFAFDFSVWEIFGALLHGGRLVIVPREV
Q884E5_A2_thr                            GTLLAHHNLMRLFAATDDWFKFNEKDVWTLFHSFAFDFSVWEIFGALLHGGRLVIVPCEV
                                         ********************************************************* **

Q884E4_A1_thr                            TRSPEEFHALLVDQQVTVLNQTPSAFKQLMRVACDSPVPMSLQKVIFGGEALDVASLKPW
Q884E5_A2_thr                            TRSPEEFHALLVDQQVTVLNQTPSAFKQLMRVACDSTSVLSLETIIFGGEALDVASLKPW
                                         ************************************.  :**:.:***************

Q884E4_A1_thr                            FARFGDQMPRLINMYGITETTVHVTYRPITLADTHNPASPIGEAIADLSWYVLDADFNPV
Q884E5_A2_thr                            FARFGDQTPRLINMYGITETTVHVTYRPITLADTHNPASPIGEAIADLSWYVLDADFNTV
                                         ******* **************************************************.*

Q884E4_A1_thr                            AQGCSGELHIGHAGLARGYHHRAALTAERFVPDPFSNDGGRLYRTGDLARYKTAGTIEYA
Q884E5_A2_thr                            AQGCSGELHIGHAGLARGYHNRAALTAERFVPDPFANDGGRLYRTGDLARYKTAGTIEYA
                                         ********************:**************:************************

Q884E4_A1_thr                            GRIDHQVKIRGFRIELGEIEARLQAHSAVREVIVLAVDGQLAAYLLPAQPDQDQQA
Q884E5_A2_thr                            GRIDHQVKIRGFRIELGEIEARLQAHSAVREVIVLAVDGQLAAYLLPAQPDQDQQA
                                         ********************************************************
