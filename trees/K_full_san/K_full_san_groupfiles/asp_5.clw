CLUSTAL W multiple sequence alignment


Q884E3_A1_asp                            AALLIHASGDDKAAQLGV--CPVLAFDAALWSEVDGGELSVRIIAEQPAYIIYTSGSTGQ
Q884E5_A1_asp                            TKVLLCAEGDRRETSLNVAGCQGLAWTPALWQSLPTSRPDIELPADSAAYVIHTSGSTGQ
                                         : :*: *.** : :.*.*  *  **: .***..:  .. .:.: *:..**:*:*******

Q884E3_A1_asp                            PKGVVISHGALANYVQGVLARLSLNDGASMAMVSTVAADLGHTLLFGALASGRPLHLLSH
Q884E5_A1_asp                            PKGVVVSQGALASYVRGLLEQLQLAPEVSMALVSTIAADLGHTVLFGALCSGRTLHVLTE
                                         *****:*:****.**:*:* :*.*   .***:***:*******:*****.***.**:*:.

Q884E3_A1_asp                            EQAFDPDGFARYMAEHQVEVLKIVPSHLQGLLQAAHPADVLPSQLLMLGGEASSWALIEQ
Q884E5_A1_asp                            SLGFDPDAFATYMAEHQIGVLKIVPGHLAALLQAAQPADVLPQHALIVGGEACSPALVEQ
                                         . .****.** ******: ******.** .*****:******.: *::****.* **:**

Q884E3_A1_asp                            VRALKPGCRIVNHYGPTETTVGILTHEV-----------AERLNAC--------RSVPVG
Q884E5_A1_asp                            VRQLKPGCRVINHYGPSETTVGVLTHEVPALSELNAIPQVSRFPQCSAHSGDPLRSVPVG
                                         ** ******::*****:*****:*****           ..*:  *        ******

Q884E3_A1_asp                            QPLANCKARVLDAYLNPVAERVSGELYLGGQGLAQGYLGRAAMTAERFVPDPDA-DGQRL
Q884E5_A1_asp                            APLPGASAYVLDDVLNPVATQVAGELYIGGDSVALGYLGQPALTAERFVPDPFAQDGARV
                                          **....* ***  ***** :*:****:**:.:* ****:.*:********* * ** *:

Q884E3_A1_asp                            YRAGDRARWV-DGVLEYLGRADDQVKIRGYRVEPGEVGQVLQTLENVAEAVVLAQPLESD
Q884E5_A1_asp                            YRSGDRMRHNHQGLLEFIGRADDQVKVRGYRVEPAEVAQVLLSLPSVAQVSVLALPVDED
                                         **:*** *   :*:**::********:*******.**.*** :* .**:. *** *::.*

Q884E3_A1_asp                            ETRLQLVAYCVAAVGVRLNVES
Q884E5_A1_asp                            ESRLQLVAYCVAAAAASLTVDS
                                         *:***********... *.*:*
