CLUSTAL W multiple sequence alignment


Q45R85_A4_thr                            PVAVLSTTAVRETLHGTVGEAVGEVPWLLLDEPATGGATAGHSAAPVTDADRRSPLLPDH
O87314_A2_thr                            PVCLLTDTAERFT----------GVPHVILAEAAQNPA---RPQAPTVS--------PDH
                                         **.:*: ** * *           ** ::* *.* . *   :. **...        ***

Q45R85_A4_thr                            PAYTIYTSGSTGRPKGVVVSHANVSRLLTACRAAV-DFGPDDVWTLFHSSAFDFSVWEMW
O87314_A2_thr                            AAYVIYTSGSTGVPKGVEVTHRNVAALFAGTTSGLYDFGPDDVWTMFHSAAFDFSVWELW
                                         .**.******** **** *:* **: *::.  :.: *********:***:********:*

Q45R85_A4_thr                            GPLAHGGRLVVVPHDVARSPGDLLDLLGRERVTVLSQTPSAFLQLLRAESDLGVPPRTTA
O87314_A2_thr                            GPLLHGGRLVVVEHDVARDPERFVDLLARERVTVLNQTPSAFYPLLEADARL----RRQL
                                         *** ******** *****.*  ::***.*******.******  **.*:: *    *   

Q45R85_A4_thr                            ALRYVVFGGEALDTAQLAPW----RGRPVRLVNMYGITETTVHVTHLELDDAAVDRGGSP
O87314_A2_thr                            ALRYVIFGGEALDVRRLAPWYANHESHSPRLVNMYGITETCVHVSHRALDTADTGAAGSV
                                         *****:*******. :****    ..:. *********** ***:*  ** * .. .** 

Q45R85_A4_thr                            IGTPLNDLRAHVLDQGLLPVPVGVVGELYVAGPGLARGYRRRPGLSATRFVADPFDTGG-
O87314_A2_thr                            IGGPLPGLRIHLLDNNLQPVPAGVVGEMYIAGGQVARGYTGRPGLTATRFVANPFDGAGE
                                         ** ** .** *:**:.* ***.*****:*:**  :****  ****:******:*** .* 

Q45R85_A4_thr                            RMYRTGDLVRRTQDGGLHYVGRSDSQVKLRGYRIEPGEIEAAARRHPDVAQAATAVHGEG
O87314_A2_thr                            RLYRSGDLAMWTDAGELVYLGRSDAQVKVRGYRIELGEVEAALVTLPGVTNAAADVRHDD
                                         *:**:***.  *: * * *:****:***:****** **:***    *.*::**: *: :.

Q45R85_A4_thr                            PQDRYLVCYVV-PAADTDP
O87314_A2_thr                            TGRARLIGYVVGDALDIGA
                                         .    *: ***  * * ..
