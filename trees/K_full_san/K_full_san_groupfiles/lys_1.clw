CLUSTAL W multiple sequence alignment


Q45R83_A1_8__lys                         APLLVLAERATEAAVADLAAPVLVLDDPSTEAAIDALDPGPVTDADRTAPLLPGHAAYVI
O05819_A1_lys                            -------------------APV-VIDEGVFAASVGA----DILEEDRAITVPVDQAAYVI
                                                            *** *:*:    *::.*     : : **: .:  .:*****

Q45R83_A1_8__lys                         HTSGSTGRPKGVTVDHRGLSRLLQAH-RRVTFSRIRPSA---GGPGRAAHVSSFSFDASW
O05819_A1_lys                            FTSGTTGTPKGVIGTHRALSAYADDHIERV----LRPAAQRLGRPLRIAHAWSFTFDAAW
                                         .***:** ****   **.**   : * .**    :**:*   * * * **. **:***:*

Q45R83_A1_8__lys                         DPLLAMVAGHELHMIDEDLRFDPPGVVAYFRDRRIDYVDLTPTYFRSLLDAGLLEEGFPC
O05819_A1_lys                            QPLVALLDGHAVHIVDDHRQRDAGALVEAIDRFGLDMIDTTPSMFAQLHNAGLLDRA---
                                         :**:*:: ** :*::*:. : *. .:*  :    :* :* **: * .* :****:..   

Q45R83_A1_8__lys                         P-SLVALGGEAMDGELWERLRAAAPR--VTAMNTYGPTETAVDAVVTVLGDLPPGTIGRP
O05819_A1_lys                            PLAVLALGGEALGAATWRMIQQNCARTAMTAFNCYGPTETTVEAVVAAVAEHARPVIGRP
                                         * :::******:..  *. ::  ..*  :**:* ******:*:***:.:.: .  .****

Q45R83_A1_8__lys                         VPRWRAYVLDAGLRPVPPGVLGELYLAGPGVARGYLGQHALTAERFVACPFGKPGERMYR
O05819_A1_lys                            TCTTRAYVMDSWLRPVPDGVAGELYLAGAQLTRGYLGRPAETAARFVAEPNGR-GSRMYR
                                         .   ****:*: ***** ** *******. ::*****: * ** **** * *: *.****

Q45R83_A1_8__lys                         TGDLARWLPDGHLVYVGRGDEQVKIRGFRIEPGEVEAALRELEGVAAAAVTVREDTPGTR
O05819_A1_lys                            TGDVVRRLPDGGLEFLGRSDDQVKIRGFRVEPGEIAAVLNGHHAVHGCHVTARGHASGP-
                                         ***:.* **** * ::**.*:********:****: *.*.  ..* .. **.* .:.*. 

Q45R83_A1_8__lys                         RLVGYVVGTPDADDARLRPAE
O05819_A1_lys                            RLTAYVAGGPQP------PP-
                                         **..**.* *:.      *. 
