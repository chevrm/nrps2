CLUSTAL W multiple sequence alignment


Q83VS0_A6_16__ser                        QAVLTTRDLSDNLPASDLPVLVLDGHDDRAQLARQQSVNPDAKALGLQPNHLAYVLYTSG
O85168_A1_ser                            QAVLTTRDLSDNLPASDLPVLVLDGHNDRAQLARQQSVNPDAKALGLQPNHLAYVLYTSG
O85168_A2_ser                            RAVLTSHELLADLPDLGVPALVLDGRDDSALLKKQPTGNPDAKALDLQPNHLAYVLYTSG
                                         :****:::*  :**  .:*.*****::* * * :* : *******.**************

Q83VS0_A6_16__ser                        STGTPKGVMNEHLGVVNRLLWARDAYQVNSQDRVLQKTPFGFDVSVWEFFLPLLTGAELV
O85168_A1_ser                            STGTPKGVMNEHLGVVNRLLWARDAYQVNSQDRVLQKTPCGFDLSVWEFFLPLLTGAELV
O85168_A2_ser                            STGTPKGVMNEHLGVVNRLLWARDAYQVNSQDRVLQKTPFGFDVSVWEFFLPLLTGAELV
                                         *************************************** ***:****************

Q83VS0_A6_16__ser                        MARPGGHQDPDYLAQVMSDAGITLLHFVPSMLDVFLEHRSTRDFPQLRRVLCSGEALPRA
O85168_A1_ser                            MAPPGGHQDPDYLAQVMSDAGITLLHFVPSMLDVFLEHRSTRDFPQLRRVLCSGEALPRA
O85168_A2_ser                            MARPSGHQDPDYLAQVISDAGITLLHFVPSMLDVFLEHRSTRDFPQLRRVLCSGEALPRA
                                         ** *.***********:*******************************************

Q83VS0_A6_16__ser                        LQRRFEQQLKGVELHNLYGPTEAAIDVTAWECRPTDPGDSVPIGRPIANIQMHVLDALGQ
O85168_A1_ser                            LQRRFEQQLKGVELHNLYGPTEAAIDVTAWECRPTDPGDSVPIGRPIANIQMHVLDALGQ
O85168_A2_ser                            LQRRFEQHLKGVELHNLYGPTEAAIDVTAWECRPTDPGDSVPIGRPIANIQMHVLDALGQ
                                         *******:****************************************************

Q83VS0_A6_16__ser                        LQPLGVAGELHIGGIGVARGYLNQPDLSAERFIADPFSEDRQARLYKTGDVGRWLANGAL
O85168_A1_ser                            LQPMGVAGELHIGGIGVARGYLNQPQLSAERFIADPFSNDPQARLYKTGDVGRWLANGAL
O85168_A2_ser                            LQPMGVAGELHIGGIGVARGYLNQPQLSAERFIADPFSNDPQARLYKTGDVGRWLANGAL
                                         ***:*********************:************:* *******************

Q83VS0_A6_16__ser                        EYLGRNDFQVKIRGLRIEIGEIEAALAKHPAVHEAVVTAREDIPGDKRLVAYYTQSAEHT
O85168_A1_ser                            EYLGRNDFQVKIRGLRIEIGEIEAALAKHPAVHEAVVTAREDIPGDKRLVAYYTQSAEHT
O85168_A2_ser                            EYLGRNDFQVKIRGLRIEIGEIEAALAKHPAVHEAVVTAREDIPGDKRLVAYYTQTAEHT
                                         *******************************************************:****

Q83VS0_A6_16__ser                        AV
O85168_A1_ser                            AV
O85168_A2_ser                            AV
                                         **
