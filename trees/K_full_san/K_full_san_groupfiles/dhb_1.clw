CLUSTAL W multiple sequence alignment


Q9FDB2_A_dhb                             AKAYLIDGAQRPFDYQALAQELLACCPTLQTVIVRGQTRVTDPKFIELASCYSASSCQAN
Q9F638/11127897_A_dhb                    AVAYVIPDKHSGFDYRTLAEKVRSAVPGLRHVVVVGEA----GPFTSLSQLYASPVDLPG
                                         * **:* . :  ***::**::: :. * *: *:* *::      * .*:. *::.   ..

Q9FDB2_A_dhb                             ADPNQIAFFQLSGGTTDTPKLIPRTHNDYAYSVTASVEICRFDQHTRYLCVLPAAHNFPL
Q9F638/11127897_A_dhb                    PIPSDVAFFQLSGGSTGVPKLIPRTHDDYIYSLRGSVEICQLDETTVYLCALPAAHNFPL
                                         . *.::********:*..********:** **: .*****::*: * ***.*********

Q9FDB2_A_dhb                             SSPGALGVFWAGGCVVLSQDASPQHAFKLIEQHKITVTALVPPLALLWMDHAEKSTYDLS
Q9F638/11127897_A_dhb                    SSPGVLGTLYAGGTAVMALHPSPDQAFPLIERERITFTALVPPLAMIWMDAAKARRHDLS
                                         ****.**.::*** .*:: ..**::** ***:.:**.********::*** *:   :***

Q9FDB2_A_dhb                             SLHFVQVGGAKFSEAAARRLPKALGCQLQQVFGMAEGLVNYTRLDDSAELIATTQGRPIS
Q9F638/11127897_A_dhb                    SLKVLQVGGARLSTEAAQRVRPTLGCALQQVYGMAEGLVNYTRLDDPDELVIATQGRPIS
                                         **:.:*****::*  **:*:  :*** ****:**************. **: :*******

Q9FDB2_A_dhb                             AHDQLLVVDEQGQPVASGEEGYLLTQGPYTIRGYYRADQHNQRAFNAQGFYITGDKVKLS
Q9F638/11127897_A_dhb                    PDDEIRIIDEDGRDVAPGETGQLLTRGPYTIRGYYNAEAHNARAFTSDGFYCTGDLVRVT
                                         ..*:: ::**:*: **.** * ***:*********.*: ** ***.::*** *** *:::

Q9FDB2_A_dhb                             SEGYVIVTGRAKDQINRGGEKIAAEEVENQLLHHPAVHDAALIAISDEYLGERSCAVIVL
Q9F638/11127897_A_dhb                    PEGYLVVEGRAKDQINRGGDKIAAEEVENHLLAHPSVHDAAVISIPDPFLGERTCAFVI-
                                         .***::* ***********:*********:** **:*****:*:*.* :****:**.:: 

Q9FDB2_A_dhb                             KPEQSVNTI--
Q9F638/11127897_A_dhb                    -PREAPPTAAT
                                          *.::  *   
