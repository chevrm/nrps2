#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

open my $list, '<', '../modextract/setlist.tsv' or die $!;
my %toget = ();
while(<$list>){
	if($_ =~ m/^K/){
		chomp;
		my ($set, $up, $gid) = split("\t", $_);
		$toget{$gid} = 1;
	}
}
close $list;
open my $kout, '>', './k_adom.faa' or die $!;
my $fa = new Bio::SeqIO(-file=>"../modextract/adom.faa", -format=>'fasta');
while(my $seq = $fa->next_seq){
	my @s = split(/_/, $seq->id);
	my $si = join('_', @s[0..$#s-3]);
	if(exists $toget{$si}){
		print $kout '>' . $seq->id . "\n" . $seq->seq . "\n";
	}
}
close $kout;
