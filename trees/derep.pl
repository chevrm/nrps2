#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

my $fa = new Bio::SeqIO(-file=>shift, -format=>'fasta');
my %seen = ();
while(my $seq = $fa->next_seq){
	print '>' . $seq->id . "\n" . $seq->seq . "\n" unless(exists $seen{$seq->id});;
	$seen{$seq->id} += 1;
}
