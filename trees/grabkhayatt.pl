#!/bin/env perl

use strict;
use warnings;

my $adir = './Adomains';
my $base = 'http://www.cmbi.ru.nl/bamics/supplementary/Khayattetal_2012_NRPSPKS/HMMs/';
my $Aad = $base . 'Adomains/';

system("mkdir $adir") unless(-d $adir);

chdir $adir or die $!;
my @ahmm = (    'Khayatt_ref_aad_1.hmm',        'Khayatt_ref_ala_1.hmm',
                'Khayatt_ref_ala_2.hmm',        'Khayatt_ref_ala_3.hmm',
                'Khayatt_ref_alabeta_1.hmm',    'Khayatt_ref_arg_1.hmm',
                'Khayatt_ref_asn_1.hmm',        'Khayatt_ref_asn_2.hmm',
                'Khayatt_ref_asp_1.hmm',        'Khayatt_ref_aspme_1.hmm',
                'Khayatt_ref_bh_1.hmm',         'Khayatt_ref_bm_1.hmm',
                'Khayatt_ref_cys_1.hmm',        'Khayatt_ref_cys_2.hmm',
                'Khayatt_ref_dab_1.hmm',        'Khayatt_ref_dhab_dh_1.hmm',
                'Khayatt_ref_dhb_sal_1.hmm',    'Khayatt_ref_dhpg_dpg_1.hmm',
                'Khayatt_ref_gln_1.hmm',        'Khayatt_ref_glu_1.hmm',
                'Khayatt_ref_gly_1.hmm',        'Khayatt_ref_gly_2.hmm',
                'Khayatt_ref_his_1.hmm',        'Khayatt_ref_hpg_hpg2Cl_1.hmm',
                'Khayatt_ref_hyv_1.hmm',        'Khayatt_ref_ile_1.hmm',
                'Khayatt_ref_iva_abu_1.hmm',    'Khayatt_ref_leu_1.hmm',
                'Khayatt_ref_leu_2.hmm',        'Khayatt_ref_leu_3.hmm',
                'Khayatt_ref_lys_1.hmm',        'Khayatt_ref_lysbeta_1.hmm',
                'Khayatt_ref_orn_1.hmm',        'Khayatt_ref_ornfn5h_1.hmm',
                'Khayatt_ref_ornha_1.hmm',      'Khayatt_ref_phe_1.hmm',
                'Khayatt_ref_pheac_1.hmm',      'Khayatt_ref_pip_1.hmm',
                'Khayatt_ref_pro_1.hmm',        'Khayatt_ref_pro_2.hmm',
                'Khayatt_ref_prom_1.hmm',       'Khayatt_ref_ser_1.hmm',
                'Khayatt_ref_thr_1.hmm',        'Khayatt_ref_thr_2.hmm',
                'Khayatt_ref_thrallo_1.hmm',    'Khayatt_ref_trp_1.hmm',
                'Khayatt_ref_tyr_1.hmm',        'Khayatt_ref_tyr_2.hmm',
                'Khayatt_ref_val_1.hmm',        'Khayatt_ref_val_2.hmm',
                'Khayatt_ref_val_3.hmm'
);
foreach(@ahmm){
        my $wga = $Aad . $_;
        system("wget $wga");
}
my $adb = 'Khayatt_nrpsA.hmmdb';
system("cat " . join(" ", @ahmm) . " > $adb");
system("hmmpress $adb");
