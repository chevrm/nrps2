CLUSTAL W multiple sequence alignment


Q01886v1_A1_pro                          RYPVERIRDIIRTTNATIALVGAGKTAALFKSADTAVQTIDITKDIPHGLSDTVVQSNTK
Q01886v2_A1_pro                          RYPVERIRDIIRTTNATIALVGAGKTAALFKSADTAVQTIDITKDIPHGLSDTVVQSNTK
                                         ************************************************************

Q01886v1_A1_pro                          IDDPAFGLFTSGSTGVPKCIVVTHSQICTAVQAYKDRFGVTSETRVLQFSSYTFDISIAD
Q01886v2_A1_pro                          IDDPAFGLFTSGSTGVPKCIVVTHSQICTAVQAYKDRFGVTSETRVLQFSSYTFDISIAD
                                         ************************************************************

Q01886v1_A1_pro                          TFTALFYGGTLCIPSEEDRMSNLQDYMVSVRPNWAVLTPTVSRFLDPGVVKDFISTLIFT
Q01886v2_A1_pro                          TFTALFYGGTLCIPSEEDRMSNLQDYMVSVRPNWAVLTPTVSRFLDPGVVKDFISTLIFT
                                         ************************************************************

Q01886v1_A1_pro                          GEASREADTVPWIEAGVNLYNVYGPAENTLITTATRIRKGKSSNIGYGVNTRTWVTDVSG
Q01886v2_A1_pro                          GEASREADTVPWIEAGVNLYNVYGPAENTLITTATRIRKGKSSNIGYGVNTRTWVTDVSG
                                         ************************************************************

Q01886v1_A1_pro                          ACLVPVGSIGELLIESGHLADKYLNRPDRTEAAFLSDLPWIPNYEGDSVRRGRRFYRTGD
Q01886v2_A1_pro                          ACLVPVGSIGELLIESGHLADKYLNRPDRTEAAFLSDLPWIPNYEGDSVRRGRRFYRTGD
                                         ************************************************************

Q01886v1_A1_pro                          LVRYCDDGSLICVGRSDTQIKLAGQRVELGDVEAHLQSDPTTSQAAVVFPRSGPLEARLI
Q01886v2_A1_pro                          LVRYCDDGSLICVGRSDTQIKLAGQRVELGDVEAHLQSDPTTSQAAVVFPRSGPLEARLI
                                         ************************************************************

Q01886v1_A1_pro                          ALLVTGNKDGTPHNQQSLPKPAFAQ
Q01886v2_A1_pro                          ALLVTGNKDGTPHNQQSLPKPAFAQ
                                         *************************
