CLUSTAL W multiple sequence alignment


Q9RLP6/5869932_A1_phe                    AVPAARVRFILDDASPVAVVTTAELRSRFDGHDLAVIDIDDPAIASLPATAVSDPRPDDI
Q9RLP6_A1_phe                            AVPAARVRFILDDASPVAVVTTAELRSRFDGHDLAVIDIDDPAIASLPATAVSDPRPDDI
                                         ************************************************************

Q9RLP6/5869932_A1_phe                    AYVIYTSGTTGTPKGVAVTHKNLTHLIAVLEERLPKPGVWPLCHSLAFDASVWEISNALL
Q9RLP6_A1_phe                            AYVIYTSGTTGTPKGVAVTHKNLTHLIAVLEERLPKPGVWPLCHSLAFDASVWEISNALL
                                         ************************************************************

Q9RLP6/5869932_A1_phe                    RGGRLVVVPEAVAGSPEDFHDLLVAEQVTFLTQTPSAVAMLSPDGLESMTLAVVGEACPP
Q9RLP6_A1_phe                            RGGRLVVVPEAVAGSPEDFHDLLVAEQVTFLTQTPSAVAMLSPDGLESMTLAVVGEACPP
                                         ************************************************************

Q9RLP6/5869932_A1_phe                    ALVDRWATNRTMINAYGPTETTICVTSSSPLEPGSVVVPIGSALPRTALFVLDPWLRPVP
Q9RLP6_A1_phe                            ALVDRWATNRTMINAYGPTETTICVTSSSPLEPGSVVVPIGSALPRTALFVLDPWLRPVP
                                         ************************************************************

Q9RLP6/5869932_A1_phe                    TGVAGELYVAGDGVTCGYIGRSGLTASRFVPCPFGEPGARMYRTGDLVRWGRDGQLEYLG
Q9RLP6_A1_phe                            TGVAGELYVAGDGVTCGYIGRSGLTASRFVPCPFGEPGARMYRTGDLVRWGRDGQLEYLG
                                         ************************************************************

Q9RLP6/5869932_A1_phe                    RADEQVKIRGYRIELGEVQSALAALDGVESAAAIMREDRPGDRRLVGYITGTADPVDI
Q9RLP6_A1_phe                            RADEQVKIRGYRIELGEVQSALAALDGVESAAAIMREDRPGDRRLVGYITGTADPVDI
                                         **********************************************************
