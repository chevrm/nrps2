CLUSTAL W multiple sequence alignment


Q45563_A1_10__ile                        ELPPERISYMLSETKAAILIVQKGLEPNTAFAGTFISADAEAMIEEHTKPLEIVTGPDDL
P94460_A1_ile                            ATSAERVSFMLEETQAKMLIVQKGLEQNAAFSGTCIISDAQGLMEENDIPINISSSPDDL
                                           ..**:*:**.**:* :******** *:**:** * :**:.::**:  *::* :.****

Q45563_A1_10__ile                        AYIMYTSGSTGRPKGVMITNRNVVSLVCNSNYTSASVNDRFILTGSISFDAVTFEMFGAL
P94460_A1_ile                            AYIMYTSGSTGRPKGVMITNRNVVSLVRNSNYTSASGDDRFIMTGSISFDAVTFEMFGAL
                                         *************************** ******** :****:*****************

Q45563_A1_10__ile                        LKGATLHIIDKSTMLTPDRFGAYLIENNITVLFLTTALFNQLAQAQADMFHRLHTLYVGG
P94460_A1_ile                            LNGASLHIIDKSTMLTPDRFGAYLLENDITVLFLTTALFNQLAQVRADMFRGLHTLYVGG
                                         *:**:*******************:**:****************.:****: ********

Q45563_A1_10__ile                        EALSPELINAVRRACPNLSLYNIYGPTENTTFSTFFEIKRDYATPIPIGKPISNCTAFIL
P94460_A1_ile                            EALSPALMNAVRHACPDLALHNIYGPTENTTFSTFFEMKRDYAGPIPIGKPISNSTAYIL
                                         ***** *:****:***:*:*:****************:***** **********.**:**

Q45563_A1_10__ile                        DAKGCLLPIGVPGELCVGGDGVAKGYLNRDDVTAAVFSPDPFIPGERIYRTGDLARWLPD
P94460_A1_ile                            DTKGRLLPIGVPGELCVGGDGVAKGYLNRVDLTNAVFSPHPFLPGERIYRTGDLARWLPD
                                         *:** ************************ *:* *****.**:*****************

Q45563_A1_10__ile                        GNLEYISRIDRQIKIRGKRIEPAEIEARLLEIEGVREAAVTLLETDGEVQLYTHYVSDES
P94460_A1_ile                            GNLEYISRIDRQMKIRGKRIEPAEIEARLLEMEGVQEAAVTLREKDGEAHVYTHYVGDHK
                                         ************:******************:***:****** *.***.::*****.*..

Q45563_A1_10__ile                        RNEKE
P94460_A1_ile                            KTDTD
                                         :.:.:
