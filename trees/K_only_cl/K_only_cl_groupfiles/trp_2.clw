CLUSTAL W multiple sequence alignment


Q93N89_A2_trp                            DHPADRIAFVLADAAPAAVLCTAQTRPMVPEGFTGLVVTLDEDLPEGDFDDGRVLTAPDA
Q93N89_A2_2__trp                         DHPADRIAFVLADAAPAAVLCTAQTRPMVPEGFTGLVVTLDEDLPEGDFDDGRVLTAPDA
                                         ************************************************************

Q93N89_A2_trp                            DHIAYVIHTSGSTGTPKGAAVTWGGLRNLVADRIERYGIGTDTRLVQLVSPSFDVSMADI
Q93N89_A2_2__trp                         DHIAYVIHTSGSTGTPKGAAVTWGGLRNLVADRIERYGIGTDTRLVQLVSPSFDVSMADI
                                         ************************************************************

Q93N89_A2_trp                            WPTLCAGGRLVLAPPGGHTTGDELGDLLADHRITHAVMPAVQLTHLPDRELPALRVLVSG
Q93N89_A2_2__trp                         WPTLCAGGRLVLAPPGGHTTGDELGDLLADHRITHAVMPAVQLTHLPDRELPALRVLVSG
                                         ************************************************************

Q93N89_A2_trp                            GDALPADTRRRWVARCDLHNEYGVTEATVVSTVTAPLDDAGPLTIGGPIARAGVHVLDGF
Q93N89_A2_2__trp                         GDALPADTRRRWVARCDLHNEYGVTEATVVSTVTAPLDDAGPLTIGGPIARAGVHVLDGF
                                         ************************************************************

Q93N89_A2_trp                            LRPVPPGVTGELYVTGTGVARGYLNRPTLTAHRFIADPRTRGGRMYRTGDLVRHTHGGEL
Q93N89_A2_2__trp                         LRPVPPGVTGELYVTGTGVARGYLNRPTLTAHRFIADPRTRGGRMYRTGDLVRHTHGGEL
                                         ************************************************************

Q93N89_A2_trp                            VFVGRADEQVKLRGHRIELGEVEAALADHPDVEQAVAAIHDARLIGYVVPADGRVPDPSA
Q93N89_A2_2__trp                         VFVGRADEQVKLRGHRIELGEVEAALADHPDVEQAVAAIHDARLIGYVVPADGRVPDPSA
                                         ************************************************************
