CLUSTAL W multiple sequence alignment


Q2N3S9_A1_9__ser                         TYPEQRLEFMARDAGVAVLLTQSSFSGVLAGFTGT-RLCLDTEASRLSSYSA------DS
Q5DIV7_A3_ser                            EYPEERQAYMLEDSGVELLLSQSHLKLPLA--QGVQRIDLDRGAPWFEDYSE------AN
Q5DIP4_A3_6__ser                         EYPEERQAYMLEDSGVQLLLSQSHLKLPLA--QGVQRIDLDQADAWLENHAE------NN
Q5DIP3_A1_ser                            EYPEERQAYMLEDSGVQLLLSQSHLKLPLA--QGVQRIDLDQADAWLENHAE------NN
Q50E73_A6_11__ser                        EYPADRLAYMAGDAAPVAVLTRGDV--ELPG--SVPRIGLDDTEIRATLATA----PGTN
Q5DIV7_A1_ser                            EYPEERQAYMLEDSGVQLLLSQSHLKLPLA--QGVQRIDLDQADAWLENHAE------NN
Q9Z4X6_A1_ser                            GYPADRIAHILRDAGAMLVLTTRDTAERLPG-DGTPRLLLDEPAAAGTTAAGAPAPPGTL
                                          ** :*  .:  *:.   :*:       *.   .. *: **         :         

Q2N3S9_A1_9__ser                         PRVEVRPEHLAYVIYTSGSTGMPKGCMLSHRAICNRLHWMQEAYALTPLDRVLQKTPFTF
Q5DIV7_A3_ser                            PDIHLDGENLAYVIYTSGSTGKPKGAGNRHSALSNRLCWMQQAYGLGVGDTVLQKTPFSF
Q5DIP4_A3_6__ser                         PGVELNGENLAYVIYTSGSTGKPKGAGNRHSALSNRLCWMQQAYGLGVGDTVLQKTPFSF
Q5DIP3_A1_ser                            PGVELNGENLAYVIYTSGSTGKPKGAGNRHSALSNRLCWMQQAYGLGVGDTVLQKTPFSF
Q50E73_A6_11__ser                        PGTPVTEAHPAYMIYTSGSTGRPKGVVVSHGAIVNRLAWMQAEYRLDATDRVLQKTPAGF
Q5DIV7_A1_ser                            PGVELNGENLAYVIYTSGSTGKPKGAGNRHSALSNRLCWMQQAYGLGVGDTVLQKTPFSF
Q9Z4X6_A1_ser                            PRALPAPGHPAYVIYTSGSTGRPKGVVISHRAIVNRLAWMQDTYGLEPSDRVLQKTPSGF
                                         *       : **:******** ***    * *: *** ***  * *   * ******  *

Q2N3S9_A1_9__ser                         DVSVWEFFWPLITGARIVLAAPGAERDPAALAGLIQEHGVTACHFVPSMLRLFLDEPRAA
Q5DIV7_A3_ser                            DVSVWEFFWPLMSGARLVVAAPGDHRDPAKLVALINREGVDTLHFVPSMLQAFLQDEDVV
Q5DIP4_A3_6__ser                         DVSVWEFFWPLMSGARLVVAAPGDHRDPAKLVALINREGVDTLHFVPSMLQAFLQDEDVA
Q5DIP3_A1_ser                            DVSVWEFFWPLMSGARLVVAAPGDHRDPAKLVKLINREGVDTLHFVPSMLQAFLQDEDVA
Q50E73_A6_11__ser                        DVSVWEFFWPLLEGAVLVFARPGGHRDAAYLAGLIERERITTAHFVPSMLRVFLEEPGAA
Q5DIV7_A1_ser                            DVSVWEFFWPLMSGARLVVAAPGDHRDPAKLVELINREGVDTLHFVPSMLQAFLQDEDVA
Q9Z4X6_A1_ser                            DVSVWEFFWPLVQGATLVVARPGGHTDPAYLAGTVRREGVTTLHFVPSMLDVFLREPAAA
                                         ***********: ** :*.* ** . *.* *.  :... : : *******  ** :  ..

Q2N3S9_A1_9__ser                         GCAS---LRHVFFSGEALPYPLMERALSTF-SAQLHNLYGPTEAAVDVSFWKCNLR-DDR
Q5DIV7_A3_ser                            SCTS---LKRIVCSGEALPADAQQQVFAKLPQAGLYNLYGPTEAAIDVTHWTCVEE-GKD
Q5DIP4_A3_6__ser                         SCTS---LKRIVCSGEALPADAQQQVFAKLPQAGLYNLYGPTEAAIDVTHWTCVEE-GKD
Q5DIP3_A1_ser                            SCTS---LKRIVCSGEALPADAQQQVFAKLPQAGLYNLYGPTEAAIDVTHWTCVEE-GKD
Q50E73_A6_11__ser                        LCTG---LRRVICSGEALGTDLAVDFRAKL-PVPLHNLYGPTEAAVDVTHHAYEPATGTA
Q5DIV7_A1_ser                            SCTS---LKRIVCSGEALPADAQQQVFAKLPQAGLYNLYGPTEAAIDVTHWTCVEE-GKD
Q9Z4X6_A1_ser                            ALGGATPVRRVFCSGEALPAELRARFRAVS-DVPLHNLYGPTEAAVDVTYWPCAEDTGDG
                                            .   ::::. *****         :    . *:*********:**:.       .  

Q2N3S9_A1_9__ser                         KVPIGRPIANIRLYILDEQQRPVRPDQTGELYIAGVGLARGYLNRPELTAERFVRDPFDP
Q5DIV7_A3_ser                            AVPIGRPIANLACYILDGNLEPVPVGVLGELYLAGQGLARGYHQRPGLTAERFVASPF--
Q5DIP4_A3_6__ser                         AVPIGRPIANLACYILDGNLEPVPVGVLGELYLAGRGLARGYHQRPGLTAERFVASPF--
Q5DIP3_A1_ser                            AVPIGRPIANLACYILDGNLEPVPVGVLGELYLAGRGLARGYHQRPGLTAERFVASPF--
Q50E73_A6_11__ser                        TVPIGRPIWNIRTYVLDAALRPVPPGVPGELYLAGAGLARGYHGRPALTAERFVACPFG-
Q5DIV7_A1_ser                            AVPIGRPIANLACYILDGNLEPVPVGVLGELYLAGQGLARGYHQRPGLTAERFVASPF--
Q9Z4X6_A1_ser                            PVPIGRPVWNTRMYVLDAALRPVPAGVPGELYIAGVQLARGYLGRPALSAERFTADPHG-
                                          ******: *   *:**   .**  .  ****:**  *****  ** *:****.  *.  

Q2N3S9_A1_9__ser                         SPGARMYKTGDRAAWLPDGNIDFLGRMDGQIKLRGLRIELGEIEAALLGHEAVREAAVAV
Q5DIV7_A3_ser                            VAGERMYRTGDLARYRADGVIEYAGRIDHQVKLRGLRIELGEIEARLLEHPWVREAAVLA
Q5DIP4_A3_6__ser                         VAGERMYRTGDLARYRADGVIEYAGRIDHQVKLRGLRIELGEIEARLLEHPWVREAAVLA
Q5DIP3_A1_ser                            VAGERMYRTGDLARYRADGVIEYAGRIDHQVKLRGLRIELGEIEARLLEHPRVREAAVLA
Q50E73_A6_11__ser                        VPGERMYRTGDLVRWRVDGTLEFVGRADDQVKVRGFRVELGEVEGAVAAHPDVVRAVVVV
Q5DIV7_A1_ser                            VAGERMYRTGDLARYRADGVIEYAGRIDHQVKLRGLRIELGEIEARLLEHPRVREAAVLA
Q9Z4X6_A1_ser                            APGSRMYRTGDLARWNHDGSLDYLGRADHQVKLRGFRIELGEIEAALVRQPEIAQAAVVL
                                          .* ***:*** . :  ** ::: ** * *:*:**:*:****:*. :  :  : .*.*  

Q2N3S9_A1_9__ser                         RDADSGDPRLVAYVVLR----------DGASFSP--------------------------
Q5DIV7_A3_ser                            VDGK----QLVGYVVLE----------SESGD----------------------------
Q5DIP4_A3_6__ser                         VDGK----QLVGYVVLE----------SEGGD----------------------------
Q5DIP3_A1_ser                            VDGR----QLVGYVVLE----------SEGGD----------------------------
Q50E73_A6_11__ser                        REDRPGDHRLVAYV----------------------------------------------
Q5DIV7_A1_ser                            VDGR----QLVGYVVLE----------SEGGD----------------------------
Q9Z4X6_A1_ser                            REDRPGDQRLVAYTVPARDADTLTGPPAEAGTHPGPGAAPDTDPATGPAAGTDSGPGSGT
                                          :      :**.*.                                              

Q2N3S9_A1_9__ser                         ----------------------QV---
Q5DIV7_A3_ser                            ---------------------------
Q5DIP4_A3_6__ser                         ---------------------------
Q5DIP3_A1_ser                            ---------------------------
Q50E73_A6_11__ser                        ----------------------TVGGV
Q5DIV7_A1_ser                            ---------------------------
Q9Z4X6_A1_ser                            GSGTGSGTGSGARPGPDGTATHTVAGA
                                                                    
