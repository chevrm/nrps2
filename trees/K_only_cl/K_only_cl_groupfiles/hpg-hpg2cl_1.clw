CLUSTAL W multiple sequence alignment


Q70AZ9b_A1_hpg|hpg2cl                    GYPAPRVAFMVADSGASRMVCSAATRDGVPEGIEAIVVTDEEAFEASAAGARPGDLAYVM
Q8KLL3_A1_hpg|hpg2cl                     AHPAPRVAFVVADSGASLMACSAATAGRVPEGVEPVVVTDEGRGDASAVPVSPGDLAYVM
Q70AZ9a_A1_hpg|hpg2cl                    GYPAPRVAFMVADSGASRMVCSAATRDGVPEGIEAIVVTDEEAFEASAAGARPGDLAYVM
                                         .:*******:******* *.***** . ****:*.:*****   :***. . ********

Q70AZ9b_A1_hpg|hpg2cl                    YTSGSTGIPKGVAVPHRSVAELAGNPGWAVEPGDAVLMHAPYAFDASLFEIWVPLVSGGR
Q8KLL3_A1_hpg|hpg2cl                     YTSGSTGTPKGVAVPHRSVAELAGNPGWAVKPGDAILMHAPHAFDASLFEIWVPLVSGAR
Q70AZ9a_A1_hpg|hpg2cl                    YTSGSTGIPKGVAVPHRSVAELAGNPGWAVEPGDAVLMHAPYAFDASLFEIWVPLVSGGR
                                         ******* **********************:****:*****:****************.*

Q70AZ9b_A1_hpg|hpg2cl                    VVIAEPGPVDARRLREAISSGVTRAHLTAGSFRAVAEESPESFAGLREVLTGGDVVPAHA
Q8KLL3_A1_hpg|hpg2cl                     VVIAEPGAVDARRLREAIAAGVTKVHLTAGSFRALAEESSESFAGLQEVLTGGDVVPAHA
Q70AZ9a_A1_hpg|hpg2cl                    VVIAEPGPVDARRLREAISSGVTRAHLTAGSFRAVAEESPESFAGLREVLTGGDVVPAHA
                                         *******.**********::***:.*********:****.******:*************

Q70AZ9b_A1_hpg|hpg2cl                    VARVRSACPRVRIRHLYGPTETTLCATWHLLEPGDEIGPVLPIGRPLPGRRAQVLDASLR
Q8KLL3_A1_hpg|hpg2cl                     VEKVRKAVPQARIRHLYGPTETTLCATWHLLQPSEALGPVLPIGRPLPGRRAQVLDASLR
Q70AZ9a_A1_hpg|hpg2cl                    VARVRSACPRVRIRHLYGPTETTLCATWHLLEPGDEIGPVLPIGRPLPGRRAQVLDASLR
                                         * :**.* *:.********************:*.: :***********************

Q70AZ9b_A1_hpg|hpg2cl                    AVAPGVIGDLYLSGAGLADGYLRRAGLTAERFVADPSAPGARMYRTGDLAQWTADGALLF
Q8KLL3_A1_hpg|hpg2cl                     PLPPGVVGDLYLSGAGLADGYLDRAALTAERFVADPSVPGGRMYRTGDLVQWTADGELLF
Q70AZ9a_A1_hpg|hpg2cl                    AVAPGVIGDLYLSGAGLADGYLRRAGLTAERFVADPSAPGARMYRTGDLAQWTADGALLF
                                         .:.***:*************** **.***********.**.********.****** ***

Q70AZ9b_A1_hpg|hpg2cl                    AGRADDQVKVRGFRIEPAEVEAALTAQPGVHEAVVRAVDGRLVGYVVAEGDAEPAV
Q8KLL3_A1_hpg|hpg2cl                     VGRADDQVKIRGFRIEPGEIEAALTAQPDVHEAVVVAIDGRLIGYAVT--DVDPVV
Q70AZ9a_A1_hpg|hpg2cl                    AGRADDQVKVRGFRIEPAEVEAALTAQPGVHEAVVRAVDGRLVGYVVAEGDAEPAV
                                         .********:*******.*:********.****** *:****:**.*:  *.:*.*
