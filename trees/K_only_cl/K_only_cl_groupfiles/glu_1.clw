CLUSTAL W multiple sequence alignment


Q70KJ7_A1_glu                            DFPADRIQYILEDSGAKAVLTEAGIQAPAADAERIDFDEAVQYETAADGVS--TQSDRLA
P27206_A1_glu                            GFPAERIQYILEDCGADFYLTESKVAAPEADAELIDLDQAIE-EGAEESLNADVNARNLA
                                         .***:********.**.  ***: : ** **** **:*:*:: * * :.:.  .:: .**

Q70KJ7_A1_glu                            YIIYTSGTTGRPKGVMIEHRQVHHLVQSLQQEIYQCGEQTLRMALLAPFHFDASVKQIFA
P27206_A1_glu                            YIIYTSGTTGRPKGVMIEHRQVHHLVESLQQTIYQSPTQTLPMAFLPPFHFDASVKQIFA
                                         **************************:**** ***.  *** **:*.*************

Q70KJ7_A1_glu                            SLLLGQTLYIVPKTTVTNGSALLDYYRQNRIEATDGTPAHLQMMVAAGDVSGIELRHMLI
P27206_A1_glu                            SLLLGQTLYIVPKKTVTNGAALTAYYRKNSIEATDGTPAHLQMLAAAGDFEGLKLKHMLI
                                         *************.*****:**  ***:* *************:.****..*::*:****

Q70KJ7_A1_glu                            GGEGLSAAVAEQLMALFHQSGRTPRLTNVYGPTETCVDASVHQVSADNGMNQQAAYVPIG
P27206_A1_glu                            GGEGLSSVVADKLLKLFKEAGTAPRLTNVYGPTETCVDASVHPVIPENAV--QSAYVPIG
                                         ******:.**::*: **:::* :******************* * .:*.:  *:******

Q70KJ7_A1_glu                            KPLGNARLYILDKHQRLQPDGTAGELYIAGDGVGRGYLNLPDLTAEKFLQDPFNGSGRMY
P27206_A1_glu                            KALGNNRLYILDQKGRLQPEGVAGELYIAGDGVGRGYLHLPELTEEKFLQDPFVPGDRMY
                                         *.*** ******:: ****:*.****************:**:** ********  ..***

Q70KJ7_A1_glu                            RTGDMARWLPDGTIEYIGREDDQVKVRGYRIELGEIETVLRKAPGAAQAVVLARPDQQGS
P27206_A1_glu                            RTGDVVRWLPDGTIEYLGREDDQVKVRGYRIELGEIEAVIQQAPDVAKAVVLARPDEQGN
                                         ****:.**********:********************:*:::**..*:********:**.

Q70KJ7_A1_glu                            LDVCAYIVQEKGTEFHP
P27206_A1_glu                            LEVCAYVVQKPGSEFAP
                                         *:****:**: *:** *
