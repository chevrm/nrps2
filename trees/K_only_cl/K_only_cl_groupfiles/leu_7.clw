CLUSTAL W multiple sequence alignment


Q84BQ5_A2_4__leu                         AYPAERIAYLLQDCAPVAVVAQASTRDLLG--GVQVIDLDNDLWQHLSDANPQVPALTPK
Q84BQ6v2_A1_leu                          AHPAERLSYLLEDSAPIAVLTQRALRGHLPALSVPVIELDQPSWSAGRVDAPKVPGLTPA
Q84BQ5_A3_5__leu                         AYPAERIAYLLQDSDPVAVLAQGATRELLG--RVPVIDLDNALWQHLPETNPQLPTLTPA
Q84BQ6v1_A1_leu                          AHPAERLSYLLEDSAPIAVLTQRALRGHLPALSVPVIELDQPSWSAGRVDAPKVPGLTPA
Q84BQ4_A1_7__leu                         AHPAERLNYLLDDSAPVAVLTQSHLRERLPALNVPVLDLDHCNWPLTVTQNPQVPGLSTA
                                         *:****: ***:*. *:**::*   *  *    * *::**:  *       *::* *:. 

Q84BQ5_A2_4__leu                         HLAYVIYTSGSTGQPKGVMVEHATLENLVHWHAEAFDLQAGSQTASVAGFGFDAMAWEVW
Q84BQ6v2_A1_leu                          NLAYVIYTSGSTGLPKGVMVEHRTLCNLVDWHADVFDLHAGSHTSSLAGFGFDAMAWEVW
Q84BQ5_A3_5__leu                         HLAYVIYTSGSTGQPKGVVVEHATLENLVHWHCEAFDLRAGSHTASVAGCGFDAMAWEVW
Q84BQ6v1_A1_leu                          NLAYVIYTSGSTGLPKGVMVEHRTLCNLVDWHADVFDLHAGSHTSSLAGFGFDAMAWEVW
Q84BQ4_A1_7__leu                         NLAYVIYTSGSTGLPKGVMVEHHTLSNLVDWHCTAFDLCAGRHTSSLAGFGFDAMAWEVW
                                         :************ ****:*** ** ***.**. .*** ** :*:*:** **********

Q84BQ5_A2_4__leu                         PALCVGATLHLPPPSIRNEHLDEMLDWWCAQPLQVSFLPTPVAEYAFSRQLQHPTLRTLL
Q84BQ6v2_A1_leu                          PALCVGATLHLAPASEGSEDIDALLDWWRAQPLDVSFLPTPIAEYAFSRQLDHPTLRTLL
Q84BQ5_A3_5__leu                         PALCVGATLHLPPPSISNEHLDELLDWWRAQPLQVSFLPTPIAEYAFSRELGHPTLRTLL
Q84BQ6v1_A1_leu                          PALCVGATLHLAPASEGSEDIDALLDWWRAQPLDVSFLPTPIAEYAFSRQLDHPTLRTLL
Q84BQ4_A1_7__leu                         PALCAGATLHLAPTHEGGEDIDALLAWWCAQPLDVSFLPTPVAEYAFSQNLEHPTLRTLL
                                         ****.******.*.   .*.:* :* ** ****:*******:******::* ********

Q84BQ5_A2_4__leu                         IGGDRLRQFSRQQTFEVINNYGPTEATVVATSGPVELDQPLHIGRPIANVRIYVLDAEQR
Q84BQ6v2_A1_leu                          IGGDRLRQFPRQPSFEVINNYGPTEATVVATSGRIDAGQALHIGKPVSNATVYLLDEQQQ
Q84BQ5_A3_5__leu                         IGGDKLRQFSRGQTFDVINNYGPTEATVVATSGSIHAGQLLHIGRPISNARIYLLDEQQR
Q84BQ6v1_A1_leu                          IGGDRLRQFPRQPSFEVINNYGPTEATVVATSGRIDAGQALHIGKPVSNATVYLLDEQQQ
Q84BQ4_A1_7__leu                         IGGDRLRQFNRNQHFDVINNYGPTEATVVATSGLVQSGDALHIGKPLSNATVYLLDEQQR
                                         ****:**** *   *:***************** :. .: ****:*::*. :*:** :*:

Q84BQ5_A2_4__leu                         PVPVGVAGELYVGGAGVARGYLNRPELSAAQFLFDPFSDEPQARMYRTGDQARWLADGNI
Q84BQ6v2_A1_leu                          PVPLGVTGELYVGGAGVARGYLNRAELSAERFLRDPFSAEPGARLYRTGDLARWRADGNL
Q84BQ5_A3_5__leu                         PVPVGVAGELYVGGAGVARGYLNRPELTAERFLEDPFSDRPQARMYRTGDLARWLADGNI
Q84BQ6v1_A1_leu                          PVPLGVTGELYVGGAGVARGYLNRAELSAERFLRDPFSAEPGARLYRTGDLARWRADGNL
Q84BQ4_A1_7__leu                         PVPLGVAGELYVGGAGVARGYLNRPDMTAERFLRDPFSRAPNARMYRTGDLARWREDGNI
                                         ***:**:*****************.:::* :** ****  * **:***** ***  ***:

Q84BQ5_A2_4__leu                         EYLGRNDDQVKLRGVRIELGEIEAALGSHVALQEAVVLVRDGR-----LIAWFTER---T
Q84BQ6v2_A1_leu                          EYLGRNDDQVKIRGIRIEPGEIESALASHSAVREAVVLVRDGQ-----LLAWFTEQ---A
Q84BQ5_A3_5__leu                         EYLGRNDDQVKLRGVRVELGEIEAALSSHPAVQDAVVLVREAR-----LIAWFTAR---T
Q84BQ6v1_A1_leu                          EYLGRNDDQVKIRGIRIEPGEIESALASHSAVREAVVLVRDGQ-----LLAWFTEQ---A
Q84BQ4_A1_7__leu                         EYLGRNDDQVKIRGVRIELGEIETCLNQLPGIQEAVLLAREDQPGQPRLVAYFTEQPQVE
                                         ***********:**:*:* ****:.* .  .:::**:*.*: :     *:*:** :    

Q84BQ5_A2_4__leu                         AVDIQQ
Q84BQ6v2_A1_leu                          PLDINE
Q84BQ5_A3_5__leu                         TVDNED
Q84BQ6v1_A1_leu                          PLDINE
Q84BQ4_A1_7__leu                         ALPVGE
                                         .:   :
