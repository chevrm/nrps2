CLUSTAL W multiple sequence alignment


Q45R85_A1_trp                            EYPAERVGFLLRDARPALLVGGTGTEPSAADCPRVPAEEL-LDAGACRAEADVPPP----
Q93N89_A2_trp                            DHPADRIAFVLADAAPAAVLCTAQTR------PMVPEG---FTGLVVTLDEDLPEGDFDD
Q5V8A8_A2_trp                            SYPASRIIYMLEDVQPCLLITTTN---SLISNPKLNIPKLQLDSFPWEASLNVTSEKQDY
Q93N89_A2_2__trp                         DHPADRIAFVLADAAPAAVLCTAQTR------PMVPEG---FTGLVVTLDEDLPEGDFDD
                                         .:**.*: ::* *. *. ::  :         * :      : .     . ::.      

Q45R85_A1_trp                            ----------GSLP--VDLPAYVVHTSGSTGRPKGVVVTHAGIAALAAEQIERYRLGPGS
Q93N89_A2_trp                            GRV-------LTAP-DADHIAYVIHTSGSTGTPKGAAVTWGGLRNLVADRIERYGIGTDT
Q5V8A8_A2_trp                            GLQANIEDNRGTQPLHVSDPAYIIYTSGSTGKPKGVVVTHAGISSMVATQIKYFEVTPES
Q93N89_A2_2__trp                         GRV-------LTAP-DADHIAYVIHTSGSTGTPKGAAVTWGGLRNLVADRIERYGIGTDT
                                                    : *  ..  **:::****** ***..** .*:  :.* :*: : : . :

Q45R85_A1_trp                            RVAQLAALGFDVAVAELVMALASGSCLVLPPHGLA--GDELASFLRDRRITTALAPAAVL
Q93N89_A2_trp                            RLVQLVSPSFDVSMADIWPTLCAGGRLVLAPPGGHTTGDELGDLLADHRITHAVMPAVQL
Q5V8A8_A2_trp                            RILQFSSLSFDGVVWELCSALLTGATLVMAPSERVQPGPELIQLIGDYHVTHAVLPPAVL
Q93N89_A2_2__trp                         RLVQLVSPSFDVSMADIWPTLCAGGRLVLAPPGGHTTGDELGDLLADHRITHAVMPAVQL
                                         *: *: : .**  : ::  :* :*. **:.*      * ** .:: * ::* *: *.. *

Q45R85_A1_trp                            ATLPPGDLPDLTDLVTGGEQPPPALIARWAPGRRMFNVYGPTEATVQATSGRCAADGDRS
Q93N89_A2_trp                            THLPDRELPALRVLVSGGDALPADTRRRWVARCDLHNEYGVTEATVVSTVT-APLDDAGP
Q5V8A8_A2_trp                            MVLSPDNIPSLTHLIVSGEAASGELVKRWSVGRCLINGYGPTETTVCATLS-SPLSGNGI
Q93N89_A2_2__trp                         THLPDRELPALRVLVSGGDALPADTRRRWVARCDLHNEYGVTEATVVSTVT-APLDDAGP
                                           *.  ::* *  *: .*:  .     **     : * ** **:** :*   .. ..   

Q45R85_A1_trp                            PDIGNPEAGVDAYVLDAALRPVPDGVTGELYLRGRGLARGYLGRPGLTAGRFVADPHTGT
Q93N89_A2_trp                            LTIGGPIARAGVHVLDGFLRPVPPGVTGELYVTGTGVARGYLNRPTLTAHRFIADP-RTR
Q5V8A8_A2_trp                            PPIGRAVINVQCYVLDDQLQLLPPGAIGELYISGPGLARGYLNQPQLTAERFLANPFRDI
Q93N89_A2_2__trp                         LTIGGPIARAGVHVLDGFLRPVPPGVTGELYVTGTGVARGYLNRPTLTAHRFIADP-RTR
                                           ** .   .  :***  *: :* *. ****: * *:*****.:* *** **:*:*    

Q45R85_A1_trp                            GERMYRTGDLVR-RVPGDGRTVLRFVGRADDQVKIRGFRVEPGEVEAALAELDGVEQALV
Q93N89_A2_trp                            GGRMYRTGDLVRHTHGGE----LVFVGRADEQVKLRGHRIELGEVEAALADHPDVEQAVA
Q5V8A8_A2_trp                            GSRMYRTGDLVRWRNHGE----LEFVGRADNQVKIRGFRVELGEVETALTNCPPVSDALA
Q93N89_A2_2__trp                         GGRMYRTGDLVRHTHGGE----LVFVGRADEQVKLRGHRIELGEVEAALADHPDVEQAVA
                                         * **********    *:    * ******:***:**.*:* ****:**::   *.:*:.

Q45R85_A1_trp                            TVREERPGDRRLVGYLTPAPGHRGSLDVE-
Q93N89_A2_trp                            AIHDAR-----LIGYVVPADGRVPDPSA--
Q5V8A8_A2_trp                            MVREDRPGEKFLVAYVV----GQDSMDTDA
Q93N89_A2_2__trp                         AIHDAR-----LIGYVVPADGRVPDPSA--
                                          ::: *     *:.*:.       . ..  
