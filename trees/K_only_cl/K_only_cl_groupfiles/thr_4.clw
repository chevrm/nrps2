CLUSTAL W multiple sequence alignment


Q6YK39_A2_thr                            DYPKERIHYMLEDSNVSILLLQHHLLEGTDYQSHTVFLDDPSSYGAETSNLNLNVMPNQL
Q70K00_A2_thr                            DYPKERIHYMLEDSNVSILLLQHHLLEGTDYQSHTVFLDDPSSYGAEASNLKLNVMPNQL
                                         ***********************************************:***:********

Q6YK39_A2_thr                            AYVIYTSGTTGNPKGTLIEHKNVVRLLFNNKNVFDFNASDTWTLFHSFCFDFSVWEMYGA
Q70K00_A2_thr                            AYVIYTSGTTGNPKGTLIEHKNVVRLLFNNKNVFDFNASDTWTLFHSFCFDFSVWEMYGA
                                         ************************************************************

Q6YK39_A2_thr                            LLYGGKLVIVPKQIAKNPERYLQLLKSEAVTILNQTPSYFYQLMQEERADPESNLNIRKI
Q70K00_A2_thr                            LLYGGKLVIIPKQIAKNPERYLQLLKSEAVTILNQTPSYFYQLMQEERADPESNLNIRKI
                                         *********:**************************************************

Q6YK39_A2_thr                            IFGGEALNPSFLKDWKLKYPLTQLINMYGITETTVHVTYKEITEREIDEGRSNIGQPIPT
Q70K00_A2_thr                            IFGGEALNPSFLKDWKLKYPLTQLINMYGITETTVHVTYKEITEREIDEGRSNIGQPIPT
                                         ************************************************************

Q6YK39_A2_thr                            LQAYILDEYQHIQVMGIPGELYVAGEGLARGYLNRPELTAEKFVEHPFAAGEKMYKTGDV
Q70K00_A2_thr                            LQAYILDEYQRIQVMGIPGELYVAGEGLARGYLNRPELTGEKFVEHPFAAGEKMYKTGDV
                                         **********:****************************.********************

Q6YK39_A2_thr                            ARWLPDGNIEYLGRIDHQVKIRGYRIEIGEVEAALLQSESVKEAVVIAIEEEGSKQLCAY
Q70K00_A2_thr                            ARWLPDGNIEYLGRIDHQVKIRGYRIEIGEVEAALLQLESVKEAVVIAIEEEGSKQLCAY
                                         ************************************* **********************

Q6YK39_A2_thr                            LSGDDSLNTAQ
Q70K00_A2_thr                            LSGDDSLNTAQ
                                         ***********
