CLUSTAL W multiple sequence alignment


Q8NJX1_A17_gln                           LHPISRREALVREVNARVLIASSDAVASCAGMAEHVVELSPSVMARLAT--SVTLKILPK
Q8NJX1_A6_gln                            LHPRNRREALVQEVGAEIMIVSPSSSVPCEGLTSIMVEFTIELLEQLSSRYDAFQEILPK
                                         *** .******:**.*.::*.*..: ..* *::. :**:: .:: :*::  ..  :****

Q8NJX1_A17_gln                           VGPRNTAYILFTSGSTGKPKGVVMQHGSFSSTTIGYGKVYNLSPLSRIFQFSNYIFDGSL
Q8NJX1_A6_gln                            AEPSNAAYVLFTSGSTGKPKGVLMEHSAFATSTLGHGGIYNLSPASRVFQFSNYIFDGSL
                                         . * *:**:*************:*:*.:*:::*:*:* :***** **:************

Q8NJX1_A17_gln                           GEIFGPLAFGGTICIPSDDERLQCAPDFMHRAKVNTAMLTPSFVRTFTPDKVPHLKTLVL
Q8NJX1_A6_gln                            GEIFTTLSFGGTVCVPSEDERLQKAPSFMREARVNTAMLTPSFVRTFAPEQVPSLRLLVL
                                         **** .*:****:*:**:***** **.**:.*:**************:*::** *: ***

Q8NJX1_A17_gln                           GGEAASKSTLEMWVDRVTLFNGYGPAEACNYATTHIFKSSAESPRLIGSSFNGACWVVEP
Q8NJX1_A6_gln                            GGEPSSKDLLETWCGRLRLVNGYGPAEACNYATTHDFKPT-DSPHTIGRGFNSACWIVDP
                                         ***.:**. ** * .*: *.*************** **.: :**: ** .**.***:*:*

Q8NJX1_A17_gln                           SNHNKLTPIGCTGELVLQGHALARGYLNDKMKTEESFVCEIGSLPSSLLHEPKRFYLTGD
Q8NJX1_A6_gln                            TDYNKLTPIGCIGELIIQGNALARGYINDADRTKNSFITNVDCLPKSIISGPHRFYLTGD
                                         :::******** ***::**:******:**  :*::**: ::..**.*::  *:*******

Q8NJX1_A17_gln                           LVRYNSNGELEYLGRKDSQVKLRGQRLELGEIEYNITQSLSSVRHVAVDVMHRQAGDSLV
Q8NJX1_A6_gln                            LVRYTPDGQLEYLGRKDTQVKLRGQRLELGEIEYHVKKSLANIEHVAVDVAHRETGDTLI
                                         ****..:*:********:****************::.:**:.:.****** **::**:*:

Q8NJX1_A17_gln                           AFISF-----SGHANTKWTSDDLCLNLVAPN
Q8NJX1_A6_gln                            AFVSFKEKMATTSGNILLLNDDL--------
                                         **:**     :  .*    .***        
