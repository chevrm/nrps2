CLUSTAL W multiple sequence alignment


Q2XNF8_A2_3__ala                         GYPRD----RVAAILADADPAIALVDRVGRASLGADAAGARDLLDLDAADPAWSDRSAAD
Q2XNF8_A1_2__ala                         ----DDAPARLRELLADADPTLVLSDAAGRRALGENALLAHAVVDLDERRPVWAGASTAD
                                             *    *:  :******::.* * .** :** :*  *: ::***   *.*:. *:**

Q2XNF8_A2_3__ala                         PEPAGLSARNLAYVIYTSGSTGTPKGVQNEHRGVVNRLAWMPEEYRLGAGDTVLQKTSFG
Q2XNF8_A1_2__ala                         PRPAGLSPRHLACAIYPAGATAAPAGAQNEHRALVDRLRWMQDAYRLGAGETVVQQTSLA
                                         *.*****.*:** .**.:*:*.:* *.*****.:*:** ** : ******:**:*:**:.

Q2XNF8_A2_3__ala                         FDVSVWEFFWPLLHGATLALAPPDAHKDPAALIELIVRHRVTTVHFVPSMLAVFLQADGV
Q2XNF8_A1_2__ala                         ADAALWELFWTWSNGATVVLAPAGAQSTPAALVELFVRHDIATAHFAPAALAAFLRADGV
                                          *.::**:**.  :***:.***..*:. ****:**:*** ::*.**.*: **.**:****

Q2XNF8_A2_3__ala                         ERCAGLRRVICSGEALPGASVRLLHKRLPQTAIHNLYGPTEAAIEATAWTCPRDFAGDTV
Q2XNF8_A1_2__ala                         ERCVGLRRLICSGEALSGASLRLAQQRLPWAAIHRLYGPAETAMDATAWTCPPDFAGDRA
                                         ***.****:*******.***:** ::*** :***.****:*:*::******* ***** .

Q2XNF8_A2_3__ala                         PIGRPIANARIYLLDPRRQPVPLGAVGELYIGGVGVARGYLNRPELTDERFLPDPFAADP
Q2XNF8_A1_2__ala                         PIGRPIANTRVYLLDRHRRPLPPGAVGELHIGGAGIGRGYLNRPELTAERFVPDPFAADP
                                         ********:*:**** :*:*:* ******:***.*:.********** ***:********

Q2XNF8_A2_3__ala                         DARMYRSGDLARHLAGGDIEFLGRNDHQVKLRGFRIELGEIETRLAAHAEVRETVVLALG
Q2XNF8_A1_2__ala                         AARMYRSGERARYRPDGELECLGRSDRQLRLRGLRIEPGDIEARLAEHPAVQRAVVLAPG
                                          *******: **: ..*::* ***.*:*::***:*** *:**:*** *. *:.:**** *

Q2XNF8_A2_3__ala                         EGSLMRLVAYVVA--AQRGDERAREP
Q2XNF8_A1_2__ala                         DGERKRLVAYLVVRPGHAGDLS----
                                         :*.  *****:*.  .: **      
