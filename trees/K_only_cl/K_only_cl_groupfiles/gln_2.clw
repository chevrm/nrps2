CLUSTAL W multiple sequence alignment


O66069_A1_gln                            GFPEERIRFMLEDSKVKVVITDSGLTFETAETVRFSEALSESLENGHPSSEAGAGHLAYI
Q65NK5_A1_gln                            GFPEERIRFMLEDSKAKAVITDSGLTFETAETVQFSEALSESRENGYPSSAAGAGHLAYI
Q45295_A1_gln                            GFPEERIPFILEDSKVKVVITDSGLTFETTETVRFSEALSESLENGHPSSEAGAGHLAYI
                                         ******* *:*****.*.***********:***:******** ***:*** *********

O66069_A1_gln                            IYTSGTTGRPKGVMIEHRQVHHLVRGLQQAVGAYDQDDLKLALLAPFHFDASVQQIFTSL
Q65NK5_A1_gln                            IYTSGTTGRPKGVMIEHRQVHHLVRGLQQAVGTYDQDDLKLALLAPFHFDASVQQIFTSL
Q45295_A1_gln                            IYTSGTTGRPKGVMIEHRQVHHLVRGLQQAVGAYDQDDLKLALLAPFHFDASVQQIFTSL
                                         ********************************:***************************

O66069_A1_gln                            LLGQTLYIVPKKTVSDGRALSDYYRRHQIDVTDGTPAHLQLLSAADDLSGVKLRHMLVGG
Q65NK5_A1_gln                            LLGQTLYIVPKKTVSDGRALSDYYRRHQIDVTDGTPAHLQLLAAADDLSGVKLRHMLVGG
Q45295_A1_gln                            LLGQTLYIVPKKTVSDGRALSDYYRRHQIDVTDGTPAHLQLLSAADDLSGVKLRHMLVGG
                                         ******************************************:*****************

O66069_A1_gln                            EALSRVATERLLQLFAETAESVPDVTNVYGPTETCVDASSFTMTNHADLQGDTAYVPIGR
Q65NK5_A1_gln                            EALSRVATERLLQLFAETAESVPAVTNVYGPTETCVDASSFTITNRTDLQYDTAYVPIGR
Q45295_A1_gln                            EALSRVATERLLQLFAETAESVPDVTNVYGPTETCVDASSFTMTNHADLQADTAYVPIGR
                                         *********************** ******************:**::*** *********

O66069_A1_gln                            PIGNNRFYILDENGALLPDGVEGELYIAGDGVGRGYLNLPDMTADKFLEDPFVPGGFMYR
Q65NK5_A1_gln                            PIGNNRFYILDENGALLPDGVEGELYIAGDGVGRGYLNLPDMTRDRFLKDPFVSGGLMYR
Q45295_A1_gln                            PIGNNRFYILDEGGALLPDGVEGELYIAGDGVGRGYLNLPDMTADKFLEDPFVPGGFMYR
                                         ************.****************************** *:**:****.**:***

O66069_A1_gln                            TGDAVRWLPDGTVDFIGRKDDQVKIRGYRIELGEIESVLQGAPAVGKAVVLARPETGGSL
Q65NK5_A1_gln                            TGDTARWLPDGTVDFIGRRDDQVKIRGFRIELGEIESVLQGAPAVEKAVVLARHETGGSL
Q45295_A1_gln                            TGDAVRWLPDGTVDFIGRKDDQVKIRGYRIELGEIESVLQGAPAVGKAVVLARPETGGSL
                                         ***:.*************:********:***************** ******* ******

O66069_A1_gln                            EVCAYVVPKQSGEIHL
Q65NK5_A1_gln                            EVCAYVVPKQGGKIHI
Q45295_A1_gln                            EVCAYVVPKQSGEIHL
                                         **********.*:**:
