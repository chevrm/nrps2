CLUSTAL W multiple sequence alignment


Q84BQ4_A3_9__ile                         NAPSERQAFMLEDSRAVALLTLSSEAIDYAAPRIDLDRLKLSGQSTHNPNLAQSSDALAY
Q84BQ4_A4_10__ile                        NAPSERQAFMVEDCQAAALLTLSREDIDYAAPRIDLDRLILSGQPTHNPNLLQSSEALAY
                                         **********:**.:*.****** * ************* ****.****** ***:****

Q84BQ4_A3_9__ile                         IMYTSGSTGTPKGVMVPHRGIARLVLNNGYADFNRQDRVAFASNPAFDASTMDIWGPLLN
Q84BQ4_A4_10__ile                        IMYTSGSTGTPKGVMVPHRAIGRLVLNNGYADFNAQDRVVFASNPAFDASTMDIWGPLLN
                                         *******************.*.************ ****.********************

Q84BQ4_A3_9__ile                         GGRVVVIDHQTLLDPNAFGRELSASGATILFVTTALFNQYVQLIPQALKGLRMVLCGGER
Q84BQ4_A4_10__ile                        GGRVVVIDHQTLLDPNAFGHELSASRATVLFVTTALFNQYVQLIPQALKGLRILLCGGER
                                         *******************:***** **:***********************::******

Q84BQ4_A3_9__ile                         GDPTSFRRLRAEAPQLRIVHCYGPTETTTYATTFEVHEVAENAESVPIGAPISNTQVYVL
Q84BQ4_A4_10__ile                        GDPAAFRRLLAEAPKLRIVHCYGPTETTTYATTFEVREVAENAESVPIGGPISNTQVYVL
                                         ***::**** ****:*********************:************.**********

Q84BQ4_A3_9__ile                         DAHQQPVPMGVTGELYIGGQGVALGYLNRADLTAEKFLPDPFSDRPGALLYRTGDLVRWL
Q84BQ4_A4_10__ile                        DAHQQPVPMGVTGELYIGGQGVALGYLNRADLTAEKFLRDPFSDQPGALLYRTGDLARWL
                                         ************************************** *****:***********.***

Q84BQ4_A3_9__ile                         APGQLDCIGRNDDQVKIRGFRIELGEIENRLLSYPGINEAVVLARRDGQEPLRLVAYYTA
Q84BQ4_A4_10__ile                        APGQLDCIGRNDDQVKIRGFRIELGEIENRLLNCQGIKEAVVLARRDGQDITRLVAYYTA
                                         ********************************.  **:***********:  ********

Q84BQ4_A3_9__ile                         HDGTLELAG
Q84BQ4_A4_10__ile                        HAGRLDSAD
                                         * * *: *.
