CLUSTAL W multiple sequence alignment


Q51338_A1_thr                            AAPEERLAHILDDSGVRLLLTQGHLLERLPRQAGVEVLAIDGLVLDGYAESDPLPTLSAD
Q51338_A2_thr                            AAPEERLAHILDDSGVRLLLTQGHLLERLPRQAGVEVLAIDGLVLDGYAESDPLPTLSAD
                                         ************************************************************

Q51338_A1_thr                            NLAYVIYTSGSTGKPKGTLLTHRNALRLFSATEAWFGFDERDVWTLFHSYAFDFSVWEIF
Q51338_A2_thr                            NLAYVIYTSGSTGKPKGTLLTHRNALRLFSATEAWFGFDERDVWTLFHSYAFDFSVWEIF
                                         ************************************************************

Q51338_A1_thr                            GALLYGGCLVIVPQWVSRSPEDFYRLLCREGVTVLNQTPSAFKQLMAVACSADMATQQPA
Q51338_A2_thr                            GALLYGGRLVIVPQWVSRSPEDFYRLLCREGVTVLNQTPSAFKQLMAVACSADMATQQPA
                                         ******* ****************************************************

Q51338_A1_thr                            LRYVIFGGEALDLQSLRPWFQRFGDRQPQLVNMYGITETTVHVTYRPVSEADLEGGLVSP
Q51338_A2_thr                            LRYVIFGGEALDLQSLRPWFQRFGDRQPQLVNMYGITETTVHVTYRPVSEADLKGGLVSP
                                         *****************************************************:******

Q51338_A1_thr                            IGGTIPDLSWYILDRDLNPVPRGAVGELYIGRAGLARGYLRRPGLSATRFVPNPFPGGAG
Q51338_A2_thr                            IGGTIPDLSWYILDRDLNPVPRGAVGELYIGRAGLARGYLRRPGLSATRFVPNPFPGGAG
                                         ************************************************************

Q51338_A1_thr                            ERLYRTGDLARFQADGNIEYIGRIDHQVKVRGFRIELGEIEAALAGLAGVRDAVVLAHDG
Q51338_A2_thr                            ERLYRTGDLARFQADGNIEYIGRIDHQVKVRGFRIELGEIEAALAGLAGVRDAVVLAHDG
                                         ************************************************************

Q51338_A1_thr                            VGGTQLVGYVVADSAEDAER
Q51338_A2_thr                            VGGTQLVGYVVADSAEDAER
                                         ********************
