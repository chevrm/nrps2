CLUSTAL W multiple sequence alignment


Q8G982_A1_8__leu                         TYPRDRLDYMLTDSAVSILLTQQSLVTNLREDLDTLKIESFCLDSDWLILENYSRENPSS
Q7WRQ4_A1_8__leu                         SYPSDRLIYMLTDAAVSILLTQQSLVDSLEAN----SAEVVCLDRDWHIIANYSQHNPVK
                                         :** *** *****:************ .*. :    . * .*** ** *: ***:.** .

Q8G982_A1_8__leu                         SVQSENLAYLIYTSGSTGKPKGVMNLHQGICNNILRTKDSYPTTNRDRLLQISSLAFDAS
Q7WRQ4_A1_8__leu                         LVKAENLAYVIYTSGSTGKPKGVMNIHKGICNNLLRTIDTYPLIAGDCILHIGVLSFDVS
                                          *::*****:***************:*:*****:*** *:**    * :*:*. *:**.*

Q8G982_A1_8__leu                         VLDIFWSLSSGMALIIPKPEGTKDLAYLIQLMIEKKVSQVFFVPSLLRLLLQQPNLENCR
Q7WRQ4_A1_8__leu                         VWEIFWSLTSGTTLVVAKPEGHKDIAYLINLIAQQQVTQVFFVPSMLRIFLQQPNLESCR
                                         * :*****:** :*::.**** **:****:*: :::*:*******:**::*******.**

Q8G982_A1_8__leu                         YLKRVFCGGEALSSELMQQFFQHFNCELHNLYGPTETSVDATCWQCPPRTDDPAIAIGRP
Q7WRQ4_A1_8__leu                         CLKRVFSGAETLSYELTQRFFERLDCELHNLYGPTEAAVDATCWQCQPQSNYQVIPIGRP
                                          *****.*.*:** ** *:**::::***********::******** *:::  .*.****

Q8G982_A1_8__leu                         IANTQIYILDRHLQPVPVGIVGELHIGGIPLARGYLNQLELTAEKFIPNPFGQGKLYKTG
Q7WRQ4_A1_8__leu                         IANTQTYILDQYLQPVPIGIAGELHIGGVQLARGYLNQPELTNERFISNPFGEGKLYKTG
                                         ***** ****::*****:**.*******: ******** *** *:**.****:*******

Q8G982_A1_8__leu                         DLVRYLADGNIEYLGRIDNQVKLRGLRIELGEIQTILDSHPQINQSVVIIQTDSEDNQRL
Q7WRQ4_A1_8__leu                         DKARYLSDGNIEYLGRIDHQVKLRGLRIELGEIEFLLDTHPQVEQTVVVLQADTSENQRL
                                         * .***:***********:**************: :**:***::*:**::*:*:.:****

Q8G982_A1_8__leu                         VAYVDSQNQALTPKE
Q7WRQ4_A1_8__leu                         VAYVVRKNSSLTPSE
                                         ****  :*.:***.*
