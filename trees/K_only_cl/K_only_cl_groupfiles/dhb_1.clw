CLUSTAL W multiple sequence alignment


Q9F638/11127897_A_dhb                    AHRSAEINYFCEFTEAVAYVIPDKHSGFDYRTLAEKVRSAVPGLRHVVVVGEA----GPF
Q9FDB2_A_dhb                             AHRLAEIRYFCQHSQAKAYLIDGAQRPFDYQALAQELLACCPTLQTVIVRGQTRVTDPKF
                                         *** ***.***:.::* **:* . :  ***::**::: :. * *: *:* *::      *

Q9F638/11127897_A_dhb                    TSLSQLYASPVDLPGPIPSDVAFFQLSGGSTGVPKLIPRTHDDYIYSLRGSVEICQLDET
Q9FDB2_A_dhb                             IELASCYSASSCQANADPNQIAFFQLSGGTTDTPKLIPRTHNDYAYSVTASVEICRFDQH
                                          .*:. *::.   ... *.::********:*..********:** **: .*****::*: 

Q9F638/11127897_A_dhb                    TVYLCALPAAHNFPLSSPGVLGTLYAGGTAVMALHPSPDQAFPLIERERITFTALVPPLA
Q9FDB2_A_dhb                             TRYLCVLPAAHNFPLSSPGALGVFWAGGCVVLSQDASPQHAFKLIEQHKITVTALVPPLA
                                         * ***.*************.**.::*** .*:: ..**::** ***:.:**.********

Q9F638/11127897_A_dhb                    MIWMDAAKARRHDLSSLKVLQVGGARLSTEAAQRVRPTLGCALQQVYGMAEGLVNYTRLD
Q9FDB2_A_dhb                             LLWMDHAEKSTYDLSSLHFVQVGGAKFSEAAARRLPKALGCQLQQVFGMAEGLVNYTRLD
                                         ::*** *:   :*****:.:*****::*  **:*:  :*** ****:*************

Q9F638/11127897_A_dhb                    DPDELVIATQGRPISPDDEIRIIDEDGRDVAPGETGQLLTRGPYTIRGYYNAEAHNARAF
Q9FDB2_A_dhb                             DSAELIATTQGRPISAHDQLLVVDEQGQPVASGEEGYLLTQGPYTIRGYYRADQHNQRAF
                                         *. **: :*******..*:: ::**:*: **.** * ***:*********.*: ** ***

Q9F638/11127897_A_dhb                    TSDGFYCTGDLVRVTPEGYLVVEGRAKDQINRGGDKIAAEEVENHLLAHPSVHDAAVISI
Q9FDB2_A_dhb                             NAQGFYITGDKVKLSSEGYVIVTGRAKDQINRGGEKIAAEEVENQLLHHPAVHDAALIAI
                                         .::*** *** *:::.***::* ***********:*********:** **:*****:*:*

Q9F638/11127897_A_dhb                    PDPFLGERTCAFVI--PREAPPTAAT
Q9FDB2_A_dhb                             SDEYLGERSCAVIVLKPEQSVNTIQ-
                                         .* :****:**.::  *.::  *   
