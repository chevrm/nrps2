CLUSTAL W multiple sequence alignment


P07702_A1_aad                            AYPPARQTIYLGVAKPRGLIVIRAAGQLDQLVEDYINDELEIVSRINSIAIQENGTIEGG
P40976_A1_aad                            AYPPARQIIYLSVAKPRALVVLEDAGVLSPTVVEYVEKSLELKTYVPALKLAKDGSLTGG
                                         ******* ***.*****.*:*:. ** *.  * :*::..**: : : :: : ::*:: **

P07702_A1_aad                            KLDNG-EDVLAPYDHYKDTRTGVVVGPDSNPTLSFTSGSEGIPKGVLGRHFSLAYYFNWM
P40976_A1_aad                            SVSKGADDILQHVLHLKSEQTGVVVGPDSTPTLSFTSGSEGIPKGVKGRHFSLAYYFDWM
                                         .:.:* :*:*    * *. :*********.**************** **********:**

P07702_A1_aad                            SKRFNLTENDKFTMLSGIAHDPIQRDMFTPLFLGAQLYVPTQDDIGTPGRLAEWMSKYGC
P40976_A1_aad                            AQEFNLSESDRFTMLSGIAHDPIQRDIFTPLFLGASLIVPTAEDIGTPGQLAQWANKYKV
                                         ::.***:*.*:***************:********.* *** :******:**:* .**  

P07702_A1_aad                            TVTHLTPAMGQLLTAQATTPFPKLHHAFFVGDILTKRDCLRLQTLAENCRIVNMYGTTET
P40976_A1_aad                            TVTHLTPAMGQLLAAQADEPIPSLHHAFFVGDILTKRDCLRLQVLANNVNVVNMYGTTET
                                         *************:***  *:*.********************.**:* .:*********

P07702_A1_aad                            QRAVSYFEVKSKNDDPNFLKKLKDVMPAGKGMLNVQLLVVNRNDRTQICGIGEIGEIYVR
P40976_A1_aad                            QRSVSYFVVPARSQDQTFLESQKDVIPAGRGMKNVQLLVINRFDTNKICGIGEVGEIYLR
                                         **:**** * ::.:* .**:. ***:***:** ******:** * .:******:****:*

P07702_A1_aad                            AGGLAEGYRGLPELNKEKFVNNWFVEKDHWNYLDK-DNGEPWRQFWLGPRDRLYRTGDLG
P40976_A1_aad                            AGGLAEGYLGNDELTSKKFLKSWFA--DPSKFVDRTPENAPWKPYWFGIRDRMYRSGDLG
                                         ******** *  **..:**::.**.  *  :::*:  :. **: :*:* ***:**:****

P07702_A1_aad                            RYLPNGDCECCGRADDQVKIRGFRIELGEIDTHISQHPLVRENITLVRKNADNEPTLITF
P40976_A1_aad                            RYLPTGNVECSGRADDQIKIRGFRIELGEINTHLSRHPNVRENITLVRRDKDEEPTLVAY
                                         ****.*: **.******:************:**:*:** *********:: *:****:::

P07702_A1_aad                            MVPRFDKPDDLSKFQSDVPKEVET
P40976_A1_aad                            IVPQGLNKDDF-----DSATESED
                                         :**:  : **:     * ..* * 
