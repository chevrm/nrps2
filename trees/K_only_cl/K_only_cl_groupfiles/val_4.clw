CLUSTAL W multiple sequence alignment


Q2VQ15_A2_5__val                         SYPLERITYMLEDSQAQLLIVQEAAMIPEGYQGKVLLLAEECWMQEEASNLELINDAQDL
Q2VQ15_A3_6__val                         SYPIDRIEHMLEDSRTKLLFVQKTEMIPASYQGEVLLLAEECWMHEDSSNLELINKTQDL
                                         ***::** :*****:::**:**:: *** .***:**********:*::*******.:***

Q2VQ15_A2_5__val                         AYVMYTSGSTGKPKGNLTTHQNILRTIINNGFIEIVPADRLLQLSNYAFDGSTFDIYSAL
Q2VQ15_A3_6__val                         AYVMYTSGSTGKPKGNLTTHQNILTTIINNGYIEIAPTDRLLQLSNYAFDGSTFDIYSAL
                                         ************************ ******:***.*:**********************

Q2VQ15_A2_5__val                         LNGATLVLVPKEVMLNPMELARIVREQDITVSFMTTSLFHTLVELDVTSMKSIRKVVFGG
Q2VQ15_A3_6__val                         LNGATLVLVPKEVMLNPMELAKIVREQDITVSFMTTSLFHTLVELDVTSMKSMRKVVFGG
                                         *********************:******************************:*******

Q2VQ15_A2_5__val                         EKASYKHVEKALDYLGEGRLVNGYGPTETTVFATTYTVDSSIKETGIVPIGRPLNNTSVY
Q2VQ15_A3_6__val                         EKASYKHVEKALDYLGEGRLVNGYGPTETTVFATTYTVDSSIKETGIVPIGRPLNNTSVY
                                         ************************************************************

Q2VQ15_A2_5__val                         ILNENNQPQPIGVPGELCVGGAGIARGYLNRPELTAERFVDNPFLVGDRMYRTGDMARFL
Q2VQ15_A3_6__val                         VLNENNQLQPIGVPGELCVGGAGIARGYLNRPELTAERFVENPFVSGDRMYRTGDLARWL
                                         :****** ********************************:***: *********:**:*

Q2VQ15_A2_5__val                         PDGNIEYIGRMDEQVKIRGHRIELGEIEKSLLEYPAISEAVLVAKRDEQGHSYLCAYVVS
Q2VQ15_A3_6__val                         PDGSMEYLGRMDEQVKVRGYRIELGEIETRLLEHPSISAAVLLAKQDEQGHSYLCAYIVA
                                         ***.:**:********:**:********. ***:*:** ***:**:***********:*:

Q2VQ15_A2_5__val                         TDQWTVAK
Q2VQ15_A3_6__val                         NGVWTVAE
                                         .. ****:
