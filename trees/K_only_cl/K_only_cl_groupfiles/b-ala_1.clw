CLUSTAL W multiple sequence alignment


O76858_A1_b-ala                          SFPANRIHHILLEAKPTLVIRDDDIDAGRFQGTPTLSTTELYAKSLQL---AGSNLLSEE
Q9FB18_A1_b-ala                          ALPEERLTRVLADARPAVVL------------TPAY----LHDRSAEITAHAGHDL----
                                         ::* :*: ::* :*:*::*:            **:     *: :* ::   ** :*    

O76858_A1_b-ala                          MLRGGNDHTAIVLYTSGSTGVPKGVRLPHESILNRLQWQWATFPYTANEAVSVFKTALTF
Q9FB18_A1_b-ala                          NLPVHPDNLAYLLHTSGSTGTPKGVLGTHRGAVNRVDWMSTAYPFRTGD-VAVARTAPGF
                                          *    *: * :*:******.****  .*.. :**::*  :::*: :.: *:* :**  *

O76858_A1_b-ala                          VDSIAELWGPLMCGLAILVVPKAVTKDPQRLVALLERYKIRRLVLVPTLLRSLLMYLKME
Q9FB18_A1_b-ala                          VDAVWELFGPLAAGVPLVLLPTDEARDPALLTAALERHRVSRMVTVPSLLTMLL-----D
                                         **:: **:*** .*:.::::*.  ::**  *.* ***::: *:* **:**  **     :

O76858_A1_b-ala                          GGGAAQKL---LYNLQIWVCSGEPLSVSLASSFFDYFDEGVHRLYNFYGSTEVLGDVTYF
Q9FB18_A1_b-ala                          ESARATDLGTRLACLRTWITSGEPLPPALARRFHDRLPG--RTLLNLYGSSETAADATAA
                                          .. * .*   *  *: *: *****. :**  *.* :    : * *:***:*. .*.*  

O76858_A1_b-ala                          ACESKKQLSLYDNVPIGIPLSNTVVYLLDADYRPVKNGEIGEIFASGLNLAAGYVNGRDP
Q9FB18_A1_b-ala                          RIDPAPGTALPERSPIGTPITGVSALVRGPDLRPLPALMPGELYAGGACVARGY-HARPA
                                           :.    :* :. *** *::.. . : ..* **:     **::*.*  :* ** :.* .

O76858_A1_b-ala                          E---RFLENPLAVEKKYARLYRTGDYGSLK-NGSIMYEGRTDSQVKIRGHRVDLSEVEKN
Q9FB18_A1_b-ala                          ETAAAFPPDPDG--GPGARMFRTGDRARLRADGRLELLGRVDRQVQIRGQRAEPGEVEHA
                                         *    *  :* .     **::**** . *: :* :   **.* **:***:*.: .***: 

O76858_A1_b-ala                          VAELPLVDKAIVLCYHAGQVDQAILAFVKLRDDAPMVTET
Q9FB18_A1_b-ala                          LLAHPAVRAAAVT---ANPDATGLWAYVRLA-PGPFA---
                                         :   * *  * *    *.    .: *:*:*   .*:.   
