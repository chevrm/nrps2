CLUSTAL W multiple sequence alignment


Q84BC7_A3_6__pro|me-pro                  EYPTERLSFILEDTQVKVLLTQRSLLDRLPQCEKAGGQGAGSRGESPSTRDRASTKGKEE
Q9RAH4_A3_pro|me-pro                     EYPPERLSFILEDAQVSVLLTQQSILDRLPQ-----------------------------
                                         ***.*********:**.*****:*:******                             

Q84BC7_A3_6__pro|me-pro                  VLSLPASYQTQLVCLDTDAELISQCSQDNLITGVQANNLGYIIYTSGSTGQPKGIAMNQL
Q9RAH4_A3_pro|me-pro                     -------HQANQVCLDTDAQLISQCSQDNLISDVQANNLAYIIYTSGSTGQPKGIAMNQL
                                                :*:: *******:***********:.******.********************

Q84BC7_A3_6__pro|me-pro                  ALCNLILWHPDNLKIARGAKTLQFASINFDVSFQEIFTTWCSGGTLFLITKELRHDTSNL
Q9RAH4_A3_pro|me-pro                     ALSNLILWHRENLKIPRGAKTLQFASINFDVSFQEIFTTWCSGGTLFLIGEELRRDTSAL
                                         **.****** :****.********************************* :***:*** *

Q84BC7_A3_6__pro|me-pro                  LRVIQEKAIQRMFLPVVGLQQLAEFAVGSELVNTHLREIITAGEQLQITPAISKWLSQLS
Q9RAH4_A3_pro|me-pro                     LGFLQQKAIERMFLPFVALQQLAEVAIGGELVNSHLREIITAGEQLQITPAISQWLSKLT
                                         * .:*:***:*****.*.******.*:*.****:*******************:***:*:

Q84BC7_A3_6__pro|me-pro                  DCTLHNHYGPSESHVATSFTLPNLVNTWPLLPPIGRPISNTQIYILDKYLQPVPIGVPGE
Q9RAH4_A3_pro|me-pro                     DCTLHNHYGPSESHLATSFTLTNSVETWPLLPPVGRPIANAQIYILDRFLQPVPVGVPGE
                                         **************:******.* *:*******:****:*:******::*****:*****

Q84BC7_A3_6__pro|me-pro                  VYIAGVLLARGYLNRPELTQEKFIQNPFGGSRGAGEQGSRGAEEQSFPSAPHSLCPSASS
Q9RAH4_A3_pro|me-pro                     LYIAGVLLSQGYFNRPELTLEKFIPNPFKRSRGAGEQGSRG---ETF-----------NC
                                         :*******::**:****** **** ***  ***********   ::*           ..

Q84BC7_A3_6__pro|me-pro                  ERLYKTGDLARYLPDGNIEYLGRIDNQVKIRGFRIELGEIEAVLSQHINVQASCA-----
Q9RAH4_A3_pro|me-pro                     DRLYKTGDLARYLSDGNIEYLGRIDNQVKIRGFRIELGEIEAVLSQ-LDVQASCAMATPA
                                         :************.******************************** ::******     

Q84BC7_A3_6__pro|me-pro                  --VVREDTPGDKRLVAYIVPQPEQRVSVNV
Q9RAH4_A3_pro|me-pro                     AGIAREDIPGNKRLVAYIVPQKEQKLTVSF
                                           :.*** **:********** **:::*..
