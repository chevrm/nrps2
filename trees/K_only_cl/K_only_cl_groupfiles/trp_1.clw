CLUSTAL W multiple sequence alignment


Q9Z4X6_A3_trp                            AHPAERITHLVSDAAPTLIVTTSALAASLPDTGTPVLLLDTPETAATLAALPGHDVTDAD
Q8CJX2_A2_11__trp                        AHPAERIAGTLDDAAPVALLTTAAVAAGLPDTDVPRLLLD--EEPAAGGGEDAADLTDAD
                                         *******:  :.****. ::**:*:**.****..* ****  * .*: ..  . *:****

Q9Z4X6_A3_trp                            RPVPLRPEHPAYMIYTSGTTGRPKGVVVTHTGLPGLLDIFTRDCAAGPGSRILQHLSPSF
Q8CJX2_A2_11__trp                        RLAPLLPGHPAYVIYTSGTTGRPKGVTVTHSGLPALLDIFTSQLDVVPGSRVLHHLSPAF
                                         * .** * ****:*************.***:***.****** :  . ****:*:****:*

Q9Z4X6_A3_trp                            DASFWELAMGLLTGATLVVAPPETTPGPELAELATRHAATHLSLTTSVLGLLPPDSLPDG
Q8CJX2_A2_11__trp                        DGGFWELAMGLLTGAALVVVEPGTVPGPALAALAVRHRVTHAAITPAVLQLIPEGALPAG
                                         *..************:***. * *.*** ** **.** .** ::*.:** *:* .:** *

Q9Z4X6_A3_trp                            LTLVVGAEAIPPELVERWSPGRTMLNSYGPTETTVCSTMSGPLSGPAVPPIGSPVANSAV
Q8CJX2_A2_11__trp                        TTLVVAAETCPPELVARWSAGRLMRNSYGPTETTVCATMSAPLAGAAVPPIGRPIADTAG
                                          ****.**: ***** ***.** * ***********:***.**:*.****** *:*::* 

Q9Z4X6_A3_trp                            YVLDAALRPVPPGVPGELYAAGAHLARGYHDRRALTAERFVANPFGEPGSRLYRTGDLVR
Q8CJX2_A2_11__trp                        YVLDDALQPVPPGVPGELYVRGPGLARGYLGRPSLTAGRFVACPFGPAGGVMYRTGDLVR
                                         **** **:***********. *. ***** .* :*** **** *** .*. :********

Q9Z4X6_A3_trp                            WRPDGQLEYLGRADTQVKIRGLRIEPTEIEAVITERPHLARAAVIVREDRPGDRRLVAYV
Q8CJX2_A2_11__trp                        HRADGDLEYLGRTDTQVKLRGMRVEPAEIEAVTAGLPGVAQAAVLVREDTPGDRRLVGYV
                                          *.**:******:*****:**:*:**:***** :  * :*:***:**** *******.**

Q9Z4X6_A3_trp                            VPEPGATVDTAE
Q8CJX2_A2_11__trp                        VPDAGASVDPGA
                                         **:.**:**.. 
