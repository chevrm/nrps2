CLUSTAL W multiple sequence alignment


Q2N3S9_A1_9__ser                         TYPEQRLEFMARDAGVAVLLTQSSFS---GVLAGFTGT-RLCLDTEASRLSSYSA-----
Q5DIP4_A3_6__ser                         EYPEERQAYMLEDSGVQLLLSQSHLK---LPLA--QGVQRIDLDQADAWLENHAE-----
Q5DIP3_A1_ser                            EYPEERQAYMLEDSGVQLLLSQSHLK---LPLA--QGVQRIDLDQADAWLENHAE-----
Q50E73_A6_11__ser                        EYPADRLAYMAGDAAPVAVLTRGDV-----ELPG--SVPRIGLDDTEIRATLATA----P
Q5DIV7_A3_ser                            EYPEERQAYMLEDSGVELLLSQSHLK---LPLA--QGVQRIDLDRGAPWFEDYSE-----
Q5DIV7_A1_ser                            EYPEERQAYMLEDSGVQLLLSQSHLK---LPLA--QGVQRIDLDQADAWLENHAE-----
Q9FB23_A1_ser                            EDPPHRLARTIANSGARLLLTETGTASRAAEAAG-PGVRALTVREGATGGERFSA-----
Q9Z4X6_A1_ser                            GYPADRIAHILRDAGAMLVLTTRDTA---ERLPG-DGTPRLLLDEPAAAGTTAAGAPAPP
                                           * .*      ::.   :*:           .   ..  : :          :      

Q2N3S9_A1_9__ser                         -DSPRVEVRPEHLAYVIYTSGSTGMPKGCMLSHRAICNRLHWMQEAYALTPLDRVLQKTP
Q5DIP4_A3_6__ser                         -NNPGVELNGENLAYVIYTSGSTGKPKGAGNRHSALSNRLCWMQQAYGLGVGDTVLQKTP
Q5DIP3_A1_ser                            -NNPGVELNGENLAYVIYTSGSTGKPKGAGNRHSALSNRLCWMQQAYGLGVGDTVLQKTP
Q50E73_A6_11__ser                        GTNPGTPVTEAHPAYMIYTSGSTGRPKGVVVSHGAIVNRLAWMQAEYRLDATDRVLQKTP
Q5DIV7_A3_ser                            -ANPDIHLDGENLAYVIYTSGSTGKPKGAGNRHSALSNRLCWMQQAYGLGVGDTVLQKTP
Q5DIV7_A1_ser                            -NNPGVELNGENLAYVIYTSGSTGKPKGAGNRHSALSNRLCWMQQAYGLGVGDTVLQKTP
Q9FB23_A1_ser                            ------DVHPEQSAYLLYTSGSTGDPKGVLVPHRAIVNRLLWMQETYRLRPGERVLHKTP
Q9Z4X6_A1_ser                            GTLPRALPAPGHPAYVIYTSGSTGRPKGVVISHRAIVNRLAWMQDTYGLEPSDRVLQKTP
                                                    : **::******* ***    * *: *** ***  * *   : **:***

Q2N3S9_A1_9__ser                         FTFDVSVWEFFWPLITGARIVLAAPGAERDPAALAGLIQEHGVTACHFVPSMLRLFLDEP
Q5DIP4_A3_6__ser                         FSFDVSVWEFFWPLMSGARLVVAAPGDHRDPAKLVALINREGVDTLHFVPSMLQAFLQDE
Q5DIP3_A1_ser                            FSFDVSVWEFFWPLMSGARLVVAAPGDHRDPAKLVKLINREGVDTLHFVPSMLQAFLQDE
Q50E73_A6_11__ser                        AGFDVSVWEFFWPLLEGAVLVFARPGGHRDAAYLAGLIERERITTAHFVPSMLRVFLEEP
Q5DIV7_A3_ser                            FSFDVSVWEFFWPLMSGARLVVAAPGDHRDPAKLVALINREGVDTLHFVPSMLQAFLQDE
Q5DIV7_A1_ser                            FSFDVSVWEFFWPLMSGARLVVAAPGDHRDPAKLVELINREGVDTLHFVPSMLQAFLQDE
Q9FB23_A1_ser                            VTFDVSMWELLWPLTAGATVVMARPGTHRDPARLVRRIAREAVTTVHFVPSMLTPFLTE-
Q9Z4X6_A1_ser                            SGFDVSVWEFFWPLVQGATLVVARPGGHTDPAYLAGTVRREGVTTLHFVPSMLDVFLREP
                                           ****:**::***  ** :*.* ** . *.* *.  : .. : : *******  ** : 

Q2N3S9_A1_9__ser                         RAAGCAS---LRHVFFSGEALPYPLMERALSTF-SAQLHNLYGPTEAAVDVSFWKCNLR-
Q5DIP4_A3_6__ser                         DVASCTS---LKRIVCSGEALPADAQQQVFAKLPQAGLYNLYGPTEAAIDVTHWTCVEE-
Q5DIP3_A1_ser                            DVASCTS---LKRIVCSGEALPADAQQQVFAKLPQAGLYNLYGPTEAAIDVTHWTCVEE-
Q50E73_A6_11__ser                        GAALCTG---LRRVICSGEALGTDLAVDFRAKL-PVPLHNLYGPTEAAVDVTHHAYEPAT
Q5DIV7_A3_ser                            DVVSCTS---LKRIVCSGEALPADAQQQVFAKLPQAGLYNLYGPTEAAIDVTHWTCVEE-
Q5DIV7_A1_ser                            DVASCTS---LKRIVCSGEALPADAQQQVFAKLPQAGLYNLYGPTEAAIDVTHWTCVEE-
Q9FB23_A1_ser                            LARGTTRLPALRRVVCSGEELPAAAVNRAAGLL-DARLYNLYGPTEAAVDVTAWPCRPP-
Q9Z4X6_A1_ser                            AAAALGGATPVRRVFCSGEALPAELRARFRAVS-DVPLHNLYGPTEAAVDVTYWPCAEDT
                                          .        ::::. *** *         .    . *:*********:**:        

Q2N3S9_A1_9__ser                         DDRKVPIGRPIANIRLYILDEQQRPVRPDQTGELYIAGVGLARGYLNRPELTAERFVRDP
Q5DIP4_A3_6__ser                         GKDAVPIGRPIANLACYILDGNLEPVPVGVLGELYLAGRGLARGYHQRPGLTAERFVASP
Q5DIP3_A1_ser                            GKDAVPIGRPIANLACYILDGNLEPVPVGVLGELYLAGRGLARGYHQRPGLTAERFVASP
Q50E73_A6_11__ser                        GTATVPIGRPIWNIRTYVLDAALRPVPPGVPGELYLAGAGLARGYHGRPALTAERFVACP
Q5DIV7_A3_ser                            GKDAVPIGRPIANLACYILDGNLEPVPVGVLGELYLAGQGLARGYHQRPGLTAERFVASP
Q5DIV7_A1_ser                            GKDAVPIGRPIANLACYILDGNLEPVPVGVLGELYLAGQGLARGYHQRPGLTAERFVASP
Q9FB23_A1_ser                            EPGPVPIGLPIANTTTEVLDGRLRPLPRPVPGELYLGGACLAHGYHHDPALTAARFLPAP
Q9Z4X6_A1_ser                            GDGPVPIGRPVWNTRMYVLDAALRPVPAGVPGELYIAGVQLARGYLGRPALSAERFTADP
                                             **** *: *    :**   .*:     ****:.*  **:**   * *:* **   *

Q2N3S9_A1_9__ser                         FDPSPGARMYKTGDRAAWLPDGNIDFLGRMDGQIKLRGLRIELGEIEAALLGHEAVREAA
Q5DIP4_A3_6__ser                         F--VAGERMYRTGDLARYRADGVIEYAGRIDHQVKLRGLRIELGEIEARLLEHPWVREAA
Q5DIP3_A1_ser                            F--VAGERMYRTGDLARYRADGVIEYAGRIDHQVKLRGLRIELGEIEARLLEHPRVREAA
Q50E73_A6_11__ser                        FG-VPGERMYRTGDLVRWRVDGTLEFVGRADDQVKVRGFRVELGEVEGAVAAHPDVVRAV
Q5DIV7_A3_ser                            F--VAGERMYRTGDLARYRADGVIEYAGRIDHQVKLRGLRIELGEIEARLLEHPWVREAA
Q5DIV7_A1_ser                            F--VAGERMYRTGDLARYRADGVIEYAGRIDHQVKLRGLRIELGEIEARLLEHPRVREAA
Q9FB23_A1_ser                            G----GGRRYRTGDLVRQRADGALVFRGRTDDQVKIGGIRVEPGEVAEALRALPGVADAA
Q9Z4X6_A1_ser                            HG-APGSRMYRTGDLARWNHDGSLDYLGRADHQVKLRGFRIELGEIEAALVRQPEIAQAA
                                              * * *:*** .    ** : : ** * *:*: *:*:* **:   :     :  *.

Q2N3S9_A1_9__ser                         VAVRDADSGDPRLVAYVVLR------------DGASFSP---------------------
Q5DIP4_A3_6__ser                         VLAVDGK----QLVGYVVLE------------SEGGD-----------------------
Q5DIP3_A1_ser                            VLAVDGR----QLVGYVVLE------------SEGGD-----------------------
Q50E73_A6_11__ser                        VVVREDRPGDHRLVAYV-------------------------------------------
Q5DIV7_A3_ser                            VLAVDGK----QLVGYVVLE------------SESGD-----------------------
Q5DIV7_A1_ser                            VLAVDGR----QLVGYVVLE------------SEGGD-----------------------
Q9FB23_A1_ser                            VVPHDG-----RLAAYAVAD--------PVGPAPAADA----------------------
Q9Z4X6_A1_ser                            VVLREDRPGDQRLVAYTVPARDADTLTGP--PAEAGTHPGPGAAPDTDPATGPAAGTDSG
                                         *   :      :*..*.                                           

Q2N3S9_A1_9__ser                         ---------------------------QV---
Q5DIP4_A3_6__ser                         --------------------------------
Q5DIP3_A1_ser                            --------------------------------
Q50E73_A6_11__ser                        ---------------------------TVGGV
Q5DIV7_A3_ser                            --------------------------------
Q5DIV7_A1_ser                            --------------------------------
Q9FB23_A1_ser                            --------------------------------
Q9Z4X6_A1_ser                            PGSGTGSGTGSGTGSGARPGPDGTATHTVAGA
                                                                         
