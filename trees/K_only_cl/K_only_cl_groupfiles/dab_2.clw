CLUSTAL W multiple sequence alignment


Q9I157_A3_4__dab                         GHPTQRLTRIVELSRTLVLVCTQACREQALA--LFDELGCVDRPRLLVWDEIQQGEG-AE
Q5DIS7_A2_dab                            SLPRSRLVNLLKLGGVPALVVGEG--HQALARELLEELPEESRPALLDW-AAQQGQGKCE
                                         . * .**..:::*. . .**  :.  .****  *::**   .** ** *   ***:* .*

Q9I157_A3_4__dab                         HDPQVYSGPQNLAYVIYTSGSTGLPKGVMVEQAGMLNNQLSKVPYLELDENDVIAQTASQ
Q5DIS7_A2_dab                            QRPGIVAPTNGLAYVIYTSGSTGHPKGVMVEQAGMLNNQLSKVPLLALDENDVIAQTASQ
                                         : * : : .:.************ ******************** * *************

Q9I157_A3_4__dab                         SFDISVWQFLAAPLFGARVAIVPNAVAHDPQGLLAHVGEQGITVLESVPSLIQGMLAEER
Q5DIS7_A2_dab                            SFDISVWQFLAAPLFGAQVDILPNDIAHDPLALSRRVRERGITVLELVPSLIQEVLDDPQ
                                         *****************:* *:** :**** .*  :* *:****** ****** :* : :

Q9I157_A3_4__dab                         QALDGLRWMLPTGEAMPPELARQWLKRYPRIGLVNAYGPAECSDDVAFFRVDLASTESTY
Q5DIS7_A2_dab                            ETLPGLRWMLSTGEALSPELARRWLTRYPQVGLMNAYGPAECSDDVSFFRVDTQSTGGTY
                                         ::* ******.****:.*****:**.***::**:************:*****  ** .**

Q9I157_A3_4__dab                         LPIGSPTDNNRLYLLGAGADDAFELVPLGAVGELCVAGTGVGRGYVGDPLRTAQAFVPHP
Q5DIS7_A2_dab                            LPIGQATDNNHLQVL----DDDLLPVPLGGIGELYVSGTGVGRGYLADPGRTALAFLPDP
                                         ****..****:* :*    ** :  ****.:*** *:********:.** *** **:*.*

Q9I157_A3_4__dab                         FG-APGERLYRTGDLARRRADGVLEYVGRIDHQVKIRGFRIELGEIEARLHERADVREAA
Q5DIS7_A2_dab                            YAQVPGSRIYRTGDLACRGKGDQLEYVGRIDHQVKVRGFRIELGEIESRLLELPIVREAV
                                         :. .**.*:******* *  .. ************:***********:** * . ****.

Q9I157_A3_4__dab                         VAVQEGANGKYLVGYLVPGETPRSSADS
Q5DIS7_A2_dab                            VLAQDGPTGKSLAAYLVPAE-----SDT
                                         * .*:*..** *..****.*     :*:
