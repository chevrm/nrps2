CLUSTAL W multiple sequence alignment


O33743_A_b-lys                           GHPEARLESVLALAGCREAVATAET-GWQPPVETVIRPRWTQTPVTTVLQGTLPESGAR-
Q6WZ98_A1_b-lys                          SDPEDRLREVVALTGARAVLGRAESLGELPGLGIPVIP---AEPPGDLAGGAPPATRADA
                                         ..** **..*:**:*.* .:. **: *  * :   : *     *   :  *: * : *  

O33743_A_b-lys                           ----PEDPAYMLFTSGSTGEPKGVVVPHRAPAAVVPLLRDLYGIGPEETVLHFHGAGGDT
Q6WZ98_A1_b-lys                          EPPLPDDLAYVMLTSGTTGTPKAVLVPHRAVTRAARSLVPLFGVTSTDRVLHWTSLIWDT
                                             *:* **:::***:** **.*:***** : ..  *  *:*: . : ***: .   **

O33743_A_b-lys                           SLEEILPALTGGATLVID-----DAAPEGFARVAEEQQVSVAILPTGFWSSLTGDLLHQG
Q6WZ98_A1_b-lys                          SGEEIYPALLGGAALVVDGRVETRSVPALLAAV-REHRVTVVDLPTAMWNELAHYLALGG
                                         * *** *** ***:**:*      :.*  :* * .*::*:*. ***.:*..*:  *   *

O33743_A_b-lys                           ARLPESLRTVVIGGEAVRADMLERW-RRLPGTDGVRLLNTYGSTETALVTHAAQLAGPGA
Q6WZ98_A1_b-lys                          EELPPALRLVVIGGEAAHARTVRLWNERVP--DRVRLLNTYGQTETVMVTHAAELGGPAG
                                          .** :** *******.:*  :. * .*:*  * ********.***.:*****:*.**..

O33743_A_b-lys                           PALPGTGLDLPIGHPLPHVRQRV----DDRGELSIAGPGLALGYHGNPGATSARFRERDG
Q6WZ98_A1_b-lys                          RAL-RDGDPVPIGRPLPHIRQVLVPSDDGPDELWTGGPGLAWGYADRPALTAAAFGPAPG
                                          **   *  :***:****:** :    *. .**  .***** ** ..*. *:* *    *

O33743_A_b-lys                           T--RWYRTGDLVTEAPGGILVFRGRSDHQVKIRGFRVDLLDVEALIGRCEGVAAVAAARV
Q6WZ98_A1_b-lys                          AGGRFYRTGDLVRTLPDGSLVHAGRADRRLKVRGVRVEPAEVERAMTTCPGVVAAAVFPV
                                         :  *:*******   *.* **. **:*:::*:**.**:  :**  :  * **.*.*.  *

O33743_A_b-lys                           -DRTEHGVLAAFFV---ALPHREPDE
Q6WZ98_A1_b-lys                          GDDPEHLRLYGAFVPSKSGPATE---
                                          * .**  * . **   : *  *   
