CLUSTAL W multiple sequence alignment


Q9Z4X5_A3_9__asn                         GSPRERIATMAEDAAPVCALTTSAVPAGVFPAELPRLLLDDPDVTARLAAQPAHDLTDED
Q9Z4X5_A3_asn                            GSPRERIATMAEDAAPVCALTTSAVPAGVFPAELPRLLLDDPDVTARLAAQPAHDLTDED
                                         ************************************************************

Q9Z4X5_A3_9__asn                         RTQPLSPWNAAYIIYTSGSTGRPKGVLVEHQPVLNYLAVSAELYPGVAGNALLHSPLSFD
Q9Z4X5_A3_asn                            RTQPLSPWNAAYIIYTSGSTGRPKGVLVEHQPVLNYLAVSAELYPGVAGNALLHSPLSFD
                                         ************************************************************

Q9Z4X5_A3_9__asn                         LTVTGLFAPLLNGGCVHLADLEELHARALDGEVPDLPQTTFLKATPSHLPLITGLPGVCV
Q9Z4X5_A3_asn                            LTVTGLFAPLLNGGCVHLADLEELHARALDGEVPDLPQTTFLKATPSHLPLITGLPGVCV
                                         ************************************************************

Q9Z4X5_A3_9__asn                         PDGELVLGGESLTGRAVRTLLAAHPGARVLNEYGPTETIVGCTTWRVEAPDDLADGVLTI
Q9Z4X5_A3_asn                            PDGELVLGGESLTGRAVRTLLAAHPGARVLNEYGPTETIVGCTTWRVEAPDDLADGVLTI
                                         ************************************************************

Q9Z4X5_A3_9__asn                         GRPFPNTRMLVLDPYLQPVPAGVPGELYVSGVQLARGYLNRPGQSASRFVANPFEGPGER
Q9Z4X5_A3_asn                            GRPFPNTRMLVLDPYLQPVPAGVPGELYVSGVQLARGYLNRPGQSASRFVANPFEGPGER
                                         ************************************************************

Q9Z4X5_A3_9__asn                         MYRTGDIVRWNRRGDLEFISRVDDQVKIRGFRVELGEVESALSRQPGVPEAVAVVREDRP
Q9Z4X5_A3_asn                            MYRTGDIVRWNRRGDLEFISRVDDQVKIRGFRVELGEVESALSRQPGVPEAVAVVREDRP
                                         ************************************************************

Q9Z4X5_A3_9__asn                         GDRRLVAYLVTGAGPVPVPSDE
Q9Z4X5_A3_asn                            GDRRLVAYLVTGAGPVPVPSDE
                                         **********************
