CLUSTAL W multiple sequence alignment


Q884E5_A1_asp                            KAPDARLQQMLSNAQTKVLLCAEGDRRETSLNVAGCQGLAWTPALWQSLPTSRPDIELPA
Q884E3_A1_asp                            AQPAERLQQLTRDSGAALLIHASGDDKAAQLGV--CPVLAFDAALWSEVDGGELSVRIIA
                                           *  ****:  :: : :*: *.** : :.*.*  *  **: .***..:  .. .:.: *

Q884E5_A1_asp                            DSAAYVIHTSGSTGQPKGVVVSQGALASYVRGLLEQLQLAPEVSMALVSTIAADLGHTVL
Q884E3_A1_asp                            EQPAYIIYTSGSTGQPKGVVISHGALANYVQGVLARLSLNDGASMAMVSTVAADLGHTLL
                                         :..**:*:************:*:****.**:*:* :*.*   .***:***:*******:*

Q884E5_A1_asp                            FGALCSGRTLHVLTESLGFDPDAFATYMAEHQIGVLKIVPGHLAALLQAAQPADVLPQHA
Q884E3_A1_asp                            FGALASGRPLHLLSHEQAFDPDGFARYMAEHQVEVLKIVPSHLQGLLQAAHPADVLPSQL
                                         ****.***.**:*:.. .****.** ******: ******.** .*****:******.: 

Q884E5_A1_asp                            LIVGGEACSPALVEQVRQLKPGCRVINHYGPSETTVGVLTHEVPALSELNAIPQVSRFPQ
Q884E3_A1_asp                            LMLGGEASSWALIEQVRALKPGCRIVNHYGPTETTVGILTHEV-----------AERLNA
                                         *::****.* **:**** ******::*****:*****:*****           ..*:  

Q884E5_A1_asp                            CSAHSGDPLRSVPVGAPLPGASAYVLDDVLNPVATQVAGELYIGGDSVALGYLGQPALTA
Q884E3_A1_asp                            C--------RSVPVGQPLANCKARVLDAYLNPVAERVSGELYLGGQGLAQGYLGRAAMTA
                                         *        ****** **....* ***  ***** :*:****:**:.:* ****:.*:**

Q884E5_A1_asp                            ERFVPDPFAQDGARVYRSGDRMRHNHQGLLEFIGRADDQVKVRGYRVEPAEVAQVLLSLP
Q884E3_A1_asp                            ERFVPDPDA-DGQRLYRAGDRARWV-DGVLEYLGRADDQVKIRGYRVEPGEVGQVLQTLE
                                         ******* * ** *:**:*** *   :*:**::********:*******.**.*** :* 

Q884E5_A1_asp                            SVAQVSVLALPVDEDESRLQLVAYCVAAAAASLTVDS
Q884E3_A1_asp                            NVAEAVVLAQPLESDETRLQLVAYCVAAVGVRLNVES
                                         .**:. *** *::.**:***********... *.*:*
