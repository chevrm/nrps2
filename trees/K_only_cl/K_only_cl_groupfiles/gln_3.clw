CLUSTAL W multiple sequence alignment


Q9R9J0_A3_gln                            DSPVERIHYIARDSGINILLTHGELPENLNFSGTCINMKEEQAYHETDINLAVPCQFDQL
Q93I55_A3_gln                            DSPSERIRYILNDSSISVLLYCGKLQDDIGFSGTCIDLMEEHFYHEKDSSLALSYQSSQL
                                         *** ***:** .**.*.:**  *:* :::.******:: **: ***.* .**:. * .**

Q9R9J0_A3_gln                            AYCIYTSGTTGTPKGTPKGTLIEHRQVIHLIEGLRNAVYSAYDGVLHVAMLAPYYFDASV
Q93I55_A3_gln                            AYAIYTSGTT----GKPKGTLIEHRQVIHLIEGLSRQVYSAYDAELNIAMLAPYYFDASV
                                         **.*******    *.****************** . ******. *::************

Q9R9J0_A3_gln                            QQIYASLLLGHTLFIVPKEAVSDGEALCQYYRQHRIDVTDGTPAHLKLLVAADDGEGVPL
Q93I55_A3_gln                            QQMYASLLSGHTLFIVPKEIVSDGAALCRYYRQHSIDITDGTPAHLKLLIAAGDLQGVTL
                                         **:***** ********** **** ***:***** **:***********:**.* :**.*

Q9R9J0_A3_gln                            RHLLIGGEALPKTTVTKFIHLFGADRAAPAITNVYGPTETCVDASLFNIEVSADAWTRSQ
Q93I55_A3_gln                            QHLLIGGEALSKTTVNKLKQLFGEHGAAPGITNVYGPTETCVDASLFNIECSSDAWARSQ
                                         :*********.****.*: :*** . ***.******************** *:***:***

Q9R9J0_A3_gln                            VHIPIGKPLGNNRMYILDSQQKLQPVGVQGELYIAGDGVGRGYLNLPELTNKKFVNDPFV
Q93I55_A3_gln                            NYVPIGKPLGRNRMYILDSKKRLQPKGVQGELYIAGDGVGRGYLNLPELTDEKFVADPFV
                                          ::*******.********:::*** ************************::*** ****

Q9R9J0_A3_gln                            PSGRMYRTGDLARLLPDGNIEFIERVDHQVKIHGFRIELGEIESIMLNIPEIQEAVASVL
Q93I55_A3_gln                            PEDRMYRTGDLARLLPDGNIEYIGRIDHQVKIQGFRIELGEIESVMLNVPDIQEAAAAAL
                                         *..******************:* *:******:***********:***:*:****.*:.*

Q9R9J0_A3_gln                            EDADGEHYICGYYVANKPFPTSQ
Q93I55_A3_gln                            KDADDEYYLCGYFAADKTIQISE
                                         :***.*:*:***:.*:*.:  *:
