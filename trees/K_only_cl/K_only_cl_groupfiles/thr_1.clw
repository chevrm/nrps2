CLUSTAL W multiple sequence alignment


Q884E4_A1_thr                            DYPQDRLSFLMQDSGIELLLTQTQLLGQLPIPAHVQTLDLADALDGYSTENPINQTSPDN
Q884E5_A2_thr                            DYPQDRLSFLMQDSGIELLLSQTQLLGQLPIPAHVQTLDLADALDGYSTENPINQTSPDN
                                         ********************:***************************************

Q884E4_A1_thr                            LAYVIYTSGSTGKPKGTLLAHHNLMRLFAATDDWFKFNEKDVWTLFHSFAFDFSVWEIFG
Q884E5_A2_thr                            LAYVIYTSGSTGKPKGTLLAHHNLMRLFAATDDWFKFNEKDVWTLFHSFAFDFSVWEIFG
                                         ************************************************************

Q884E4_A1_thr                            ALLHGGRLVIVPREVTRSPEEFHALLVDQQVTVLNQTPSAFKQLMRVACDSPVPMSLQKV
Q884E5_A2_thr                            ALLHGGRLVIVPCEVTRSPEEFHALLVDQQVTVLNQTPSAFKQLMRVACDSTSVLSLETI
                                         ************ **************************************.  :**:.:

Q884E4_A1_thr                            IFGGEALDVASLKPWFARFGDQMPRLINMYGITETTVHVTYRPITLADTHNPASPIGEAI
Q884E5_A2_thr                            IFGGEALDVASLKPWFARFGDQTPRLINMYGITETTVHVTYRPITLADTHNPASPIGEAI
                                         ********************** *************************************

Q884E4_A1_thr                            ADLSWYVLDADFNPVAQGCSGELHIGHAGLARGYHHRAALTAERFVPDPFSNDGGRLYRT
Q884E5_A2_thr                            ADLSWYVLDADFNTVAQGCSGELHIGHAGLARGYHNRAALTAERFVPDPFANDGGRLYRT
                                         *************.*********************:**************:*********

Q884E4_A1_thr                            GDLARYKTAGTIEYAGRIDHQVKIRGFRIELGEIEARLQAHSAVREVIVLAVDGQLAAYL
Q884E5_A2_thr                            GDLARYKTAGTIEYAGRIDHQVKIRGFRIELGEIEARLQAHSAVREVIVLAVDGQLAAYL
                                         ************************************************************

Q884E4_A1_thr                            LPAQPDQDQQA
Q884E5_A2_thr                            LPAQPDQDQQA
                                         ***********
