CLUSTAL W multiple sequence alignment


Q50E73_A2_7__asp                         EWPVERVGWVLADAGVGLVVVGEGLSHVVGDFPGGEVFEFSRVVRESCLVELVAADGVEV
Q50E73_A4_9__asp                         EWPVERVGWVLADAGVGLVVVGEGLSHVVGDFPGGEVFEFSRVVRESCLVELVAADGVEV
                                         ************************************************************

Q50E73_A2_7__asp                         RNVTDGERASRLLPGHPLYVVYTSGSTGRPKGVVVTHASVGGYLARGRDVYAGAVGGVGF
Q50E73_A4_9__asp                         RNVTDGERASRLLPGHPLYVVYTSGSTGRPKGVVVTHASVGGYLARGRDVYAGAVGGVGF
                                         ************************************************************

Q50E73_A2_7__asp                         VHSSLAFDLTVTVLFTPLVSGGCVVLGELDESAQGVGASFVKVTPSHLGLLGELEGVVAG
Q50E73_A4_9__asp                         VHSSLAFDLTVTVLFTPLVSGGCVVLGELDESAQGVGASFVKVTPSHLGLLGELEGVVAG
                                         ************************************************************

Q50E73_A2_7__asp                         NGMLLVGGEALSGGALREWRERNPGVVVVNAYGPTELTVNCAEFLIAPGEEVPDGPVPIG
Q50E73_A4_9__asp                         NGMLLVGGEALSGGALREWRERNPGVVVVNAYGPTELTVNCAEFLIAPGEEVPDGPVPIG
                                         ************************************************************

Q50E73_A2_7__asp                         RPFAGQRMFVLDAALRVVPVGVVGELYVAGVGLARGYLGRAGLTAERFVACPFGAPGERM
Q50E73_A4_9__asp                         RPFAGQRMFVLDAALRVVPVGVVGELYVAGVGLARGYLGRVGLTAERFVACPFGVPGERM
                                         ****************************************.*************.*****

Q50E73_A2_7__asp                         YRTGDLVRWRVDGALEFVGRADDQVKVRGFRVELGEVEGAVAAHPDVVRAVVVVREDRPG
Q50E73_A4_9__asp                         YRTGDLVRWRVDGALEFVGRADDQVKVRGFRVELGEVEGAVAAHPDVVRAVVVVREDRPG
                                         ************************************************************

Q50E73_A2_7__asp                         DHRLVAYVTGVDTGG-
Q50E73_A4_9__asp                         DHRLVAYVTAGGVGGD
                                         *********. ..** 
