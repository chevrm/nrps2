CLUSTAL W multiple sequence alignment


Q8G983/24744797_A2_7__ala                HYPTERLELILTDSQVPVLLTQQTLLSHLSHYSGDRVVLDRDAAIIAQQPKTNPKSQVSS
Q9S1A9_A2_7__ala                         AYPTERLNVILEDAQVSLLLTQAKLVEKLGNYPGNLVILEAEARNIALESPENLSLPVSS
Q6E7J5_A1_ala                            SYPSERLAYTLEDAAVPVLLASESVLDSLPEHQAKIVCLDTIGETITNEGTDNPASQVTP
                                          **:***   * *: *.:**:. .::. * .: .. * *:  .  *: :   *    *:.

Q8G983/24744797_A2_7__ala                SNSVYVIYTSGSTGKPKGVVIEHRSTVTLLDWARTVFSFDELAGVLASTSVCFDLSVFEL
Q9S1A9_A2_7__ala                         SNTAYVIYTSGSTGKPKGVVIEHHSTTTLLNWSKEVFSSEELAGVLGSTSICFDLSVFEL
Q6E7J5_A1_ala                            ENLAYVIYTSGSTGKPKGVAIEHHSPVALCYWASQVFTIKELAAVLASTSICFDLSVFEI
                                         .* .***************.***:*..:*  *:  **: .***.**.***:********:

Q8G983/24744797_A2_7__ala                FLPLTVGGAVILADNALALPNLAAATEVTLINTVPTAIAQLLALEAIPQSVRTVNLAGEP
Q9S1A9_A2_7__ala                         FLPLAVGGKIILAQNVLDLPSLSAAKEVTLINTVPTAIAQLLEIEAIPETVQTVNFAGEA
Q6E7J5_A1_ala                            FVTLSQGGQVILAQNALDLPNIKS--EITLINTVPSAITELSRINAIPDSVKVINLAGEP
                                         *:.*: ** :***:*.* **.: :  *:*******:**::*  ::***::*:.:*:***.

Q8G983/24744797_A2_7__ala                LSNSLVQKLYQLPQIERVYNLYGPSEDTSYSTFSCIEKGATTQPSIGRPIAHSQVYLLNA
Q9S1A9_A2_7__ala                         LSNQLVQKLYQQENIKNVYNLYGPSEDTTYSTFSLVPKKDHGQPSIGRPIANTQVYILDS
Q6E7J5_A1_ala                            FSNQLLQRLYQQENIQSVYNLYGPSEYTTYTTFTLLKKGTTTQPTIGHPIANTQVYILDS
                                         :**.*:*:***  :*: ********* *:*:**: : *    **:**:***::***:*::

Q8G983/24744797_A2_7__ala                THQPVPLGTIGELYMGGAGLARGYLHRPELTAEKFIANPFHSPLQKYQGLSPSPKLYKTG
Q9S1A9_A2_7__ala                         FKQPVPLGTIGDLYIGGKGLARCYLNQPELTAEKFISNPFSN--------EPKAKLYKTG
Q6E7J5_A1_ala                            HLQPVPIGVVGELYIGGDGLARGYHNRPELTAEKFIPNPFDN--------STSTRLYKTG
                                           ****:*.:*:**:** **** * ::*********.*** .        ....:*****

Q8G983/24744797_A2_7__ala                DLARYRPDGNLEFLGRSDYQIKLRGFRIELGEIEVVLENCPGVKQAVVLRREDLVGGAKL
Q9S1A9_A2_7__ala                         DLARYLPDGNIDFLGRGDNQVKLRGFRIELGEIEATLGTYPQVKQKVVKVWEDSYRNKRL
Q6E7J5_A1_ala                            DLARYLPDGNIAFIGRIDHQVKIRGFRIEMGEIEAFVSQYPDVKENAVIAQSDPAGGKRL
                                         ***** ****: *:** * *:*:******:****. :   * **: .*   .*   . :*

Q8G983/24744797_A2_7__ala                VAYVVSENSTLTTQD------
Q9S1A9_A2_7__ala                         VAYLVGENGPINTED------
Q6E7J5_A1_ala                            VVYIVPKQE--STQDTSLIPH
                                         *.*:* ::   .*:*      
