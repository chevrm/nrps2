CLUSTAL W multiple sequence alignment


Q45R85_A4_thr                            DHPAERTSYILHDCRPVAVLSTTAVRETLHGTVGEAVGEVPWLLLDEPATGGATAGHSAA
O87314_A2_thr                            GAPAARVQHILADSAPVCLLTDTAERFT----------GVPHVILAEAAQNPA---RPQA
                                         . ** *..:** *. **.:*: ** * *           ** ::* *.* . *   :. *

Q45R85_A4_thr                            PVTDADRRSPLLPDHPAYTIYTSGSTGRPKGVVVSHANVSRLLTACRAAV-DFGPDDVWT
O87314_A2_thr                            PTVS--------PDHAAYVIYTSGSTGVPKGVEVTHRNVAALFAGTTSGLYDFGPDDVWT
                                         *...        ***.**.******** **** *:* **: *::.  :.: *********

Q45R85_A4_thr                            LFHSSAFDFSVWEMWGPLAHGGRLVVVPHDVARSPGDLLDLLGRERVTVLSQTPSAFLQL
O87314_A2_thr                            MFHSAAFDFSVWELWGPLLHGGRLVVVEHDVARDPERFVDLLARERVTVLNQTPSAFYPL
                                         :***:********:**** ******** *****.*  ::***.*******.******  *

Q45R85_A4_thr                            LRAESDLGVPPRTTAALRYVVFGGEALDTAQLAPW----RGRPVRLVNMYGITETTVHVT
O87314_A2_thr                            LEADARL----RRQLALRYVIFGGEALDVRRLAPWYANHESHSPRLVNMYGITETCVHVS
                                         *.*:: *    *   *****:*******. :****    ..:. *********** ***:

Q45R85_A4_thr                            HLELDDAAVDRGGSPIGTPLNDLRAHVLDQGLLPVPVGVVGELYVAGPGLARGYRRRPGL
O87314_A2_thr                            HRALDTADTGAAGSVIGGPLPGLRIHLLDNNLQPVPAGVVGEMYIAGGQVARGYTGRPGL
                                         *  ** * .. .** ** ** .** *:**:.* ***.*****:*:**  :****  ****

Q45R85_A4_thr                            SATRFVADPFDTGG-RMYRTGDLVRRTQDGGLHYVGRSDSQVKLRGYRIEPGEIEAAARR
O87314_A2_thr                            TATRFVANPFDGAGERLYRSGDLAMWTDAGELVYLGRSDAQVKVRGYRIELGEVEAALVT
                                         :******:*** .* *:**:***.  *: * * *:****:***:****** **:***   

Q45R85_A4_thr                            HPDVAQAATAVHGEGPQDRYLVCYVV-PAADTDPD
O87314_A2_thr                            LPGVTNAAADVRHDDTGRARLIGYVVGDALDIGA-
                                          *.*::**: *: :..    *: ***  * * .. 
