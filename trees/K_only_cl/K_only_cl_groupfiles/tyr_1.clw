CLUSTAL W multiple sequence alignment


Q70AZ9_A2_tyr                            AYPAERIRLILADADPALVVCAGKTREAVPAAFADRLLVVDEMDLTGGSAARLPRVRPGD
Q8KLL3_A2_tyr                            AYPAERVSLLLGDADPVLVVCDGKARDAVPEEFADRSLVIDEVDLSAVPDAELPRVGPDD
                                         ******: *:*.****.**** **:*:***  **** **:**:**:. . *.**** *.*

Q70AZ9_A2_tyr                            AAYVIYTSGSTGRPKGVVVPHAGLGNLALAQIDRFGVSPSSRVLQFAALGFDAMVSEVLM
Q8KLL3_A2_tyr                            VAYVIYTSGSTGTPKGVVVTHAGLGNLAAAQIDRFAVSPSSRVLQFAALGFDATVSEALM
                                         .*********** ******.******** ******.***************** ***.**

Q70AZ9_A2_tyr                            ALLSGARLVMAPEHQLPPRVSLAEALQRWDVTHVTVPPSVLATAEALPARLETVVVAGEA
Q8KLL3_A2_tyr                            ALLSGATLVMAPKQDLPPRVSLAEALERWDVTHVTVPPSVLATADVLPESLETVVVAGEA
                                         ****** *****:::***********:*****************:.**  **********

Q70AZ9_A2_tyr                            CPPSLADRWSAGLRLVNAYGPTEATVCAAMSMPLVASRPVVPIGTPIAGGRCYVLDAFLR
Q8KLL3_A2_tyr                            CPPGLADRWSEGRRLINAYGPTEATVCAAMSMPLTAGRDVVPIGEPIAGSRCHVLDAFLR
                                         ***.****** * **:******************.*.* ***** ****.**:*******

Q70AZ9_A2_tyr                            PLPPGLTGELYVAGIGLARGYLGRAALTAERFVADPFVPGERMYRTGDLAYRTGEGELVF
Q8KLL3_A2_tyr                            PLPPGVTGELYVSGIGLARGYLGRAALTAERFVADPFVPGERMYRTGDLAHLTSSGELVF
                                         *****:******:*************************************: *..*****

Q70AZ9_A2_tyr                            AGRADDQVKVRGFRIEPGEVESALSGHPGVAQAAVIVRGDRLLAYVSPAGVDPQAV
Q8KLL3_A2_tyr                            AGRADDQVKLRGFRIEPGEIESVLSGHPQVAQAAVTVRDDRLLAHVSPTEVDPHAV
                                         *********:*********:**.***** ****** **.*****:***: ***:**
