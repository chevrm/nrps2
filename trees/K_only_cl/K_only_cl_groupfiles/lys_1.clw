CLUSTAL W multiple sequence alignment


Q45R83_A1_8__lys                         RSPHERLAAVERDVAPLLVLAERATEAAVADLAAPVLVLDDPSTEAAIDALDPGPVTDAD
O05819_A1_lys                            AMPGERVAEILRQT------------------SAPV-VIDEGVFAASVGA----DILEED
                                           * **:* : *:.                  :*** *:*:    *::.*     : : *

Q45R83_A1_8__lys                         RTAPLLPGHAAYVIHTSGSTGRPKGVTVDHRGLSRLLQAH-RRVTFSRIRPSA---GGPG
O05819_A1_lys                            RAITVPVDQAAYVIFTSGTTGTPKGVIGTHRALSAYADDHIERV----LRPAAQRLGRPL
                                         *: .:  .:*****.***:** ****   **.**   : * .**    :**:*   * * 

Q45R83_A1_8__lys                         RAAHVSSFSFDASWDPLLAMVAGHELHMIDEDLRFDPPGVVAYFRDRRIDYVDLTPTYFR
O05819_A1_lys                            RIAHAWSFTFDAAWQPLVALLDGHAVHIVDDHRQRDAGALVEAIDRFGLDMIDTTPSMFA
                                         * **. **:***:*:**:*:: ** :*::*:. : *. .:*  :    :* :* **: * 

Q45R83_A1_8__lys                         SLLDAGLLEEGFPCP-SLVALGGEAMDGELWERLRAAAPR--VTAMNTYGPTETAVDAVV
O05819_A1_lys                            QLHNAGLLDRA---PLAVLALGGEALGAATWRMIQQNCARTAMTAFNCYGPTETTVEAVV
                                         .* :****:..   * :::******:..  *. ::  ..*  :**:* ******:*:***

Q45R83_A1_8__lys                         TVLGDLPPGTIGRPVPRWRAYVLDAGLRPVPPGVLGELYLAGPGVARGYLGQHALTAERF
O05819_A1_lys                            AAVAEHARPVIGRPTCTTRAYVMDSWLRPVPDGVAGELYLAGAQLTRGYLGRPAETAARF
                                         :.:.: .  .****.   ****:*: ***** ** *******. ::*****: * ** **

Q45R83_A1_8__lys                         VACPFGKPGERMYRTGDLARWLPDGHLVYVGRGDEQVKIRGFRIEPGEVEAALRELEGVA
O05819_A1_lys                            VAEPNGR-GSRMYRTGDVVRRLPDGGLEFLGRSDDQVKIRGFRVEPGEIAAVLNGHHAVH
                                         ** * *: *.*******:.* **** * ::**.*:********:****: *.*.  ..* 

Q45R83_A1_8__lys                         AAAVTVREDTPGTRRLVGYVVGTPDADDARLRPAEV--
O05819_A1_lys                            GCHVTARGHASGP-RLTAYVAGGP-------QPPPVAE
                                         .. **.* .:.*. **..**.* *       :*. *  
