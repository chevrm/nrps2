CLUSTAL W multiple sequence alignment


Q8RTG4_A1_arg                            DYPTERLGDILSDSDVSLVLTQESLGDFLPQTGAELLCLDRDWEKIATYSPENPFNLTTP
Q9S1A7_A1_10__arg                        NYPPERLDYMISDSAISLLLTQQSLVQFLPENQAEILCLDTDWSRIANYSQE---NLTSP
Q9RNA9_A1_arg                            NYPPERLDYMISDSAISLLLTQQSLVQFLPENQAEILCLDTDWSRIANYSQE---NLTSP
                                         :**.***. ::*** :**:***:** :***:. **:**** **.:**.** *   ***:*

Q8RTG4_A1_arg                            ---ENLAYVIYTSGSTGKPKGVMNIHRGICNTLTYAIGHYNINSEDRILQITSLSFDVSV
Q9S1A7_A1_10__arg                        VKPENLAYVIYTSGSTGKPKGVMNIHQGICNTLKYNIDNYNLNSEERILQITPFSFDVSV
Q9RNA9_A1_arg                            VKTENLAYVIYTSGSTGKPKGVMNIHQGICNTLKYNIDNYNLNSEDRILQITPFSFDVSV
                                            ***********************:******.* *.:**:***:******.:******

Q8RTG4_A1_arg                            WEVFLSLISGASLVVAKPDGYKDIDYLIDLIVQEQVTCFTCVPSILRVFLQHPKSKDCHC
Q9S1A7_A1_10__arg                        WEIFLSLTSGATLVVAKPDGYKDIDYLIDLIVQEQVTCFTCVPSILRVFLQHSKSKDCHC
Q9RNA9_A1_arg                            WEVFSSLTSGATLVVTKPDGYKDIDYLIDLIVQEQVTCFTCVPSILRVFLQHPKSKDCHC
                                         **:* ** ***:***:************************************.*******

Q8RTG4_A1_arg                            LKRVIVGGEALSYELNQRFFQQLNCELYNAYGPTEVAVETTIWCCQPNSQ-ISIGTPIAN
Q9S1A7_A1_10__arg                        LKRVIVGGEALSYELNQRFFQQLNCELYNAYGPTEVAVETTIWCCQPNSQ-ISIGTPIAN
Q9RNA9_A1_arg                            LKRVIVGGEALSYELNQRFFQQLNCELYNAYGPTEVAVDATVWCCQPNSQLISIGRPIAN
                                         **************************************::*:******** **** ****

Q8RTG4_A1_arg                            AQVYILDSYLQPVPIGVAGELHIGGMGLARGYLNQPELTAEKFIPHPFAQGKLYKTGDLA
Q9S1A7_A1_10__arg                        AQVYILDSYLQPVPIGVAGELHIGGMGLARGYLNQPELTAEKFIPHPFAQGKLYKTGDLA
Q9RNA9_A1_arg                            VQVYILDSYLQPVPIGVAGELHIGGMGLARGYLNQAELTAEKFIPHPFAEGKLYKTGDLA
                                         .**********************************.*************:**********

Q8RTG4_A1_arg                            RYLPEGNIEYLGRIDNQVKLRGLRIELGEIQTVLETHPNVEQTVVIPRGSA---------
Q9S1A7_A1_10__arg                        RYLPDGNIEYLGRIDNQVKLRGLRIELGEIQTVLETHPNVEQTVVIMREDTLYNQRLVAY
Q9RNA9_A1_arg                            RYLPDGNIEYLGRIDNQVKLRGFRIELGEIQTVLETHPNVEQTVVIMREDTLYNQRLVAY
                                         ****:*****************:*********************** * .:         

Q8RTG4_A1_arg                            ---GNSLLSPQD
Q9S1A7_A1_10__arg                        VIRKDTLLTSQD
Q9RNA9_A1_arg                            VMRKEPLLTSQD
                                             :.**:.**
