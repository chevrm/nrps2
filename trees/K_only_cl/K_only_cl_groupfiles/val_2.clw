CLUSTAL W multiple sequence alignment


Q9FDB3_A3_3__val                         NAPVERQTFMIEDSQAHVLLTLSRMSLTASTQRIDLDGLTLDGLKDTDLTL--PQSSESV
Q83VS0_A1_11__val                        NAPAERQAFMLQDCGARQVLTLSRHDLPDGIQRIDLDLL---ELQSDAPNPVHSASAESV
Q83VS1_A2_7__val                         NAPVERQSFMIEDSRARVLLTHSQVSLTTSAQRIDLDGLTLERLKGTDLAL-PPQSSESV
Q83VS1_A2_val                            NAPVERQSFMIEDSRARVLLTHSQVSLTTSAQRIDLDGLTLERLKGTDLAL-PPQSSESV
Q9FDB3_A4_4__val                         NAPQERQTFMVQDSQAVLVLTYSTETLDPGTQRLDLDTRPASTVPADNPNP--EACADSV
Q83VS1_A3_val                            NAPVERQAFMIEDSQASVLLTLSQMSLTTSTQRVDLDRLMLNGLDNDDLAL--AQSSESV
Q83VS1_A3_8__val                         NAPVERQAFMIEDSQASVLLTLSQMSLTTSTQRVDLDRLMLNGLDNDDLAL--AQSSESV
                                         *** ***:**::*. *  :** *   *  . **:***      :           .::**

Q9FDB3_A3_3__val                         AYIMYTSGSTGVPKGVLVPHRAISRLVINNGYADFNAQDRVAFASNPAFDASTLDVWAPL
Q83VS0_A1_11__val                        AYIMYTSGSTGMPKGVLVPHRAVSRLVLNNGYADFNAGDRVAFASNPAFDASTLDVWAPL
Q83VS1_A2_7__val                         AYIMYTSGSTGTPKGVLVPHRAISRLAINNGYADFNAQDRVAFASNPAFDASTLDVWAPL
Q83VS1_A2_val                            AYIMYTSGSTGTPKGVLVPHRAISRLAINNGYADFNAQDRVAFASNPAFDASTLDVWAPL
Q9FDB3_A4_4__val                         AYIMYTSGSTGTPKGVLVPHRAISRLVIDNGYADFNGQDRVAFASNPAFDASTLDVWAPL
Q83VS1_A3_val                            AYIMYTSGSTGTPKGVLVPHRAISRLVINNGYADFNAQDRVAFASNPAFDASTLDVWAPL
Q83VS1_A3_8__val                         AYIMYTSGSTGTPKGVLVPHRAISRLVINNGYADFNAQDRVAFASNPAFDASTLDVWAPL
                                         *********** **********:***.::*******. **********************

Q9FDB3_A3_3__val                         LNGGCVVVIGQHDLLSPLNFQRLLLEQSVSVLWMTAGLFHQYATGLGEAFSRLRYLIVGG
Q83VS0_A1_11__val                        LNGGCVVVVEQSVLLSLDEFRALLLSQSVSVLWMTAGLFHQYASGLMEALARLRYLIVGG
Q83VS1_A2_7__val                         LNGGCVVVIGQHDLLSPLNFQRLLLEQSVSVLWMTAGLFHQYASGLGEAFSRLRYLIVGG
Q83VS1_A2_val                            LNGGCVVVIGQHDLLSPLNFQRLLLEQSVSVLWMTAGLFHQYASGLGEAFSRLRYLIVGG
Q9FDB3_A4_4__val                         LNGGCVIVVEQSVLLSQEAFRALLLAQSISVLWLTAGLFHQYADGLMEVFAGLRYLIVGG
Q83VS1_A3_val                            LNGGCVVVIGQHDLLSPLNFQRLLLEQSVTVLWMTAGLFHQYASGLGEAFSRLRYLIVGG
Q83VS1_A3_8__val                         LNGGCVVVIGQHDLLSPLNFQRLLLEQSVTVLWMTAGLFHQYASGLGEAFSRLRYLIVGG
                                         ******:*: *  ***   *: *** **::***:********* ** *.:: ********

Q9FDB3_A3_3__val                         DVLDPAVIARVLANNAPQHLLNGYGPTEATTFSATYEITSVDNGSIPIGKPVGNTRLYVL
Q83VS0_A1_11__val                        DVLDPAVIARVLAEGAPQHLLNGYGPTEATTFSTTHEITSVGSGGIPIGRPIGNSQVYVL
Q83VS1_A2_7__val                         DVLDPAVIGRVLANNPPQHLLNGYGPTEATTFSATYEIVSVGNGSIPIGKPVGNSRLYVL
Q83VS1_A2_val                            DVLDPAVIGRVLANNPPQHLLNGYGPTEATTFSATYEIVSVGNGSIPIGKPVGNSRLYVL
Q9FDB3_A4_4__val                         DVLDPAVIARVLAQGAPQHLLNGYGPTEATTFSTTYEITAADHGGIPIGRPIANSQVYVL
Q83VS1_A3_val                            DVLDPAVIARVLANNAPQHLLNGYGPTEATTFSATYEIVSVDNGSIPIGKPVGNTRLYVL
Q83VS1_A3_8__val                         DVLDPAVIARVLANNAPQHLLNGYGPTEATTFSATYEIVSVDNGSIPIGKPVGNTRLYVL
                                         ********.****:..*****************:*:**.:.. *.****:*:.*:::***

Q9FDB3_A3_3__val                         DSQGQPAPLGVAGELYIGGQGVARGYLHRDELTLEKFLADPFDSDPQARLYRTGDLVRWR
Q83VS0_A1_11__val                        DTLRQPVAVGVAGELYIGGQGVAKGYLNRPELNATQFVANPFSDDAGALLYRTGDLGRWN
Q83VS1_A2_7__val                         DNQGQPVPLGVPGELYIGGQGVARGYLHRDELTLERFVADPFDSDPQARLYRTGDLVRWR
Q83VS1_A2_val                            DNQGQPVPLGVPGELYIGGQGVARGYLHRDELTLERFVADPFDSDPQARLYRTGDLVRWR
Q9FDB3_A4_4__val                         DALRQPVAVGVPGELYIGGQGVAKGYLNRDELSTTQFVADPFSGHDGALMYRTGDLGRWR
Q83VS1_A3_val                            DSQGQPVPLGVPGELYIGGQGVAKGYLNRDELSTTQFVVDPFSEQIDALMYRTGDLVRWR
Q83VS1_A3_8__val                         DSQGQPVPLGVPGELYIGGQGVAKGYLNRDELSTTQFVVDPFSEQIDALMYRTGDLVRWR
                                         *   **..:**.***********:***:* **.  :*:.:**. .  * :****** **.

Q9FDB3_A3_3__val                         ADGNLEYLGRNDDQVKIRGFRVELGEIEARLAEHVDVREAVVLCRQDVPGDKRLVAYVTA
Q83VS0_A1_11__val                        ADGIVEYLGRNDDQVKIRGFRIELGEIEARLVECPGVREAVVLARQDESAHKRLVAYVVG
Q83VS1_A2_7__val                         ADGNLEYLGRNDEQVKIRGFRVELGEIEARLAEHAEVREAVVLCRQDAPGDKRLVAYVTA
Q83VS1_A2_val                            ADGNLEYLGRNDEQVKIRGFRVELGEIEARLAEHAEVREAVVLCRQDAPGDKRLVAYVTA
Q9FDB3_A4_4__val                         ADGNLEYLGRNDGQVKIRGFRIELSEIEAALATHPAVHEAVLLARQD-TGEKRLVAYFTL
Q83VS1_A3_val                            ADGNLEYLGRNDDQVKIRGFRVELGEIEARLAEHADVREAVVLCRQDVPGDKRLVAYVTA
Q83VS1_A3_8__val                         ADGNLEYLGRNDDQVKIRGFRVELGEIEARLAEHADVREAVVLCRQDVPGDKRLVAYVTA
                                         *** :******* ********:**.**** *.    *:***:*.*** ...******.. 

Q9FDB3_A3_3__val                         QQAETALDIEH
Q83VS0_A1_11__val                        EENSALSAVEL
Q83VS1_A2_7__val                         QQPETALDIEH
Q83VS1_A2_val                            QQPETALDIEH
Q9FDB3_A4_4__val                         REPQQTLEIET
Q83VS1_A3_val                            QQSENALDIEH
Q83VS1_A3_8__val                         QQSENALDIEH
                                         .: .    :* 
