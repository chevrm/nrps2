CLUSTAL W multiple sequence alignment


Q84BC7_A1_4__ile                         EYPQERLTFMLEDTQLSVILTQEKLVNKLGERLRRGFAERNASVICLDSNWDIINQQTQN
Q9K5L9_A1_7__ile                         EYPTERLRFMWADAQVSVLLTQQHLVEKL--------PEHQVPVVCLDTDWLVICESSPE
                                         *** *** **  *:*:**:***::**:**        .*::..*:***::* :* :.: :

Q84BC7_A1_4__ile                         NPTTSVTADNLAYVMYTSGSTGQPKGVSIVHRSVVRLVKETNYISISADDVIAQASNHAF
Q9K5L9_A1_7__ile                         SPITEVQPGNLAYVIYTSGSTGTPKGVVVNHQAVNRLVKNTNYVQLTPDDRVAQAANIAF
                                         .* *.* ..*****:******* **** : *::* ****:***:.::.** :***:* **

Q84BC7_A1_4__ile                         DAATFEIWGALLNGARLVGVSKDLALSPRDFAVFMRSQSISVLFLTTALFNQIAQEVPSA
Q9K5L9_A1_7__ile                         DAATFEIWGALLNGAKLVMITKSVLLSPQEFAANIRDREVSVLFLTTALFNQLASFVPQA
                                         ***************:** ::*.: ***::**. :*.:.:************:*. **.*

Q84BC7_A1_4__ile                         FNSLRHLLFGGEAVDPKWVKEVLNNGAPQRLLHVYGPTENTTFSSWYLVQDVPEGATTIP
Q9K5L9_A1_7__ile                         FSSLRYLLFGGEAVDPQWVQEVLEKGAPKQLLHVYGPTENTTFSSWYLVEELTTIATTIP
                                         *.***:**********:**:***::***::*******************:::.  *****

Q84BC7_A1_4__ile                         IGQPISNTQIYLLDSQLQPVGIGVPGELYIGGDGLAREYLNRTELTQEKLIQNPFGGSRG
Q9K5L9_A1_7__ile                         IGRAISNTQIYLLDQNLQPVPVGVPGELHVGGAGLARGYLNRPELTQEKFIPNPFDNS--
                                         **:.**********.:**** :******::** **** ****.******:* ***..*  

Q84BC7_A1_4__ile                         AGEQGSKGAEEQSFPSASSERLYKTGDKARYLSDGNIEYLGRIDDQVKIRGLRIELGEIE
Q9K5L9_A1_7__ile                         --------------------KLYKTGDLARYLPDGNIEYLGRIDHQVKIRGFRIELGEIE
                                                             :****** ****.***********.******:********

Q84BC7_A1_4__ile                         AVLSQHSDVQVSCVIVREDTPGD--------KRLVAYIVTHQDCQPTMGE
Q9K5L9_A1_7__ile                         AVLSQHEDVQISCVIVREDTPGETCTERSRSKQLVAYIVPQKDVTLTTSE
                                         ******.***:***********:        *:******.::*   * .*
