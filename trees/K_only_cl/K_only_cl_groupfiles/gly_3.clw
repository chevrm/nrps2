CLUSTAL W multiple sequence alignment


Q9L8H4_A2_5__gly                         DHPAARLSHVLGDARPALLLTDTRTEQHLPADADTRRLALDSAEVRALLADCPDT----D
Q45R83_A3_10__gly                        DHPRSRLADVLADSAPGCVITTTDLARRLPP-VPAPLLVLDDPATAARLAATTATALAED
                                         *** :**:.**.*: *. ::* *   ::**. . :  *.**.. . * **  . *    *

Q9L8H4_A2_5__gly                         PAEEGVTPAPG----SAAYVIYTSGSTGRPKGVVVPHSALVNFVTAMRRQAPLRPQERLL
Q45R83_A3_10__gly                        PREQN-----GEWGEELAYTIYTSGSTGRPKGVMVTRSAVANFLADMNERLELGPGDRLL
                                         * *:.     *    . **.*************:*.:**:.**:: *..:  * * :***

Q9L8H4_A2_5__gly                         AVTTVAFDIAALELYHPLLSGAAVVLAPKEAVPQPSAVLDLIARHGVTTVQGTPSLWQLL
Q45R83_A3_10__gly                        AVTTVSFDIAVLELLAPLLTGGTVVLADATTQRDPAAVRSLCAREGVTVIQATPSWWHAM
                                         *****:****.***  ***:*.:****   :  :*:** .* **.***.:*.*** *: :

Q9L8H4_A2_5__gly                         VGHDAEALRGLRMLVGGEALPLSLAEAL----RA-LTDDLVNLYGPTETTIWSTAAELAG
Q45R83_A3_10__gly                        AVDGGLDLTALRVLVGGEALPPALARTLLEPGRAPLGDYLLNLYGPTETTVWSTVARITA
                                         . ...  * .**:******** :**.:*    ** * * *:*********:***.*.::.

Q9L8H4_A2_5__gly                         GT-----GAAPIGRPIANTRVYVLDDGLQPVAPGVVGELYIAGAGLARGYLDRPALTAER
Q45R83_A3_10__gly                        DSLEAHGGAVPTGTPIARTAAYVLDAALRPVPDGVPGELYLAGAGLARGYLGRPGMTAER
                                         .:     **.* * ***.* .**** .*:**. ** ****:**********.**.:****

Q9L8H4_A2_5__gly                         FPADPYGLEPGGRMYRTGDLVRWNPDGELEFVGRADHQVKVRGFRIEPGEIEKVLTDHPD
Q45R83_A3_10__gly                        FVACPFG-EPGERMYRTGDLARWRADGNLEHLGRTDDQVKVRGFRIELGEVERALTQAHG
                                         * * *:* *** ********.**..**:**.:**:*.********** **:*:.**:  .

Q9L8H4_A2_5__gly                         IAQAAVVVREDQPGDARLVAYVVTGGSADAR
Q45R83_A3_10__gly                        VGRAAAAVHPDAAGSARLVGYLVPAGGSGA-
                                         :.:**..*: * .*.****.*:*..*.:.* 
