CLUSTAL W multiple sequence alignment


P0C063_A4_leu                            AYPQERIQYLLEDSGAALLLTQSHVL-------NKLPVDIEWLDLTDEQNYVEDGTNLPF
O33743_A6_10__leu                        GYPQERIQYLLEDSNAALLLSQAHLLPLLAQVSSELP---ECLDLNAELDAGLSGSNLPA
P0C064_A4_5__leu                         AYPQERIQYLLEDSGATLLLTQSHVL-------NKLPVDIEWLDLTDEQNYVEDGTNLPF
                                         .*************.*:***:*:*:*       .:**   * ***. * :   .*:*** 

P0C063_A4_leu                            MNQSTDLAYIIYTSGTTGKPKGVMIEHQSIINCLQWRKEEYEFGPGDTALQVFSFAFDGF
O33743_A6_10__leu                        VNQPTDLAYVIYTSGTTGKPKGVMIPHQGIVNCLQWRRDEYGFGPSDKALQVFSFAFDGF
P0C064_A4_5__leu                         MNQSTDLAYIIYTSGTTGKPKGVMIEHQSIINCLQWRKEEYEFGPGDTALQVFSFAFDGF
                                         :**.*****:*************** **.*:******::** ***.*.************

P0C063_A4_leu                            VASLFAPILAGATSVLPKEEEAKDPVALKKLIASEEITHYYGVPSLFSAILDVSSSKDLQ
O33743_A6_10__leu                        VASLFAPLLGGATCVLPQEAAAKDPVALKKLMAATEVTHYYGVPSLFQAILDCSTTTDFN
P0C064_A4_5__leu                         VASLFAPILAGATSVLPKEEEAKDPVALKKLIASEEITHYYGVPSLFSAILDVSSSKDLQ
                                         *******:*.***.***:*  **********:*: *:**********.**** *::.*::

P0C063_A4_leu                            NLRCVTLGGEKLPAQIVKKIKEKNKEIEVNNEYGPTENSVVTTIMRDIQVEQEITIGRPL
O33743_A6_10__leu                        QLRCVTLGGEKLPVQLVQKTKEKHPAIEINNEYGPTENSVVTTISRSIEAGQAITIGRPL
P0C064_A4_5__leu                         NLRCVTLGGEKLPAQIVKKIKEKNKEIEVNNEYGPTENSVVTTIMRDIQVEQEITIGCPL
                                         :************.*:*:* ***:  **:*************** *.*:. * **** **

P0C063_A4_leu                            SNVDVYIVNCNHQLQPVGVVGELCIGGQGLARGYLNKPELTADKFVVNPFVPGERMYKTG
O33743_A6_10__leu                        ANVQVYIVDEQHHLQPIGVVGELCIGGAGLARGYLNKPELTAEKFVANPFRPGERMYKTG
P0C064_A4_5__leu                         SNVDVYIVNCNHQLQPVGVVGELCIGGQGLARGYLNKPELTADKFVVNPFVPGERMYKTG
                                         :**:****: :*:***:********** **************:***.*** *********

P0C063_A4_leu                            DLAKWRSDGMIEYVGRVDEQVKVRGYRIELGEIESAILEYEKIKEAVVMVSEHTASE-QM
O33743_A6_10__leu                        DLVKWRTDGTIEYIGRADEQVKVRGYRIEIGEIESAVLAYQGIDQAVVVARDDDATAGSY
P0C064_A4_5__leu                         DLAKWRSDGMIEYVGRVDEQVKVRGYRIELGEIESAILEYEKIKEAVVIVSEHTASE-QM
                                         **.***:** ***:**.************:******:* *: *.:***:. :. *:  . 

P0C063_A4_leu                            LCAYIVGEEDVLTLD
O33743_A6_10__leu                        LCAYFVAATAVSVSG
P0C064_A4_5__leu                         LCAYIVGEEDVLTLD
                                         ****:*.   * . .
