CLUSTAL W multiple sequence alignment


P39846_A2_thr                            EYPKERISFMLNDSGAKLLLTERGLNKPADYTGHILYIDECENNSIPADVNIEEIVTDQP
O87704_A2_4__thr                         EYPQERISFMLNDSGARILLTEHGHNKPADYHGQILYLNDAENELISPDLKAQETLADQP
                                         ***:************::****:* ****** *:***:::.**: *..*:: :* ::***

P39846_A2_thr                            AYVIYTSGTTGQPKGVIVEHRNVISLLKHQNLPFEFNHEDVWTLFHSYCFDFSVWEMFGA
O87704_A2_4__thr                         AYVIYTSGTTGQPKGVVVEHRNVISLLKHQDLPFDFGSEDVWTLFHSYCFDFSVWEMFGA
                                         ****************:*************:***:*. **********************

P39846_A2_thr                            LLNGSTLVVVSKETARDPQAFRLLLKKERVTVLNQTPTAFYGLMLEDQNHTDHLNIRYVI
O87704_A2_4__thr                         LLNGSTLVVVSRETARDPNAFRLLLKNEGVTVLNQTPTAFYGLIHEEENHTDRLHVRYVI
                                         ***********:******:*******:* **************: *::****:*::****

P39846_A2_thr                            FGGEALQPGLLQSWNEKYPHTDLINMYGITETTVHVTFKKLSAADIAKNKSNIGRPLSTL
O87704_A2_4__thr                         FGGEALQPGMLVTWNEKYPDTDLINMYGITETTVHVTYKKLSSADIEKNKSNIGKPLATL
                                         *********:* :******.*****************:****:*** *******:**:**

P39846_A2_thr                            QAHVMDAHMNLQPTGVPGELYIGGEGVARGYLNRDELTADRFVSNPYLPGDRLYRTGDLA
O87704_A2_4__thr                         QAYVMDAHMNLQPTGVPGELYIGGEGVARGYLNRDDLTAARFVPNPYLPGDRLYRTGDLA
                                         **:********************************:*** ***.****************

P39846_A2_thr                            KRLSNGELEYLGRIDEQVKVRGHRIELGEIQAALLQYPMIKEAAVITRADEQGQTAIYAY
O87704_A2_4__thr                         KRLASGDLEYMGRIDDQVKVRGHRIELGEIQASLLQLPIIKEAAVITRDDEQGQSAVYAY
                                         ***:.*:***:****:****************:*** *:********* *****:*:***

P39846_A2_thr                            MVIKDQQAANISD
O87704_A2_4__thr                         LVAEDGQVVNEAD
                                         :* :* *..* :*
