CLUSTAL W multiple sequence alignment


O87314_A1_orn                            ------------------------------------------------------------
O87314_A3_orn                            DHRQRRTRAARHDDRPHGTDRARTSPRCRCHRARRRGNRCHTRRYARAPADRRRTRGFRV
                                                                                                     

O87314_A1_orn                            -----EHPAYLIYTSGSTGRPKGVLTGYAGLTNMYFNHREAIFAPTVARAGSAEQLRIAH
O87314_A3_orn                            RQHAPEHPAYLIYTSGSTGRPKGVLTGYAGLTNMYFNHREAIFAPTVARAGSAEQLRIAH
                                              *******************************************************

O87314_A1_orn                            TVSFSFDMSWEELFWLVEGHQVHVCDEELRRDAPALVAYCHRHRIDVINVTPTYAHHLFD
O87314_A3_orn                            TVSFSFDMSWEELFWLVEGHQVHVCDEELRRDAPALVAYCHRHRIDVINVTPTYAHHLFD
                                         ************************************************************

O87314_A1_orn                            AGLLDDGAHTPPLVLLGGEAVGDGVWSALRDHPDSAGYNLYGPTEYTINTLGGGTDDSDT
O87314_A3_orn                            AGLLDDGAHTPPLVLLGGEAVGDGVWSALRDHPDSAGYNLYGPTEYTINTLGGGTDDSDT
                                         ************************************************************

O87314_A1_orn                            PTVGQPIWNTRGYILDAALRPVPDGAVGELYIAGTGLALGYHRRAGLTAATMVADPYVPG
O87314_A3_orn                            PTVGQPIWNTRGYILDAALRPVPDGAVGELYIAGTGLALGYHRRAGLTAATMVADPYVPG
                                         ************************************************************

O87314_A1_orn                            GRMYRTGDLVRRRPGSAELLDYLGRVDDQVKIRGYRVELGEIESVLTRADGVARCAVVAR
O87314_A3_orn                            GRMYRTGDLVRRRPGSAELLDYLGRVDDQVKIRGYRVELGEIESVLTRADGVARCAVVAR
                                         ************************************************************

O87314_A1_orn                            ATGANPPVKTLAAYVIPDRWPAED
O87314_A3_orn                            ATGANPPVKTLAAYVIPDRWPAED
                                         ************************
