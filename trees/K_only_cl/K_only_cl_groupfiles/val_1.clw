CLUSTAL W multiple sequence alignment


Q09164_A4_val                            KAPAARIDAIVSSLPGNKLILLGANVTPPKLQEAAIDFVPIRDTFTTLTDGTLQDG--PT
Q09164_A9_val                            NAPAGRIETILSSLPGNRLILLGSDTQAVKLHANSV-------RFTRISDALVESGSPPT
                                         :***.**::*:******:*****::. . **:  ::        ** ::*. ::.*  **

Q09164_A4_val                            IE---RPSAQSLAYAMFTSGSTGRPKGVMVQHRNIVRLVKNSNVVAKQPAAARIAHISNL
Q09164_A9_val                            EELSTRPTAQSLAYVMFTSGSTGVPKGVMVEHRGITRLVKNSNVVAKQPAAAAIAHLSNI
                                          *   **:******.******** ******:**.*.**************** ***:**:

Q09164_A4_val                            AFDASSWEIYAPLLNGGAIVCADYFTTIDPQALQETFQEHEIRGAMLPPSLLKQCLVQAP
Q09164_A9_val                            AFDASSWEIYAPLLNGGTVVCIDYYTTIDIKALEAVFKQHHIRGAMLPPALLKQCLVSAP
                                         *****************::** **:**** :**: .*::*.********:*******.**

Q09164_A4_val                            DMISRLDILFAAGDRFSSVDALQAQRLVGSGVFNAYGPTENTILSTIYNVAENDSFVNGV
Q09164_A9_val                            TMISSLEILFAAGDRLSSQDAILARRAVGSGVYNAYGPTENTVLSTIHNIGENEAFSNGV
                                          *** *:********:** **: *:* *****:*********:****:*:.**::* ***

Q09164_A4_val                            PIGSAVSNSGAYIMDKNQQLVPAGVMGELVVTGDGLARGYMDPKLDADRFIQLTVNGSEQ
Q09164_A9_val                            PIGNAVSNSGAFVMDQNQQLVSAGVIGELVVTGDGLARGYTDSKLRVDRFIYITLDGN-R
                                         ***.*******::**:*****.***:************** *.** .**** :*::*. :

Q09164_A4_val                            VRAYRTGDRVRYRPKDFQIEFFGRMDQQIKIRGHRIEPAEVEQAFLNDGFVEDVAIVIRT
Q09164_A9_val                            VRAYRTGDRVRHRPKDGQIEFFGRMDQQIKIRGHRIEPAEVEQALARDPAISDSAVITQL
                                         ***********:**** ***************************: .*  :.* *:: : 

Q09164_A4_val                            PENQEPEMVAFVTAKGD-------NSAREEEA
Q09164_A9_val                            TDEEEPELVAFFSLKGNANGTNGVNGVSDQEK
                                         .:::***:***.: **:       *.. ::* 
