CLUSTAL W multiple sequence alignment


O52819_A1_leu                            AYPAPRVAFMVADSAVSLTVCSAATRDGVPEGIESI---VITDEDASDTSVATVRPGDLA
Q939Z1_A1_leu                            GYPAPRVAFMVADSAAKLVVCSAASRGAVPAGVESLEPAAAAEEGASDAPAATVRPGDPA
                                         .**************..*.*****:*..** *:**:   . ::*.***:..******* *

O52819_A1_leu                            YVMYTSGSTGTPKGVAITHGTIAELAEDPGWVMEPGEAVLMHSPHTFDASLFEVWTPLSL
Q939Z1_A1_leu                            YVMYTSGSTGTPKGVTISQGCVAELTMDAGWAMEPGEAVLMHSPHAFDASLFELWMPLAS
                                         ***************:*::* :***: *.**.*************:*******:* **: 

O52819_A1_leu                            GARVVIAEPGSVDVRRLREAAAAGVTRVYLTAGSFRAVAEESPESFAAFREVLTGGDVVP
Q939Z1_A1_leu                            GVRVVLAEPGSVDARRLREAAAAGVTRVYLTAGSLRAVAEEAPESFAEFREVLTGGDVVP
                                         *.***:*******.********************:******:***** ************

O52819_A1_leu                            AHAVERVREACPGARVRNMYGPTEATMCATWHLLQPGDVMGPVMPIGRPLAGRRIQVLDE
Q939Z1_A1_leu                            AHAVERVRTAAPRARFRNMYGPTEATMCATWHLLQPGDVVGPVVPIGRPLTGRRVQVLDA
                                         ******** *.* **.***********************:***:******:***:**** 

O52819_A1_leu                            SLRPVEPGVVGDLYLSGGLAEGYFNRAGLTAERFVADPSAPGQRMYWTGDLAQWTADGEL
Q939Z1_A1_leu                            SLRPVGPGVVGDLYLSGALAEGYFNRAALTAERFVADPSAPGQRMYWTGDLAQWTADGEL
                                         ***** ***********.*********.********************************

O52819_A1_leu                            LFAGRADHQVKVRGFRIEPGEIEAALIALPDVQDAVVAAIDGRLVGYVVADGDVDPAL
Q939Z1_A1_leu                            VFAGRADDQVKIRGFRIEPGEIEAALIAQPDVHDAVVAAVDGRLIGYVVTEGDADPRV
                                         :******.***:**************** ***:******:****:****::**.** :
