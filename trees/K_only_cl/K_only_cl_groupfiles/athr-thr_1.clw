CLUSTAL W multiple sequence alignment


Q54959_A1_athr|thr                       DYPADRLAWMLEDAAPVLVL---------------------DPQAMTEDLAG------YP
Q9Z4X6_A2_athr|thr                       GYPADRLAHALSDSAPAALLTDRATAGRLPAHE-VPRIVLDAPAPADGGTTGGDPADAHP
removed_A1_2__athr|thr                   EYPANRLAHMVTDAQPTLILTTTETEAKLPDRHTAPALRLDDPETLAA-LAG------QP
P45745_A2_3__athr|thr                    DYPADRIAFMLKDAQPAFIMTNTKAANHIPPVENVPKIVLDDPELAEKLNT-------YP
                                          ***:*:*  : *: *. ::                      *       :        *

Q54959_A1_athr|thr                       DTAPRTA-----VDGAHPAYVIYTSGSTGRPKGVVIPHSNVVRLFTSTDHWFGFGPDDVW
Q9Z4X6_A2_athr|thr                       ATDLAQGERVRPLDPRDTAYVIYTSGSTGRPKGVAVPHGNVVRLFSATAPWFGFDEHDVW
removed_A1_2__athr|thr                   ANSPAVG-----LRPDHPAYVIYTSGSTGVPKGVVNTHRNVVRLFDATRPWFDFGPDDVW
P45745_A2_3__athr|thr                    AGNPKNKDRTQPLSPLNTAYVIYTSGSTGVPKGVMIPHQNVTRLFAATEHWFRFSSGDIW
                                                     :   ..*********** ****  .* **.*** :*  ** *.  *:*

Q54959_A1_athr|thr                       TLFHSYAFDFSVWEIWGALLHGGRLVVVPYHVSRSPGDFLDLLAREKVTVLNQTPTAFHQ
Q9Z4X6_A2_athr|thr                       TLFHSYAFDFSVWELWGPLLHGGRLVVVPHDVTRDPAAFLALLARERVTVLNQTPSAFHQ
removed_A1_2__athr|thr                   TLFHSYAFDFSVWELWGALLHGGRLVVVPYDVSRSPHAFLDLLADQGVTVLNQTPSAFHQ
P45745_A2_3__athr|thr                    TMFHSYAFDFSVWEIWGPLLHGGRLVIVPHHVSRSPEAFLRLLVKEGVTVLNQTPSAFYQ
                                         *:************:**.********:**:.*:*.*  ** **. : ********:**:*

Q54959_A1_athr|thr                       L--DAADRARTA--APELALRYVVFGGEALDVARLADWYARRGT-AARLVNMYGITETTV
Q9Z4X6_A2_athr|thr                       L--AAADREN-P---TELALRTVVFGGEALDLSRLADWYERHAEDAPALVNMYGITETTV
removed_A1_2__athr|thr                   LAQAAADPGR-P--PRRLALRTVVFGGEALQPARLAEWYRRHPEDTPQLVNMYGITETTV
P45745_A2_3__athr|thr                    F--MQAEREQ-PDLGQALSLRYVIFGGEALELSRLEDWYNRHPENRPQLINMYGITETTV
                                         :    *:  . .     *:** *:******: :** :** *:    . *:**********

Q54959_A1_athr|thr                       HVTHAPLGPGHAVPGTPSLLGGPIPDLTPRVLDAALRPVPPGFTGELYVAGAGLARGYLN
Q9Z4X6_A2_athr|thr                       HVSHFALDRATAAASSASTIGVNIPDLRVYVLDDRLRPTAPGVTGEMYVAGAGLARGYLG
removed_A1_2__athr|thr                   HVTHQPLTRDRAAAGAASVIGAGISDLRTHVLDGGLQLVPPGAVGELYVAGPGLARGYLG
P45745_A2_3__athr|thr                    HVSYIELDRSMAALRANSLIGCGIPDLGVYVLDERLQPVPPGVAGELYVSGAGLARGYLG
                                         **::  *    *.  : * :*  *.**   ***  *: ..** .**:**:*.*******.

Q54959_A1_athr|thr                       RPALTAQRFPADP----YGAPGTRMYRTGDLVRHLDDGTYAYLGRGDDQVKIRGFRIELG
Q9Z4X6_A2_athr|thr                       RPALTADRFPADPYAALFGERGTRMYRTGDLARRRTDGGLDYLGRADQQVKIRGFRIEPG
removed_A1_2__athr|thr                   RPALTAERFVADP----YGAPGARMYRTGDLVRRNPDGELEFVGRADHQVKVRGFRIELG
P45745_A2_3__athr|thr                    RPGLTSERFIADP----FGPPGTRMYRTGDVARLRADGSLDYVGRADHQVKIRGFRIELG
                                         **.**::** ***    :*  *:*******:.*   **   ::**.*.***:****** *

Q54959_A1_athr|thr                       EIENVLATHPGVAQAAAVVREDRHGDLRLAAYAVPTPGTEPDVA-
Q9Z4X6_A2_athr|thr                       EIEAVLAAHPAVDDVAVVAREDVQGDPRLVAYVVTGSGA---TA-
removed_A1_2__athr|thr                   EVEAALLAHPDVEQATVIVREDRPGDTRLVAYVVGREALRPEQV-
P45745_A2_3__athr|thr                    EIEAALVQHPQLEDAAVIVREDQPGDKRLAAYVIPSEETF-DTAE
                                         *:* .*  ** : :.:.:.***  ** **.**.:         . 
