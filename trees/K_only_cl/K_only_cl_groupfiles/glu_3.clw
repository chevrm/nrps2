CLUSTAL W multiple sequence alignment


Q6YK40_A4_glu                            DSPQERIRYILEDSGAKVLLAQTHLQDRVSFAGEILLLNDERMNSGDSSNLVTAAGPDHL
O68006_A4_glu                            AYPQERISYIVKDSDVSVLCAAGDVDPGEAYTGDIIRIDQTGQND-HVENLKHDIKPQHL
Q70JZ9_A4_glu                            DSPQERIRYILEDSGAKVLLAQPHLQDRVSFAGEILLLNDERMNSGDGSNLVTAAGPDHL
                                           ***** **::**...** *  .::   :::*:*: :::   *. . .**     *:**

Q6YK40_A4_glu                            AYVIYTSGTTGKPKGTLIEHRQVLHLMEGLRGQVYGAYDSGLRVSLLAPYYFDASVKQIF
O68006_A4_glu                            AYVIYTSGSTGKPKGVMIEHHSVNNLVHGLNERIYQHLDAHLNVALVAPYIFDASVKQIF
Q70JZ9_A4_glu                            AYVIYTSGTTGKPKGTLIEHRQVLHLMEGLRGQVYGAYDSGLRVSLLAPYYFDASVKQIF
                                         ********:******.:***:.* :*:.**. ::*   *: *.*:*:*** *********

Q6YK40_A4_glu                            ASLLGGHALYIVPKASVSDGYALSNYYRTHRIDVTDGTPSHLQLLIAADSLH-GVTIRHM
O68006_A4_glu                            AALLFGHTLCIVPRETAWDAMSLIEYYSKNNINVSDMTPAHLNMLAYVDKTELEFDVKEL
Q70JZ9_A4_glu                            AALLGGHALYIVPKASVSDGYALSNYYRTHRIDVTDGTPSHLQLLIAADSLH-GVTIRHM
                                         *:** **:* ***: :. *. :* :** .:.*:*:* **:**::*  .*. .  . ::.:

Q6YK40_A4_glu                            LIGGEALPQATVAQLLELFASNGSSMPLITNVYGPTETCVDASVFHI----VPETLASAD
O68006_A4_glu                            IVGGDALTPDVIGGLFHKFPNLSCN---ITNVYGPTECCVDAASHQIESGKVPQTPS---
Q70JZ9_A4_glu                            LIGGEALPQATVAQLLELFASNGSSMPLITNVYGPTETCVDASVFHI----VPETLASAD
                                         ::**:**.  .:. *:. *.. ...   ********* ****: .:*    **:* :   

Q6YK40_A4_glu                            DGGYVPIGKPLGNNRVYIVDSHDRMLPIGVKGELCIAGDGVGRGYLNLPELTGEKFVADP
O68006_A4_glu                            ----IPIGRPLLNTSIYIVDKELRPLPVGIAGELCIAGEGVARGYVNRPELTAEKFVDHP
Q70JZ9_A4_glu                            DGGYVPIGKPLGNNRVYIVDSHDRMLPIGVKGELCIAGDGVGRGYLNLPELTGVKFVADP
                                             :***:** *. :****.. * **:*: *******:**.***:* ****. *** .*

Q6YK40_A4_glu                            FVIGERMYKTGDLARYLPDGNIEYAGRKDHQVKIRGYRIELGEVEAALLNIEHVQEAVIL
O68006_A4_glu                            FEPGKKMYKTGDLAMWLPDGQIEFLGRADHQVKIRGYRIELGEVEQQLLTHEKIKEAAVI
Q70JZ9_A4_glu                            FVIGERMYKTGDLARYLPDGNIEYAGRKDHQVKIRGYRIELGEVEAALLNIEHVQEAVIL
                                         *  *::******** :****:**: ** *****************  **. *:::**.::

Q6YK40_A4_glu                            ARENAEGQSDLYAYFTGEKSLPINQ
O68006_A4_glu                            AGKDQNGNSYLCAYIASDKELPAAD
Q70JZ9_A4_glu                            ARENAEGQSDLYAYFTGEKSLPINQ
                                         * :: :*:* * **::.:*.**  :
