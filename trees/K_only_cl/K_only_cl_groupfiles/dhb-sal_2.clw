CLUSTAL W multiple sequence alignment


Q8Z8L1_A1_dhb|sal                        SHQRTELNAYAMQIAPTLVIADRQHTLFAGEDFLNRFVAEHRSARVVLLRNDDGDHSLDA
P10378/2506183_A1_dhb|sal                SHQRSELNAYASQIEPALLIADRQHALFSGDDFLNTFVTEHSSIRVVQLLNDSGEHNLQD
                                         ****:****** ** *:*:******:**:*:**** **:** * *** * **.*:*.*: 

Q8Z8L1_A1_dhb|sal                        AMRQAAEGFTATPSPADEVAYFQLSGGTTGTPKLIPRTHNDYYYSVRRSNEICGFNEETR
P10378/2506183_A1_dhb|sal                AINHPAEDFTATPSPADEVAYFQLSGGTTGTPKLIPRTHNDYYYSVRRSVEICQFTQQTR
                                         *:.:.**.***************************************** *** *.::**

Q8Z8L1_A1_dhb|sal                        FLCAIPAAHNYAMSSPGALGVFLAKGTVVLATDPGATLCFPLIEKHQINATALVPPAVSL
P10378/2506183_A1_dhb|sal                YLCAIPAAHNYAMSSPGSLGVFLAGGTVVLAADPSATLCFPLIEKHQVNVTALVPPAVSL
                                         :****************:****** ******:**.************:*.**********

Q8Z8L1_A1_dhb|sal                        WLQAIQEWGGNAPLASLRLLQVGGARLSATLAARIPAEIGCQLQQVFGMAEGLVNYTRLD
P10378/2506183_A1_dhb|sal                WLQALIEGESRAQLASLKLLQVGGARLSATLAARIPAEIGCQLQQVFGMAEGLVNYTRLD
                                         ****: *  ..* ****:******************************************

Q8Z8L1_A1_dhb|sal                        DSPERIINTQGRPMCPDDEVWVADADGNPLPPGEIGRLMTRGPYTFRGYFNSPQHNVSAF
P10378/2506183_A1_dhb|sal                DSAEKIIHTQGYPMCPDDEVWVADAEGNPLPQGEVGRLMTRGPYTFRGYYKSPQHNASAF
                                         **.*:**:*** *************:***** **:**************::*****.***

Q8Z8L1_A1_dhb|sal                        DANGFYCSGDLISIDQDGYITVHGREKDQINRGGEKIAAEEIENLLLRHPAVIHAALVSM
P10378/2506183_A1_dhb|sal                DANGFYCSGDLISIDPEGYITVQGREKDQINRGGEKIAAEEIENLLLRHPAVIYAALVSM
                                         *************** :*****:******************************:******

Q8Z8L1_A1_dhb|sal                        EDELLGEKSCAYLVVKEPLRAVQ
P10378/2506183_A1_dhb|sal                EDELMGEKSCAYLVVKEPLRAVQ
                                         ****:******************
