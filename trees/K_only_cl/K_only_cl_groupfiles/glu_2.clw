CLUSTAL W multiple sequence alignment


Q70JZ9_A4_glu                            DSPQERIRYILEDSGAKVLLAQPHLQDRVSFAGEILLLNDERMNSGDGSNLVTAAGPDHL
Q6YK40_A4_glu                            DSPQERIRYILEDSGAKVLLAQTHLQDRVSFAGEILLLNDERMNSGDSSNLVTAAGPDHL
                                         **********************.************************.************

Q70JZ9_A4_glu                            AYVIYTSGTTGKPKGTLIEHRQVLHLMEGLRGQVYGAYDSGLRVSLLAPYYFDASVKQIF
Q6YK40_A4_glu                            AYVIYTSGTTGKPKGTLIEHRQVLHLMEGLRGQVYGAYDSGLRVSLLAPYYFDASVKQIF
                                         ************************************************************

Q70JZ9_A4_glu                            AALLGGHALYIVPKASVSDGYALSNYYRTHRIDVTDGTPSHLQLLIAADSLHGVTIRHML
Q6YK40_A4_glu                            ASLLGGHALYIVPKASVSDGYALSNYYRTHRIDVTDGTPSHLQLLIAADSLHGVTIRHML
                                         *:**********************************************************

Q70JZ9_A4_glu                            IGGEALPQATVAQLLELFASNGSSMPLITNVYGPTETCVDASVFHIVPETLASADDGGYV
Q6YK40_A4_glu                            IGGEALPQATVAQLLELFASNGSSMPLITNVYGPTETCVDASVFHIVPETLASADDGGYV
                                         ************************************************************

Q70JZ9_A4_glu                            PIGKPLGNNRVYIVDSHDRMLPIGVKGELCIAGDGVGRGYLNLPELTGVKFVADPFVIGE
Q6YK40_A4_glu                            PIGKPLGNNRVYIVDSHDRMLPIGVKGELCIAGDGVGRGYLNLPELTGEKFVADPFVIGE
                                         ************************************************ ***********

Q70JZ9_A4_glu                            RMYKTGDLARYLPDGNIEYAGRKDHQVKIRGYRIELGEVEAALLNIEHVQEAVILARENA
Q6YK40_A4_glu                            RMYKTGDLARYLPDGNIEYAGRKDHQVKIRGYRIELGEVEAALLNIEHVQEAVILARENA
                                         ************************************************************

Q70JZ9_A4_glu                            EGQSDLYAYFTGEKSLPINQ
Q6YK40_A4_glu                            EGQSDLYAYFTGEKSLPINQ
                                         ********************
