CLUSTAL W multiple sequence alignment


Q56950b_A1_dhb|sal                       SQRALDIDALIELAQPVAYVIHGENHAELARQMAHKHACLRHVLVAGETVSDDFTPLFSL
Q56950a_A1_dhb|sal                       SQRALDIDALIELAQPVAYVIHGENHAELARQMAHKHACLRHVLVAGETVSDDFTPLFSL
                                         ************************************************************

Q56950b_A1_dhb|sal                       HGERQAWPQPDVSATALLLLSGGTTGTPKLIPRRHADYSYNFSASAELCGISQQSVYLAV
Q56950a_A1_dhb|sal                       HGERQAWPQPDVSATALLLLSGGTTGTPKLIPRRHADYSYNFSASAELCGISQQSVYLAV
                                         ************************************************************

Q56950b_A1_dhb|sal                       LPVAHNFPLACPGILGTLACGGKVVLTDSASCDEVMPLIAQERVTHVALVPALAQLWVQA
Q56950a_A1_dhb|sal                       LPVAHNFPLACPGILGTLACGGKVVLTDSASCDEVMPLIAQERVTHVALVPALAQLWVQA
                                         ************************************************************

Q56950b_A1_dhb|sal                       REWEDSDLSSLRVIQAGGARLDPTLAEQVIATFDCTLQQVFGMAEGLLCFTRLDDPHATI
Q56950a_A1_dhb|sal                       REWEDSDLSSLRVIQAGGARLDPTLAEQVIATFDCTLQQVFGMAEGLLCFTRLDDPHATI
                                         ************************************************************

Q56950b_A1_dhb|sal                       LHSQGRPLSPLDEIRIVDQDENDVAPGETGQLLTRGPYTISGYYRAPAHNAQAFTAQGFY
Q56950a_A1_dhb|sal                       LHSQGRPLSPLDEIRIVDQDENDVAPGETGQLLTRGPYTISGYYRAPAHNAQAFTAQGFY
                                         ************************************************************

Q56950b_A1_dhb|sal                       RTGDNVRLDEVGNLHVEGRIKEQINRAGEKIAAAEVESALLRLAEVQDCAVVAAPDTLLG
Q56950a_A1_dhb|sal                       RTGDNVRLDEVGNLHVEGRIKEQINRAGEKIAAAEVESALLRLAEVQDCAVVAAPDTLLG
                                         ************************************************************

Q56950b_A1_dhb|sal                       ERICAFIIAQQVPTDYQQ
Q56950a_A1_dhb|sal                       ERICAFIIAQQVPTDYQQ
                                         ******************
