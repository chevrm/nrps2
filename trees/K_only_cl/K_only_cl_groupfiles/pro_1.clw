CLUSTAL W multiple sequence alignment


Q9L8H4_A1_4__pro                         EYPANRLAHMVTDAQPTLVLTTTETEAKLPDRHPGLLLDDPAVLADLSGRP-----AHDP
O05647_A1_pro                            EYPSDRIAAMLEDARPALVLTE----------------DD--VDEDLSGIPDGNLTDAER
                                         ***::*:* *: **:*:****                 **  *  **** *       : 

Q9L8H4_A1_4__pro                         VVELHPDHPAYVIYTSGSTGVPKGVVMPAGGLLNLLQWHHRAVGDEPGTRTAQFTAISFD
O05647_A1_pro                            TAPLTPAHPAYVIYTSGSTGRPKAVVMPGAAVVNLLAWHRREIPAGAGTTVAQFASLSFD
                                         .. * * ************* **.****...::*** **:* :   .** .***:::***

Q9L8H4_A1_4__pro                         VSAQEVLSSVAFGKTLVIPDEEVRRDAARFAGWLDDRQVDELFAPNLVLEALAEAAVETG
O05647_A1_pro                            VAAQEILSTLLYGATLAVPTDAVRRDADAFAAWLEEYRVNELYAPNLVVEALAEAAAEQG
                                         *:***:**:: :* **.:* : *****  **.**:: :*:**:*****:*******.* *

Q9L8H4_A1_4__pro                         RTLPQLRTVAQAGEALTLSRTVRAFHRSAPGRRLHNHYGPTETHVVTAHALGDDPEDWRL
O05647_A1_pro                            RTLPDLRHIAQAGEALTAGPRVRDFCAALPGRRLHNHYGPAETHVMTGIELPVDPGGWPE
                                         ****:** :******** .  ** *  : ***********:****:*.  *  ** .*  

Q9L8H4_A1_4__pro                         PAPIGRPIDNTHAYVRTRAVRLVEPGVVGELYIAGAGLARGYLGRPALTAERFVADPYGL
O05647_A1_pro                            RVPIGGPVDNARLYVLDGFLRPVPPGVVGELYLAGAGVARGYLNRPGLTAERFVADPFG-
                                          .*** *:**:: **    :* * ********:****:*****.**.**********:* 

Q9L8H4_A1_4__pro                         EPGGRMYRTGDLVRRNPDGELEFCGRADHQVKVRGFRIEPGEIEKVLTDHPDIAQAAVVT
O05647_A1_pro                            GPGTRMYRTGDLARWAGSGVLEFAGRADHQVKVRGFRIEPGEVESVLAAQPGVARAVVLA
                                          ** ********.*   .* ***.******************:*.**: :*.:*:*.*::

Q9L8H4_A1_4__pro                         RPHRPGDTRLVAYVVGREALRPEQV--
O05647_A1_pro                            REDRPGERRLVAYLVAVPGSVPDPGVL
                                         * .***: *****:*.  .  *:    
