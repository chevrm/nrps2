CLUSTAL W multiple sequence alignment


Q01886_A3_ala                            SQPRSRLQNLIEESGAKLVLTLPESANALATLSGLTKVIPVSLSELVQQITDNTTKKDEY
Q01886/62867029_A3_ala                   SQPRSRLQNLIEESGAKLVLTLPESANALATLSGLTKVIPVSLSELVQQITDNTTKKDEY
                                         ************************************************************

Q01886_A3_ala                            CKSGDTDPSSPAYLLYTSGTSGKPKGVVMEHRAWSLGFTCHAEYMGFNSCTRILQFSSLM
Q01886/62867029_A3_ala                   CKSGDTDPSSPAYLLYTSGTSGKPKGVVMEHRAWSLGFTCHAEYMGFNSCTRILQFSSLM
                                         ************************************************************

Q01886_A3_ala                            FDLSILEIWAVLYAGGCLFIPSDKERVNNLQDFTRINDINTVFLTPSIGKLLNPKDLPNI
Q01886/62867029_A3_ala                   FDLSILEIWAVLYAGGCLFIPSDKERVNNLQDFTRINDINTVFLTPSIGKLLNPKDLPNI
                                         ************************************************************

Q01886_A3_ala                            SFAGFIGEPMTRSLIDAWTLPGRRLVNSYGPTEACVLVTAREISPTAPHDKPSSNIGHAL
Q01886/62867029_A3_ala                   SFAGFIGEPMTRSLIDAWTLPGRRLVNSYGPTEACVLVTAREISPTAPHDKPSSNIGHAL
                                         ************************************************************

Q01886_A3_ala                            GANIWVVEPQRTALVPIGAVGELCIEAPSLARCYLANPERTEYSFPSTVLDNWQTKKGTR
Q01886/62867029_A3_ala                   GANIWVVEPQRTALVPIGAVGELCIEAPSLARCYLANPERTEYSFPSTVLDNWQTKKGTR
                                         ************************************************************

Q01886_A3_ala                            VYRTGDLVRYASDGTLDFLGRKDGQIKLRGQRIELGEIEHHIRRLMSDDPRFHEASVQLY
Q01886/62867029_A3_ala                   VYRTGDLVRYASDGTLDFLGRKDGQIKLRGQRIELGEIEHHIRRLMSDDPRFHEASVQLY
                                         ************************************************************

Q01886_A3_ala                            NPATDPDRDATVDVQMREPYLAGLLVLDLVSLMRSWDSMHV----LNIANTSEN
Q01886/62867029_A3_ala                   NPATDPDRDATVDVQMREPYLAGLLVLDLV---FTDEVMGIPCTSLTSANTSEN
                                         ******************************    : : * :    *. ******
