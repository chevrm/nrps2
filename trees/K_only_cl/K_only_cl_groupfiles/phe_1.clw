CLUSTAL W multiple sequence alignment


O07944_A2_phe                            DYPPERIAHMRADARPALVV----------DAIP----DDTTLAAYADSRLTDADRSAPL
O05647_A2_phe                            AYPAERIAYLLQDGAPALVLTHTSVAAGLPGGVPQLLVDQVGLDDVPGHDLTDAERTTPL
                                          **.****::  *. ****:          ..:*    *:. *   ..  ****:*::**

O07944_A2_phe                            LPAHPAYVIYTSGSTGAPKGVVVAHRSLAATVPAQAAAFGLGTHSRVLNFASISFDAAVW
O05647_A2_phe                            HPLHPAYVIYTSGSTGLPKGVPVPHRSVASVLVPLIEEFGLGPGSRVLQFASISFDAALW
                                          * ************* **** *.***:*:.: .    ****. ****:*********:*

O07944_A2_phe                            ELTSALFTGAGLVLADADDLLPGPSLARLVHDRHITLIALPPSALPALPDGALPPGTDLI
O05647_A2_phe                            EITLALLSGATLVVAPAEQLQPGPALAELVARTGTTFLTLPPTALAVLADDALPAGVDLV
                                         *:* **::** **:* *::* ***:**.**     *:::***:**..*.*.***.*.**:

O07944_A2_phe                            VAGDATAPDQAARFAPGRRMVNAYGLTETTVCATMSEPATGDGAPPIGRPVAHARVYVLD
O05647_A2_phe                            VAGEATSPDQVGRWSTGRRMTNAYGPTEAAVCTTISAPLTGAVVPPIGRPVPNARAYVLD
                                         ***:**:***..*::.****.**** **::**:*:* * **  .*******.:**.****

O07944_A2_phe                            ERLRPVPPGVTGEMYVSGAGVARGYLHRPALTAQRFVPDPYALLFGETGTRMYRTGDLAR
O05647_A2_phe                            ALLQPVPPGVVGELYLAGGGLARGYRNRPGLTAERFVADP----FGTPGARMYRTGDLAR
                                           *:******.**:*::*.*:**** :**.***:***.**    ** .*:**********

O07944_A2_phe                            LDADGRLHFAGRADQQVKIRGFRIEPGEIETVLTAHPAVAAGAVIA--REDTPGDKQLVA
O05647_A2_phe                            WRPDGELEFAGRTDHQVKIRGFRIEPGEVEAALATHPAVERAAVIAARHED---DRRLVA
                                           .**.*.****:*:*************:*:.*::****  .****  :**   *::***

O07944_A2_phe                            YL-------
O05647_A2_phe                            YLVPAGAGT
                                         **       
