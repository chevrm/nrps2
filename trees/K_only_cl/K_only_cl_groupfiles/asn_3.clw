CLUSTAL W multiple sequence alignment


Q45R83_A4_11__asn                        ELPAERIAHML-ENARPVLVLAH-------TATQD---ALPEGAGPVVRLDAPAIEAALA
Q9FB27_A1_asn                            GFPRKRLEFVLRETAAPVLLCTADVRDRIGTRTLDDAGVTPVA------LDADRRRIA--
Q9FB23_A2_asn                            VHPPRRQRQVLTEAGARLL-------------------VLPAG------LDTPLRACGLP
                                           * .*   :* * .  :*                   . * .      **:     .  

Q45R83_A4_11__asn                        GLDGGDCTDADRRAP---ATHHD-PAYVVYTSGSTGTPKGVVVEQRSLAAFLVRSAARYR
Q9FB27_A1_asn                            ---------AHPAGPTGIATTPDAPAYVVYTSGTTGKPNGVRVPHRGLTNYLTWCTGAYG
Q9FB23_A2_asn                            VVAPDD--LGAPIAP--VSVHPEQLAAVMATSGSTGTPKTIGVPQRALAGYLRWAIGHYR
                                                  .   .*   :.  :  * *: ***:**.*: : * :*.*: :*  . . * 

Q45R83_A4_11__asn                        GAAGT-ALLHGSPAFDLTVTTLFTPLIAGGC-IVVADLDAPERDAPARP-----DLLKVT
Q9FB27_A1_asn                            LDGGTGTLVHTSISFDLTLTTLFGPLLAGGQVVMLSETAGVTGLIAALRSRRDLTLVKLT
Q9FB23_A2_asn                            LDEETVSPVHSSLGFDLTVTALLAPLAAGGQ-ARLTDSGDPGALGAALAAGHH-TLLKIT
                                             * : :* * .****:*:*: ** ***    :::        .*        *:*:*

Q45R83_A4_11__asn                        PSHLALLDTIASWATP----AA--DLVVGGEQLTASRLARLRRAHPDMRVFNDYGPTEAT
Q9FB27_A1_asn                            PTHL---DVVNQLLTPDELRGAVRTLVVGGEAVRAESLEPFR--ASGTRVVNEYGPSETV
Q9FB23_A2_asn                            PAHLA--ALAHQLGAP----TALRTVVAGGEPLHAGHVRALRAFAPGARLVNEYGPTETT
                                         *:**       .  :*     *   :*.*** : *  :  :*   .. *:.*:***:*:.

Q45R83_A4_11__asn                        VSCADFVL---EPGDAPPTDTVPIGRPLAGHRLFVLDDRLRPVPANVPGELYVSGVGVAR
Q9FB27_A1_asn                            VGSVAHVVDAATPRTGP----VPIGRPIANTTVHLLDQRRRPVPDGVVGELWIGGAGVAD
Q9FB23_A2_asn                            VGCCAHDV-APDPGEAP----IPVGTPIAGLSACVVDD-ALPAPPGVRGELYIGGTGVTR
                                         *..  . :    *  .*    :*:* *:*.    ::*:   *.* .* ***::.*.**: 

Q45R83_A4_11__asn                        GYLGRPGMTAERFVACPFGEPGERMYRTGDLARRRADGNLEYLGRRDGQVKVRGFRVETG
Q9FB27_A1_asn                            GYLGRPELTGERFLPSDYPPDGGRVYRTGDLARRRADGTLEYLGRTDAQVKIRGVRVEPA
Q9FB23_A2_asn                            GYLGRPAATAAAYVPDPAAP-GARRYRTGDLARRLPDGTLLLAGRADRQVKIRGHRVEPG
                                         ******  *.  ::.      * * ********* .**.*   ** * ***:** ***..

Q45R83_A4_11__asn                        EIETALLDRPEIGQAAVVLR-----GER--------LLAYV-----AAPPER-
Q9FB27_A1_asn                            ETEAVLASHPGVGQAVVVARLDEDPGRSSPLAGELTLTGYVVPARGAQAPPHE
Q9FB23_A2_asn                            EVEQVLGGHPGVREAAVVAHPAPGGGRR--------LVAYWVPAEPARPPS--
                                         * * .* .:* : :*.** :     *.         * .*      * .*   
