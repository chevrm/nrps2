CLUSTAL W multiple sequence alignment


Q9Z4X5_A2_8__gly                         HYPADRVEYMLADAGPALTVTEPVAEAGLSGYGDADLGADELRGPVHGAHPAYTIYTSGS
Q9Z4X5_A2_gly                            HYPADRVEYMLADAGPALTVTEPVAEAGLSGYGDADLGADELRGPVHGAHPAYTIYTSGS
                                         ************************************************************

Q9Z4X5_A2_8__gly                         TGRPKGVVVPRGALDNFLADMGRRFTPGSGDRLLAVTTVGFDIAGLEIFLPLLHGAVLVL
Q9Z4X5_A2_gly                            TGRPKGVVVPRGALDNFLADMGRRFTPGSGDRLLAVTTVGFDIAGLEIFLPLLHGAVLVL
                                         ************************************************************

Q9Z4X5_A2_8__gly                         ADEETARDPHALLHRVSASGITMVQATPSLWQGVAAVAGDELAGVRVLVGGEALPSELAR
Q9Z4X5_A2_gly                            ADEETARDPHALLHRVSASGITMVQATPSLWQGVAAVAGDELAGVRVLVGGEALPSELAR
                                         ************************************************************

Q9Z4X5_A2_8__gly                         ALTDRARSVTNLYGPTEATIWATAADVAESGPVIGRPLANTSAYVLDSALRPVPVGVPGE
Q9Z4X5_A2_gly                            ALTDRARSVTNLYGPTEATIWATAADVAESGPVIGRPLANTSAYVLDSALRPVPVGVPGE
                                         ************************************************************

Q9Z4X5_A2_8__gly                         LYLAGEQLAQGYHLRPALTSERFTADPYGPAGTRMYRTGDLVCRRRDGALRYLSRVDQQV
Q9Z4X5_A2_gly                            LYLAGEQLAQGYHLRPALTSERFTADPYGPAGTRMYRTGDLVCRRRDGALRYLSRVDQQV
                                         ************************************************************

Q9Z4X5_A2_8__gly                         KLRGFRIELGEIEAELSRHPAVAESAVTVREDRPGDRRLVGYVVPKGPEG
Q9Z4X5_A2_gly                            KLRGFRIELGEIEAELSRHPAVAESAVTVREDRPGDRRLVGYVVPKGPEG
                                         **************************************************
