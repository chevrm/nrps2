CLUSTAL W multiple sequence alignment


Q9L8H4_A3_6__val                         RYPADRYRLVLDETRTKLLIT-------DHTTDLDTTTTQFNPADTPHDGEDPGNPNHTT
removed_A2_3__val                        RYPADRIRLVLDETRTKLLIT-------DHTTDLDTTTTQFNPADTPHDGEDPGNPNHTT
Q5V8A8_A1_val                            RLPLARVHWILDETRAQTLLTISNSKNLDKIKNIQTTIR----VDVHLKKEDPDSPAIDT
                                         * *  * : :*****:: *:*       *: .:::**      .*.  . ***..*   *

Q9L8H4_A3_6__val                         HPDDAAYIMYTSGSTGRPKGVIATHRNITALALDPRFDPTAHRRVLLHSPTAFDASTYEI
removed_A2_3__val                        HPDDAAYIMYTSGSTGRPKGVIATHRNITALALDPRFDPTAHRRVLLHSPTAFDASTYEI
Q5V8A8_A1_val                            QPDDLAHIMYTSGSTGHPKGVAITHRAILEFVADRCWKNELQERVLFHSSLGFDISNYEL
                                         :*** *:*********:****  *** *  :. *  :.   :.***:**. .** *.**:

Q9L8H4_A3_6__val                         WVPLLNGNTVVLAPTGDLDVHTYHRVITDQQITALWLTSWVFNLLTEQSPETFTRVRQIW
removed_A2_3__val                        WVPLLNGNTVVLAPTGDLDVHTYHRVITDQQITAVFLTTALFNLLTEHDPACLAGVREVW
Q5V8A8_A1_val                            WVPLLRSGQLVMAPSGPLDVSTLKQVIQKQSITSLFLTTSLFNLVTEENPRCLAGVQQVW
                                         *****... :*:**:* *** * ::** .*.**:::**: :***:**..*  :: *:::*

Q9L8H4_A3_6__val                         TGGEAVSGATVTRLQQACPDTTVVDGYGPTETTTFATHHPVPTPYTGSAVVPIGRPMATM
removed_A2_3__val                        TGGEAVSAFSVRRVQEACPSVVVVDVYGPTETTTFATHNPVPTPYTGPAVVAIGRPMATM
Q5V8A8_A1_val                            IGGEQASVAAIQRMWDACPEITVVNGYGPTETTTYVTLYTIESRPQGNR-VPIGRPMENT
                                          *** .*  :: *: :***. .**: ********:.*  .: :   *   *.***** . 

Q9L8H4_A3_6__val                         HTYVLDDSLQPVAPGVTGELYLAGSGLARGYLDRPALTAERFVANPYAAPGERMYRTGDL
removed_A2_3__val                        HAYVLDDALQPVAPGVVGELYLGGAGLARGYLDRPALTAERFVANPH-RPGERMYRTGDL
Q5V8A8_A1_val                            QVYVLDEELKPVPSGTPGEIYLAGTGLARGYFGQPSLTSTRFLANPFGLPGSRMYRTGDL
                                         :.****: *:**..*. **:**.*:******:.:*:**: **:***.  **.********

Q9L8H4_A3_6__val                         ARWNPDDHLEYAGRADHQVKVRGFRIEPGEIENVLTDHPAVAQAAVHLNRDQPGNPRLVA
removed_A2_3__val                        ARWSADAQLEFVGRADQQVKVRGFRIEPGEIENVLTGHPAVAQAAILVREDQPGRPRLVA
Q5V8A8_A1_val                            GVWIDSDQLVCLGRSDRQVKIRGIRIEPSEIEAELRNHPEIGEAVVTVREDSPDEKRLIA
                                         . *  . :*   **:*:***:**:****.***  * .** :.:*.: :..*.*.. **:*

Q9L8H4_A3_6__val                         YVVAD-TSAPSSDVD
removed_A2_3__val                        YVVADGGTAPDG---
Q5V8A8_A1_val                            YLIA-----------
                                         *::*           
