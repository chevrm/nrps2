CLUSTAL W multiple sequence alignment


Q6WZB2_A1_2__ser                         TYPAARIRHMLDDSGARAVLLRQELRDRLPEDLRDGTGQVAVVPVGAESGAGTSTRRPVT
Q6WZB2_A2_3__ser                         DNPPERIAGLVRDSGARVLLTQRRQTASLP--------KLPGVTVLVVDDHEALSRFPAT
                                           *. **  :: *****.:* ::.    **        ::. *.* . ..  : :* *.*

Q6WZB2_A1_2__ser                         PVEQEPRPERLAYIVYTSGSTGLPKGVMVEHRGIVSYLLGMLEHFPMGPRDRMLQVTSLS
Q6WZB2_A2_3__ser                         VPKPVPRPQDLAYVIYTSGSTGRPKGVMVEHHSVVNYLTTLQEKFRLTSDDRLLLKSPLS
                                           :  ***: ***::******* ********:.:*.**  : *:* : . **:*  :.**

Q6WZB2_A1_2__ser                         FDVSVYEIFLPLLTGGATVLPRSGSHTDAAYLSGLIAEHGVTSFHMVPSLLRTFVDGLD-
Q6WZB2_A2_3__ser                         FDVSVREVFWALSTGATLVVAEAGRHADPDYLVEAIERERVTVVHFVPSMLHVLLETLDG
                                         ***** *:* .* **.: *:..:* *:*. **   * .. ** .*:***:*:.::: ** 

Q6WZB2_A1_2__ser                         PRQCAGLRRIFVSGEALDTTLVVDVHDRLPCDVVNLYGATEVSVDST-----WWTAPRDL
Q6WZB2_A2_3__ser                         PGRCPTLRQVMTSGETLPVQTARRCLELLGAELRNMYGPTETTVEMTDCEVRGRTDTERL
                                         * :*. **:::.***:* .  .    : * .:: *:**.**.:*: *       * .. *

Q6WZB2_A1_2__ser                         PDAPVLVGRPMAGATAYVLDDEMRRLAPNEVGEVYLGGASVTRGYHGRAALTAQRFLPDP
Q6WZB2_A2_3__ser                         P-----IGRPFPNTRVYVLDDELRLVPRGTVGELYVSGAPVARGYLGRPALTADRFLPDP
                                         *     :***:..: .******:* :. . ***:*:.**.*:*** **.****:******

Q6WZB2_A1_2__ser                         YGPPGSRLYRTGDLGRVEDNGELRLLGRIDHQVKLHGRRIEPGEIEAAMTAHPHVSLAAA
Q6WZB2_A2_3__ser                         YGPPGSRMYRTGDLGRFTGEGLLDFQGRGDFQVQLRGHRIEPGEIETVLCEQPGVTAAVA
                                         *******:********. .:* * : ** *.**:*:*:********:.:  :* *: *.*

Q6WZB2_A1_2__ser                         V---PAGAGAGATLTGFFTGAEADA---
Q6WZB2_A2_3__ser                         VVRRPDSPEA-AHLVAYAVRAEEPHGTD
                                         *   * .. * * *..: . **      
