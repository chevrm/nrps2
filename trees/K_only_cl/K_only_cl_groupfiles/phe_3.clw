CLUSTAL W multiple sequence alignment


O30408_A3_4__phe                         DYPEERIRYMLEDSQAKLVVTH---AHLLHKVSSQSEVVDVDDPGSYATQTDNLPCANTP
P09095_A1_phe                            EYPRDRIQYILQDSQTKIVLTQKSVSQLVHDVGYSGEVVVLDEEQLDARETANLHQPSKP
P0C062_A1_1__phe                         EYPKERIQYILDDSQARMLLTQKHLVHLIHNIQFNGQVEIFEEDTIKIREGTNLHVPSKS
                                         :**.:**:*:*:***:::::*:    :*:*.:  ..:*  .::      :  **  ....

O30408_A3_4__phe                         SDLAYIIYTSGTTGKPKGVMLEHKGVANLQAVFAHHLGVTPQDRAGHFASISFDASVWDM
P09095_A1_phe                            TDLAYVIYTSGTTGKPKGTMLEHKGIANLQSFFQNSFGVTEQDRIGLFASMSFDASVWEM
P0C062_A1_1__phe                         TDLAYVIYTSGTTGNPKGTMLEHKGISNLKVFFENSLNVTEKDRIGQFASISFDASVWEM
                                         :****:********:***.******::**: .* : :.** :** * ***:*******:*

O30408_A3_4__phe                         FGPLLSGATLYVLSRDVINDFQRFAEYVRDNAITFLTLPPTYAIYLEPEQVPSLRTLITA
P09095_A1_phe                            FMALLSGASLYILSKQTIHDFAAFEHYLSENELTIITLPPTYLTHLTPERITSLRIMITA
P0C062_A1_1__phe                         FMALLTGASLYIILKDTINDFVKFEQYINQKEITVITLPPTYVVHLDPERILSIQTLITA
                                         * .**:**:**:: ::.*:**  * .*: :: :*.:******  :* **:: *:: :***

O30408_A3_4__phe                         GSASSVALVDKWKEKVTYVNGYGPTESTVCATLWKA-KPDEPVETITIGKPIQNTKLYIV
P09095_A1_phe                            GSASSAPLVNKWKDKLRYINAYGPTETSICATIWEAPSNQLSVQSVPIGKPIQNTHIYIV
P0C062_A1_1__phe                         GSATSPSLVNKWKEKVTYINAYGPTETTICATTWVA-TKETTGHSVPIGAPIQNTQIYIV
                                         ***:* .**:***:*: *:*.*****:::*** * * . : . .::.** *****::***

O30408_A3_4__phe                         DDQLQLKAPGQMGELCISGLSLARGYWNRPELTAEKFVDNPFVPGTKMYRTGDLARWLPD
P09095_A1_phe                            NEDLQLLPTGSEGELCIGGVGLARGYWNRPDLTAEKFVDNPFVPGEKMYRTGDLAKWLTD
P0C062_A1_1__phe                         DENLQLKSVGEAGELCIGGEGLARGYWKRPELTSQKFVDNPFVPGEKLYKTGDQARWLPD
                                         :::*** . *. *****.* .******:**:**::********** *:*:*** *:**.*

O30408_A3_4__phe                         GTIEYLGRIDHQVKIRGHRVELGEVESVLLRYDTVKEAAAITHEDDRGQAYLCAYYVAEG
P09095_A1_phe                            GTIEFLGRIDHQVKIRGHRIELGEIESVLLAHEHITEAVVIAREDQHAGQYLCAYYISQQ
P0C062_A1_1__phe                         GNIEYLGRIDNQVKIRGHRVELEEVESILLKHMYISETAVSVHKDHQEQPYLCAIFVSEK
                                         *.**:*****:********:** *:**:** :  :.*:.. .::*.:   **** :::: 

O30408_A3_4__phe                         EATPAQ
P09095_A1_phe                            EATPAQ
P0C062_A1_1__phe                         HIPLEQ
                                         . .  *
