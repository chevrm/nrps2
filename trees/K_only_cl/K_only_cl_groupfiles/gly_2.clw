CLUSTAL W multiple sequence alignment


Q9L8H4_A2_5__gly                         DHPAARLSHVLGDARPALLLTDTRTEQHLPADADTRRLALDSAEVRALLADCPDTDPAEE
P45745_A1_2__gly                         EFPADRISYMLEDAKPSCIITTEEIAASLPDDLAVPELVLDQAVTQEIIK---RYSPENQ
P45745_A1_gly                            EFPADRISYMLEDAKPSCIITTEEIAASLPDDLAVPELVLDQAVTQEIIK---RYSPENQ
                                         :.** *:*::* **:*: ::*  .    ** *  . .*.**.* .: ::      .* ::

Q9L8H4_A2_5__gly                         GVTPAPGSAAYVIYTSGSTGRPKGVVVPHSALVNFVTAMRRQAPLRPQERLLAVTTVAFD
P45745_A1_2__gly                         DVSVSLDHPAYIIYTSGSTGRPKGVVVTQKSLSNFLLSMQEAFSLGEEDRLLAVTTVAFD
P45745_A1_gly                            DVSVSLDHPAYIIYTSGSTGRPKGVVVTQKSLSNFLLSMQEAFSLGEEDRLLAVTTVAFD
                                         .*: : . .**:***************.:.:* **: :*:.  .*  ::***********

Q9L8H4_A2_5__gly                         IAALELYHPLLSGAAVVLAPKEAVPQPSAVLDLIARHGVTTVQGTPSLWQLLVGHDAEAL
P45745_A1_2__gly                         ISALELYLPLISGAQIVIAKKETIREPQALAQMIENFDINIMQATPTLWHALVTSEPEKL
P45745_A1_gly                            ISALELYLPLISGAQIVIAKKETIREPQALAQMIENFDINIMQATPTLWHALVTSEPEKL
                                         *:***** **:*** :*:* **:: :*.*: ::* ...:. :*.**:**: **  :.* *

Q9L8H4_A2_5__gly                         RGLRMLVGGEALPLSLAEALRALTDDLVNLYGPTETTIWSTAAELAGG-TGAAPIGRPIA
P45745_A1_2__gly                         RGLRVLVGGEALPSGLLQELQDLHCSVTNLYGPTETTIWSAAAFLEEGLKGVPPIGKPIW
P45745_A1_gly                            RGLRVLVGGEALPSGLLQELQDLHCSVTNLYGPTETTIWSAAAFLEEGLKGVPPIGKPIW
                                         ****:******** .* : *: *  .:.************:** *  * .*..***:** 

Q9L8H4_A2_5__gly                         NTRVYVLDDGLQPVAPGVVGELYIAGAGLARGYLDRPALTAERFPADPYGLEPGGRMYRT
P45745_A1_2__gly                         NTQVYVLDNGLQPVPPGVVGELYIAGTGLARGYFHRPDLTAERFVADPYG-PPGTRMYRT
P45745_A1_gly                            NTQVYVLDNGLQPVPPGVVGELYIAGTGLARGYFHRPDLTAERFVADPYG-PPGTRMYRT
                                         **:*****:*****.***********:******:.** ****** *****  ** *****

Q9L8H4_A2_5__gly                         GDLVRWNPDGELEFVGRADHQVKVRGFRIEPGEIEKVLTDHPDIAQAAVVVREDQPGDAR
P45745_A1_2__gly                         GDQARWRADGSLDYIGRADHQIKIRGFRIELGEIDAVLANHPHIEQAAVVVREDQPGDKR
P45745_A1_gly                            GDQARWRADGSLDYIGRADHQIKIRGFRIELGEIDAVLANHPHIEQAAVVVREDQPGDKR
                                         ** .**..**.*:::******:*:****** ***: **::**.* ************* *

Q9L8H4_A2_5__gly                         LVAYVVTGGSADAR
P45745_A1_2__gly                         LAAYVVADAAIDTA
P45745_A1_gly                            LAAYVVADAAIDTA
                                         *.****:..: *: 
