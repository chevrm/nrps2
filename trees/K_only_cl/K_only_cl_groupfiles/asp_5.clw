CLUSTAL W multiple sequence alignment


Q84BQ4_A5_11__asp                        GYPVERLAYMLKDSAPTAVLVQTATRGLFDDAVATVIDLDRSTWQHLPDHDSSVPGLSAS
Q84BQ6v1_A2_asp                          DYPLERLNYMLQNSAPVALLVHGATRQLLGEPSVPLINLDHGSWEQQPSGNPQVPGLNAS
Q84BQ6v2_A2_asp                          DYPLERLNYMLQNSAPVALLVHGATRQLLGEPSVPLINLDHGSWEQQPSGNPQVPGLNAS
                                         .**:*** ***::***.*:**: *** *:.:. ..:*:**:.:*:: *. :..****.**

Q84BQ4_A5_11__asp                        NLAYMIYTSGSTGLPKGVMIEHRSACNMVHWGSQISPPTEHGALLQKAPFSFDSSVWEIF
Q84BQ6v1_A2_asp                          NLAYMIYTSGSTGLPKGVMIEHRSACNMVHWGSQLSPPTGHGALLQKAPFSFDSSVWEIF
Q84BQ6v2_A2_asp                          NLAYMIYTSGSTGLPKGVMIEHRSACNMVHWGSQLSPPTGHGALLQKAPFSFDSSVWEIF
                                         **********************************:**** ********************

Q84BQ4_A5_11__asp                        WPLCSGMRLVLARPNGNRDSAYVVQTIREQQVTVVKFVPALLQQFIEQDGVEQCTSLTDV
Q84BQ6v1_A2_asp                          WPLCSGMRLVLARPDGNRDSAYVVQTIREHQVTVVKFVPALLQQFIEQDGVEQCTSLTDV
Q84BQ6v2_A2_asp                          WPLCSGMRLVLARPDGNRDSAYVVQTIREHQVTVVKFVPALLQQFIEQDGVEQCTSLTDV
                                         **************:**************:******************************

Q84BQ4_A5_11__asp                        LNGGGELSAVLARQVRDRLPWVRLHNVYGPTETTVDSTGWTLEPHMPVPDNVVPIGTALS
Q84BQ6v1_A2_asp                          LNGGGELSAALARQVRDRLPWVRLHNVYGPTETTVDSTGWTLDPEMPVPDNVVPIGTALS
Q84BQ6v2_A2_asp                          LNGGGELSAALARQVRDRLPWVRLHNVYGPTETTVDSTGWTLDPEMPVPDNVVPIGTALS
                                         *********.********************************:*.***************

Q84BQ4_A5_11__asp                        NTRLYVLDAYGQPVPQGVSGELHIGGVGVARGYHGLPEMQAERFIDSPFVAGDRLYRTGD
Q84BQ6v1_A2_asp                          NTRLYVLDAYGQPVPQGVSGELHIGGVGVARGYHGLPEMQAERFIDSPFVPGDRLYRTGD
Q84BQ6v2_A2_asp                          NTRLYVLDAYGQPVPQGVSGELHIGGVGVARGYHGLPEMQAERFIDSPFVPGDRLYRTGD
                                         **************************************************.*********

Q84BQ4_A5_11__asp                        LARYNNHGELEFLGRNDFQIKLRGLRLEPGEIEARLIEHPAIREAVVMVRDERLVAWYTV
Q84BQ6v1_A2_asp                          LARYNHHGELEFLGRNDFQIKLRGLRLEPGEIEARLIEHPAIRQAVVMVRDERLVAWYTV
Q84BQ6v2_A2_asp                          LARYNHHGELEFLGRNDFQIKLRGLRLEPGEIEARLIEHPAIRQAVVMVRDERLVAWYTV
                                         *****:*************************************:****************

Q84BQ4_A5_11__asp                        RSGVEAPS
Q84BQ6v1_A2_asp                          RSGVEAPS
Q84BQ6v2_A2_asp                          RSGVEAPS
                                         ********
