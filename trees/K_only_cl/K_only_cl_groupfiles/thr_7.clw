CLUSTAL W multiple sequence alignment


Q9RLP6v2_A2_thr                          AHPDARIEFVLKDAAPVAAVSSADLCTRLIASGVPVIEVDDPAIGAEASTSLPVPAVDDI
Q9RLP6v1_A2_thr                          AHPDARIEFVLKDAAPVAAVSSADLCTRLIASGVPVIEVDDPAIGAEASTSLPVPAVDDI
                                         ************************************************************

Q9RLP6v2_A2_thr                          AYIIYTSGTTGTPKGVAVTHRNVAQLLDTLGAQLELGQTWTQCHSLAFDYSVWEIWGPLL
Q9RLP6v1_A2_thr                          AYIIYTSGTTGTPKGVAVTHRNVAQLLDTLGAQLELGQTWTQCHSLAFDYSVWEIWGPLL
                                         ************************************************************

Q9RLP6v2_A2_thr                          NGGRLLMVPDAVVRSPEDLHAMLVAEQVSMLSQTPSAFYALQTADALYPERGEQLKLQTV
Q9RLP6v1_A2_thr                          NGGRLLMVPDAVVRSPEDLHAMLVAEQVSMLSQTPSAFYALQTADALYPERGEQLKLQTV
                                         ************************************************************

Q9RLP6v2_A2_thr                          VFGGEALEPHRLSGWMHAHPGMPRMINMYGITETTVHASFREIGEADLANSTSPIGVPLE
Q9RLP6v1_A2_thr                          VFGGEALEPHRLSGWMHAHPGMPRMINMYGITETTVHASFREIGEADLANSTSPIGVPLE
                                         ************************************************************

Q9RLP6v2_A2_thr                          HLSFFVLDGWLRQVPVGVVGELYVAGEGLACGYISRSDLTSTRFVACPFGAPGARMYRTG
Q9RLP6v1_A2_thr                          HLSFFVLDGWLRQVPVGVVGELYVAGEGLACGYISRSDLTSTRFVACPFGAPGARMYRTG
                                         ************************************************************

Q9RLP6v2_A2_thr                          DLVRWGADGQLQYVGRADEQVKIRGYRIELGEVHAALVGLDGVEQAAVIAREDRPGDKRL
Q9RLP6v1_A2_thr                          DLVRWGADGQLQYVGRADEQVKIRGYRIELGEVHAALVGLDGVEQAAVIAREDRPGDKRL
                                         ************************************************************

Q9RLP6v2_A2_thr                          VGYVTGAVDPVKA
Q9RLP6v1_A2_thr                          VGYVTGAVDPVKA
                                         *************
