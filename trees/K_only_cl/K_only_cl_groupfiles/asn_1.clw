CLUSTAL W multiple sequence alignment


Q939Z1_A3_asn                            GYPADRIAFMLDDAGPALVITTAVLSASPIGDVLAARSRTVVLDEPAAAGQLAGRDRAPV
O52819_A3_asn                            DYPADRIAYMLDDAEPALVITTAALSASPVGEVLAARSTTMVIDDPSAAGEVAGRDRAPV
                                         .*******:***** ********.*****:*:****** *:*:*:*:***::********

Q939Z1_A3_asn                            TDTDRARALDPRHPAYLIYTSGSTGRPKAVVVTHRNLTNYLLHCGRMYPGLRGRSVLHSS
O52819_A3_asn                            TDTDRTRRLDPRHPAYLIYTSGSTGRPKAVVITHRNLTNYLFHCGRMYPGLRGRSVMHSS
                                         *****:* ***********************:*********:**************:***

Q939Z1_A3_asn                            IAFDLTVTATFTPLIVGGEIHVGALEDLIGVVEAAPSIFLKATPSHLLTLDTASRGSAGS
O52819_A3_asn                            IAFDLTITAMFTPLTVGGTVHVGALEAVIGAVDSAPSIFLKATPSHLRTLDTGSRESAVS
                                         ******:** **** *** :****** :**.*::************* ****.** ** *

Q939Z1_A3_asn                            GDLLLGGEQLPADTVVQWRRKYPNIVVVNEYGPTEATVGCVEYRLEPGQECPPGGVVPIG
O52819_A3_asn                            GDLLLGGEQLPVDTIVQWRRTYPNTVVVNEYGPTEATVGCVEYRLEPGQECPPGGVVPIG
                                         ***********.**:*****.*** ***********************************

Q939Z1_A3_asn                            TPLANMRAFVLDSWLRLVPPGAVGELYVAGAGLARGYLGRAGLTATRFVADPFGSGERMY
O52819_A3_asn                            TPLTNMRAFVLDSWLRLVPPGAVGELYVSGVGVARGYLGRAGLTASRFVADPFGSGERMY
                                         ***:************************:*.*:************:**************

Q939Z1_A3_asn                            RTGDLVQWNPDGQLVFAGRVDDQVKVRGFRIEPGEIEAALVAQESVGQAVVVARDSEIGT
O52819_A3_asn                            RTGDLVRWNPDGQLVFAGRVDDQVKVRGFRIEPGEIEAALVAQESVGQAVVVAHDSDVGK
                                         ******:**********************************************:**::*.

Q939Z1_A3_asn                            RLIGYVTAAGESGVDEAA
O52819_A3_asn                            RLIAYVTAAGQTGVDTAA
                                         ***.******::*** **
