CLUSTAL W multiple sequence alignment


Q8NJX1_A16_abu|iva                       SHPEQRLRQVTSQTRSKLALTSNNTRNLLSGIIDRVIEVSSALYRHI-NVSLGAKNPQVS
Q8NJX1_A1_abu|iva                        SHPIQRQQQVVSQTKAALALASISNVDMCSKLIGNVVEVSHTLDEALAQTEMSSHGPVSN
Q8NJX1_A12_abu|iva                       SHPEQRLRQVVGQTLAKFALSSPTNAALCNKLVHNVIEVSPSLIDELSKFCDGFNSPAIN
                                         *** ** :**..** : :**:* ..  : . :: .*:*** :*   : :   . :.*  .

Q8NJX1_A16_abu|iva                       VSSHNAAYVLFTSGSTGVPKGLVMTHGGISTSQMAIKRKWGINSSNRTLQFVPNVFDLCL
Q8NJX1_A1_abu|iva                        VSPRNAAYVLSTSGSTGTPKGLVMQHQAVCTSQTAITKRLRMTSDVKMLQFASFVFDLSI
Q8NJX1_A12_abu|iva                       VPSSNAAYVLFTSGSTGTPKGLVMQHGAVCTSQTAIAKRLSLTPDVRILQFAAYVFDLSI
                                         *.. ****** ******.****** * .:.*** ** ::  :... : ***.. ****.:

Q8NJX1_A16_abu|iva                       GESILQLISGACIFIPSEYTRMNGLKDFITEHKINTLFLTPSFVRTLSPDQLPSVTLLLL
Q8NJX1_A1_abu|iva                        GEIFGPWVVGGCICVPSEETRMNDLVNFINTMKINWAYLTPSFARTLNPVDVPGLELLLF
Q8NJX1_A12_abu|iva                       GEIVAPLIHGACVCVPSEETRMNGLKEFIRDARINWAYLTPSFVRTLRPEDVPSLQLLLL
                                         ** .   : *.*: :*** ****.* :**   :**  :*****.*** * ::*.: ***:

Q8NJX1_A16_abu|iva                       AGEAVPRDILTTWFGKVRLWNGWGPAETCLFSSLHQFQSVDESPLTIGRPVGGFCWVVDP
Q8NJX1_A1_abu|iva                        AGEAVRRDVFEAWFGRLRLINGWGPAETCVFSTLHEWKSLDESPLTIGRPVGGHCWIVDP
Q8NJX1_A12_abu|iva                       AGEAVGRDILDTWFGKVRLINGWGPAETCVFSTLHEWSSIDESPLTIGRPVGGYCWIVEA
                                         ***** **:: :***::** *********:**:**::.*:*************.**:*:.

Q8NJX1_A16_abu|iva                       TNANKLAPIGTLGEVVIQGPTVLREYLADVERTKATTMYKLPAWAPYQDQPS--WSRFFK
Q8NJX1_A1_abu|iva                        QDPQRLAPIGTFGEVVIQGPTILREYLADFTKTESSMVRSLPEWVPNRTS-TH-WDRFYK
Q8NJX1_A12_abu|iva                       EDSNKLTPIGCLGEVVLQGPTLLREYLADPQRSKETIITELPPWAPKQVSDAHLWSRFYK
                                          :.::*:*** :****:****:*******  ::: : : .** *.* : . :  *.**:*

Q8NJX1_A16_abu|iva                       SGDLASYNPDGTLEFSNRKDTQVKIRGLRVELGEVEYHVRVAL----KVLVKLLLTYSRP
Q8NJX1_A1_abu|iva                        SGDLCRYNADGTMEFGSRKDGQVKIRGLRVELGEIEHHIREALEGVKQVAVDVANGDGGA
Q8NJX1_A12_abu|iva                       SGDLCFYNPNGTLEFYSRKDTQVKIRGLRVELGEVEHHIRELLEGVRQVAVDVLTSETGT
                                         ****. **.:**:** .*** *************:*:*:*  *    :* *.:      .

Q8NJX1_A16_abu|iva                       IPVQGSSHISVSRMKHARQA-FLNQMESIFLPVTEE
Q8NJX1_A1_abu|iva                        IIV---SYFCFTDETRTAGKNYETNVRDIFAPMTSD
Q8NJX1_A12_abu|iva                       QLV---SYICFNDDSQPSSP--ELKASDIYLPLDAD
                                           *   *::...  .:.       :  .*: *:  :
