CLUSTAL W multiple sequence alignment


Q9RLP6v2_A3_ala                          AHPDARIGFMMSDAKPVAALTTADLRSRLDQYDLAVIDMADPAIDRRPSDALSGPRPDDL
Q9RLP6v1_A3_ala                          AHPDARIGFMMSDAKPVAALTTADLRSRLDQYDLAVIDMADPAIDRRPSDALSGPRPDDL
                                         ************************************************************

Q9RLP6v2_A3_ala                          AYMTYTSGTTGVPKAVAVTHHNVTQLVSALHADLPSGPGQVWSQWHSLVFDVSVWEIWGA
Q9RLP6v1_A3_ala                          AYMTYTSGTTGVPKAVAVTHHNVTQLVSALHADLPSGPGQVWSQWHSLVFDVSVWEIWGA
                                         ************************************************************

Q9RLP6v2_A3_ala                          LLHGGRLVVVPESVGSSPDDLHNLLITEKVSVLCQTPSAAGMLSPEGLESTTLIVAGEAC
Q9RLP6v1_A3_ala                          LLHGGRLVVVPESVGSSPDDLHNLLITEKVSVLCQTPSAAGMLSPEGLESTTLIVAGEAC
                                         ************************************************************

Q9RLP6v2_A3_ala                          PTELVDRWAPGRVMINAYGPTEATIYAAMSEPLTAGTGVAPIGAPVPGAALFVLDKWLRP
Q9RLP6v1_A3_ala                          PTELVDRWAPGRVMINAYGPTEATIYAAMSEPLTAGTGVAPIGAPVPGAALFVLDKWLRP
                                         ************************************************************

Q9RLP6v2_A3_ala                          APEGVVGELYVAGHGVATGYIGRPDLTASRFVACPFGETGERMYRTGDLVRWGSDGQLEY
Q9RLP6v1_A3_ala                          APEGVVGELYVAGHGVATGYIGRPDLTASRFVACPFGETGERMYRTGDLVRWGSDGQLEY
                                         ************************************************************

Q9RLP6v2_A3_ala                          LGRADEQVKIRGYRIELGEIQAALAKLDGVDQAVVIAREDRPGDKRLVGYITGTADPAQL
Q9RLP6v1_A3_ala                          LGRADEQVKIRGYRIELGEIQAALAKLDGVDQAVVIAREDRPGDKRLVGYITGTADPAQL
                                         ************************************************************
