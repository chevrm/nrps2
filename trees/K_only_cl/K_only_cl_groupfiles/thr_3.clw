CLUSTAL W multiple sequence alignment


Q847C7_A1_6__thr                         SYPAERLTYTLQDAAVPILLTQKSLLPSLPENQAIVMCLDRDWGVIAACSQE-----NIV
Q9K5M1_A3_5__thr                         DYPQERLNLILEDAQVSVLLIQQHLVEKLQQHQAHIVCLDSDGEKIAQNSNS-----NPL
Q50JA3_A1_thr                            ASPRERLALILEDAEVTALVTETKRRDELPTEKVPTIFVD------ALQWQEGQRAPNPA
                                           * ***   *:** *. *: :     .*  .:.  : :*      *   :.     *  

Q847C7_A1_6__thr                         SHAQPQNLAYVIYTSGSTGKPKGVLINHQNVIRLFAATQAWYHFGASDVFTLFHSIAFDF
Q9K5M1_A3_5__thr                         NIATPSNLAYVIYTSGSTGKPKGVLVNHSHVVRLFAATDSWYNFNSQDVWTMFHSYAFDF
Q50JA3_A1_thr                            PGLTPDNAAYVIYTSGSTGRPKGVIVTHANATRLFTTTDALYGFGPSDVWTLFHSAAFDF
                                             *.* ***********:****::.* :. ***::*:: * *...**:*:*** ****

Q847C7_A1_6__thr                         SVWELWGALLYGGSLVIVPYWVSRDPSAFHTLLRQEQVTVLNQTPSAFRQLIRVEE-LAK
Q9K5M1_A3_5__thr                         SVWEVWGALLYGGRLVVVGYLVTRSPKSFYELLCQEKVTILNQTPSAFRQLIPAEQSIAT
Q50JA3_A1_thr                            SVWELWGALFYGGRLVVVPHWMTRSPEAFGELIAREGVTVLNQTPSAFRALLRAPSIADG
                                         ****:****:*** **:* : ::*.*.:*  *: :* **:********* *: . .    

Q847C7_A1_6__thr                         TGESQLSLRLVIFGGEALEPQSLQPWFEGYKDQSPQLVNMYGITETTVHVTYRPLSIADV
Q9K5M1_A3_5__thr                         VGD--LNLRLVIFGGETLELNSLQPWFDRHGDQSPQLVNMYGITETTVHVTYRPLSKADL
Q50JA3_A1_thr                            VGG--RGLKWIIFGGEALDAATVRPWFERYADAATRLINMYGITETTVHVTYHHVTQADL
                                         .*    .*: :*****:*:  :::***: : * :.:*:**************: :: **:

Q847C7_A1_6__thr                         NNSKSLIGVPIPDLQLYILDEQLKPLPIGIKGEMYIGGAGLARGYLNRPELTAERFIPNP
Q9K5M1_A3_5__thr                         HGKASVIGRPIGDLQVYVLDEHLQPVPIGVAGEMYVGGAGVTRGYLNRAELTAQRFISNP
Q50JA3_A1_thr                            SSAASLIGRPIPDLVINLLDEHGQPVPDGVPGEMYVGGAGVARGYLKRPELTAQRFIHNP
                                          .  *:** ** ** : :***: :*:* *: ****:****::****:*.****:*** **

Q847C7_A1_6__thr                         FSDAPEARLYKTGDLARYLENGDIEYLDRIDNQVKIRGFRIELGEIEAALLKYPEVQEAV
Q9K5M1_A3_5__thr                         FNGNSEQLLYKSGDLARYLPNGELEYLGRIDNQVKIRGFRIELGEIEAALSQLREVREVV
Q50JA3_A1_thr                            --SAPSERLYRSGDLAIRQQDGTFTYLGRIDDQVKIRGFRIELGEIQSVMARHPAVADAY
                                           . ..  **::****    :* : **.***:**************::.: :   * :. 

Q847C7_A1_6__thr                         VMARTDQPGDKRLVAYIVAKSSNPASENQ
Q9K5M1_A3_5__thr                         VVARSDQPDNKRLVAYVVPQQKNLESSKK
Q50JA3_A1_thr                            VSTYERSADDRRIVAYVVPKQGASE----
                                         * :   ...::*:***:*.:.        
