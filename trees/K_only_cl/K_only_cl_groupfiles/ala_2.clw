CLUSTAL W multiple sequence alignment


Q01886v1_A2_ala                          GHPESRLSGIIKQVQAELLLCSPATSRMGALQNISTQMGTEFKIVELEPEFIRSLPLPPK
Q01886v2_A2_ala                          GHPESRLSGIIKQVQAELLLCSPATSRMGALQNISTQMGTEFKIVELEPEFIRSLPLPPK
                                         ************************************************************

Q01886v1_A2_ala                          PNHQPMVGLNDDLYVVFTSGSTGVPKGAVATHQAYATGIYEHAVACGMTSLGAPPRSLQF
Q01886v2_A2_ala                          PNHQPMVGLNDDLYVVFTSGSTGVPKGAVATHQAYATGIYEHAVACGMTSLGAPPRSLQF
                                         ************************************************************

Q01886v1_A2_ala                          ASYSFDASIGDIFTTLAVGGCLCIPREEDRNPAGITTFINRYGVTWAGITPSLALHLDPD
Q01886v2_A2_ala                          ASYSFDASIGDIFTTLAVGGCLCIPREEDRNPAGITTFINRYGVTWAGITPSLALHLDPD
                                         ************************************************************

Q01886v1_A2_ala                          AVPTLKALCVAGEPLSMSVVTVWSKRLNLINMYGPTEATVACIANQVTCTTTTVSDIGRG
Q01886v2_A2_ala                          AVPTLKALCVAGEPLSMSVVTVWSKRLNLINMYGPTEATVACIANQVTCTTTTVSDIGRG
                                         ************************************************************

Q01886v1_A2_ala                          YRATTWVVQPDNHNSLVPIGAVGELIIEGSILCRGYLNDPERTAEVFIRSPSWLHDLRPN
Q01886v2_A2_ala                          YRATTWVVQPDNHNSLVPIGAVGELIIEGSILCRGYLNDPERTAEVFIRSPSWLHDLRPN
                                         ************************************************************

Q01886v1_A2_ala                          STLYKTGDLVRYSADGKIIFIGRKDTQVKMNGQRFELGEVEHALQLQLDPSDGPIIVDLL
Q01886v2_A2_ala                          STLYKTGDLVRYSADGKIIFIGRKDTQVKMNGQRFELGEVEHALQLQLDPSDGPIIVDLL
                                         ************************************************************

Q01886v1_A2_ala                          KRTQSGEPDLLIAFLFVGRANTGTGNSDEIFIATSTSS
Q01886v2_A2_ala                          KRTQSGEPDLLIAFLFVGRANTGTGNSDEIFIATSTSS
                                         **************************************
