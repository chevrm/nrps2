#!/bin/env perl

use strict;
use warnings;
use Bio::TreeIO;
use Bio::SeqIO;
use Cwd 'abs_path';

## usage:
##	perl treehmmbuilder.pl set.ph set.faa

unless(scalar(@ARGV) >= 2){
	die "Usage:\n\tperl treehmmbuilder.pl set.ph set.faa\n";
}

## Setup base path
my $base_dir = abs_path($0);
$base_dir =~ s/\/trees\/treehmmbuilder\.pl//;

## Initial arguments
my ($treef, $faa) = (shift, shift);
my $fin = $faa;
$fin =~ s/_trim\d+\.faa//;

my %lastspec = ();
my @f = ();
## Read tree
my $treein = new Bio::TreeIO(-file=>$treef, -format=>'newick');
while(my $tree = $treein->next_tree){
	## Loop through leaves to get distances
	## Due to large processing time for big trees, walking out from each node
	## is done rather than generating all distances
	print "Generating distance metrics...";
	my %distfrom = ();
	my @leaves = $tree->get_leaf_nodes;
	for (my $o=0;$o<scalar(@leaves);$o+=1){
		my $oid = $leaves[$o]->id;
		my $osp = getspec($oid);
		my ($bdis, $fdis, $allowed) = (0,0,2); ## Disagreement counters, allow for 2 before break
		for(my $b=$o-1;$b>=0;$b=$b-1){ ## Walk backwards
			last unless(defined $leaves[$b]);
			my $bid = $leaves[$b]->id;
			next if(exists $distfrom{$oid}{$bid}); ## Already processed
			my $bsp = getspec($bid);
			my $d = $tree->distance(-nodes => [$leaves[$o], $leaves[$b]]);
			$distfrom{$oid}{$bid} = $d;
			$distfrom{$bid}{$oid} = $d;
			$bdis += 1 if($bsp ne $osp);
			last if($bdis >= $allowed);
		}
		for(my $f=$o+1;$f<scalar(@leaves);$f+=1){ ## Walk forwards
			last unless(defined $leaves[$f]);
			my $fid = $leaves[$f]->id;
			next if(exists $distfrom{$oid}{$fid}); ## Already processed
			my $fsp = getspec($fid);
			my $d = $tree->distance(-nodes => [$leaves[$o], $leaves[$f]]);
			$distfrom{$oid}{$fid} = $d;
			$distfrom{$fid}{$oid} = $d;
			$fdis += 1 if($fsp ne $osp);
			last if($fdis >= $allowed);
		}
	}
	print "DONE!\n";
	## Loop through leaves to assign groups
	print "Assigning groups...";
	my %group = ();
	my %added = ();
	my $g = 1;
	foreach my $leafout (@leaves){
		my $oid = $leafout->id;
		next if(exists $added{$oid}); ## Seen
		my @oidarr = split(/_+/, $oid);
		my $g2add = $oid;
		$added{$oid} = 1;
		my $num = 1;
		foreach my $iid (sort {$distfrom{$oid}{$a} <=> $distfrom{$oid}{$b}} keys %{$distfrom{$oid}}){
			my @iidarr = split(/_+/, $iid);
			last if($oidarr[-1] ne $iidarr[-1]);
			$g2add .= ',' . $iid;
			$added{$iid} = 1;
			$num += 1;
		}
		next unless($num>1);  ## Comment this out to include hits of only 1
		$group{$g}{'spec'} = $oidarr[-1];
		$group{$g}{'mem'} = $g2add;
		$g += 1;
	}
	print "DONE!\n";
	## Grab group members
	print "Retrieving sequences...";
	foreach my $r (sort {$a <=> $b} keys %group){
		my %get = ();
		my @m = split(/,/, $group{$r}{'mem'});
		foreach(@m){
			$get{$_} = 'not_def';
		}
		## Read faa
		my $fa = new Bio::SeqIO(-file=>$faa, -format=>'fasta');
		my @s = ();
		while(my $seq = $fa->next_seq){
			my $cleanid = $seq->id;
			$cleanid =~ s/[\(\)]/_/g;
			if(exists $get{$cleanid}){
				$get{$cleanid} = $seq->seq;
			}
		}
		## Print faa
		$lastspec{$group{$r}{'spec'}} += 1;
		my $ofn = $group{$r}{'spec'} . '_' . $lastspec{$group{$r}{'spec'}} . '.faa';
		$ofn =~ s/\|/-/g;
		open my $ofa, '>', $ofn or die $!;
		foreach my $s (keys %get){
			die "Issue with seq $s\n" if($get{$s} eq 'not_def');
			print $ofa '>' . "$s\n" . $get{$s} . "\n";
		}
		close $ofa;
		push @f, $ofn;
	}
	print "DONE!\n";
}

my $dir = $fin . '_groupfiles';
system("rm -r $dir") if(-d $dir);
system("mkdir $dir");
print "Generating alignments and pHMMs...";
foreach my $faafi (@f){
	my $pref = $faafi;
	$pref =~ s/\.faa//;
	chomp(my $seqs = `grep '>' $faafi|wc -l`);
	open my $clwo, '>', "$pref.clw" or die $!;
	## Generate alignment
	system("mafft --clustalout --quiet --namelength 40 $faafi > tmp.clw");
	## Format alignment
	open my $clwi, '<', 'tmp.clw' or die $!;
	while(<$clwi>){
		$_ = "CLUSTAL W multiple sequence alignment\n" if($_ =~ m/^CLUSTAL/);
		print $clwo $_;
	}
	close $clwi;
	system("rm tmp.clw");
	close $clwo;
	## Generate pHMM
	system("hmmbuild $pref.hmm $pref.clw > /dev/null");
	## Cleanup
	my @mv = ($faafi, "$pref.clw", "$pref.hmm");
	foreach my $m(@mv){
		system("mv $m $dir/");
	}
}
print "DONE!\n";
## Create and press full pHMM
my $db = $fin . '_nrpsA.hmmdb';
print "Creating and pressing full pHMM into $db...";
foreach(glob("$db*")){
	system("rm $_");
}
system("cat $dir/*.hmm > $db");
system("hmmpress $db > /dev/null");
print "DONE!\n";


sub getspec{
	my @idarr = split(/_+/, $_[0]);
	return $idarr[-1];
}
