#!/bin/env perl

use strict;
use warnings;

my @fullsan = ();
my @redseq = ();

my ($fullf, $redf) = ('K_full_san.faa', 'K_reduced.faa');
open my $ffh, '<', $fullf or die $!;
while(<$ffh>){
	chomp;
	if($_ =~ m/^>/){
		push @fullsan, $_;
	}
}
close $ffh;
open my $rfh, '<', $redf or die $!;
while(<$rfh>){
	chomp;
	unless($_ =~ m/^>/){
		push @redseq, $_;
	}
}
close $rfh;
for(my $i=0;$i<scalar(@redseq);$i+=1){
	print $fullsan[$i] . "\n" . $redseq[$i] . "\n";
}
