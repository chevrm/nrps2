CLUSTAL W multiple sequence alignment


Q9Z4X6_A3_trp                            TTSALAASLPDTGTPVLLLDTPETAATLAALPGHDVTDADRPVPLRPEHPAYMIYTSGTT
Q8CJX2_A2_11__trp                        TTAAVAAGLPDTDVPRLLLD--EEPAAGGGEDAADLTDADRLAPLLPGHPAYVIYTSGTT
                                         **:*:**.****..* ****  * .*: ..  . *:***** .** * ****:*******

Q9Z4X6_A3_trp                            GRPKGVVVTHTGLPGLLDIFTRDCAAGPGSRILQHLSPSFDASFWELAMGLLTGATLVVA
Q8CJX2_A2_11__trp                        GRPKGVTVTHSGLPALLDIFTSQLDVVPGSRVLHHLSPAFDGGFWELAMGLLTGAALVVV
                                         ******.***:***.****** :  . ****:*:****:**..************:***.

Q9Z4X6_A3_trp                            PPETTPGPELAELATRHAATHLSLTTSVLGLLPPDSLPDGLTLVVGAEAIPPELVERWSP
Q8CJX2_A2_11__trp                        EPGTVPGPALAALAVRHRVTHAAITPAVLQLIPEGALPAGTTLVVAAETCPPELVARWSA
                                          * *.*** ** **.** .** ::*.:** *:* .:** * ****.**: ***** ***.

Q9Z4X6_A3_trp                            GRTMLNSYGPTETTVCSTMSGPLSGPAVPPIGSPVANSAVYVLDAALRPVPPGVPGELYA
Q8CJX2_A2_11__trp                        GRLMRNSYGPTETTVCATMSAPLAGAAVPPIGRPIADTAGYVLDDALQPVPPGVPGELYV
                                         ** * ***********:***.**:*.****** *:*::* **** **:***********.

Q9Z4X6_A3_trp                            AGAHLARGYHDRRALTAERFVANPFGEPGSRLYRTGDLVRWRPDGQLEYLGRADTQVKIR
Q8CJX2_A2_11__trp                        RGPGLARGYLGRPSLTAGRFVACPFGPAGGVMYRTGDLVRHRADGDLEYLGRTDTQVKLR
                                          *. ***** .* :*** **** *** .*. :******** *.**:******:*****:*

Q9Z4X6_A3_trp                            GLRIEPTEIEAVITERPHLARAAVIVREDRPGDRRLVAYVVP
Q8CJX2_A2_11__trp                        GMRVEPAEIEAVTAGLPGVAQAAVLVREDTPGDRRLVGYVVP
                                         *:*:**:***** :  * :*:***:**** *******.****
