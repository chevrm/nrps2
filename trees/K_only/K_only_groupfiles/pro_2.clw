CLUSTAL W multiple sequence alignment


O05647_A1_pro                            LVLTE----------------DD--VDEDLSGIPDGNLTDAERTAPLTPAHPAYVIYTSG
Q9L8H4_A1_4__pro                         LVLTTTETEAKLPDRHPGLLLDDPAVLADLSGRP-----AHDPVVELHPDHPAYVIYTSG
                                         ****                 **  *  **** *       : .. * * **********

O05647_A1_pro                            STGRPKAVVMPGAAVVNLLAWHRREIPAGAGTTVAQFASLSFDVAAQEILSTLLYGATLA
Q9L8H4_A1_4__pro                         STGVPKGVVMPAGGLLNLLQWHHRAVGDEPGTRTAQFTAISFDVSAQEVLSSVAFGKTLV
                                         *** **.****...::*** **:* :   .** .***:::****:***:**:: :* **.

O05647_A1_pro                            VPTDAVRRDADAFAAWLEEYRVNELYAPNLVVEALAEAAAEQGRTLPDLRHIAQAGEALT
Q9L8H4_A1_4__pro                         IPDEEVRRDAARFAGWLDDRQVDELFAPNLVLEALAEAAVETGRTLPQLRTVAQAGEALT
                                         :* : *****  **.**:: :*:**:*****:*******.* *****:** :********

O05647_A1_pro                            AGPRVRDFCAALPGRRLHNHYGPAETHVMTGIELPVDPGGWPERVPIGGPVDNARLYVLD
Q9L8H4_A1_4__pro                         LSRTVRAFHRSAPGRRLHNHYGPTETHVVTAHALGDDPEDWRLPAPIGRPIDNTHAYVRT
                                          .  ** *  : ***********:****:*.  *  ** .*   .*** *:**:: **  

O05647_A1_pro                            GFLRPVPPGVVGELYLAGAGVARGYLNRPGLTAERFVADPFG-GPGTRMYRTGDLARWAG
Q9L8H4_A1_4__pro                         RAVRLVEPGVVGELYIAGAGLARGYLGRPALTAERFVADPYGLEPGGRMYRTGDLVRRNP
                                           :* * ********:****:*****.**.**********:*  ** ********.*   

O05647_A1_pro                            SGVLEFAGRADHQVKVRGFRIEPGEVESVLAAQPGVARAVVLAREDRPGERRLVAYLVA
Q9L8H4_A1_4__pro                         DGELEFCGRADHQVKVRGFRIEPGEIEKVLTDHPDIAQAAVVTRPHRPGDTRLVAYVVG
                                         .* ***.******************:*.**: :*.:*:*.*::* .***: *****:*.
