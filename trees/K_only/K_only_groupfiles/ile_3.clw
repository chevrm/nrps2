CLUSTAL W multiple sequence alignment


Q9K5L9_A1_7__ile                         TQQHLVEKL--------PEHQVPVVCLDTDWLVICESSPESPITEVQPGNLAYVIYTSGS
Q84BC7_A1_4__ile                         TQEKLVNKLGERLRRGFAERNASVICLDSNWDIINQQTQNNPTTSVTADNLAYVMYTSGS
                                         **::**:**        .*::..*:***::* :* :.: :.* *.* ..*****:*****

Q9K5L9_A1_7__ile                         TGTPKGVVVNHQAVNRLVKNTNYVQLTPDDRVAQAANIAFDAATFEIWGALLNGAKLVMI
Q84BC7_A1_4__ile                         TGQPKGVSIVHRSVVRLVKETNYISISADDVIAQASNHAFDAATFEIWGALLNGARLVGV
                                         ** **** : *::* ****:***:.::.** :***:* *****************:** :

Q9K5L9_A1_7__ile                         TKSVLLSPQEFAANIRDREVSVLFLTTALFNQLASFVPQAFSSLRYLLFGGEAVDPQWVQ
Q84BC7_A1_4__ile                         SKDLALSPRDFAVFMRSQSISVLFLTTALFNQIAQEVPSAFNSLRHLLFGGEAVDPKWVK
                                         :*.: ***::**. :*.:.:************:*. **.**.***:**********:**:

Q9K5L9_A1_7__ile                         EVLEKGAPKQLLHVYGPTENTTFSSWYLVEELTTIATTIPIGRAISNTQIYLLDQNLQPV
Q84BC7_A1_4__ile                         EVLNNGAPQRLLHVYGPTENTTFSSWYLVQDVPEGATTIPIGQPISNTQIYLLDSQLQPV
                                         ***::***::*******************:::.  *******:.**********.:****

Q9K5L9_A1_7__ile                         PVGVPGELHVGGAGLARGYLNRPELTQEKFIPNPFDNS----------------------
Q84BC7_A1_4__ile                         GIGVPGELYIGGDGLAREYLNRTELTQEKLIQNPFGGSRGAGEQGSKGAEEQSFPSASSE
                                          :******::** **** ****.******:* ***..*                      

Q9K5L9_A1_7__ile                         KLYKTGDLARYLPDGNIEYLGRIDHQVKIRGFRIELGEIEAVLSQHEDVQISCVIVREDT
Q84BC7_A1_4__ile                         RLYKTGDKARYLSDGNIEYLGRIDDQVKIRGLRIELGEIEAVLSQHSDVQVSCVIVREDT
                                         :****** ****.***********.******:**************.***:*********

Q9K5L9_A1_7__ile                         PGETCTERSRSKQLVAYIVP
Q84BC7_A1_4__ile                         PGD--------KRLVAYIVT
                                         **:        *:******.
