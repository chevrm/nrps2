CLUSTAL W multiple sequence alignment


P09095_A1_phe                            KSVSQLVHDVGYSGEVVVLDEEQLDARETANLHQPSKPTDLAYVIYTSGTTGKPKGTMLE
O30408_A3_4__phe                         -THAHLLHKVSSQSEVVDVDDPGSYATQTDNLPCANTPSDLAYIIYTSGTTGKPKGVMLE
P0C062_A1_1__phe                         KHLVHLIHNIQFNGQVEIFEEDTIKIREGTNLHVPSKSTDLAYVIYTSGTTGNPKGTMLE
                                             :*:*.:  ..:*  .::      :  **  ....:****:********:***.***

P09095_A1_phe                            HKGIANLQSFFQNSFGVTEQDRIGLFASMSFDASVWEMFMALLSGASLYILSKQTIHDFA
O30408_A3_4__phe                         HKGVANLQAVFAHHLGVTPQDRAGHFASISFDASVWDMFGPLLSGATLYVLSRDVINDFQ
P0C062_A1_1__phe                         HKGISNLKVFFENSLNVTEKDRIGQFASISFDASVWEMFMALLTGASLYIILKDTINDFV
                                         ***::**: .* : :.** :** * ***:*******:** .**:**:**:: ::.*:** 

P09095_A1_phe                            AFEHYLSENELTIITLPPTYLTHLTPERITSLRIMITAGSASSAPLVNKWKDKLRYINAY
O30408_A3_4__phe                         RFAEYVRDNAITFLTLPPTYAIYLEPEQVPSLRTLITAGSASSVALVDKWKEKVTYVNGY
P0C062_A1_1__phe                         KFEQYINQKEITVITLPPTYVVHLDPERILSIQTLITAGSATSPSLVNKWKEKVTYINAY
                                          * .*: :: :*.:******  :* **:: *:: :******:* .**:***:*: *:*.*

P09095_A1_phe                            GPTETSICATIWEAPSNQLSVQSVPIGKPIQNTHIYIVNEDLQLLPTGSEGELCIGGVGL
O30408_A3_4__phe                         GPTESTVCATLWKA-KPDEPVETITIGKPIQNTKLYIVDDQLQLKAPGQMGELCISGLSL
P0C062_A1_1__phe                         GPTETTICATTWVA-TKETTGHSVPIGAPIQNTQIYIVDENLQLKSVGEAGELCIGGEGL
                                         ****:::*** * * . : . .::.** *****::***:::*** . *. *****.* .*

P09095_A1_phe                            ARGYWNRPDLTAEKFVDNPFVPGEKMYRTGDLAKWLTDGTIEFLGRIDHQVKIRGHRIEL
O30408_A3_4__phe                         ARGYWNRPELTAEKFVDNPFVPGTKMYRTGDLARWLPDGTIEYLGRIDHQVKIRGHRVEL
P0C062_A1_1__phe                         ARGYWKRPELTSQKFVDNPFVPGEKLYKTGDQARWLPDGNIEYLGRIDNQVKIRGHRVEL
                                         *****:**:**::********** *:*:*** *:**.**.**:*****:********:**

P09095_A1_phe                            GEIESVLLAHEHITEAVVIAREDQHAGQYLCAYYIS
O30408_A3_4__phe                         GEVESVLLRYDTVKEAAAITHEDDRGQAYLCAYYVA
P0C062_A1_1__phe                         EEVESILLKHMYISETAVSVHKDHQEQPYLCAIFVS
                                          *:**:** :  :.*:.. .::*.:   **** :::
