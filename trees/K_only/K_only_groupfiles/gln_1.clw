CLUSTAL W multiple sequence alignment


Q65NK5_A1_gln                            TDSGLTFETAETVQFSEALSESRENGYPSSAAGAGHLAYIIYTSGTTGRPKGVMIEHRQV
O66069_A1_gln                            TDSGLTFETAETVRFSEALSESLENGHPSSEAGAGHLAYIIYTSGTTGRPKGVMIEHRQV
Q45295_A1_gln                            TDSGLTFETTETVRFSEALSESLENGHPSSEAGAGHLAYIIYTSGTTGRPKGVMIEHRQV
                                         *********:***:******** ***:*** *****************************

Q65NK5_A1_gln                            HHLVRGLQQAVGTYDQDDLKLALLAPFHFDASVQQIFTSLLLGQTLYIVPKKTVSDGRAL
O66069_A1_gln                            HHLVRGLQQAVGAYDQDDLKLALLAPFHFDASVQQIFTSLLLGQTLYIVPKKTVSDGRAL
Q45295_A1_gln                            HHLVRGLQQAVGAYDQDDLKLALLAPFHFDASVQQIFTSLLLGQTLYIVPKKTVSDGRAL
                                         ************:***********************************************

Q65NK5_A1_gln                            SDYYRRHQIDVTDGTPAHLQLLAAADDLSGVKLRHMLVGGEALSRVATERLLQLFAETAE
O66069_A1_gln                            SDYYRRHQIDVTDGTPAHLQLLSAADDLSGVKLRHMLVGGEALSRVATERLLQLFAETAE
Q45295_A1_gln                            SDYYRRHQIDVTDGTPAHLQLLSAADDLSGVKLRHMLVGGEALSRVATERLLQLFAETAE
                                         **********************:*************************************

Q65NK5_A1_gln                            SVPAVTNVYGPTETCVDASSFTITNRTDLQYDTAYVPIGRPIGNNRFYILDENGALLPDG
O66069_A1_gln                            SVPDVTNVYGPTETCVDASSFTMTNHADLQGDTAYVPIGRPIGNNRFYILDENGALLPDG
Q45295_A1_gln                            SVPDVTNVYGPTETCVDASSFTMTNHADLQADTAYVPIGRPIGNNRFYILDEGGALLPDG
                                         *** ******************:**::*** *********************.*******

Q65NK5_A1_gln                            VEGELYIAGDGVGRGYLNLPDMTRDRFLKDPFVSGGLMYRTGDTARWLPDGTVDFIGRRD
O66069_A1_gln                            VEGELYIAGDGVGRGYLNLPDMTADKFLEDPFVPGGFMYRTGDAVRWLPDGTVDFIGRKD
Q45295_A1_gln                            VEGELYIAGDGVGRGYLNLPDMTADKFLEDPFVPGGFMYRTGDAVRWLPDGTVDFIGRKD
                                         *********************** *:**:****.**:******:.*************:*

Q65NK5_A1_gln                            DQVKIRGFRIELGEIESVLQGAPAVEKAVVLARHETGGSLEVCAYVVP
O66069_A1_gln                            DQVKIRGYRIELGEIESVLQGAPAVGKAVVLARPETGGSLEVCAYVVP
Q45295_A1_gln                            DQVKIRGYRIELGEIESVLQGAPAVGKAVVLARPETGGSLEVCAYVVP
                                         *******:***************** ******* **************
