CLUSTAL W multiple sequence alignment


Q9L8H4_A3_6__val                         LITDHTTDLDTTTTQFNPADTPHDGEDPGNPNHTTHPDDAAYIMYTSGSTGRPKGVIATH
removed_A2_3__val                        LITDHTTDLDTTTTQFNPADTPHDGEDPGNPNHTTHPDDAAYIMYTSGSTGRPKGVIATH
                                         ************************************************************

Q9L8H4_A3_6__val                         RNITALALDPRFDPTAHRRVLLHSPTAFDASTYEIWVPLLNGNTVVLAPTGDLDVHTYHR
removed_A2_3__val                        RNITALALDPRFDPTAHRRVLLHSPTAFDASTYEIWVPLLNGNTVVLAPTGDLDVHTYHR
                                         ************************************************************

Q9L8H4_A3_6__val                         VITDQQITALWLTSWVFNLLTEQSPETFTRVRQIWTGGEAVSGATVTRLQQACPDTTVVD
removed_A2_3__val                        VITDQQITAVFLTTALFNLLTEHDPACLAGVREVWTGGEAVSAFSVRRVQEACPSVVVVD
                                         *********::**: :******:.*  :: **::********. :* *:*:***...***

Q9L8H4_A3_6__val                         GYGPTETTTFATHHPVPTPYTGSAVVPIGRPMATMHTYVLDDSLQPVAPGVTGELYLAGS
removed_A2_3__val                        VYGPTETTTFATHNPVPTPYTGPAVVAIGRPMATMHAYVLDDALQPVAPGVVGELYLGGA
                                          ************:********.***.*********:*****:********.*****.*:

Q9L8H4_A3_6__val                         GLARGYLDRPALTAERFVANPYAAPGERMYRTGDLARWNPDDHLEYAGRADHQVKVRGFR
removed_A2_3__val                        GLARGYLDRPALTAERFVANPH-RPGERMYRTGDLARWSADAQLEFVGRADQQVKVRGFR
                                         *********************:  **************..* :**:.****:********

Q9L8H4_A3_6__val                         IEPGEIENVLTDHPAVAQAAVHLNRDQPGNPRLVAYVVA
removed_A2_3__val                        IEPGEIENVLTGHPAVAQAAILVREDQPGRPRLVAYVVA
                                         ***********.********: :..****.*********
