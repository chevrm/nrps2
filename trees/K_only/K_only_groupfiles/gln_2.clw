CLUSTAL W multiple sequence alignment


Q93I55_A3_gln                            LQDDIGFSGTCIDLMEEHFYHEKDSSLALSYQSSQLAYAIYTSGTT----GKPKGTLIEH
Q9R9J0_A3_gln                            LPENLNFSGTCINMKEEQAYHETDINLAVPCQFDQLAYCIYTSGTTGTPKGTPKGTLIEH
                                         * :::.******:: **: ***.* .**:. * .****.*******    *.********

Q93I55_A3_gln                            RQVIHLIEGLSRQVYSAYDAELNIAMLAPYYFDASVQQMYASLLSGHTLFIVPKEIVSDG
Q9R9J0_A3_gln                            RQVIHLIEGLRNAVYSAYDGVLHVAMLAPYYFDASVQQIYASLLLGHTLFIVPKEAVSDG
                                         ********** . ******. *::**************:***** ********** ****

Q93I55_A3_gln                            AALCRYYRQHSIDITDGTPAHLKLLIAAGDLQGVTLQHLLIGGEALSKTTVNKLKQLFGE
Q9R9J0_A3_gln                            EALCQYYRQHRIDVTDGTPAHLKLLVAADDGEGVPLRHLLIGGEALPKTTVTKFIHLFGA
                                          ***:***** **:***********:**.* :**.*:*********.****.*: :*** 

Q93I55_A3_gln                            HGAAPGITNVYGPTETCVDASLFNIECSSDAWARSQNYVPIGKPLGRNRMYILDSKKRLQ
Q9R9J0_A3_gln                            DRAAPAITNVYGPTETCVDASLFNIEVSADAWTRSQVHIPIGKPLGNNRMYILDSQQKLQ
                                         . ***.******************** *:***:*** ::*******.********:::**

Q93I55_A3_gln                            PKGVQGELYIAGDGVGRGYLNLPELTDEKFVADPFVPEDRMYRTGDLARLLPDGNIEYIG
Q9R9J0_A3_gln                            PVGVQGELYIAGDGVGRGYLNLPELTNKKFVNDPFVPSGRMYRTGDLARLLPDGNIEFIE
                                         * ************************::*** *****..******************:* 

Q93I55_A3_gln                            RIDHQVKIQGFRIELGEIESVMLNVPDIQEAAAAALKDADDEYYLCGYFAA
Q9R9J0_A3_gln                            RVDHQVKIHGFRIELGEIESIMLNIPEIQEAVASVLEDADGEHYICGYYVA
                                         *:******:***********:***:*:****.*:.*:***.*:*:***:.*
