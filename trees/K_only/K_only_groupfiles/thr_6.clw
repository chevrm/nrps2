CLUSTAL W multiple sequence alignment


Q6YK39_A2_thr                            LQHHLLEGTDYQSHTVFLDDPSSYGAETSNLNLNVMPNQLAYVIYTSGTTGNPKGTLIEH
Q70K00_A2_thr                            LQHHLLEGTDYQSHTVFLDDPSSYGAEASNLKLNVMPNQLAYVIYTSGTTGNPKGTLIEH
                                         ***************************:***:****************************

Q6YK39_A2_thr                            KNVVRLLFNNKNVFDFNASDTWTLFHSFCFDFSVWEMYGALLYGGKLVIVPKQIAKNPER
Q70K00_A2_thr                            KNVVRLLFNNKNVFDFNASDTWTLFHSFCFDFSVWEMYGALLYGGKLVIIPKQIAKNPER
                                         *************************************************:**********

Q6YK39_A2_thr                            YLQLLKSEAVTILNQTPSYFYQLMQEERADPESNLNIRKIIFGGEALNPSFLKDWKLKYP
Q70K00_A2_thr                            YLQLLKSEAVTILNQTPSYFYQLMQEERADPESNLNIRKIIFGGEALNPSFLKDWKLKYP
                                         ************************************************************

Q6YK39_A2_thr                            LTQLINMYGITETTVHVTYKEITEREIDEGRSNIGQPIPTLQAYILDEYQHIQVMGIPGE
Q70K00_A2_thr                            LTQLINMYGITETTVHVTYKEITEREIDEGRSNIGQPIPTLQAYILDEYQRIQVMGIPGE
                                         **************************************************:*********

Q6YK39_A2_thr                            LYVAGEGLARGYLNRPELTAEKFVEHPFAAGEKMYKTGDVARWLPDGNIEYLGRIDHQVK
Q70K00_A2_thr                            LYVAGEGLARGYLNRPELTGEKFVEHPFAAGEKMYKTGDVARWLPDGNIEYLGRIDHQVK
                                         *******************.****************************************

Q6YK39_A2_thr                            IRGYRIEIGEVEAALLQSESVKEAVVIAIEEEGSKQLCAYLSG
Q70K00_A2_thr                            IRGYRIEIGEVEAALLQLESVKEAVVIAIEEEGSKQLCAYLSG
                                         ***************** *************************
