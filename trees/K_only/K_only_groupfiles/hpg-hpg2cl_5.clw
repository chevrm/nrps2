CLUSTAL W multiple sequence alignment


Q5J1Q7_A3_hpg|hpg2cl                     RDDPVFGDTPLLTVADLRPGAPAPVRP----------------PEPDRLAYVAYTSGSTG
Q5J1Q7_A1_hpg|hpg2cl                     --GALHAGAPGAGSTDAVLAAEAPVLPVHVGDDEGAEGVPWPEPERDQAAYLVYTSGSTG
                                           ..:...:*    :*   .* *** *                ** *: **:.*******

Q5J1Q7_A3_hpg|hpg2cl                     EPKGVQCAHHGLANQLMWSRRAYPLNPGEALAQVAAVGFDISLWELLHPLTSGGRLVVLD
Q5J1Q7_A1_hpg|hpg2cl                     RPKGVLVTHGALANRMLWWQGEHPLGPDDVLMATASPAFDIAVWELLAAFVGGARLVIAE
                                         .****  :* .***:::* :  :**.*.:.*  .*: .***::**** .:..*.***: :

Q5J1Q7_A3_hpg|hpg2cl                     QERHGDVVAIAELVAAERVVVLHLVPTLLEH---YLDEGPADSLRHVVCGGERLSPGLPA
Q5J1Q7_A1_hpg|hpg2cl                     HRLRGVVPHLPELMTDHRVTVAHFVPSVLEELLGWMADGGRVGLRLVVCGGEAVPPSQRD
                                         :. :* *  :.**:: .**.* *:**::**.   :: :*   .** ****** :.*.   

Q5J1Q7_A3_hpg|hpg2cl                     RFAARTPAALNHTYGPTEASIIVTH--WRSPDPAPDAVSLGAPLPGARVYLLDPHGQPVP
Q5J1Q7_A1_hpg|hpg2cl                     RLLALSGARMVHAYGPTETTITVVHDECRADDPAP-GLPLGRPMHNAAVAVVDADGRRAP
                                         *: * : * : *:*****::* *.*   *: **** .:.** *: .* * ::*..*: .*

Q5J1Q7_A3_hpg|hpg2cl                     VGVVGELVLGGEVLARGYLGRPGATAERFLPDPFSDVP-GARAYRTGDLARHRPDGGLEF
Q5J1Q7_A1_hpg|hpg2cl                     VGVAGELVVGGVPLARGYLGRPGETAARFVPDWLGLGPAGGRVYRTGDRARRLPDGRIEF
                                         ***.****:**  ********** ** **:** :.  * *.*.***** **: *** :**

Q5J1Q7_A3_hpg|hpg2cl                     VGRADRQVKILGVRVEPHEVETALVANPAVAACAVLPREDARGAVGLVGYLVP
Q5J1Q7_A1_hpg|hpg2cl                     LGRVDDEFKVRGHRVDPAEIESLLHQHPLVGRAAV---RLADGA-----HVVA
                                         :**.* :.*: * **:* *:*: *  :* *. .**   . * **     ::*.
