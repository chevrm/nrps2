CLUSTAL W multiple sequence alignment


Q9RAH4_A3_pro|me-pro                     TQQSILDRLPQ------------------------------------HQANQVCLDTDAQ
Q84BC7_A3_6__pro|me-pro                  TQRSLLDRLPQCEKAGGQGAGSRGESPSTRDRASTKGKEEVLSLPASYQTQLVCLDTDAE
                                         **:*:******                                    :*:: *******:

Q9RAH4_A3_pro|me-pro                     LISQCSQDNLISDVQANNLAYIIYTSGSTGQPKGIAMNQLALSNLILWHRENLKIPRGAK
Q84BC7_A3_6__pro|me-pro                  LISQCSQDNLITGVQANNLGYIIYTSGSTGQPKGIAMNQLALCNLILWHPDNLKIARGAK
                                         ***********:.******.**********************.****** :****.****

Q9RAH4_A3_pro|me-pro                     TLQFASINFDVSFQEIFTTWCSGGTLFLIGEELRRDTSALLGFLQQKAIERMFLPFVALQ
Q84BC7_A3_6__pro|me-pro                  TLQFASINFDVSFQEIFTTWCSGGTLFLITKELRHDTSNLLRVIQEKAIQRMFLPVVGLQ
                                         ***************************** :***:*** ** .:*:***:*****.*.**

Q9RAH4_A3_pro|me-pro                     QLAEVAIGGELVNSHLREIITAGEQLQITPAISQWLSKLTDCTLHNHYGPSESHLATSFT
Q84BC7_A3_6__pro|me-pro                  QLAEFAVGSELVNTHLREIITAGEQLQITPAISKWLSQLSDCTLHNHYGPSESHVATSFT
                                         ****.*:*.****:*******************:***:*:**************:*****

Q9RAH4_A3_pro|me-pro                     LTNSVETWPLLPPVGRPIANAQIYILDRFLQPVPVGVPGELYIAGVLLSQGYFNRPELTL
Q84BC7_A3_6__pro|me-pro                  LPNLVNTWPLLPPIGRPISNTQIYILDKYLQPVPIGVPGEVYIAGVLLARGYLNRPELTQ
                                         *.* *:*******:****:*:******::*****:*****:*******::**:****** 

Q9RAH4_A3_pro|me-pro                     EKFIPNPFKRSRGAGEQGSRG---ETF-----------NCDRLYKTGDLARYLSDGNIEY
Q84BC7_A3_6__pro|me-pro                  EKFIQNPFGGSRGAGEQGSRGAEEQSFPSAPHSLCPSASSERLYKTGDLARYLPDGNIEY
                                         **** ***  ***********   ::*           ..:************.******

Q9RAH4_A3_pro|me-pro                     LGRIDNQVKIRGFRIELGEIEAVLSQ-LDVQASCAMATPAAGIAREDIPGNKRLVAYIVP
Q84BC7_A3_6__pro|me-pro                  LGRIDNQVKIRGFRIELGEIEAVLSQHINVQASCA-------VVREDTPGDKRLVAYIVP
                                         ************************** ::******       :.*** **:*********
