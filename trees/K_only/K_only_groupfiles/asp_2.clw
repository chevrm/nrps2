CLUSTAL W multiple sequence alignment


Q84BQ6v1_A2_asp                          VHGATRQLLGEPSVPLINLDH-----GSWEQQPSGNPQVPGLNASNLAYMIYTSGSTGLP
O85168_A8_asp                            TQGALSLPVGD--TPLMLLDSAESLLAADDQAFDANPVVDGLTAENLAYVIYTSGSTGQS
Q84BQ6v2_A2_asp                          VHGATRQLLGEPSVPLINLDH-----GSWEQQPSGNPQVPGLNASNLAYMIYTSGSTGLP
                                         .:**    :*:  .**: **      .: :*  ..** * **.*.****:******** .

Q84BQ6v1_A2_asp                          KGVMIEHRSACNMVHWG------SQLSPPTGHGALLQKAPFSFDSSVWEIFWPLCSGMRL
O85168_A8_asp                            KGVMVEHRSVFNF--WNVLTRTTHQHCPTPATVAL--NAGFFFDMSIKGI-SQLFSGHKL
Q84BQ6v2_A2_asp                          KGVMIEHRSACNMVHWG------SQLSPPTGHGALLQKAPFSFDSSVWEIFWPLCSGMRL
                                         ****:****. *:  *.       * .*...  **  :* * ** *:  *   * ** :*

Q84BQ6v1_A2_asp                          V----LARPDGNRDSAYVVQTIREHQVTVVKFVPALLQQFIEQDGVEQCT-SLTDVLNGG
O85168_A8_asp                            VIIPQLLRANGSE----LLDFLEAHQVHAFDSTPSQLDTLLSAGLLERSSYQPVSVLLGG
Q84BQ6v2_A2_asp                          V----LARPDGNRDSAYVVQTIREHQVTVVKFVPALLQQFIEQDGVEQCT-SLTDVLNGG
                                         *    * *.:*..    ::: :. *** ... .*: *: ::. . :*:.: . ..** **

Q84BQ6v1_A2_asp                          GELSAALARQVRDRLPWVRLHNVYGPTETTVDSTGWTLD-----PEMPVPDNVVPIGTAL
O85168_A8_asp                            EAINASTWEKLRN-CPTIRLYNMYGPTECTVDA---TIDLIRDLGEKP------SIGRPI
Q84BQ6v2_A2_asp                          GELSAALARQVRDRLPWVRLHNVYGPTETTVDSTGWTLD-----PEMPVPDNVVPIGTAL
                                           :.*:  .::*:  * :**:*:***** ***:   *:*      * *      .** .:

Q84BQ6v1_A2_asp                          SNTRLYVLDAYGQPVPQGVSGELHIGGVGVARGYHGLPEMQAERFIDSPF--VPGDRLYR
O85168_A8_asp                            ANVQVHVLDARGEPAPLGVAGEIHIGGSGVARGYLNRDELSAERFIVDPFSDAANARLYK
Q84BQ6v2_A2_asp                          SNTRLYVLDAYGQPVPQGVSGELHIGGVGVARGYHGLPEMQAERFIDSPF--VPGDRLYR
                                         :*.:::**** *:*.* **:**:**** ****** .  *:.***** .**  ... ***:

Q84BQ6v1_A2_asp                          TGDLARYNHHGELEFLGRNDFQIKLRGLRLEPGEIEARLIEHPAIRQAVVMVRD------
O85168_A8_asp                            TGDLGRWLADGTLEYMGRNDFQVKVRGFRIELGEIENVLLAVPGIREVVVIARNDSQGDS
Q84BQ6v2_A2_asp                          TGDLARYNHHGELEFLGRNDFQIKLRGLRLEPGEIEARLIEHPAIRQAVVMVRD------
                                         ****.*:  .* **::******:*:**:*:* ****  *:  *.**:.**:.*:      

Q84BQ6v1_A2_asp                          --ERLVAWYTV
O85168_A8_asp                            DSQRLVAYVCG
Q84BQ6v2_A2_asp                          --ERLVAWYTV
                                           :****:   
