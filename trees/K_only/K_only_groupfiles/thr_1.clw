CLUSTAL W multiple sequence alignment


Q45R85_A4_thr                            STTAVRETLHGTVGEAVGEVPWLLLDEPATGGATAGHSAAPVTDADRRSPLLPDHPAYTI
O87314_A2_thr                            TDTAERFT----------GVPHVILAEAAQNPA---RPQAPTVS--------PDHAAYVI
                                         : ** * *           ** ::* *.* . *   :. **...        ***.**.*

Q45R85_A4_thr                            YTSGSTGRPKGVVVSHANVSRLLTACRAAV-DFGPDDVWTLFHSSAFDFSVWEMWGPLAH
O87314_A2_thr                            YTSGSTGVPKGVEVTHRNVAALFAGTTSGLYDFGPDDVWTMFHSAAFDFSVWELWGPLLH
                                         ******* **** *:* **: *::.  :.: *********:***:********:**** *

Q45R85_A4_thr                            GGRLVVVPHDVARSPGDLLDLLGRERVTVLSQTPSAFLQLLRAESDLGVPPRTTAALRYV
O87314_A2_thr                            GGRLVVVEHDVARDPERFVDLLARERVTVLNQTPSAFYPLLEADARL----RRQLALRYV
                                         ******* *****.*  ::***.*******.******  **.*:: *    *   *****

Q45R85_A4_thr                            VFGGEALDTAQLAPW----RGRPVRLVNMYGITETTVHVTHLELDDAAVDRGGSPIGTPL
O87314_A2_thr                            IFGGEALDVRRLAPWYANHESHSPRLVNMYGITETCVHVSHRALDTADTGAAGSVIGGPL
                                         :*******. :****    ..:. *********** ***:*  ** * .. .** ** **

Q45R85_A4_thr                            NDLRAHVLDQGLLPVPVGVVGELYVAGPGLARGYRRRPGLSATRFVADPFDTGG-RMYRT
O87314_A2_thr                            PGLRIHLLDNNLQPVPAGVVGEMYIAGGQVARGYTGRPGLTATRFVANPFDGAGERLYRS
                                          .** *:**:.* ***.*****:*:**  :****  ****:******:*** .* *:**:

Q45R85_A4_thr                            GDLVRRTQDGGLHYVGRSDSQVKLRGYRIEPGEIEAAARRHPDVAQAATAVHGEGPQDRY
O87314_A2_thr                            GDLAMWTDAGELVYLGRSDAQVKVRGYRIELGEVEAALVTLPGVTNAAADVRHDDTGRAR
                                         ***.  *: * * *:****:***:****** **:***    *.*::**: *: :..    

Q45R85_A4_thr                            LVCYVVP
O87314_A2_thr                            LIGYVVG
                                         *: *** 
