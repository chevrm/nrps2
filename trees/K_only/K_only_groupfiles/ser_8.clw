CLUSTAL W multiple sequence alignment


Q6WZB2_A2_3__ser                         RRQTASLPKLP--------GVTVLVVDDHEAL---SRFPATVPKPVPRPQDLAYVIYTSG
Q6WZB2_A1_2__ser                         -RQELR-DRLPEDLRDGTGQVAVVPVGAESGAGTSTRRPVTPVEQEPRPERLAYIVYTSG
                                          **     :**         *:*: *. ...    :* *.*  :  ***: ***::****

Q6WZB2_A2_3__ser                         STGRPKGVMVEHHSVVNYLTTLQEKFRLTSDDRLLLKSPLSFDVSVREVFWALSTGATLV
Q6WZB2_A1_2__ser                         STGLPKGVMVEHRGIVSYLLGMLEHFPMGPRDRMLQVTSLSFDVSVYEIFLPLLTGGATV
                                         *** ********:.:*.**  : *:* : . **:*  :.******* *:* .* **.: *

Q6WZB2_A2_3__ser                         VAEAGRHADPDYLVEAIERERVTVVHFVPSMLHVLLETLDGPGRCPTLRQVMTSGETLPV
Q6WZB2_A1_2__ser                         LPRSGSHTDAAYLSGLIAEHGVTSFHMVPSLLRTFVDGLD-PRQCAGLRRIFVSGEALDT
                                         :..:* *:*. **   * .. ** .*:***:*:.::: ** * :*. **:::.***:* .

Q6WZB2_A2_3__ser                         QTARRCLELLGAELRNMYGPTETTVEMTDCEVRGRTDTERLP-----IGRPFPNTRVYVL
Q6WZB2_A1_2__ser                         TLVVDVHDRLPCDVVNLYGATEVSVDST-----WWTAPRDLPDAPVLVGRPMAGATAYVL
                                           .    : * .:: *:**.**.:*: *       * .. **     :***:..: .***

Q6WZB2_A2_3__ser                         DDELRLVPRGTVGELYVSGAPVARGYLGRPALTADRFLPDPYGPPGSRMYRTGDLGRFTG
Q6WZB2_A1_2__ser                         DDEMRRLAPNEVGEVYLGGASVTRGYHGRAALTAQRFLPDPYGPPGSRLYRTGDLGRVED
                                         ***:* :. . ***:*:.**.*:*** **.****:*************:********. .

Q6WZB2_A2_3__ser                         EGLLDFQGRGDFQVQLRGHRIEPGEIETVLCEQPGVTAAVAVVRRPDSPEA-AHLVAYAV
Q6WZB2_A1_2__ser                         NGELRLLGRIDHQVKLHGRRIEPGEIEAAMTAHPHVSLAAAV---PAGAGAGATLTGFFT
                                         :* * : ** *.**:*:*:********:.:  :* *: *.**   * .. * * *..: .

Q6WZB2_A2_3__ser                         R
Q6WZB2_A1_2__ser                         G
                                          
