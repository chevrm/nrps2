CLUSTAL W multiple sequence alignment


Q50E73_A2_7__asp                         GDFPGGEVFEFSRVVRESCLVELVAADGVEVRNVTDGERASRLLPGHPLYVVYTSGSTGR
Q50E73_A4_9__asp                         GDFPGGEVFEFSRVVRESCLVELVAADGVEVRNVTDGERASRLLPGHPLYVVYTSGSTGR
                                         ************************************************************

Q50E73_A2_7__asp                         PKGVVVTHASVGGYLARGRDVYAGAVGGVGFVHSSLAFDLTVTVLFTPLVSGGCVVLGEL
Q50E73_A4_9__asp                         PKGVVVTHASVGGYLARGRDVYAGAVGGVGFVHSSLAFDLTVTVLFTPLVSGGCVVLGEL
                                         ************************************************************

Q50E73_A2_7__asp                         DESAQGVGASFVKVTPSHLGLLGELEGVVAGNGMLLVGGEALSGGALREWRERNPGVVVV
Q50E73_A4_9__asp                         DESAQGVGASFVKVTPSHLGLLGELEGVVAGNGMLLVGGEALSGGALREWRERNPGVVVV
                                         ************************************************************

Q50E73_A2_7__asp                         NAYGPTELTVNCAEFLIAPGEEVPDGPVPIGRPFAGQRMFVLDAALRVVPVGVVGELYVA
Q50E73_A4_9__asp                         NAYGPTELTVNCAEFLIAPGEEVPDGPVPIGRPFAGQRMFVLDAALRVVPVGVVGELYVA
                                         ************************************************************

Q50E73_A2_7__asp                         GVGLARGYLGRAGLTAERFVACPFGAPGERMYRTGDLVRWRVDGALEFVGRADDQVKVRG
Q50E73_A4_9__asp                         GVGLARGYLGRVGLTAERFVACPFGVPGERMYRTGDLVRWRVDGALEFVGRADDQVKVRG
                                         ***********.*************.**********************************

Q50E73_A2_7__asp                         FRVELGEVEGAVAAHPDVVRAVVVVREDRPGDHRLVAYVTG
Q50E73_A4_9__asp                         FRVELGEVEGAVAAHPDVVRAVVVVREDRPGDHRLVAYVTA
                                         ****************************************.
