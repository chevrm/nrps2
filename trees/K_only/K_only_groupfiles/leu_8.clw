CLUSTAL W multiple sequence alignment


Q7WRQ4_A1_8__leu                         TQQSLVDSLEAN----SAEVVCLDRDWHIIANYSQHNPVKLVKAENLAYVIYTSGSTGKP
Q8G982_A1_8__leu                         TQQSLVTNLREDLDTLKIESFCLDSDWLILENYSRENPSSSVQSENLAYLIYTSGSTGKP
                                         ****** .*. :    . * .*** ** *: ***:.** . *::*****:**********

Q7WRQ4_A1_8__leu                         KGVMNIHKGICNNLLRTIDTYPLIAGDCILHIGVLSFDVSVWEIFWSLTSGTTLVVAKPE
Q8G982_A1_8__leu                         KGVMNLHQGICNNILRTKDSYPTTNRDRLLQISSLAFDASVLDIFWSLSSGMALIIPKPE
                                         *****:*:*****:*** *:**    * :*:*. *:**.** :*****:** :*::.***

Q7WRQ4_A1_8__leu                         GHKDIAYLINLIAQQQVTQVFFVPSMLRIFLQQPNLESCRCLKRVFSGAETLSYELTQRF
Q8G982_A1_8__leu                         GTKDLAYLIQLMIEKKVSQVFFVPSLLRLLLQQPNLENCRYLKRVFCGGEALSSELMQQF
                                         * **:****:*: :::*:*******:**::*******.** *****.*.*:** ** *:*

Q7WRQ4_A1_8__leu                         FERLDCELHNLYGPTEAAVDATCWQCQPQSNYQVIPIGRPIANTQTYILDQYLQPVPIGI
Q8G982_A1_8__leu                         FQHFNCELHNLYGPTETSVDATCWQCPPRTDDPAIAIGRPIANTQIYILDRHLQPVPVGI
                                         *::::***********::******** *:::  .*.********* ****::*****:**

Q7WRQ4_A1_8__leu                         AGELHIGGVQLARGYLNQPELTNERFISNPFGEGKLYKTGDKARYLSDGNIEYLGRIDHQ
Q8G982_A1_8__leu                         VGELHIGGIPLARGYLNQLELTAEKFIPNPFGQGKLYKTGDLVRYLADGNIEYLGRIDNQ
                                         .*******: ******** *** *:**.****:******** .***:***********:*

Q7WRQ4_A1_8__leu                         VKLRGLRIELGEIEFLLDTHPQVEQTVVVLQADTSENQRLVAYVVR
Q8G982_A1_8__leu                         VKLRGLRIELGEIQTILDSHPQINQSVVIIQTDSEDNQRLVAYVDS
                                         *************: :**:***::*:**::*:*:.:********  
