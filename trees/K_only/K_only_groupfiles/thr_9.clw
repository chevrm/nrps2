CLUSTAL W multiple sequence alignment


Q9FB24_A1_thr                            VSDTA-LPPLHRVDVTA---TLPDGAPEPTAR--AVLPGN-LAYAVYTSGSTGGPKGVLV
Q9RBY6_A1_thr                            -GDAANLPKISSLSVLALDELLSAPALQPAAQDTRIDPNNSTAYIIYTSGSTGEPKGVQV
                                          .*:* ** :  :.* *    *.  * :*:*:   : *.*  ** :******* **** *

Q9FB24_A1_thr                            THANVTGLLAACREALPALDAPRTWSATHSPAFDFSVWEVWGPLTAGGRL--------VL
Q9RBY6_A1_thr                            SHGNVSRLLESTQRAY-GFNAQDVWSMFHSIGFDFSVWEIWGALAHGARWPLSRMTSRAL
                                         :*.**: ** : :.*  .::*  .**  ** .*******:**.*: *.*         .*

Q9FB24_A1_thr                            VPPDVARAPDELWDTLRDEQVEVLSQTP-SAFHHLLPTAVRRAAQATALELVVLGGEACE
Q9RBY6_A1_thr                            LPPCVNGSP--------------ISASPCSARRHRLSVVLMRPIVATPRRCAALRGARWR
                                         :** *  :*              :* :* ** :* *...: *.  **. . ..* *   .

Q9FB24_A1_thr                            --PAR-LTPWWDALGDRRPAVVNMYGITENTIHVTVRRMTAADRSGS---PVGRPLPGQR
Q9RBY6_A1_thr                            TLPASVLRPWVERHGDQKPALINMYGITEATVHTTFKRVLAQDLETAAMVSLGKPL-DVR
                                           **  * ** :  **::**::******* *:*.*.:*: * * . :   .:*:** . *

Q9FB24_A1_thr                            ADLLDPHGRPVAPGGRGELFVGGVGLARGYLGRPGLTARSFLPDDTPGWPGA-RRYRSGD
Q9RBY6_A1_thr                            LHLLDANQAPVAAGTTGELYIEGAGVAQGYLNRERLNVERFVE-----LPGAVRAYRTGD
                                          .***.:  ***.*  ***:: *.*:*:***.*  *... *:       *** * **:**

Q9FB24_A1_thr                            LARLLPDGGLDYAGRSDAQVKVRGYRVEPAETEAAALTHPAV--RHCVVVPRGDGDRRHL
Q9RBY6_A1_thr                            LMTLESNGEYRYAGRCDEQLKISGFRIEPGEIEASLQTSPSVAAAHVGVHDYGDGDLR-L
                                         *  * .:*   ****.* *:*: *:*:**.* **:  * *:*   *  *   **** * *

Q9FB24_A1_thr                            AAYVVA
Q9RBY6_A1_thr                            VAYVVP
                                         .****.
