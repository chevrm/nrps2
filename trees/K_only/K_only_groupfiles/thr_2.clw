CLUSTAL W multiple sequence alignment


Q51338_A2_thr                            TQGHLLERLPRQAGVEVLAIDGLVLDGYAESDPLPTLSADNLAYVIYTSGSTGKPKGTLL
Q51338_A1_thr                            TQGHLLERLPRQAGVEVLAIDGLVLDGYAESDPLPTLSADNLAYVIYTSGSTGKPKGTLL
                                         ************************************************************

Q51338_A2_thr                            THRNALRLFSATEAWFGFDERDVWTLFHSYAFDFSVWEIFGALLYGGRLVIVPQWVSRSP
Q51338_A1_thr                            THRNALRLFSATEAWFGFDERDVWTLFHSYAFDFSVWEIFGALLYGGCLVIVPQWVSRSP
                                         *********************************************** ************

Q51338_A2_thr                            EDFYRLLCREGVTVLNQTPSAFKQLMAVACSADMATQQPALRYVIFGGEALDLQSLRPWF
Q51338_A1_thr                            EDFYRLLCREGVTVLNQTPSAFKQLMAVACSADMATQQPALRYVIFGGEALDLQSLRPWF
                                         ************************************************************

Q51338_A2_thr                            QRFGDRQPQLVNMYGITETTVHVTYRPVSEADLKGGLVSPIGGTIPDLSWYILDRDLNPV
Q51338_A1_thr                            QRFGDRQPQLVNMYGITETTVHVTYRPVSEADLEGGLVSPIGGTIPDLSWYILDRDLNPV
                                         *********************************:**************************

Q51338_A2_thr                            PRGAVGELYIGRAGLARGYLRRPGLSATRFVPNPFPGGAGERLYRTGDLARFQADGNIEY
Q51338_A1_thr                            PRGAVGELYIGRAGLARGYLRRPGLSATRFVPNPFPGGAGERLYRTGDLARFQADGNIEY
                                         ************************************************************

Q51338_A2_thr                            IGRIDHQVKVRGFRIELGEIEAALAGLAGVRDAVVLAHDGVGGTQLVGYVVA
Q51338_A1_thr                            IGRIDHQVKVRGFRIELGEIEAALAGLAGVRDAVVLAHDGVGGTQLVGYVVA
                                         ****************************************************
