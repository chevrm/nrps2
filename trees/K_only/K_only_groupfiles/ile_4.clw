CLUSTAL W multiple sequence alignment


Q84BQ4_A4_10__ile                        TLSREDIDYAAPRIDLDRLILSGQPTHNPNLLQSSEALAYIMYTSGSTGTPKGVMVPHRA
Q84BQ4_A3_9__ile                         TLSSEAIDYAAPRIDLDRLKLSGQSTHNPNLAQSSDALAYIMYTSGSTGTPKGVMVPHRG
                                         *** * ************* ****.****** ***:***********************.

Q84BQ4_A4_10__ile                        IGRLVLNNGYADFNAQDRVVFASNPAFDASTMDIWGPLLNGGRVVVIDHQTLLDPNAFGH
Q84BQ4_A3_9__ile                         IARLVLNNGYADFNRQDRVAFASNPAFDASTMDIWGPLLNGGRVVVIDHQTLLDPNAFGR
                                         *.************ ****.***************************************:

Q84BQ4_A4_10__ile                        ELSASRATVLFVTTALFNQYVQLIPQALKGLRILLCGGERGDPAAFRRLLAEAPKLRIVH
Q84BQ4_A3_9__ile                         ELSASGATILFVTTALFNQYVQLIPQALKGLRMVLCGGERGDPTSFRRLRAEAPQLRIVH
                                         ***** **:***********************::*********::**** ****:*****

Q84BQ4_A4_10__ile                        CYGPTETTTYATTFEVREVAENAESVPIGGPISNTQVYVLDAHQQPVPMGVTGELYIGGQ
Q84BQ4_A3_9__ile                         CYGPTETTTYATTFEVHEVAENAESVPIGAPISNTQVYVLDAHQQPVPMGVTGELYIGGQ
                                         ****************:************.******************************

Q84BQ4_A4_10__ile                        GVALGYLNRADLTAEKFLRDPFSDQPGALLYRTGDLARWLAPGQLDCIGRNDDQVKIRGF
Q84BQ4_A3_9__ile                         GVALGYLNRADLTAEKFLPDPFSDRPGALLYRTGDLVRWLAPGQLDCIGRNDDQVKIRGF
                                         ****************** *****:***********.***********************

Q84BQ4_A4_10__ile                        RIELGEIENRLLNCQGIKEAVVLARRDGQDITRLVAYYTA
Q84BQ4_A3_9__ile                         RIELGEIENRLLSYPGINEAVVLARRDGQEPLRLVAYYTA
                                         ************.  **:***********:  ********
