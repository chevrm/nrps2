CLUSTAL W multiple sequence alignment


Q2VQ15_A3_6__val                         QKTEMIPASYQGEVLLLAEECWMHEDSSNLELINKTQDLAYVMYTSGSTGKPKGNLTTHQ
Q2VQ15_A2_5__val                         QEAAMIPEGYQGKVLLLAEECWMQEEASNLELINDAQDLAYVMYTSGSTGKPKGNLTTHQ
                                         *:: *** .***:**********:*::*******.:************************

Q2VQ15_A3_6__val                         NILTTIINNGYIEIAPTDRLLQLSNYAFDGSTFDIYSALLNGATLVLVPKEVMLNPMELA
Q2VQ15_A2_5__val                         NILRTIINNGFIEIVPADRLLQLSNYAFDGSTFDIYSALLNGATLVLVPKEVMLNPMELA
                                         *** ******:***.*:*******************************************

Q2VQ15_A3_6__val                         KIVREQDITVSFMTTSLFHTLVELDVTSMKSMRKVVFGGEKASYKHVEKALDYLGEGRLV
Q2VQ15_A2_5__val                         RIVREQDITVSFMTTSLFHTLVELDVTSMKSIRKVVFGGEKASYKHVEKALDYLGEGRLV
                                         :******************************:****************************

Q2VQ15_A3_6__val                         NGYGPTETTVFATTYTVDSSIKETGIVPIGRPLNNTSVYVLNENNQLQPIGVPGELCVGG
Q2VQ15_A2_5__val                         NGYGPTETTVFATTYTVDSSIKETGIVPIGRPLNNTSVYILNENNQPQPIGVPGELCVGG
                                         ***************************************:****** *************

Q2VQ15_A3_6__val                         AGIARGYLNRPELTAERFVENPFVSGDRMYRTGDLARWLPDGSMEYLGRMDEQVKVRGYR
Q2VQ15_A2_5__val                         AGIARGYLNRPELTAERFVDNPFLVGDRMYRTGDMARFLPDGNIEYIGRMDEQVKIRGHR
                                         *******************:***: *********:**:****.:**:********:**:*

Q2VQ15_A3_6__val                         IELGEIETRLLEHPSISAAVLLAKQDEQGHSYLCAYIVA
Q2VQ15_A2_5__val                         IELGEIEKSLLEYPAISEAVLVAKRDEQGHSYLCAYVVS
                                         *******. ***:*:** ***:**:***********:*:
