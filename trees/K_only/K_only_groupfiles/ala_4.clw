CLUSTAL W multiple sequence alignment


Q9RLP6v1_A3_ala                          TTADLRSRLDQYDLAVIDMADPAIDRRPSDALSGPRPDDLAYMTYTSGTTGVPKAVAVTH
Q9RLP6v2_A3_ala                          TTADLRSRLDQYDLAVIDMADPAIDRRPSDALSGPRPDDLAYMTYTSGTTGVPKAVAVTH
                                         ************************************************************

Q9RLP6v1_A3_ala                          HNVTQLVSALHADLPSGPGQVWSQWHSLVFDVSVWEIWGALLHGGRLVVVPESVGSSPDD
Q9RLP6v2_A3_ala                          HNVTQLVSALHADLPSGPGQVWSQWHSLVFDVSVWEIWGALLHGGRLVVVPESVGSSPDD
                                         ************************************************************

Q9RLP6v1_A3_ala                          LHNLLITEKVSVLCQTPSAAGMLSPEGLESTTLIVAGEACPTELVDRWAPGRVMINAYGP
Q9RLP6v2_A3_ala                          LHNLLITEKVSVLCQTPSAAGMLSPEGLESTTLIVAGEACPTELVDRWAPGRVMINAYGP
                                         ************************************************************

Q9RLP6v1_A3_ala                          TEATIYAAMSEPLTAGTGVAPIGAPVPGAALFVLDKWLRPAPEGVVGELYVAGHGVATGY
Q9RLP6v2_A3_ala                          TEATIYAAMSEPLTAGTGVAPIGAPVPGAALFVLDKWLRPAPEGVVGELYVAGHGVATGY
                                         ************************************************************

Q9RLP6v1_A3_ala                          IGRPDLTASRFVACPFGETGERMYRTGDLVRWGSDGQLEYLGRADEQVKIRGYRIELGEI
Q9RLP6v2_A3_ala                          IGRPDLTASRFVACPFGETGERMYRTGDLVRWGSDGQLEYLGRADEQVKIRGYRIELGEI
                                         ************************************************************

Q9RLP6v1_A3_ala                          QAALAKLDGVDQAVVIAREDRPGDKRLVGYITG
Q9RLP6v2_A3_ala                          QAALAKLDGVDQAVVIAREDRPGDKRLVGYITG
                                         *********************************
