CLUSTAL W multiple sequence alignment


Q9RLP6/5869932_A1_phe                    TTAELRSRFDGHDLAVIDIDDPAIASLPATAVSDPRPDDIAYVIYTSGTTGTPKGVAVTH
Q9RLP6_A1_phe                            TTAELRSRFDGHDLAVIDIDDPAIASLPATAVSDPRPDDIAYVIYTSGTTGTPKGVAVTH
                                         ************************************************************

Q9RLP6/5869932_A1_phe                    KNLTHLIAVLEERLPKPGVWPLCHSLAFDASVWEISNALLRGGRLVVVPEAVAGSPEDFH
Q9RLP6_A1_phe                            KNLTHLIAVLEERLPKPGVWPLCHSLAFDASVWEISNALLRGGRLVVVPEAVAGSPEDFH
                                         ************************************************************

Q9RLP6/5869932_A1_phe                    DLLVAEQVTFLTQTPSAVAMLSPDGLESMTLAVVGEACPPALVDRWATNRTMINAYGPTE
Q9RLP6_A1_phe                            DLLVAEQVTFLTQTPSAVAMLSPDGLESMTLAVVGEACPPALVDRWATNRTMINAYGPTE
                                         ************************************************************

Q9RLP6/5869932_A1_phe                    TTICVTSSSPLEPGSVVVPIGSALPRTALFVLDPWLRPVPTGVAGELYVAGDGVTCGYIG
Q9RLP6_A1_phe                            TTICVTSSSPLEPGSVVVPIGSALPRTALFVLDPWLRPVPTGVAGELYVAGDGVTCGYIG
                                         ************************************************************

Q9RLP6/5869932_A1_phe                    RSGLTASRFVPCPFGEPGARMYRTGDLVRWGRDGQLEYLGRADEQVKIRGYRIELGEVQS
Q9RLP6_A1_phe                            RSGLTASRFVPCPFGEPGARMYRTGDLVRWGRDGQLEYLGRADEQVKIRGYRIELGEVQS
                                         ************************************************************

Q9RLP6/5869932_A1_phe                    ALAALDGVESAAAIMREDRPGDRRLVGYITG
Q9RLP6_A1_phe                            ALAALDGVESAAAIMREDRPGDRRLVGYITG
                                         *******************************
