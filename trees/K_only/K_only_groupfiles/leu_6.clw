CLUSTAL W multiple sequence alignment


Q939Z1_A1_leu                            SRGAVPAGVESLEPAAAAEEGASDAPAATVRPGDPAYVMYTSGSTGTPKGVTISQGCVAE
O52819_A1_leu                            TRDGVPEGIESI---VITDEDASDTSVATVRPGDLAYVMYTSGSTGTPKGVAITHGTIAE
                                         :*..** *:**:   . ::*.***:..******* ****************:*::* :**

Q939Z1_A1_leu                            LTMDAGWAMEPGEAVLMHSPHAFDASLFELWMPLASGVRVVLAEPGSVDARRLREAAAAG
O52819_A1_leu                            LAEDPGWVMEPGEAVLMHSPHTFDASLFEVWTPLSLGARVVIAEPGSVDVRRLREAAAAG
                                         *: *.**.*************:*******:* **: *.***:*******.**********

Q939Z1_A1_leu                            VTRVYLTAGSLRAVAEEAPESFAEFREVLTGGDVVPAHAVERVRTAAPRARFRNMYGPTE
O52819_A1_leu                            VTRVYLTAGSFRAVAEESPESFAAFREVLTGGDVVPAHAVERVREACPGARVRNMYGPTE
                                         **********:******:***** ******************** *.* **.********

Q939Z1_A1_leu                            ATMCATWHLLQPGDVVGPVVPIGRPLTGRRVQVLDASLRPVGPGVVGDLYLSGALAEGYF
O52819_A1_leu                            ATMCATWHLLQPGDVMGPVMPIGRPLAGRRIQVLDESLRPVEPGVVGDLYLSGGLAEGYF
                                         ***************:***:******:***:**** ***** ***********.******

Q939Z1_A1_leu                            NRAALTAERFVADPSAPGQRMYWTGDLAQWTADGELVFAGRADDQVKIRGFRIEPGEIEA
O52819_A1_leu                            NRAGLTAERFVADPSAPGQRMYWTGDLAQWTADGELLFAGRADHQVKVRGFRIEPGEIEA
                                         ***.********************************:******.***:************

Q939Z1_A1_leu                            ALIAQPDVHDAVVAAVDGRLIGYVVT
O52819_A1_leu                            ALIALPDVQDAVVAAIDGRLVGYVVA
                                         **** ***:******:****:****:
