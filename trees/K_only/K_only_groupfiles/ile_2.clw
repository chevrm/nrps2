CLUSTAL W multiple sequence alignment


P94460_A1_ile                            VQKGLEQNAAFSGTCIISDAQGLMEENDIPINISSSPDDLAYIMYTSGSTGRPKGVMITN
Q45563_A1_10__ile                        VQKGLEPNTAFAGTFISADAEAMIEEHTKPLEIVTGPDDLAYIMYTSGSTGRPKGVMITN
                                         ****** *:**:** * :**:.::**:  *::* :.************************

P94460_A1_ile                            RNVVSLVRNSNYTSASGDDRFIMTGSISFDAVTFEMFGALLNGASLHIIDKSTMLTPDRF
Q45563_A1_10__ile                        RNVVSLVCNSNYTSASVNDRFILTGSISFDAVTFEMFGALLKGATLHIIDKSTMLTPDRF
                                         ******* ******** :****:******************:**:***************

P94460_A1_ile                            GAYLLENDITVLFLTTALFNQLAQVRADMFRGLHTLYVGGEALSPALMNAVRHACPDLAL
Q45563_A1_10__ile                        GAYLIENNITVLFLTTALFNQLAQAQADMFHRLHTLYVGGEALSPELINAVRRACPNLSL
                                         ****:**:****************.:****: ************* *:****:***:*:*

P94460_A1_ile                            HNIYGPTENTTFSTFFEMKRDYAGPIPIGKPISNSTAYILDTKGRLLPIGVPGELCVGGD
Q45563_A1_10__ile                        YNIYGPTENTTFSTFFEIKRDYATPIPIGKPISNCTAFILDAKGCLLPIGVPGELCVGGD
                                         :****************:***** **********.**:***:** ***************

P94460_A1_ile                            GVAKGYLNRVDLTNAVFSPHPFLPGERIYRTGDLARWLPDGNLEYISRIDRQMKIRGKRI
Q45563_A1_10__ile                        GVAKGYLNRDDVTAAVFSPDPFIPGERIYRTGDLARWLPDGNLEYISRIDRQIKIRGKRI
                                         ********* *:* *****.**:*****************************:*******

P94460_A1_ile                            EPAEIEARLLEMEGVQEAAVTLREKDGEAHVYTHYVG
Q45563_A1_10__ile                        EPAEIEARLLEIEGVREAAVTLLETDGEVQLYTHYVS
                                         ***********:***:****** *.***.::*****.
