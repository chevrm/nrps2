CLUSTAL W multiple sequence alignment


P94873_A2_cys                            TDRAYAERLDRVAD-GLPCEVFGVQDLPLEP-YPAANPRSAATSTDLAYAIYTSGTTGRP
P19787_A2_cys                            ADSCYLPRIKGMAASGTLL-YPSVLPANPDSKWSVSNPSPLSRSTDLAYIIYTSGTTGRP
Q9C1G0v1_A2_cys                          ADGPYLERLESIMEGSLPL-IPSDEALRLPP--SPVHPNSNCHSSDLAYVMYTSGTTGLP
P25464_A2_cys                            TDSPHIDRLRSITNNRLPV-IQSDFALQLPP--SPVHPVSNCKPSDLAYIMYTSGTTGNP
P27743_A2_cys                            TNEIHSDRLRSLAETGTP--VLEIELLHLDD-QPAVNPVTETTSTDLAYAIYTSGTTGKP
Q9C1G0v2_A2_cys                          ADGPYLERLESIMEGSLPL-IPSDEALRLPP--SPVHPNSNCHSSDLAYVMYTSGTTGLP
P27742_A2_cys                            SDACYLSRIQELAGESVRL-YRSDISTQTDGNWSVSNPAPSSTSTDLAYIIYTSGTTGKP
P26046_A2_cys                            ADSCYLPRIKGMAASGTLL-YPSVLPANPDSKWSVSNPSPLSRSTDLAYIIYTSGTTGRP
                                         ::  :  *:  :                     .  :* .   .:**** :******* *

P94873_A2_cys                            KAVLIEHRGVVNLHTSLERLFDLSRDRGDEAVLSFSNYVFDHFVEQMTDALLSGQTLVML
P19787_A2_cys                            KGVTVEHHGVVNLQVSLSKVFGL-RDTDDEVILSFSNYVFDHFVEQMTDAILNGQTLLVL
Q9C1G0v1_A2_cys                          KGVMVEHHGVVNLAFSLAQIFGL-RDTDDEVILSFSNYIFDHFVEQMTDALLNGQTLVVL
P25464_A2_cys                            KGVMVEHHGVVNLCVSLCRLFGL-RNTDDEVILSFSNYVFDHFVEQMTDALLNGQTLVVL
P27743_A2_cys                            KAVLVEHRGVVNLQVSLAKLFGLDKAHRDEALLSFSNYIFDHFVEQMTDALLNGQKLVVL
Q9C1G0v2_A2_cys                          KGVMVEHHGVVNLAFSLAQIFGL-RDTDDEVILSFSNYIFDHFVEQMTDALLNGQTLVVL
P27742_A2_cys                            KGVMVEHHGVVNLQISLSKTFGL-RDTDDEVILSFSNYVFDHFVEQMTDAILNGQTLVML
P26046_A2_cys                            KGVTVEHHGVVNLQVSLSKVFGL-RDTDDEVILSFSNYVFDHFVEQMTDAILNGQTLLVL
                                         *.* :**:*****  ** : *.* :   **.:******:***********:*.**.*::*

P94873_A2_cys                            DDSMRSDQQRLYAYMNANAVTYLSGTPSVLSLYEYGSIPS-LKRIDAIGEDFTTPVFDKI
P19787_A2_cys                            NDGMRGDKERLYRYIEKNRVTYLSGTPSVVSMYEFSRFKDHLRRVDCVGEAFSEPVFDKI
Q9C1G0v1_A2_cys                          NDEMRGDKERLYKYIEDNKVTYLSGTPSVISMYEFDRFHSHMRRIDCVGEAFSEPVFDKI
P25464_A2_cys                            NDEMRGDKERLYRYIETNRVTYLSGTPSVISMYEFDRFRDHLRRVDCVGEAFSEPVFDKI
P27743_A2_cys                            DDSMRTDPGRLCRYMNDEQVTYLSGTPSVLSLYDYSSATS-LTRIDAIGEDFTEPVFAKI
Q9C1G0v2_A2_cys                          NDEMRGDKERLYKYIEDNKVTYLSGTPSVISMYEFDRFHSHMRRIDCVGEAFSEPVFDKI
P27742_A2_cys                            NDAMRSDKERLYQYIETNRVTYLSGTPSVISMYEFSRFKDHLRRVDCVGEAFSQPVFDQI
P26046_A2_cys                            NDGMRGDKERLYRYIEKNRVTYLSGTPSVVSMYEFSRFKDHLRRVDCVGEAFSEPVFDKI
                                         :* ** *  **  *:: : **********:*:*::.   . : *:*.:** *: *** :*

P94873_A2_cys                            RSSFGGLIINGYGPTEISITSHKRLYLKHEPRLDKSIGHPVANTACYVLNPAMQRVPVGG
P19787_A2_cys                            RETFHGLVINGYGPTEVSITTHKRLYPFPERRMDKSIGQQVHNSTSYVLNEDMKRTPIGS
Q9C1G0v1_A2_cys                          RETFPGLVINGYGPTEVSITTSKRLYPFPERRTNKSIGSQVSNSKSYVLSDDMKRMPIGA
P25464_A2_cys                            RETFPGLIINGYGPTEVSITTHKRPYPFPERRTDKSIGCQLDNSTSYVLNDDMKRVPIGA
P27743_A2_cys                            RGTFPGLIINGYGPTEISITSHKRPYPPDVHRVNKSIGFPVANTKCHVLNKAMKPVPVGG
Q9C1G0v2_A2_cys                          RETFPGLVINGYGPTEVSITTSKRLYPFPERRTNKSIGSQVSNSKSYVLSDDMKRMPIGA
P27742_A2_cys                            RDTFQGLIINGYGPTEISITTHKRLYPFPERRTDKSIGQQIGNSTSYVLNADMKRVPIGA
P26046_A2_cys                            RETFHGLVINGYGPTEVSITTHKRLYPFPERRMDKSIGQQVHNSTSYVLNEDMKRTPIGA
                                         * :* **:********:***: ** *     * :****  : *: .:**.  *:  *:*.

P94873_A2_cys                            MGELYIGGIGVARGYLNRPELTAERFVSNPFQSAGEKALGNNARLYKTGDLVRWLPN---
P19787_A2_cys                            VGELYLGGEGVVRGYHNRADVTAERFIPNPFQSEEDKREGRNSRLYKTGDLVRWIPGSS-
Q9C1G0v1_A2_cys                          VGELYLGGDGVARGYHNRPELTAERFPQNPFQTEQEKREGRNGRLYKTGDLVRWIQGSN-
P25464_A2_cys                            VGELYLGGDGVARGYHNRPDLTADRFPANPFQTEQERLEGRNARLYKTGDLVRWIHNANG
P27743_A2_cys                            IGELYIGGIGVTRGYLNREDLTADRFVENPFQTAEERRLGENGRLYKTGDLVRWLPN---
Q9C1G0v2_A2_cys                          VGELYLGGDGVARGYHNRPELTAERFPQNPFQTEQEKREGRNGRLYKTGDLVRWIQGSN-
P27742_A2_cys                            VGELYLGGEGVARGYHNRPEVTAERFLRNPFQTDSERQNGRNSRLYRTGDLVRWIPGSN-
P26046_A2_cys                            VGELYLGGEGVVRGYHNRADVTAERFIPNPFQSEEDKREGRNSRLYKTGDLVRWIPGSS-
                                         :****:** **.*** ** ::**:**  ****:  ::  *.*.***:*******: .   

P94873_A2_cys                            -GELEYLGRNDMQVKIRGQRVELGEVEAILASYPGVTRALVLAREYAASAAGGEASQKYL
P19787_A2_cys                            -GEVEYLGRNDFQVKIRGLRIELGEIEAILSSYHGIKQSVVIAKDCR------EGAQKFL
Q9C1G0v1_A2_cys                          -GEVEYLGRNDFQVKIRGQRIELGEIEAVLSSYEGIKQSVVLAKDRK------IDNQKFL
P25464_A2_cys                            DGEIEYLGRNDFQVKIRGQRIELGEIEAVLSSYPGIKQSVVLAKDRK------NDGQKYL
P27743_A2_cys                            -GEVEYLGRTDLQVKIRGQRVELGEVEAALSSYPGVVRSLVVAREHA-------VGQKYL
Q9C1G0v2_A2_cys                          -GEVEYLGRNDFQVKIRGQRIELGEIEAVLSSYEGIKQSVVLAKDRK------IDNQKFL
P27742_A2_cys                            -GEIEYLGRNDFQVKIRGLRIELGEIEAVMSSHPDIKQSVVIAKSGK------EGDQKFL
P26046_A2_cys                            -GEVEYLGRNDFQVKIRGLRIELGEIEAILSSYHGIKQSVVIAKDCR------EGAQKFL
                                          **:*****.*:****** *:****:** ::*: .: :::*:*:.           **:*

P94873_A2_cys                            VAFYLSAEE-
P19787_A2_cys                            VGYYVADAA-
Q9C1G0v1_A2_cys                          VGYYLGSES-
P25464_A2_cys                            VGYFVSSAGS
P27743_A2_cys                            VGFYVGEQE-
Q9C1G0v2_A2_cys                          VGYYLGSES-
P27742_A2_cys                            VGYFVASSP-
P26046_A2_cys                            VGYYVADAA-
                                         *.:::.    
