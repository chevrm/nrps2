CLUSTAL W multiple sequence alignment


Q8NJX1_A15_abu|iva                       TSAANKNLVSSLSKSVIIVDSELDLQLSKVEEYSQKAQVTATSSDNAVYVLFTSGSTGTP
Q8NJX1_A1_abu|iva                        ASISNVDMCSKLIGNVVEVSHTLDEALAQTEMSSHGPVSNVSPR-NAAYVLSTSGSTGTP
Q8NJX1_A9_abu|iva                        SSSANAVLCSTLVRKVVEVNAELDNKLLATESSAHGPVVDVSSR-NAAYVLFTSGSTGIP
Q8NJX1_A12_abu|iva                       SSPTNAALCNKLVHNVIEVSPSLIDELSKFCDGFNSPAINVPSS-NAAYVLFTSGSTGTP
                                         :* :*  : ..*  .*: *.  *   *       : .   ...  **.*** ****** *

Q8NJX1_A15_abu|iva                       KGLVMQHGSVCTSQTAIVKRLGLTPSVRMLQFAAFVFDLSIGEIIAPLITGACLCIPSEH
Q8NJX1_A1_abu|iva                        KGLVMQHQAVCTSQTAITKRLRMTSDVKMLQFASFVFDLSIGEIFGPWVVGGCICVPSEE
Q8NJX1_A9_abu|iva                        KGLIMEHGSVCTSQVAIAKRLGLNSKVRILQFAAFVFDLSIGEIVGPLISGACICVPSEH
Q8NJX1_A12_abu|iva                       KGLVMQHGAVCTSQTAIAKRLSLTPDVRILQFAAYVFDLSIGEIVAPLIHGACVCVPSEE
                                         ***:*:* :*****.**.*** :...*::****::*********..* : *.*:*:***.

Q8NJX1_A15_abu|iva                       TRMNSLTQFIRDMEINWAFLTPSFIRTINPVEVPGLDLVLLAGEAVPRDVLTTWFGQVRL
Q8NJX1_A1_abu|iva                        TRMNDLVNFINTMKINWAYLTPSFARTLNPVDVPGLELLLFAGEAVRRDVFEAWFGRLRL
Q8NJX1_A9_abu|iva                        IRKNSIANFINRQGITWTYLTPSFVRTIKASEVPNVKLLLLAGEAVPRDIFATWFGKLRL
Q8NJX1_A12_abu|iva                       TRMNGLKEFIRDARINWAYLTPSFVRTLRPEDVPSLQLLLLAGEAVGRDILDTWFGKVRL
                                          * *.: :**.   *.*::***** **:.. :**.:.*:*:***** **:: :***::**

Q8NJX1_A15_abu|iva                       INGWGPAETCVFSTLHEWKSV-NESPLTIGKPVGGFCWVVNPEDPHRLAPIGTLGEVVIQ
Q8NJX1_A1_abu|iva                        INGWGPAETCVFSTLHEWKSL-DESPLTIGRPVGGHCWIVDPQDPQRLAPIGTFGEVVIQ
Q8NJX1_A9_abu|iva                        INGWGPAETCCFSTLHEMAICGRESPYC-RRPVGGFCWIVDPENPHRLAPTGALGEVIIQ
Q8NJX1_A12_abu|iva                       INGWGPAETCVFSTLHEWSSI-DESPLTIGRPVGGYCWIVEAEDSNKLTPIGCLGEVVLQ
                                         ********** ******      ***    :****.**:*:.::.::*:* * :***::*

Q8NJX1_A15_abu|iva                       GPTLLSEYLSDPERTRSSTLYNLPKWAPR--PDSKHWNKFYKSGDLCYYNQDGTIEFATR
Q8NJX1_A1_abu|iva                        GPTILREYLADFTKTESSMVRSLPEWVPN--RTSTHWDRFYKSGDLCRYNADGTMEFGSR
Q8NJX1_A9_abu|iva                        GPTILREYLSDVDRTEAAVIKSLPDWAPF--REQSSGSRFNKSGDLGVYNPDGTIEFSSR
Q8NJX1_A12_abu|iva                       GPTLLREYLADPQRSKETIITELPPWAPKQVSDAHLWSRFYKSGDLCFYNPNGTLEFYSR
                                         ***:* ***:*  ::. : : .** *.*         .:* *****  ** :**:** :*

Q8NJX1_A15_abu|iva                       KDTQIKIRGLRVELGEVQHHIQQALPDARQVAVDVYKGENGTNLAAYFCFS
Q8NJX1_A1_abu|iva                        KDGQVKIRGLRVELGEIEHHIREALEGVKQVAVDVANGDGGAIIVSYFCFT
Q8NJX1_A9_abu|iva                        KDTQVKIRGLRVELGEVEHAVQVALDGVHQIAVDVFKGDNGTQPRGYFSFS
Q8NJX1_A12_abu|iva                       KDTQVKIRGLRVELGEVEHHIRELLEGVRQVAVDVLTSETGTQLVSYICFN
                                         ** *:***********::* ::  * ..:*:**** ..: *:   .*:.*.
