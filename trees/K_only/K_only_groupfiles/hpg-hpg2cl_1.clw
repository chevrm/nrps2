CLUSTAL W multiple sequence alignment


Q70AZ9b_A1_hpg|hpg2cl                    TRDGVPEGIEAIVVTDEEAFEASAAGARPGDLAYVMYTSGSTGIPKGVAVPHRSVAELAG
Q70AZ9a_A1_hpg|hpg2cl                    TRDGVPEGIEAIVVTDEEAFEASAAGARPGDLAYVMYTSGSTGIPKGVAVPHRSVAELAG
Q8KLL3_A1_hpg|hpg2cl                     TAGRVPEGVEPVVVTDEGRGDASAVPVSPGDLAYVMYTSGSTGTPKGVAVPHRSVAELAG
                                         * . ****:*.:*****   :***. . *************** ****************

Q70AZ9b_A1_hpg|hpg2cl                    NPGWAVEPGDAVLMHAPYAFDASLFEIWVPLVSGGRVVIAEPGPVDARRLREAISSGVTR
Q70AZ9a_A1_hpg|hpg2cl                    NPGWAVEPGDAVLMHAPYAFDASLFEIWVPLVSGGRVVIAEPGPVDARRLREAISSGVTR
Q8KLL3_A1_hpg|hpg2cl                     NPGWAVKPGDAILMHAPHAFDASLFEIWVPLVSGARVVIAEPGAVDARRLREAIAAGVTK
                                         ******:****:*****:****************.********.**********::***:

Q70AZ9b_A1_hpg|hpg2cl                    AHLTAGSFRAVAEESPESFAGLREVLTGGDVVPAHAVARVRSACPRVRIRHLYGPTETTL
Q70AZ9a_A1_hpg|hpg2cl                    AHLTAGSFRAVAEESPESFAGLREVLTGGDVVPAHAVARVRSACPRVRIRHLYGPTETTL
Q8KLL3_A1_hpg|hpg2cl                     VHLTAGSFRALAEESSESFAGLQEVLTGGDVVPAHAVEKVRKAVPQARIRHLYGPTETTL
                                         .*********:****.******:************** :**.* *:.*************

Q70AZ9b_A1_hpg|hpg2cl                    CATWHLLEPGDEIGPVLPIGRPLPGRRAQVLDASLRAVAPGVIGDLYLSGAGLADGYLRR
Q70AZ9a_A1_hpg|hpg2cl                    CATWHLLEPGDEIGPVLPIGRPLPGRRAQVLDASLRAVAPGVIGDLYLSGAGLADGYLRR
Q8KLL3_A1_hpg|hpg2cl                     CATWHLLQPSEALGPVLPIGRPLPGRRAQVLDASLRPLPPGVVGDLYLSGAGLADGYLDR
                                         *******:*.: :***********************.:.***:*************** *

Q70AZ9b_A1_hpg|hpg2cl                    AGLTAERFVADPSAPGARMYRTGDLAQWTADGALLFAGRADDQVKVRGFRIEPAEVEAAL
Q70AZ9a_A1_hpg|hpg2cl                    AGLTAERFVADPSAPGARMYRTGDLAQWTADGALLFAGRADDQVKVRGFRIEPAEVEAAL
Q8KLL3_A1_hpg|hpg2cl                     AALTAERFVADPSVPGGRMYRTGDLVQWTADGELLFVGRADDQVKIRGFRIEPGEIEAAL
                                         *.***********.**.********.****** ***.********:*******.*:****

Q70AZ9b_A1_hpg|hpg2cl                    TAQPGVHEAVVRAVDGRLVGYVVA
Q70AZ9a_A1_hpg|hpg2cl                    TAQPGVHEAVVRAVDGRLVGYVVA
Q8KLL3_A1_hpg|hpg2cl                     TAQPDVHEAVVVAIDGRLIGYAVT
                                         ****.****** *:****:**.*:
