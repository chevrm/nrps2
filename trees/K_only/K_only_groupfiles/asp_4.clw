CLUSTAL W multiple sequence alignment


Q884E3_A1_asp                            DKAAQLGV--CPVLAFDAALWSEVDGGELSVRIIAEQPAYIIYTSGSTGQPKGVVISHGA
Q884E5_A1_asp                            -RETSLNVAGCQGLAWTPALWQSLPTSRPDIELPADSAAYVIHTSGSTGQPKGVVVSQGA
                                          : :.*.*  *  **: .***..:  .. .:.: *:..**:*:************:*:**

Q884E3_A1_asp                            LANYVQGVLARLSLNDGASMAMVSTVAADLGHTLLFGALASGRPLHLLSHEQAFDPDGFA
Q884E5_A1_asp                            LASYVRGLLEQLQLAPEVSMALVSTIAADLGHTVLFGALCSGRTLHVLTESLGFDPDAFA
                                         **.**:*:* :*.*   .***:***:*******:*****.***.**:*:.. .****.**

Q884E3_A1_asp                            RYMAEHQVEVLKIVPSHLQGLLQAAHPADVLPSQLLMLGGEASSWALIEQVRALKPGCRI
Q884E5_A1_asp                            TYMAEHQIGVLKIVPGHLAALLQAAQPADVLPQHALIVGGEACSPALVEQVRQLKPGCRV
                                          ******: ******.** .*****:******.: *::****.* **:**** ******:

Q884E3_A1_asp                            VNHYGPTETTVGILTHEV-----------AERLNAC--------RSVPVGQPLANCKARV
Q884E5_A1_asp                            INHYGPSETTVGVLTHEVPALSELNAIPQVSRFPQCSAHSGDPLRSVPVGAPLPGASAYV
                                         :*****:*****:*****           ..*:  *        ****** **....* *

Q884E3_A1_asp                            LDAYLNPVAERVSGELYLGGQGLAQGYLGRAAMTAERFVPDPDA-DGQRLYRAGDRARWV
Q884E5_A1_asp                            LDDVLNPVATQVAGELYIGGDSVALGYLGQPALTAERFVPDPFAQDGARVYRSGDRMRHN
                                         **  ***** :*:****:**:.:* ****:.*:********* * ** *:**:*** *  

Q884E3_A1_asp                            -DGVLEYLGRADDQVKIRGYRVEPGEVGQVLQTLENVAEAVVLAQPLESDETRLQLVAYC
Q884E5_A1_asp                            HQGLLEFIGRADDQVKVRGYRVEPAEVAQVLLSLPSVAQVSVLALPVDEDESRLQLVAYC
                                          :*:**::********:*******.**.*** :* .**:. *** *::.**:********

Q884E3_A1_asp                            VA
Q884E5_A1_asp                            VA
                                         **
