CLUSTAL W multiple sequence alignment


Q9K5M1_A3_5__thr                         IQQHLVEKLQQHQAHIVCLDSDGEKIAQNSNSNPLNIATPSNLAYVIYTSGSTGKPKGVL
Q847C7_A1_6__thr                         TQKSLLPSLPENQAIVMCLDRDWGVIAACSQENIVSHAQPQNLAYVIYTSGSTGKPKGVL
                                          *: *: .* ::** ::*** *   **  *:.* :. * *.*******************

Q9K5M1_A3_5__thr                         VNHSHVVRLFAATDSWYNFNSQDVWTMFHSYAFDFSVWEVWGALLYGGRLVVVGYLVTRS
Q847C7_A1_6__thr                         INHQNVIRLFAATQAWYHFGASDVFTLFHSIAFDFSVWELWGALLYGGSLVIVPYWVSRD
                                         :**.:*:******::**:*.:.**:*:*** ********:******** **:* * *:*.

Q9K5M1_A3_5__thr                         PKSFYELLCQEKVTILNQTPSAFRQLIPAEQSIATVGD--LNLRLVIFGGETLELNSLQP
Q847C7_A1_6__thr                         PSAFHTLLRQEQVTVLNQTPSAFRQLIRVEE-LAKTGESQLSLRLVIFGGEALEPQSLQP
                                         *.:*: ** **:**:************ .*: :*..*:  *.*********:** :****

Q9K5M1_A3_5__thr                         WFDRHGDQSPQLVNMYGITETTVHVTYRPLSKADLHGKASVIGRPIGDLQVYVLDEHLQP
Q847C7_A1_6__thr                         WFEGYKDQSPQLVNMYGITETTVHVTYRPLSIADVNNSKSLIGVPIPDLQLYILDEQLKP
                                         **: : ************************* **::.. *:** ** ***:*:***:*:*

Q9K5M1_A3_5__thr                         VPIGVAGEMYVGGAGVTRGYLNRAELTAQRFISNPFNGNSEQLLYKSGDLARYLPNGELE
Q847C7_A1_6__thr                         LPIGIKGEMYIGGAGLARGYLNRPELTAERFIPNPFSDAPEARLYKTGDLARYLENGDIE
                                         :***: ****:****::******.****:***.***.. .*  ***:******* **::*

Q9K5M1_A3_5__thr                         YLGRIDNQVKIRGFRIELGEIEAALSQLREVREVVVVARSDQPDNKRLVAYVVP
Q847C7_A1_6__thr                         YLDRIDNQVKIRGFRIELGEIEAALLKYPEVQEAVVMARTDQPGDKRLVAYIVA
                                         **.********************** :  **:*.**:**:***.:******:*.
