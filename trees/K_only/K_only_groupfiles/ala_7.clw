CLUSTAL W multiple sequence alignment


Q01886/62867029_A3_ala                   TLPESANALATLSGLTKV-----IPVSLSELVQQITDNTTKKDEYCKSGDTDP-------
Q01886v2_A2_ala                          -----SPATSRMGALQNISTQMGTEFKIVEL----------EPEFIRSLPLPPKPNHQPM
Q01886_A3_ala                            TLPESANALATLSGLTKV-----IPVSLSELVQQITDNTTKKDEYCKSGDTDP-------
Q01886v1_A2_ala                          -----SPATSRMGALQNISTQMGTEFKIVEL----------EPEFIRSLPLPPKPNHQPM
                                              : * : :..* ::       ..: **          : *: :*    *       

Q01886/62867029_A3_ala                   ---SSPAYLLYTSGTSGKPKGVVMEHRAWSLGFTCHAEYMGFNSC---TRILQFSSLMFD
Q01886v2_A2_ala                          VGLNDDLYVVFTSGSTGVPKGAVATHQAYATGIYEHAVACGMTSLGAPPRSLQFASYSFD
Q01886_A3_ala                            ---SSPAYLLYTSGTSGKPKGVVMEHRAWSLGFTCHAEYMGFNSC---TRILQFSSLMFD
Q01886v1_A2_ala                          VGLNDDLYVVFTSGSTGVPKGAVATHQAYATGIYEHAVACGMTSLGAPPRSLQFASYSFD
                                            ..  *:::***::* ***.*  *:*:: *:  **   *:.*    .* ***:*  **

Q01886/62867029_A3_ala                   LSILEIWAVLYAGGCLFIPSDKERVNNLQDFTRINDINTVF--LTPSIGKLLNPKDLPNI
Q01886v2_A2_ala                          ASIGDIFTTLAVGGCLCIPREEDR-NPAGITTFINRYGVTWAGITPSLALHLDPDAVPTL
Q01886_A3_ala                            LSILEIWAVLYAGGCLFIPSDKERVNNLQDFTRINDINTVF--LTPSIGKLLNPKDLPNI
Q01886v1_A2_ala                          ASIGDIFTTLAVGGCLCIPREEDR-NPAGITTFINRYGVTWAGITPSLALHLDPDAVPTL
                                          ** :*::.* .**** ** :::* *     * **  ...:  :***:.  *:*. :*.:

Q01886/62867029_A3_ala                   SFAGFIGEPMTRSLIDAWTLPGRR--LVNSYGPTEACVLVTAREISPTAPHDKPSSNIGH
Q01886v2_A2_ala                          KALCVAGEPLSMSVVTVWS---KRLNLINMYGPTEATVACIANQVTCTT---TTVSDIGR
Q01886_A3_ala                            SFAGFIGEPMTRSLIDAWTLPGRR--LVNSYGPTEACVLVTAREISPTAPHDKPSSNIGH
Q01886v1_A2_ala                          KALCVAGEPLSMSVVTVWS---KRLNLINMYGPTEATVACIANQVTCTT---TTVSDIGR
                                         .   . ***:: *:: .*:   :*  *:* ****** *   *.::: *:   .. *:**:

Q01886/62867029_A3_ala                   ALGANIWVVEP-QRTALVPIGAVGELCIEAPSLARCYLANPERTEYSF---PSTVLDNWQ
Q01886v2_A2_ala                          GYRATTWVVQPDNHNSLVPIGAVGELIIEGSILCRGYLNDPERTAEVFIRSPSWLHD---
Q01886_A3_ala                            ALGANIWVVEP-QRTALVPIGAVGELCIEAPSLARCYLANPERTEYSF---PSTVLDNWQ
Q01886v1_A2_ala                          GYRATTWVVQPDNHNSLVPIGAVGELIIEGSILCRGYLNDPERTAEVFIRSPSWLHD---
                                         .  *. ***:* ::.:********** **.. *.* ** :****   *   ** : *   

Q01886/62867029_A3_ala                   TKKGTRVYRTGDLVRYASDGTLDFLGRKDGQIKLRGQRIELGEIEHHIRRLMSDDPRFHE
Q01886v2_A2_ala                          LRPNSTLYKTGDLVRYSADGKIIFIGRKDTQVKMNGQRFELGEVEHALQ-----------
Q01886_A3_ala                            TKKGTRVYRTGDLVRYASDGTLDFLGRKDGQIKLRGQRIELGEIEHHIRRLMSDDPRFHE
Q01886v1_A2_ala                          LRPNSTLYKTGDLVRYSADGKIIFIGRKDTQVKMNGQRFELGEVEHALQ-----------
                                          : .: :*:*******::**.: *:**** *:*:.***:****:** ::           

Q01886/62867029_A3_ala                   ASVQLYNPATDP-DRDATVDV----QMREPYLAGLLVLDLVFT
Q01886v2_A2_ala                          --LQL-----DPSDGPIIVDLLKRTQSGEPDL--LIAFLFVG-
Q01886_A3_ala                            ASVQLYNPATDP-DRDATVDV----QMREPYLAGLLVLDLVS-
Q01886v1_A2_ala                          --LQL-----DPSDGPIIVDLLKRTQSGEPDL--LIAFLFVG-
                                           :**     ** *    **:    *  ** *  *:.: :*  
