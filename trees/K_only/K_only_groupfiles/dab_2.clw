CLUSTAL W multiple sequence alignment


Q5DIS7_A2_dab                            HQALARELLEELPEESRPALLDW-AAQQGQGKCEQRPGIVAPTNGLAYVIYTSGSTGHPK
Q9I157_A3_4__dab                         EQALA--LFDELGCVDRPRLLVWDEIQQGEG-AEHDPQVYSGPQNLAYVIYTSGSTGLPK
                                         .****  *::**   .** ** *   ***:* .*: * : : .:.************ **

Q5DIS7_A2_dab                            GVMVEQAGMLNNQLSKVPLLALDENDVIAQTASQSFDISVWQFLAAPLFGAQVDILPNDI
Q9I157_A3_4__dab                         GVMVEQAGMLNNQLSKVPYLELDENDVIAQTASQSFDISVWQFLAAPLFGARVAIVPNAV
                                         ****************** * ******************************:* *:** :

Q5DIS7_A2_dab                            AHDPLALSRRVRERGITVLELVPSLIQEVLDDPQETLPGLRWMLSTGEALSPELARRWLT
Q9I157_A3_4__dab                         AHDPQGLLAHVGEQGITVLESVPSLIQGMLAEERQALDGLRWMLPTGEAMPPELARQWLK
                                         **** .*  :* *:****** ****** :* : :::* ******.****:.*****:**.

Q5DIS7_A2_dab                            RYPQVGLMNAYGPAECSDDVSFFRVDTQSTGGTYLPIGQATDNNHLQVL----DDDLLPV
Q9I157_A3_4__dab                         RYPRIGLVNAYGPAECSDDVAFFRVDLASTESTYLPIGSPTDNNRLYLLGAGADDAFELV
                                         ***::**:************:*****  ** .******..****:* :*    ** :  *

Q5DIS7_A2_dab                            PLGGIGELYVSGTGVGRGYLADPGRTALAFLPDPYAQVPGSRIYRTGDLACRGKGDQLEY
Q9I157_A3_4__dab                         PLGAVGELCVAGTGVGRGYVGDPLRTAQAFVPHPFG-APGERLYRTGDLARRRADGVLEY
                                         ***.:*** *:********:.** *** **:*.*:. .**.*:******* *  .. ***

Q5DIS7_A2_dab                            VGRIDHQVKVRGFRIELGEIESRLLELPIVREAVVLAQDGPTGKSLAAYLVP
Q9I157_A3_4__dab                         VGRIDHQVKIRGFRIELGEIEARLHERADVREAAVAVQEGANGKYLVGYLVP
                                         *********:***********:** * . ****.* .*:*..** *..****
