CLUSTAL W multiple sequence alignment


Q93N89_A2_2__trp                         TAQTRPMVPEGFTGLVVTLDEDLPEGDFDDGRVLTAPDADHIAYVIHTSGSTGTPKGAAV
Q93N89_A2_trp                            TAQTRPMVPEGFTGLVVTLDEDLPEGDFDDGRVLTAPDADHIAYVIHTSGSTGTPKGAAV
                                         ************************************************************

Q93N89_A2_2__trp                         TWGGLRNLVADRIERYGIGTDTRLVQLVSPSFDVSMADIWPTLCAGGRLVLAPPGGHTTG
Q93N89_A2_trp                            TWGGLRNLVADRIERYGIGTDTRLVQLVSPSFDVSMADIWPTLCAGGRLVLAPPGGHTTG
                                         ************************************************************

Q93N89_A2_2__trp                         DELGDLLADHRITHAVMPAVQLTHLPDRELPALRVLVSGGDALPADTRRRWVARCDLHNE
Q93N89_A2_trp                            DELGDLLADHRITHAVMPAVQLTHLPDRELPALRVLVSGGDALPADTRRRWVARCDLHNE
                                         ************************************************************

Q93N89_A2_2__trp                         YGVTEATVVSTVTAPLDDAGPLTIGGPIARAGVHVLDGFLRPVPPGVTGELYVTGTGVAR
Q93N89_A2_trp                            YGVTEATVVSTVTAPLDDAGPLTIGGPIARAGVHVLDGFLRPVPPGVTGELYVTGTGVAR
                                         ************************************************************

Q93N89_A2_2__trp                         GYLNRPTLTAHRFIADPRTRGGRMYRTGDLVRHTHGGELVFVGRADEQVKLRGHRIELGE
Q93N89_A2_trp                            GYLNRPTLTAHRFIADPRTRGGRMYRTGDLVRHTHGGELVFVGRADEQVKLRGHRIELGE
                                         ************************************************************

Q93N89_A2_2__trp                         VEAALADHPDVEQAVAAIHDARLIGYVVP
Q93N89_A2_trp                            VEAALADHPDVEQAVAAIHDARLIGYVVP
                                         *****************************
