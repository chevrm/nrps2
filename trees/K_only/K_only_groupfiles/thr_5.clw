CLUSTAL W multiple sequence alignment


Q847C7_A1_6__thr                         TQKSLLPSLPENQAIVMCLDR-DWGVIAACSQE-----NIVSHAQPQNLAYVIYTSGSTG
Q50JA3_A1_thr                            TETKRRDELPTEKVPTIFVDALQW-------QEGQRAPNPAPGLTPDNAAYVIYTSGSTG
                                         *:..   .** ::. .: :*  :*       **     * ..   *:* ***********

Q847C7_A1_6__thr                         KPKGVLINHQNVIRLFAATQAWYHFGASDVFTLFHSIAFDFSVWELWGALLYGGSLVIVP
Q50JA3_A1_thr                            RPKGVIVTHANATRLFTTTDALYGFGPSDVWTLFHSAAFDFSVWELWGALFYGGRLVVVP
                                         :****::.* *. ***::*:* * **.***:***** *************:*** **:**

Q847C7_A1_6__thr                         YWVSRDPSAFHTLLRQEQVTVLNQTPSAFRQLIRVEELAKTGESQLSLRLVIFGGEALEP
Q50JA3_A1_thr                            HWMTRSPEAFGELIAREGVTVLNQTPSAFRALLRAPSIAD-GVGGRGLKWIIFGGEALDA
                                         :*::*.*.**  *: :* ************ *:*. .:*. * .  .*: :*******:.

Q847C7_A1_6__thr                         QSLQPWFEGYKDQSPQLVNMYGITETTVHVTYRPLSIADVNNSKSLIGVPIPDLQLYILD
Q50JA3_A1_thr                            ATVRPWFERYADAATRLINMYGITETTVHVTYHHVTQADLSSAASLIGRPIPDLVINLLD
                                          :::**** * * :.:*:**************: :: **:..: **** ***** : :**

Q847C7_A1_6__thr                         EQLKPLPIGIKGEMYIGGAGLARGYLNRPELTAERFIPNPFSDAPEARLYKTGDLARYLE
Q50JA3_A1_thr                            EHGQPVPDGVPGEMYVGGAGVARGYLKRPELTAQRFIHNP--SAPSERLYRSGDLAIRQQ
                                         *: :*:* *: ****:****:*****:******:*** **  .**. ***::****   :

Q847C7_A1_6__thr                         NGDIEYLDRIDNQVKIRGFRIELGEIEAALLKYPEVQEAVVMARTDQPGDKRLVAYIVA
Q50JA3_A1_thr                            DGTFTYLGRIDDQVKIRGFRIELGEIQSVMARHPAVADAYVSTYERSADDRRIVAYVVP
                                         :* : **.***:**************::.: ::* * :* * :   ...*:*:***:*.
