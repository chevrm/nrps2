CLUSTAL W multiple sequence alignment


O07944_A1_pro                            VVLTGEDTGQDLSGYDDTDLTDADRTAPLLPAHPAYVIYTSGSTGTPKAVVMPGAALVNL
O05647_A1_pro                            LVLTEDDVDEDLSGIPDGNLTDAERTAPLTPAHPAYVIYTSGSTGRPKAVVMPGAAVVNL
                                         :*** :*..:****  * :****:***** *************** **********:***

O07944_A1_pro                            LAWHRREIPGEAGAPVAQFTTIGFDVAAQEILATWLHGKTLAVPSQEVRRSAEQLAAWLD
O05647_A1_pro                            LAWHRREIPAGAGTTVAQFASLSFDVAAQEILSTLLYGATLAVPTDAVRRDADAFAAWLE
                                         *********. **:.****:::.*********:* *:* *****:: ***.*: :****:

O07944_A1_pro                            EQHVSELYAPNLVIEALAEAAAEAGRTLPALRHIAQAGEALTLTRTVREFAAAVPGRQLH
O05647_A1_pro                            EYRVNELYAPNLVVEALAEAAAEQGRTLPDLRHIAQAGEALTAGPRVRDFCAALPGRRLH
                                         * :*.********:********* ***** ************    **:*.**:***:**

O07944_A1_pro                            NHYGPAETHVMTGTALPEDPAAWSEHAPLGRPVSGARVYVLDSALRPVAPGVTGELYLAG
O05647_A1_pro                            NHYGPAETHVMTGIELPVDPGGWPERVPIGGPVDNARLYVLDGFLRPVPPGVVGELYLAG
                                         *************  ** **..*.*:.*:* **..**:****. ****.***.*******

O07944_A1_pro                            AGVSRGYLNRPVLTAERFVADPYAPSPGARMYRTGDLGRWNTRGELEFAGRADHQVKIRG
O05647_A1_pro                            AGVARGYLNRPGLTAERFVADPFG-GPGTRMYRTGDLARWAGSGVLEFAGRADHQVKVRG
                                         ***:******* **********:. .**:********.**   * ************:**

O07944_A1_pro                            FRIEPGEIEAALTDLPAVARAAVVVREDRPGDKRLVAYAV-
O05647_A1_pro                            FRIEPGEVESVLAAQPGVARAVVLAREDRPGERRLVAYLVA
                                         *******:*:.*:  *.****.*:.******::***** * 
