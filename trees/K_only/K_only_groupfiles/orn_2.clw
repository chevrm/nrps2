CLUSTAL W multiple sequence alignment


O87314_A3_orn                            RCHTRRYARAPADRRRTRGFRVRQHAPEHPAYLIYTSGSTGRPKGVLTGYAGLTNMYFNH
O87314_A1_orn                            ---------------------------EHPAYLIYTSGSTGRPKGVLTGYAGLTNMYFNH
                                                                    *********************************

O87314_A3_orn                            REAIFAPTVARAGSAEQLRIAHTVSFSFDMSWEELFWLVEGHQVHVCDEELRRDAPALVA
O87314_A1_orn                            REAIFAPTVARAGSAEQLRIAHTVSFSFDMSWEELFWLVEGHQVHVCDEELRRDAPALVA
                                         ************************************************************

O87314_A3_orn                            YCHRHRIDVINVTPTYAHHLFDAGLLDDGAHTPPLVLLGGEAVGDGVWSALRDHPDSAGY
O87314_A1_orn                            YCHRHRIDVINVTPTYAHHLFDAGLLDDGAHTPPLVLLGGEAVGDGVWSALRDHPDSAGY
                                         ************************************************************

O87314_A3_orn                            NLYGPTEYTINTLGGGTDDSDTPTVGQPIWNTRGYILDAALRPVPDGAVGELYIAGTGLA
O87314_A1_orn                            NLYGPTEYTINTLGGGTDDSDTPTVGQPIWNTRGYILDAALRPVPDGAVGELYIAGTGLA
                                         ************************************************************

O87314_A3_orn                            LGYHRRAGLTAATMVADPYVPGGRMYRTGDLVRRRPGSAELLDYLGRVDDQVKIRGYRVE
O87314_A1_orn                            LGYHRRAGLTAATMVADPYVPGGRMYRTGDLVRRRPGSAELLDYLGRVDDQVKIRGYRVE
                                         ************************************************************

O87314_A3_orn                            LGEIESVLTRADGVARCAVVARATGANPPVKTLAAYVIP
O87314_A1_orn                            LGEIESVLTRADGVARCAVVARATGANPPVKTLAAYVIP
                                         ***************************************
