CLUSTAL W multiple sequence alignment


Q45R83_A1_8__lys                         AAVADLAAPVLVLDDPSTEAAIDALDPGPVTDADRTAPLLPGHAAYVIHTSGSTGRPKGV
O05819_A1_lys                            ----QTSAPV-VIDEGVFAASVGA----DILEEDRAITVPVDQAAYVIFTSGTTGTPKGV
                                             : :*** *:*:    *::.*     : : **: .:  .:*****.***:** ****

Q45R83_A1_8__lys                         TVDHRGLSRLLQAH-RRVTFSRIRPSA---GGPGRAAHVSSFSFDASWDPLLAMVAGHEL
O05819_A1_lys                            IGTHRALSAYADDHIERV----LRPAAQRLGRPLRIAHAWSFTFDAAWQPLVALLDGHAV
                                            **.**   : * .**    :**:*   * * * **. **:***:*:**:*:: ** :

Q45R83_A1_8__lys                         HMIDEDLRFDPPGVVAYFRDRRIDYVDLTPTYFRSLLDAGLLEEGFPCP-SLVALGGEAM
O05819_A1_lys                            HIVDDHRQRDAGALVEAIDRFGLDMIDTTPSMFAQLHNAGLLDRA---PLAVLALGGEAL
                                         *::*:. : *. .:*  :    :* :* **: * .* :****:..   * :::******:

Q45R83_A1_8__lys                         DGELWERLRAAAPR--VTAMNTYGPTETAVDAVVTVLGDLPPGTIGRPVPRWRAYVLDAG
O05819_A1_lys                            GAATWRMIQQNCARTAMTAFNCYGPTETTVEAVVAAVAEHARPVIGRPTCTTRAYVMDSW
                                         ..  *. ::  ..*  :**:* ******:*:***:.:.: .  .****.   ****:*: 

Q45R83_A1_8__lys                         LRPVPPGVLGELYLAGPGVARGYLGQHALTAERFVACPFGKPGERMYRTGDLARWLPDGH
O05819_A1_lys                            LRPVPDGVAGELYLAGAQLTRGYLGRPAETAARFVAEPNGR-GSRMYRTGDVVRRLPDGG
                                         ***** ** *******. ::*****: * ** **** * *: *.*******:.* **** 

Q45R83_A1_8__lys                         LVYVGRGDEQVKIRGFRIEPGEVEAALRELEGVAAAAVTVREDTPGTRRLVGYVVG
O05819_A1_lys                            LEFLGRSDDQVKIRGFRVEPGEIAAVLNGHHAVHGCHVTARGHASGP-RLTAYVAG
                                         * ::**.*:********:****: *.*.  ..* .. **.* .:.*. **..**.*
