CLUSTAL W multiple sequence alignment


Q884E5_A2_thr                            SQTQLLGQLPIPAHVQTLDLADALDGYSTENPINQTSPDNLAYVIYTSGSTGKPKGTLLA
Q884E4_A1_thr                            TQTQLLGQLPIPAHVQTLDLADALDGYSTENPINQTSPDNLAYVIYTSGSTGKPKGTLLA
                                         :***********************************************************

Q884E5_A2_thr                            HHNLMRLFAATDDWFKFNEKDVWTLFHSFAFDFSVWEIFGALLHGGRLVIVPCEVTRSPE
Q884E4_A1_thr                            HHNLMRLFAATDDWFKFNEKDVWTLFHSFAFDFSVWEIFGALLHGGRLVIVPREVTRSPE
                                         **************************************************** *******

Q884E5_A2_thr                            EFHALLVDQQVTVLNQTPSAFKQLMRVACDSTSVLSLETIIFGGEALDVASLKPWFARFG
Q884E4_A1_thr                            EFHALLVDQQVTVLNQTPSAFKQLMRVACDSPVPMSLQKVIFGGEALDVASLKPWFARFG
                                         *******************************.  :**:.:********************

Q884E5_A2_thr                            DQTPRLINMYGITETTVHVTYRPITLADTHNPASPIGEAIADLSWYVLDADFNTVAQGCS
Q884E4_A1_thr                            DQMPRLINMYGITETTVHVTYRPITLADTHNPASPIGEAIADLSWYVLDADFNPVAQGCS
                                         ** **************************************************.******

Q884E5_A2_thr                            GELHIGHAGLARGYHNRAALTAERFVPDPFANDGGRLYRTGDLARYKTAGTIEYAGRIDH
Q884E4_A1_thr                            GELHIGHAGLARGYHHRAALTAERFVPDPFSNDGGRLYRTGDLARYKTAGTIEYAGRIDH
                                         ***************:**************:*****************************

Q884E5_A2_thr                            QVKIRGFRIELGEIEARLQAHSAVREVIVLAVDGQLAAYLLP
Q884E4_A1_thr                            QVKIRGFRIELGEIEARLQAHSAVREVIVLAVDGQLAAYLLP
                                         ******************************************
