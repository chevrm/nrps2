CLUSTAL W multiple sequence alignment


Q84BQ6v1_A2_asp                          VHGATRQLLGEPS--VPLINLDHGSW-----EQQPSGNPQVPGLNASNLAYMIYTSGSTG
Q84BQ6v2_A2_asp                          VHGATRQLLGEPS--VPLINLDHGSW-----EQQPSGNPQVPGLNASNLAYMIYTSGSTG
O85168_A8_asp                            ----TQGALSLPVGDTPLMLLDSAESLLAADDQAFDANPVVDGLTAENLAYVIYTSGSTG
Q84BQ4_A5_11__asp                        VQTATRGLFDDAV--ATVIDLDRSTW-----QHLPDHDSSVPGLSASNLAYMIYTSGSTG
                                             *:  :. .   ..:: ** .       ::  . :. * **.*.****:********

Q84BQ6v1_A2_asp                          LPKGVMIEHRSACNMVHWG------SQLSPPTGHGALLQKAPFSFDSSVWEIFWPLCSGM
Q84BQ6v2_A2_asp                          LPKGVMIEHRSACNMVHWG------SQLSPPTGHGALLQKAPFSFDSSVWEIFWPLCSGM
O85168_A8_asp                            QSKGVMVEHRSVFNF--WNVLTRTTHQHCPTPATVAL--NAGFFFDMSIKGI-SQLFSGH
Q84BQ4_A5_11__asp                        LPKGVMIEHRSACNMVHWG------SQISPPTEHGALLQKAPFSFDSSVWEIFWPLCSGM
                                          .****:****. *:  *.       * .*..   **  :* * ** *:  *   * ** 

Q84BQ6v1_A2_asp                          RLV----LARPDGNRDSAYVVQTIREHQVTVVKFVPALLQQFIEQDGVEQCT-SLTDVLN
Q84BQ6v2_A2_asp                          RLV----LARPDGNRDSAYVVQTIREHQVTVVKFVPALLQQFIEQDGVEQCT-SLTDVLN
O85168_A8_asp                            KLVIIPQLLRANGSE----LLDFLEAHQVHAFDSTPSQLDTLLSAGLLERSSYQPVSVLL
Q84BQ4_A5_11__asp                        RLV----LARPNGNRDSAYVVQTIREQQVTVVKFVPALLQQFIEQDGVEQCT-SLTDVLN
                                         :**    * *.:*..    ::: :. :** ... .*: *: ::. . :*:.: . ..** 

Q84BQ6v1_A2_asp                          GGGELSAALARQVRDRLPWVRLHNVYGPTETTVDSTGWTLDPEMPVPDNVVPIGTALSNT
Q84BQ6v2_A2_asp                          GGGELSAALARQVRDRLPWVRLHNVYGPTETTVDSTGWTLDPEMPVPDNVVPIGTALSNT
O85168_A8_asp                            GGEAINASTWEKLRN-CPTIRLYNMYGPTECTVDATIDLIRDLGEKPS----IGRPIANV
Q84BQ4_A5_11__asp                        GGGELSAVLARQVRDRLPWVRLHNVYGPTETTVDSTGWTLEPHMPVPDNVVPIGTALSNT
                                         **  :.*   .::*:  * :**:*:***** ***:*   :      *.    ** .::*.

Q84BQ6v1_A2_asp                          RLYVLDAYGQPVPQGVSGELHIGGVGVARGYHGLPEMQAERFIDSPF--VPGDRLYRTGD
Q84BQ6v2_A2_asp                          RLYVLDAYGQPVPQGVSGELHIGGVGVARGYHGLPEMQAERFIDSPF--VPGDRLYRTGD
O85168_A8_asp                            QVHVLDARGEPAPLGVAGEIHIGGSGVARGYLNRDELSAERFIVDPFSDAANARLYKTGD
Q84BQ4_A5_11__asp                        RLYVLDAYGQPVPQGVSGELHIGGVGVARGYHGLPEMQAERFIDSPF--VAGDRLYRTGD
                                         :::**** *:*.* **:**:**** ****** .  *:.***** .**  ... ***:***

Q84BQ6v1_A2_asp                          LARYNHHGELEFLGRNDFQIKLRGLRLEPGEIEARLIEHPAIRQAVVMVRD--------E
Q84BQ6v2_A2_asp                          LARYNHHGELEFLGRNDFQIKLRGLRLEPGEIEARLIEHPAIRQAVVMVRD--------E
O85168_A8_asp                            LGRWLADGTLEYMGRNDFQVKVRGFRIELGEIENVLLAVPGIREVVVIARNDSQGDSDSQ
Q84BQ4_A5_11__asp                        LARYNNHGELEFLGRNDFQIKLRGLRLEPGEIEARLIEHPAIREAVVMVRD--------E
                                         *.*:  .* **::******:*:**:*:* ****  *:  *.**:.**:.*:        :

Q84BQ6v1_A2_asp                          RLVAWYTV
Q84BQ6v2_A2_asp                          RLVAWYTV
O85168_A8_asp                            RLVAYVCG
Q84BQ4_A5_11__asp                        RLVAWYTV
                                         ****:   
