CLUSTAL W multiple sequence alignment


Q01886v1_A1_pro                          GAGKTAALFKSADTAVQTIDITKDIPHGLSDTVVQSNTKIDDPAFGLFTSGSTGVPKCIV
Q01886v2_A1_pro                          GAGKTAALFKSADTAVQTIDITKDIPHGLSDTVVQSNTKIDDPAFGLFTSGSTGVPKCIV
                                         ************************************************************

Q01886v1_A1_pro                          VTHSQICTAVQAYKDRFGVTSETRVLQFSSYTFDISIADTFTALFYGGTLCIPSEEDRMS
Q01886v2_A1_pro                          VTHSQICTAVQAYKDRFGVTSETRVLQFSSYTFDISIADTFTALFYGGTLCIPSEEDRMS
                                         ************************************************************

Q01886v1_A1_pro                          NLQDYMVSVRPNWAVLTPTVSRFLDPGVVKDFISTLIFTGEASREADTVPWIEAGVNLYN
Q01886v2_A1_pro                          NLQDYMVSVRPNWAVLTPTVSRFLDPGVVKDFISTLIFTGEASREADTVPWIEAGVNLYN
                                         ************************************************************

Q01886v1_A1_pro                          VYGPAENTLITTATRIRKGKSSNIGYGVNTRTWVTDVSGACLVPVGSIGELLIESGHLAD
Q01886v2_A1_pro                          VYGPAENTLITTATRIRKGKSSNIGYGVNTRTWVTDVSGACLVPVGSIGELLIESGHLAD
                                         ************************************************************

Q01886v1_A1_pro                          KYLNRPDRTEAAFLSDLPWIPNYEGDSVRRGRRFYRTGDLVRYCDDGSLICVGRSDTQIK
Q01886v2_A1_pro                          KYLNRPDRTEAAFLSDLPWIPNYEGDSVRRGRRFYRTGDLVRYCDDGSLICVGRSDTQIK
                                         ************************************************************

Q01886v1_A1_pro                          LAGQRVELGDVEAHLQSDPTTSQAAVVFPRSGPLEARLIALLVTG
Q01886v2_A1_pro                          LAGQRVELGDVEAHLQSDPTTSQAAVVFPRSGPLEARLIALLVTG
                                         *********************************************
