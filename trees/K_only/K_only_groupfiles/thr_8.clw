CLUSTAL W multiple sequence alignment


Q9RLP6v1_A2_thr                          SSADLCTRLIASGVPVIEVDDPAIGAEASTSLPVPAVDDIAYIIYTSGTTGTPKGVAVTH
Q9RLP6v2_A2_thr                          SSADLCTRLIASGVPVIEVDDPAIGAEASTSLPVPAVDDIAYIIYTSGTTGTPKGVAVTH
                                         ************************************************************

Q9RLP6v1_A2_thr                          RNVAQLLDTLGAQLELGQTWTQCHSLAFDYSVWEIWGPLLNGGRLLMVPDAVVRSPEDLH
Q9RLP6v2_A2_thr                          RNVAQLLDTLGAQLELGQTWTQCHSLAFDYSVWEIWGPLLNGGRLLMVPDAVVRSPEDLH
                                         ************************************************************

Q9RLP6v1_A2_thr                          AMLVAEQVSMLSQTPSAFYALQTADALYPERGEQLKLQTVVFGGEALEPHRLSGWMHAHP
Q9RLP6v2_A2_thr                          AMLVAEQVSMLSQTPSAFYALQTADALYPERGEQLKLQTVVFGGEALEPHRLSGWMHAHP
                                         ************************************************************

Q9RLP6v1_A2_thr                          GMPRMINMYGITETTVHASFREIGEADLANSTSPIGVPLEHLSFFVLDGWLRQVPVGVVG
Q9RLP6v2_A2_thr                          GMPRMINMYGITETTVHASFREIGEADLANSTSPIGVPLEHLSFFVLDGWLRQVPVGVVG
                                         ************************************************************

Q9RLP6v1_A2_thr                          ELYVAGEGLACGYISRSDLTSTRFVACPFGAPGARMYRTGDLVRWGADGQLQYVGRADEQ
Q9RLP6v2_A2_thr                          ELYVAGEGLACGYISRSDLTSTRFVACPFGAPGARMYRTGDLVRWGADGQLQYVGRADEQ
                                         ************************************************************

Q9RLP6v1_A2_thr                          VKIRGYRIELGEVHAALVGLDGVEQAAVIAREDRPGDKRLVGYVTG
Q9RLP6v2_A2_thr                          VKIRGYRIELGEVHAALVGLDGVEQAAVIAREDRPGDKRLVGYVTG
                                         **********************************************
