CLUSTAL W multiple sequence alignment


Q9F638/11127897_A_dhb                    AEKVRSAVPGLRHVVVVGEA----GPFTSLSQLYASPVDLPGPIPSDVAFFQLSGGSTGV
Q9FDB2_A_dhb                             AQELLACCPTLQTVIVRGQTRVTDPKFIELASCYSASSCQANADPNQIAFFQLSGGTTDT
                                         *::: :. * *: *:* *::      * .*:. *::.   ... *.::********:*..

Q9F638/11127897_A_dhb                    PKLIPRTHDDYIYSLRGSVEICQLDETTVYLCALPAAHNFPLSSPGVLGTLYAGGTAVMA
Q9FDB2_A_dhb                             PKLIPRTHNDYAYSVTASVEICRFDQHTRYLCVLPAAHNFPLSSPGALGVFWAGGCVVLS
                                         ********:** **: .*****::*: * ***.*************.**.::*** .*::

Q9F638/11127897_A_dhb                    LHPSPDQAFPLIERERITFTALVPPLAMIWMDAAKARRHDLSSLKVLQVGGARLSTEAAQ
Q9FDB2_A_dhb                             QDASPQHAFKLIEQHKITVTALVPPLALLWMDHAEKSTYDLSSLHFVQVGGAKFSEAAAR
                                          ..**::** ***:.:**.********::*** *:   :*****:.:*****::*  **:

Q9F638/11127897_A_dhb                    RVRPTLGCALQQVYGMAEGLVNYTRLDDPDELVIATQGRPISPDDEIRIIDEDGRDVAPG
Q9FDB2_A_dhb                             RLPKALGCQLQQVFGMAEGLVNYTRLDDSAELIATTQGRPISAHDQLLVVDEQGQPVASG
                                         *:  :*** ****:**************. **: :*******..*:: ::**:*: **.*

Q9F638/11127897_A_dhb                    ETGQLLTRGPYTIRGYYNAEAHNARAFTSDGFYCTGDLVRVTPEGYLVVEGRAKDQINRG
Q9FDB2_A_dhb                             EEGYLLTQGPYTIRGYYRADQHNQRAFNAQGFYITGDKVKLSSEGYVIVTGRAKDQINRG
                                         * * ***:*********.*: ** ***.::*** *** *:::.***::* **********

Q9F638/11127897_A_dhb                    GDKIAAEEVENHLLAHPSVHDAAVISIPDPFLGERTCAFVIP
Q9FDB2_A_dhb                             GEKIAAEEVENQLLHHPAVHDAALIAISDEYLGERSCAVIVL
                                         *:*********:** **:*****:*:*.* :****:**.:: 
