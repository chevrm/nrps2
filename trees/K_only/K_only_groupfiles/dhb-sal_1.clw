CLUSTAL W multiple sequence alignment


Q56950b_A1_dhb|sal                       ARQMAHKHACLRHVLVAGETVSDDFTPLFSLHGERQAWPQPDVSA----TALLLLSGGTT
Q9RFM9_A1_dhb|sal                        ARELLASGAC-RMALIHGEAEA----PLQALAPLYQADALEDCAARAEDIACFQLSGGTT
Q56950a_A1_dhb|sal                       ARQMAHKHACLRHVLVAGETVSDDFTPLFSLHGERQAWPQPDVSA----TALLLLSGGTT
                                         **::  . ** * .*: **: :    ** :*    ** .  * :*     * : ******

Q56950b_A1_dhb|sal                       GTPKLIPRRHADYSYNFSASAELCGISQQSVYLAVLPVAHNFPLACPGILGTLACGGKVV
Q9RFM9_A1_dhb|sal                        GTPKLIPRRHREYLYNVRASAEVCGFDEHTVYLTGLPMAHNFTLCCPGVIGTLLAGGRVV
Q56950a_A1_dhb|sal                       GTPKLIPRRHADYSYNFSASAELCGISQQSVYLAVLPVAHNFPLACPGILGTLACGGKVV
                                         ********** :* **. ****:**:.:::***: **:****.*.***::*** .**:**

Q56950b_A1_dhb|sal                       LTDSASCDEVMPLIAQERVTHVALVPALAQLWVQAREWEDSDLSSLRVIQAGGARLDPTL
Q9RFM9_A1_dhb|sal                        VSQRADPEHCFALIARERVTHTALVPPLAMLWLDAQESRRADLSSLRLLQVGGSRLGSSA
Q56950a_A1_dhb|sal                       LTDSASCDEVMPLIAQERVTHVALVPALAQLWVQAREWEDSDLSSLRVIQAGGARLDPTL
                                         ::: *. :. :.***:*****.****.** **::*:* . :******::*.**:**..: 

Q56950b_A1_dhb|sal                       AEQVIATFDCTLQQVFGMAEGLLCFTRLDDPHATILHSQGRPLSPLDEIRIVDQDENDVA
Q9RFM9_A1_dhb|sal                        AQRVEPVLGCQLQQVLGMAEGLICYTRLDDPPERVLHTQGRPLSPDDEVRVVDAEGREVG
Q56950a_A1_dhb|sal                       AEQVIATFDCTLQQVFGMAEGLLCFTRLDDPHATILHSQGRPLSPLDEIRIVDQDENDVA
                                         *::* ..:.* ****:******:*:******   :**:******* **:*:** : .:*.

Q56950b_A1_dhb|sal                       PGETGQLLTRGPYTISGYYRAPAHNAQAFTAQGFYRTGDNVRLDEVGNLHVEGRIKEQIN
Q9RFM9_A1_dhb|sal                        PGEVGELTVRGPYTIRGYYRLPEHNAKAFSADGFYRTGDRVSRDKDGYLVVEGRDKDQIN
Q56950a_A1_dhb|sal                       PGETGQLLTRGPYTISGYYRAPAHNAQAFTAQGFYRTGDNVRLDEVGNLHVEGRIKEQIN
                                         ***.*:* .****** **** * ***:**:*:*******.*  *: * * **** *:***

Q56950b_A1_dhb|sal                       RAGEKIAAAEVESALLRLAEVQDCAVVAAPDTLLGERICAFIIA
Q9RFM9_A1_dhb|sal                        RGGEKIAAEEVENLLIAHPQVHDATVVAMPDSLLGERTCAFVIP
Q56950a_A1_dhb|sal                       RAGEKIAAAEVESALLRLAEVQDCAVVAAPDTLLGERICAFIIA
                                         *.****** ***. *:  .:*:*.:*** **:***** ***:*.
