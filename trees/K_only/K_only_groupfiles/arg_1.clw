CLUSTAL W multiple sequence alignment


Q9S1A7_A1_10__arg                        TQQSLVQFLPENQAEILCLDTDWSRIANYSQE---NLTSPVKPENLAYVIYTSGSTGKPK
Q8RTG4_A1_arg                            TQESLGDFLPQTGAELLCLDRDWEKIATYSPENPFNLTTP---ENLAYVIYTSGSTGKPK
Q9RNA9_A1_arg                            TQQSLVQFLPENQAEILCLDTDWSRIANYSQE---NLTSPVKTENLAYVIYTSGSTGKPK
                                         **:** :***:. **:**** **.:**.** *   ***:*   *****************

Q9S1A7_A1_10__arg                        GVMNIHQGICNTLKYNIDNYNLNSEERILQITPFSFDVSVWEIFLSLTSGATLVVAKPDG
Q8RTG4_A1_arg                            GVMNIHRGICNTLTYAIGHYNINSEDRILQITSLSFDVSVWEVFLSLISGASLVVAKPDG
Q9RNA9_A1_arg                            GVMNIHQGICNTLKYNIDNYNLNSEDRILQITPFSFDVSVWEVFSSLTSGATLVVTKPDG
                                         ******:******.* *.:**:***:******.:********:* ** ***:***:****

Q9S1A7_A1_10__arg                        YKDIDYLIDLIVQEQVTCFTCVPSILRVFLQHSKSKDCHCLKRVIVGGEALSYELNQRFF
Q8RTG4_A1_arg                            YKDIDYLIDLIVQEQVTCFTCVPSILRVFLQHPKSKDCHCLKRVIVGGEALSYELNQRFF
Q9RNA9_A1_arg                            YKDIDYLIDLIVQEQVTCFTCVPSILRVFLQHPKSKDCHCLKRVIVGGEALSYELNQRFF
                                         ********************************.***************************

Q9S1A7_A1_10__arg                        QQLNCELYNAYGPTEVAVETTIWCCQPNSQ-ISIGTPIANAQVYILDSYLQPVPIGVAGE
Q8RTG4_A1_arg                            QQLNCELYNAYGPTEVAVETTIWCCQPNSQ-ISIGTPIANAQVYILDSYLQPVPIGVAGE
Q9RNA9_A1_arg                            QQLNCELYNAYGPTEVAVDATVWCCQPNSQLISIGRPIANVQVYILDSYLQPVPIGVAGE
                                         ******************::*:******** **** ****.*******************

Q9S1A7_A1_10__arg                        LHIGGMGLARGYLNQPELTAEKFIPHPFAQGKLYKTGDLARYLPDGNIEYLGRIDNQVKL
Q8RTG4_A1_arg                            LHIGGMGLARGYLNQPELTAEKFIPHPFAQGKLYKTGDLARYLPEGNIEYLGRIDNQVKL
Q9RNA9_A1_arg                            LHIGGMGLARGYLNQAELTAEKFIPHPFAEGKLYKTGDLARYLPDGNIEYLGRIDNQVKL
                                         ***************.*************:**************:***************

Q9S1A7_A1_10__arg                        RGLRIELGEIQTVLETHPNVEQTVVIMR---EDTLYNQRLVAYVIR
Q8RTG4_A1_arg                            RGLRIELGEIQTVLETHPNVEQTVVIPRGSAGNSL-----------
Q9RNA9_A1_arg                            RGFRIELGEIQTVLETHPNVEQTVVIMR---EDTLYNQRLVAYVMR
                                         **:*********************** *    ::*           
