CLUSTAL W multiple sequence alignment


Q01886v1_A2_ala                          SPATSRMGALQNISTQMGTEFKIVELEPEFIRSLPLPPKPNHQPMVGLNDDLYVVFTSGS
Q01886v2_A2_ala                          SPATSRMGALQNISTQMGTEFKIVELEPEFIRSLPLPPKPNHQPMVGLNDDLYVVFTSGS
                                         ************************************************************

Q01886v1_A2_ala                          TGVPKGAVATHQAYATGIYEHAVACGMTSLGAPPRSLQFASYSFDASIGDIFTTLAVGGC
Q01886v2_A2_ala                          TGVPKGAVATHQAYATGIYEHAVACGMTSLGAPPRSLQFASYSFDASIGDIFTTLAVGGC
                                         ************************************************************

Q01886v1_A2_ala                          LCIPREEDRNPAGITTFINRYGVTWAGITPSLALHLDPDAVPTLKALCVAGEPLSMSVVT
Q01886v2_A2_ala                          LCIPREEDRNPAGITTFINRYGVTWAGITPSLALHLDPDAVPTLKALCVAGEPLSMSVVT
                                         ************************************************************

Q01886v1_A2_ala                          VWSKRLNLINMYGPTEATVACIANQVTCTTTTVSDIGRGYRATTWVVQPDNHNSLVPIGA
Q01886v2_A2_ala                          VWSKRLNLINMYGPTEATVACIANQVTCTTTTVSDIGRGYRATTWVVQPDNHNSLVPIGA
                                         ************************************************************

Q01886v1_A2_ala                          VGELIIEGSILCRGYLNDPERTAEVFIRSPSWLHDLRPNSTLYKTGDLVRYSADGKIIFI
Q01886v2_A2_ala                          VGELIIEGSILCRGYLNDPERTAEVFIRSPSWLHDLRPNSTLYKTGDLVRYSADGKIIFI
                                         ************************************************************

Q01886v1_A2_ala                          GRKDTQVKMNGQRFELGEVEHALQLQLDPSDGPIIVDLLKRTQSGEPDLLIAFLFVG
Q01886v2_A2_ala                          GRKDTQVKMNGQRFELGEVEHALQLQLDPSDGPIIVDLLKRTQSGEPDLLIAFLFVG
                                         *********************************************************
