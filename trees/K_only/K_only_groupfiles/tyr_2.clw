CLUSTAL W multiple sequence alignment


Q6YK40_A1_tyr                            VQHHLKNSLAFDGPVIDLNDETSYHADCSLLSPVAGHSHLAYVIYTSGTTGKPKGVMVEH
Q9R9J0_A1_tyr                            VQRHFKNSLVFDGPMIDLNDETSYHADCSLLSPIAEHSHLAYVIYTSGTTGKPKGVMVEH
Q93I55_A1_tyr                            VQHHLKNSLAFDGPVIDLNDETSYHADCSLLSPVAGHSHLAYVIYTSGTTGKPKGVMVEH
Q70JZ9_A1_tyr                            VQHHLKNSLAFDGPVIDLNDETSYHADCSLLSPVAGHSHLAYVIYTSGTTGKPKGVMVEH
                                         **:*:****.****:******************:* ************************

Q6YK40_A1_tyr                            SGIVNSLQWKKAFFKHSPADRVLVLYPYVFDAFILNFFGPLISGATLHLLPNEENKETFA
Q9R9J0_A1_tyr                            GGIVNSLQWKKAFFKHSAEDRVLVLYPYVFDAFILNFFGPLISGAALYLLPNEDNKDLFA
Q93I55_A1_tyr                            GGIVNSLQWKKAFFRHSPADRVLVLYPYVFDAFILNFFGPLISGATLHLLPNKENKETFA
Q70JZ9_A1_tyr                            GGIVNSLQWKKAFFKHSPADRVLVLYPYVFDAFILNFFGPLISGATLHLLPNEENKETFA
                                         .*************:**. **************************:*:****::**: **

Q6YK40_A1_tyr                            IQNAIKQERITHFSTSPRLLKTMIEQMNREDFIHVQHVVVGGEQLETDTVEKLHSLQPRI
Q9R9J0_A1_tyr                            IQNVLKLERITHFSTSPRLLQAMTEQMNAEDFYHVQHVVVGGEKLEPDTVERLFSLQPQI
Q93I55_A1_tyr                            IQNAIKQERITHFSTSPRLLKTMIEQMNREDFIHVQHVVVGGEQLEADTVEKLHSLQPRI
Q70JZ9_A1_tyr                            IQNAIKQERITHFSTSPRLLKTMIEQMNREDFIHVQHVVVGGEQLEADTVEKLHSLQPRI
                                         ***.:* *************::* **** *** **********:**.****:*.****:*

Q6YK40_A1_tyr                            RINNEYGPTENSVVSTFHPVQSADEQITIGSPVANHQAYILGAHHQIQPIGVPGELYVGG
Q9R9J0_A1_tyr                            RINNEYGPTENSVVSTFQPVYSADEQITIGKPVANHQAYILGAHRQIQPIGVPGELYVGG
Q93I55_A1_tyr                            RINNEYGPTENSVVSTFHPVQSADEQITIGSPVANHQAYILGAHHQIQPIGVPGELYVGG
Q70JZ9_A1_tyr                            RINNEYGPTENSVVSTFHPVQSADEQITIGSPVANHQAYILGAHHQIQPIGVPGELYVGG
                                         *****************:** *********.*************:***************

Q6YK40_A1_tyr                            AGVARGYLNRPELTEEKFVEHLHVPGQKMYKTGDLARWLPDGRIEYLGRIDHQVKIRGYR
Q9R9J0_A1_tyr                            SGVARGYLNQPDLTEEKFVDHLLIPRRKMYKTGDLARWLPDGRIEYLGRIDHQVKIRGYR
Q93I55_A1_tyr                            AGVARGYLNRPELTEEKFVEHLHVPGQKMYKTGDLARWLPDGRIEYLGRIDHQVKIRGYR
Q70JZ9_A1_tyr                            AGVARGYLNRPELTEEKFVEHLHVPGQKMYKTGDLARWLPDGRIEYLGRIDHQVKIRGYR
                                         :********:*:*******:** :* :*********************************

Q6YK40_A1_tyr                            IEIGEVEAAMFNLENVREAAVVAREDADGAKQLYAYYVG
Q9R9J0_A1_tyr                            IELGEVEAALSNLEEVRETTVESREGIDGTKQLYAYYVG
Q93I55_A1_tyr                            IEIGEVEAAMFNLENVREAAVVAREDADGAKQLYAYYVG
Q70JZ9_A1_tyr                            IEIGEVEAAMFNLENVREAAVVAREDADGAKQLYAYYVG
                                         **:******: ***:***::* :**. **:*********
