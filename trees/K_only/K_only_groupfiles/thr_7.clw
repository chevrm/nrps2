CLUSTAL W multiple sequence alignment


O87704_A2_4__thr                         TEHGHNKPADYHGQILYLNDAENELISPDLKAQETLADQPAYVIYTSGTTGQPKGVVVEH
P39846_A2_thr                            TERGLNKPADYTGHILYIDECENNSIPADVNIEEIVTDQPAYVIYTSGTTGQPKGVIVEH
                                         **:* ****** *:***:::.**: *..*:: :* ::*******************:***

O87704_A2_4__thr                         RNVISLLKHQDLPFDFGSEDVWTLFHSYCFDFSVWEMFGALLNGSTLVVVSRETARDPNA
P39846_A2_thr                            RNVISLLKHQNLPFEFNHEDVWTLFHSYCFDFSVWEMFGALLNGSTLVVVSKETARDPQA
                                         **********:***:*. *********************************:******:*

O87704_A2_4__thr                         FRLLLKNEGVTVLNQTPTAFYGLIHEEENHTDRLHVRYVIFGGEALQPGMLVTWNEKYPD
P39846_A2_thr                            FRLLLKKERVTVLNQTPTAFYGLMLEDQNHTDHLNIRYVIFGGEALQPGLLQSWNEKYPH
                                         ******:* **************: *::****:*::*************:* :******.

O87704_A2_4__thr                         TDLINMYGITETTVHVTYKKLSSADIEKNKSNIGKPLATLQAYVMDAHMNLQPTGVPGEL
P39846_A2_thr                            TDLINMYGITETTVHVTFKKLSAADIAKNKSNIGRPLSTLQAHVMDAHMNLQPTGVPGEL
                                         *****************:****:*** *******:**:****:*****************

O87704_A2_4__thr                         YIGGEGVARGYLNRDDLTAARFVPNPYLPGDRLYRTGDLAKRLASGDLEYMGRIDDQVKV
P39846_A2_thr                            YIGGEGVARGYLNRDELTADRFVSNPYLPGDRLYRTGDLAKRLSNGELEYLGRIDEQVKV
                                         ***************:*** ***.*******************:.*:***:****:****

O87704_A2_4__thr                         RGHRIELGEIQASLLQLPIIKEAAVITRDDEQGQSAVYAYLVA
P39846_A2_thr                            RGHRIELGEIQAALLQYPMIKEAAVITRADEQGQTAIYAYMVI
                                         ************:*** *:********* *****:*:***:* 
