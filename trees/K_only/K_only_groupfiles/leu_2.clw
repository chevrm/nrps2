CLUSTAL W multiple sequence alignment


P0C063_A4_leu                            TQSHVL-------NKLPVDIEWLDLTDEQNYVEDGTNLPFMNQSTDLAYIIYTSGTTGKP
P0C064_A4_5__leu                         TQSHVL-------NKLPVDIEWLDLTDEQNYVEDGTNLPFMNQSTDLAYIIYTSGTTGKP
O33743_A6_10__leu                        SQAHLLPLLAQVSSELP---ECLDLNAELDAGLSGSNLPAVNQPTDLAYVIYTSGTTGKP
                                         :*:*:*       .:**   * ***. * :   .*:*** :**.*****:**********

P0C063_A4_leu                            KGVMIEHQSIINCLQWRKEEYEFGPGDTALQVFSFAFDGFVASLFAPILAGATSVLPKEE
P0C064_A4_5__leu                         KGVMIEHQSIINCLQWRKEEYEFGPGDTALQVFSFAFDGFVASLFAPILAGATSVLPKEE
O33743_A6_10__leu                        KGVMIPHQGIVNCLQWRRDEYGFGPSDKALQVFSFAFDGFVASLFAPLLGGATCVLPQEA
                                         ***** **.*:******::** ***.*.*******************:*.***.***:* 

P0C063_A4_leu                            EAKDPVALKKLIASEEITHYYGVPSLFSAILDVSSSKDLQNLRCVTLGGEKLPAQIVKKI
P0C064_A4_5__leu                         EAKDPVALKKLIASEEITHYYGVPSLFSAILDVSSSKDLQNLRCVTLGGEKLPAQIVKKI
O33743_A6_10__leu                        AAKDPVALKKLMAATEVTHYYGVPSLFQAILDCSTTTDFNQLRCVTLGGEKLPVQLVQKT
                                          **********:*: *:**********.**** *::.*:::************.*:*:* 

P0C063_A4_leu                            KEKNKEIEVNNEYGPTENSVVTTIMRDIQVEQEITIGRPLSNVDVYIVNCNHQLQPVGVV
P0C064_A4_5__leu                         KEKNKEIEVNNEYGPTENSVVTTIMRDIQVEQEITIGCPLSNVDVYIVNCNHQLQPVGVV
O33743_A6_10__leu                        KEKHPAIEINNEYGPTENSVVTTISRSIEAGQAITIGRPLANVQVYIVDEQHHLQPIGVV
                                         ***:  **:*************** *.*:. * **** **:**:****: :*:***:***

P0C063_A4_leu                            GELCIGGQGLARGYLNKPELTADKFVVNPFVPGERMYKTGDLAKWRSDGMIEYVGRVDEQ
P0C064_A4_5__leu                         GELCIGGQGLARGYLNKPELTADKFVVNPFVPGERMYKTGDLAKWRSDGMIEYVGRVDEQ
O33743_A6_10__leu                        GELCIGGAGLARGYLNKPELTAEKFVANPFRPGERMYKTGDLVKWRTDGTIEYIGRADEQ
                                         ******* **************:***.*** ***********.***:** ***:**.***

P0C063_A4_leu                            VKVRGYRIELGEIESAILEYEKIKEAVVMVSEHTASE-QMLCAYIVG
P0C064_A4_5__leu                         VKVRGYRIELGEIESAILEYEKIKEAVVIVSEHTASE-QMLCAYIVG
O33743_A6_10__leu                        VKVRGYRIEIGEIESAVLAYQGIDQAVVVARDDDATAGSYLCAYFVA
                                         *********:******:* *: *.:***:. :. *:  . ****:*.
