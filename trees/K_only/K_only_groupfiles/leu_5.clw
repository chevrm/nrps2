CLUSTAL W multiple sequence alignment


Q84BQ5_A2_4__leu                         AQASTRDLLG--GVQVIDLDNDLWQHLSDANPQVPALTPKHLAYVIYTSGSTGQPKGVMV
Q84BQ6v2_A1_leu                          TQRALRGHLPALSVPVIELDQPSWSAGRVDAPKVPGLTPANLAYVIYTSGSTGLPKGVMV
Q84BQ6v1_A1_leu                          TQRALRGHLPALSVPVIELDQPSWSAGRVDAPKVPGLTPANLAYVIYTSGSTGLPKGVMV
Q84BQ4_A1_7__leu                         TQSHLRERLPALNVPVLDLDHCNWPLTVTQNPQVPGLSTANLAYVIYTSGSTGLPKGVMV
Q84BQ5_A3_5__leu                         AQGATRELLG--RVPVIDLDNALWQHLPETNPQLPTLTPAHLAYVIYTSGSTGQPKGVVV
                                         :*   *  *    * *::**:  *       *::* *:. :************ ****:*

Q84BQ5_A2_4__leu                         EHATLENLVHWHAEAFDLQAGSQTASVAGFGFDAMAWEVWPALCVGATLHLPPPSIRNEH
Q84BQ6v2_A1_leu                          EHRTLCNLVDWHADVFDLHAGSHTSSLAGFGFDAMAWEVWPALCVGATLHLAPASEGSED
Q84BQ6v1_A1_leu                          EHRTLCNLVDWHADVFDLHAGSHTSSLAGFGFDAMAWEVWPALCVGATLHLAPASEGSED
Q84BQ4_A1_7__leu                         EHHTLSNLVDWHCTAFDLCAGRHTSSLAGFGFDAMAWEVWPALCAGATLHLAPTHEGGED
Q84BQ5_A3_5__leu                         EHATLENLVHWHCEAFDLRAGSHTASVAGCGFDAMAWEVWPALCVGATLHLPPPSISNEH
                                         ** ** ***.**. .*** ** :*:*:** **************.******.*.   .*.

Q84BQ5_A2_4__leu                         LDEMLDWWCAQPLQVSFLPTPVAEYAFSRQLQHPTLRTLLIGGDRLRQFSRQQTFEVINN
Q84BQ6v2_A1_leu                          IDALLDWWRAQPLDVSFLPTPIAEYAFSRQLDHPTLRTLLIGGDRLRQFPRQPSFEVINN
Q84BQ6v1_A1_leu                          IDALLDWWRAQPLDVSFLPTPIAEYAFSRQLDHPTLRTLLIGGDRLRQFPRQPSFEVINN
Q84BQ4_A1_7__leu                         IDALLAWWCAQPLDVSFLPTPVAEYAFSQNLEHPTLRTLLIGGDRLRQFNRNQHFDVINN
Q84BQ5_A3_5__leu                         LDELLDWWRAQPLQVSFLPTPIAEYAFSRELGHPTLRTLLIGGDKLRQFSRGQTFDVINN
                                         :* :* ** ****:*******:******::* ************:**** *   *:****

Q84BQ5_A2_4__leu                         YGPTEATVVATSGPVELDQPLHIGRPIANVRIYVLDAEQRPVPVGVAGELYVGGAGVARG
Q84BQ6v2_A1_leu                          YGPTEATVVATSGRIDAGQALHIGKPVSNATVYLLDEQQQPVPLGVTGELYVGGAGVARG
Q84BQ6v1_A1_leu                          YGPTEATVVATSGRIDAGQALHIGKPVSNATVYLLDEQQQPVPLGVTGELYVGGAGVARG
Q84BQ4_A1_7__leu                         YGPTEATVVATSGLVQSGDALHIGKPLSNATVYLLDEQQRPVPLGVAGELYVGGAGVARG
Q84BQ5_A3_5__leu                         YGPTEATVVATSGSIHAGQLLHIGRPISNARIYLLDEQQRPVPVGVAGELYVGGAGVARG
                                         ************* :. .: ****:*::*. :*:** :*:***:**:*************

Q84BQ5_A2_4__leu                         YLNRPELSAAQFLFDPFSDEPQARMYRTGDQARWLADGNIEYLGRNDDQVKLRGVRIELG
Q84BQ6v2_A1_leu                          YLNRAELSAERFLRDPFSAEPGARLYRTGDLARWRADGNLEYLGRNDDQVKIRGIRIEPG
Q84BQ6v1_A1_leu                          YLNRAELSAERFLRDPFSAEPGARLYRTGDLARWRADGNLEYLGRNDDQVKIRGIRIEPG
Q84BQ4_A1_7__leu                         YLNRPDMTAERFLRDPFSRAPNARMYRTGDLARWREDGNIEYLGRNDDQVKIRGVRIELG
Q84BQ5_A3_5__leu                         YLNRPELTAERFLEDPFSDRPQARMYRTGDLARWLADGNIEYLGRNDDQVKLRGVRVELG
                                         ****.:::* :** ****  * **:***** ***  ***:***********:**:*:* *

Q84BQ5_A2_4__leu                         EIEAALGSHVALQEAVVLVRDGR-----LIAWFTE
Q84BQ6v2_A1_leu                          EIESALASHSAVREAVVLVRDGQ-----LLAWFTE
Q84BQ6v1_A1_leu                          EIESALASHSAVREAVVLVRDGQ-----LLAWFTE
Q84BQ4_A1_7__leu                         EIETCLNQLPGIQEAVLLAREDQPGQPRLVAYFTE
Q84BQ5_A3_5__leu                         EIEAALSSHPAVQDAVVLVREAR-----LIAWFTA
                                         ***:.* .  .:::**:*.*: :     *:*:** 
