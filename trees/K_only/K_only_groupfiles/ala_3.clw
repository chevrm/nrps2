CLUSTAL W multiple sequence alignment


Q2XNF8_A2_3__ala                         GRASLGADAAGARDLLDLDAADPAWSDRSAADPEPAGLSARNLAYVIYTSGSTGTPKGVQ
Q2XNF8_A1_2__ala                         ------ENALLAHAVVDLDERRPVWAGASTADPRPAGLSPRHLACAIYPAGATAAPAGAQ
                                                :*  *: ::***   *.*:. *:***.*****.*:** .**.:*:*.:* *.*

Q2XNF8_A2_3__ala                         NEHRGVVNRLAWMPEEYRLGAGDTVLQKTSFGFDVSVWEFFWPLLHGATLALAPPDAHKD
Q2XNF8_A1_2__ala                         NEHRALVDRLRWMQDAYRLGAGETVVQQTSLAADAALWELFWTWSNGATVVLAPAGAQST
                                         ****.:*:** ** : ******:**:*:**:. *.::**:**.  :***:.***..*:. 

Q2XNF8_A2_3__ala                         PAALIELIVRHRVTTVHFVPSMLAVFLQADGVERCAGLRRVICSGEALPGASVRLLHKRL
Q2XNF8_A1_2__ala                         PAALVELFVRHDIATAHFAPAALAAFLRADGVERCVGLRRLICSGEALSGASLRLAQQRL
                                         ****:**:*** ::*.**.*: **.**:*******.****:*******.***:** ::**

Q2XNF8_A2_3__ala                         PQTAIHNLYGPTEAAIEATAWTCPRDFAGDTVPIGRPIANARIYLLDPRRQPVPLGAVGE
Q2XNF8_A1_2__ala                         PWAAIHRLYGPAETAMDATAWTCPPDFAGDRAPIGRPIANTRVYLLDRHRRPLPPGAVGE
                                         * :***.****:*:*::******* ***** .********:*:**** :*:*:* *****

Q2XNF8_A2_3__ala                         LYIGGVGVARGYLNRPELTDERFLPDPFAADPDARMYRSGDLARHLAGGDIEFLGRNDHQ
Q2XNF8_A1_2__ala                         LHIGGAGIGRGYLNRPELTAERFVPDPFAADPAARMYRSGERARYRPDGELECLGRSDRQ
                                         *:***.*:.********** ***:******** *******: **: ..*::* ***.*:*

Q2XNF8_A2_3__ala                         VKLRGFRIELGEIETRLAAHAEVRETVVLALGEGSLMRLVAYVVA
Q2XNF8_A1_2__ala                         LRLRGLRIEPGDIEARLAEHPAVQRAVVLAPGDGERKRLVAYLVV
                                         ::***:*** *:**:*** *. *:.:**** *:*.  *****:*.
