CLUSTAL W multiple sequence alignment


Q9Z4X6_A2_athr|thr                       TDRATAGRLPAHE-VPRIVLDAPAPADGGTTGGDPADAHPATDLAQGERVRPLDPRDTAY
Q54959_A1_athr|thr                       --------------APVLVLDPQAMTEDLAG-------YPDTAPRTA-----VDGAHPAY
removed_A1_2__athr|thr                   TTTETEAKLPDRHTAPALRLDDPETLAALAG-------QPANSPAVG-----LRPDHPAY
P45745_A2_3__athr|thr                    TNTKAANHIPPVENVPKIVLDDPELAEKLNT-------YPAGNPKNKDRTQPLSPLNTAY
                                                       .* : **                  *            :   ..**

Q9Z4X6_A2_athr|thr                       VIYTSGSTGRPKGVAVPHGNVVRLFSATAPWFGFDEHDVWTLFHSYAFDFSVWELWGPLL
Q54959_A1_athr|thr                       VIYTSGSTGRPKGVVIPHSNVVRLFTSTDHWFGFGPDDVWTLFHSYAFDFSVWEIWGALL
removed_A1_2__athr|thr                   VIYTSGSTGVPKGVVNTHRNVVRLFDATRPWFDFGPDDVWTLFHSYAFDFSVWELWGALL
P45745_A2_3__athr|thr                    VIYTSGSTGVPKGVMIPHQNVTRLFAATEHWFRFSSGDIWTMFHSYAFDFSVWEIWGPLL
                                         ********* ****  .* **.*** :*  ** *.  *:**:************:**.**

Q9Z4X6_A2_athr|thr                       HGGRLVVVPHDVTRDPAAFLALLARERVTVLNQTPSAFHQL--AAADREN-P---TELAL
Q54959_A1_athr|thr                       HGGRLVVVPYHVSRSPGDFLDLLAREKVTVLNQTPTAFHQL--DAADRARTA--APELAL
removed_A1_2__athr|thr                   HGGRLVVVPYDVSRSPHAFLDLLADQGVTVLNQTPSAFHQLAQAAADPGR-P--PRRLAL
P45745_A2_3__athr|thr                    HGGRLVIVPHHVSRSPEAFLRLLVKEGVTVLNQTPSAFYQF--MQAEREQ-PDLGQALSL
                                         ******:**:.*:*.*  ** **. : ********:**:*:    *:  . .     *:*

Q9Z4X6_A2_athr|thr                       RTVVFGGEALDLSRLADWYERHAEDAPALVNMYGITETTVHVSHFALDRATAAASSASTI
Q54959_A1_athr|thr                       RYVVFGGEALDVARLADWYARRGT-AARLVNMYGITETTVHVTHAPLGPGHAVPGTPSLL
removed_A1_2__athr|thr                   RTVVFGGEALQPARLAEWYRRHPEDTPQLVNMYGITETTVHVTHQPLTRDRAAAGAASVI
P45745_A2_3__athr|thr                    RYVIFGGEALELSRLEDWYNRHPENRPQLINMYGITETTVHVSYIELDRSMAALRANSLI
                                         * *:******: :** :** *:    . *:************::  *    *.  : * :

Q9Z4X6_A2_athr|thr                       GVNIPDLRVYVLDDRLRPTAPGVTGEMYVAGAGLARGYLGRPALTADRFPADPYAALFGE
Q54959_A1_athr|thr                       GGPIPDLTPRVLDAALRPVPPGFTGELYVAGAGLARGYLNRPALTAQRFPADP----YGA
removed_A1_2__athr|thr                   GAGISDLRTHVLDGGLQLVPPGAVGELYVAGPGLARGYLGRPALTAERFVADP----YGA
P45745_A2_3__athr|thr                    GCGIPDLGVYVLDERLQPVPPGVAGELYVSGAGLARGYLGRPGLTSERFIADP----FGP
                                         *  *.**   ***  *: ..** .**:**:*.*******.**.**::** ***    :* 

Q9Z4X6_A2_athr|thr                       RGTRMYRTGDLARRRTDGGLDYLGRADQQVKIRGFRIEPGEIEAVLAAHPAVDDVAVVAR
Q54959_A1_athr|thr                       PGTRMYRTGDLVRHLDDGTYAYLGRGDDQVKIRGFRIELGEIENVLATHPGVAQAAAVVR
removed_A1_2__athr|thr                   PGARMYRTGDLVRRNPDGELEFVGRADHQVKVRGFRIELGEVEAALLAHPDVEQATVIVR
P45745_A2_3__athr|thr                    PGTRMYRTGDVARLRADGSLDYVGRADHQVKIRGFRIELGEIEAALVQHPQLEDAAVIVR
                                          *:*******:.*   **   ::**.*.***:****** **:* .*  ** : :.:.:.*

Q9Z4X6_A2_athr|thr                       EDVQGDPRLVAYVVT
Q54959_A1_athr|thr                       EDRHGDLRLAAYAVP
removed_A1_2__athr|thr                   EDRPGDTRLVAYVVG
P45745_A2_3__athr|thr                    EDQPGDKRLAAYVIP
                                         **  ** **.**.: 
