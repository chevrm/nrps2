CLUSTAL W multiple sequence alignment


Q70K00_A1_ser                            QNQSQERVPFTGKVLDMKDPQNFCEDGSNVEPAAGPDHLAYVIYTSGSTGKPKGVMVEHR
Q93I54v2_A2_ser                          QSHLRKSVPFTGKVLDLEDPRFSWEDGSNLKQTAGPNHLAYVIYTSGSTGRPKGVMVEHR
Q6YK39_A1_ser                            QNQSQERVPFTGKVLDMKNPQNFCEDGSNVEPAAGPDHLAYVIYTSGSTGKPKGVMVEHR
Q9R9I9_A1_ser                            QHHLREKVPFTGKVLDMEDPQTFSEDGSNLESISGPNQLAYVIYTSGSTGKPKGVMVEHR
Q93I54v1_A2_ser                          QSHLRKSVPFTGKVLDLEDPRFSWEDGSNLKQTAGPNHLAYVIYTSGSTGRPKGVMVEHR
                                         * : :: *********:::*:   *****::  :**::************:*********

Q70K00_A1_ser                            SVINRLVWMQEKYPLDERDAILQKTAITFDVSVWELFWWTISGSRLVLLPNGGEKNPELI
Q93I54v2_A2_ser                          SVINRLVWMQEHYPLDKQDVILQKTPITFDVSVWELFWWSMTGSKAVLLSNGGEKNPDVI
Q6YK39_A1_ser                            SVINRLVWMQEKYPLDERDAILQKTAITFDVSVWELFWWTISGSRLVLLPNGGEKNPELI
Q9R9I9_A1_ser                            SVINRLVWMQENYPLDERDAILQKTAITFDVSVWELFWWSIVGSKVVLLPNGGEKNPELI
Q93I54v1_A2_ser                          SVINRLVWMQEHYPLDKQDVILQKTPITFDVSVWELFWWSMTGSKAVLLSNGGEKNPDVI
                                         ***********:****::*.*****.*************:: **: ***.*******::*

Q70K00_A1_ser                            LDTIAQKGVSTMHFVPAMLHAFLESMDQKPSGMLKQKLASLRHVFASGEALKPVHVAGFK
Q93I54v2_A2_ser                          LDTIAQKNVTIMHFVPAMLHAFLESMEQKSDEELKRKLASLKYVFASGEALTPAHVAGFH
Q6YK39_A1_ser                            LDTIAQKGVSTMHFVPAMLHAFLESMDQKPSGMLKQKLASLRHVFASGEALKPVHVAGFK
Q9R9I9_A1_ser                            LDTIEQKGVSTLHFVPAMLHAFLESMEQTPSGKLKRKLASLRYVFASGEALTPKHVDGFQ
Q93I54v1_A2_ser                          LDTIAQKNVTIMHFVPAMLHAFLESMEQKSDEELKRKLASLKYVFASGEALTPAHVAGFH
                                         **** **.*: :**************:*...  **:*****::********.* ** **:

Q70K00_A1_ser                            RIITSVSQAQIINLYGPTEATIDVSYFDCQTEETYASIPIGKPISNIQLYILHADLEHMQ
Q93I54v2_A2_ser                          RMITPAGEAQIINLYGPTEATIDVSYFECEAGETLNSVPIGKPISNIQLYIVQPGSEYFQ
Q6YK39_A1_ser                            HIITSVSQAQIINLYGPTEATIDVSYFDCQTEETYASIPIGKPISNIQLYILHADLEHMQ
Q9R9I9_A1_ser                            RIITPVSHAQIINLYGPTEATIDVSYFECEADKRYNSVPIGKPISNIQLYILQAG--YMQ
Q93I54v1_A2_ser                          RMITPAGEAQIINLYGPTEATIDVSYFECEAGETLNSVPIGKPISNIQLYIVQPGSEYFQ
                                         ::**....*******************:*:: :   *:*************::..  ::*

Q70K00_A1_ser                            PIGVAGELCIAGDGLARGYLNRPELTAEKFVNHPIASGERIYRTGDLARWLPDGNIEYLG
Q93I54v2_A2_ser                          PLGVAGELCIAGDGLARGYLNRPELTAEKFAAHPFEAGKRMYRTGDLARWLPDGNIEYLG
Q6YK39_A1_ser                            PIGVAGELCIAGDGLARGYLNRPELTAEKFVNHPIASGERIYRTGDLARWLPDGNIEYLG
Q9R9I9_A1_ser                            PVGVAGELCIAGDGLARGYLNRPELTAEKFVKNPFSAGERMYRTGDLARWLPDGNIEYLG
Q93I54v1_A2_ser                          PLGVAGELCIAGDGLARGYLNRPELTAEKFAAHPFEAGKRMYRTGDLARWLPDGNIEYLG
                                         *:****************************. :*: :*:*:*******************

Q70K00_A1_ser                            RIDHQVKIRGYRIEIGEVEGAFFQLPAIKEAIIIAREIDGETSLCAYYTA
Q93I54v2_A2_ser                          RIDHQVKIRGYRIELGEIEASLLQLDAVKEAVVIAIEKEGSKQLCAYLSG
Q6YK39_A1_ser                            RIDHQVKIRGYRIEIGEVEGAFFQLPVIKEAIVIAREIDGETSLCAYYTA
Q9R9I9_A1_ser                            RIDHQVKIRGYRIETGEVEAALFHIPSIQESIVLAQEINEEISLCAYYTA
Q93I54v1_A2_ser                          RIDHQVKIRGYRIELGEIEASLLQLDAVKEAVVIAIEKEGSKQLCAYLSG
                                         ************** **:*.:::::  ::*::::* * : . .**** :.
