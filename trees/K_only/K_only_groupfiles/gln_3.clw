CLUSTAL W multiple sequence alignment


Q8NJX1_A17_gln                           ASSDAVASCAGMAEHVVELSPSVMARLAT--SVTLKILPKVGPRNTAYILFTSGSTGKPK
Q8NJX1_A6_gln                            VSPSSSVPCEGLTSIMVEFTIELLEQLSSRYDAFQEILPKAEPSNAAYVLFTSGSTGKPK
                                         .*..: ..* *::. :**:: .:: :*::  ..  :****. * *:**:***********

Q8NJX1_A17_gln                           GVVMQHGSFSSTTIGYGKVYNLSPLSRIFQFSNYIFDGSLGEIFGPLAFGGTICIPSDDE
Q8NJX1_A6_gln                            GVLMEHSAFATSTLGHGGIYNLSPASRVFQFSNYIFDGSLGEIFTTLSFGGTVCVPSEDE
                                         **:*:*.:*:::*:*:* :***** **:**************** .*:****:*:**:**

Q8NJX1_A17_gln                           RLQCAPDFMHRAKVNTAMLTPSFVRTFTPDKVPHLKTLVLGGEAASKSTLEMWVDRVTLF
Q8NJX1_A6_gln                            RLQKAPSFMREARVNTAMLTPSFVRTFAPEQVPSLRLLVLGGEPSSKDLLETWCGRLRLV
                                         *** **.**:.*:**************:*::** *: ******.:**. ** * .*: *.

Q8NJX1_A17_gln                           NGYGPAEACNYATTHIFKSSAESPRLIGSSFNGACWVVEPSNHNKLTPIGCTGELVLQGH
Q8NJX1_A6_gln                            NGYGPAEACNYATTHDFKPT-DSPHTIGRGFNSACWIVDPTDYNKLTPIGCIGELIIQGN
                                         *************** **.: :**: ** .**.***:*:*:::******** ***::**:

Q8NJX1_A17_gln                           ALARGYLNDKMKTEESFVCEIGSLPSSLLHEPKRFYLTGDLVRYNSNGELEYLGRKDSQV
Q8NJX1_A6_gln                            ALARGYINDADRTKNSFITNVDCLPKSIISGPHRFYLTGDLVRYTPDGQLEYLGRKDTQV
                                         ******:**  :*::**: ::..**.*::  *:***********..:*:********:**

Q8NJX1_A17_gln                           KLRGQRLELGEIEYNITQSLSSVRHVAVDVMHRQAGDSLVAFISFS
Q8NJX1_A6_gln                            KLRGQRLELGEIEYHVKKSLANIEHVAVDVAHRETGDTLIAFVSFK
                                         **************::.:**:.:.****** **::**:*:**:**.
