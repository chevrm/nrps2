CLUSTAL W multiple sequence alignment


Q9Z5F4_A1_gly                            RQASRDKVAAIAG----ASCKVCVLEDVKAG-------ATSA---PAGT----SPNGLAY
P45745_A1_gly                            --TTEEIAASLPDD---LAVPELVLDQAVTQEIIK---RYS----PENQDVSVSLDHPAY
P45745_A1_2__gly                         --TTEEIAASLPDD---LAVPELVLDQAVTQEIIK---RYS----PENQDVSVSLDHPAY
Q50E73_A5_10__gly                        --TTEDISARIP------GGSHVVLDSEQVTGELHDHPATS----PAGRGNPAGP---AY
Q5DIS9_A3_7__gly                         ---QSWLSPGLPLA---KGVAAL---DLDRTGWLDGYPSNDPLR-PLA------AENLAY
Q50858_A1_gly                            ---------RRELAHHGEGIPHVV--SLPEAGLVGEGDAN--LE-PVAD-----AAQLAY
Q45R83_A3_10__gly                        --TTTDLARRLPP----VPAPLLVLDDPATAARLAATTATALAEDPREQNGEWGEE-LAY
Q9L8H4_A2_5__gly                         --TDTRTEQHLPAD---ADTRRLALDSAEVRALLADCPDTD----PAEEGVTPAPGSAAY
Q5DIP4_A1_4__gly                         ---------RLPQS---DGLQSLLLDDLER--LVHGYPAENPDL-PEA------PDSLCY
                                                                   .                  *            .*

Q9Z5F4_A1_gly                            VIYTSGSTGRPKGVMIPHRGVVNFLLCMRRTLGLKRTDSLLAVTTYCFDIAALELLLPLC
P45745_A1_gly                            IIYTSGSTGRPKGVVVTQKSLSNFLLSMQEAFSLGEEDRLLAVTTVAFDISALELYLPLI
P45745_A1_2__gly                         IIYTSGSTGRPKGVVVTQKSLSNFLLSMQEAFSLGEEDRLLAVTTVAFDISALELYLPLI
Q50E73_A5_10__gly                        VIYTSGSTGQPKGVVVPSAALVNFLADMVPRLGLRGGDRLLSVTTVGFDIAALELFVPLL
Q5DIS9_A3_7__gly                         VIYTSGSTGRPKGVQIEHRSLLNFLASMAREPGCTSSDRLLQLTSLSFDIAGLELYLGLT
Q50858_A1_gly                            VLYTSGSSGRPKGVMVSHGALANFLTTMAREPGLRAEDVLAAVTTFSFDIAALELYLPLV
Q45R83_A3_10__gly                        TIYTSGSTGRPKGVMVTRSAVANFLADMNERLELGPGDRLLAVTTVSFDIAVLELLAPLL
Q9L8H4_A2_5__gly                         VIYTSGSTGRPKGVVVPHSALVNFVTAMRRQAPLRPQERLLAVTTVAFDIAALELYHPLL
Q5DIP4_A1_4__gly                         AIYTSGSTGQPKGVMVRHRALTNFVCSIARQPGMLARDRLLSVTTFSFDIFGLELYVPLA
                                          :*****:*:**** :   .: **:  :         : *  :*:  ***  ***   * 

Q9Z5F4_A1_gly                            AGAQVIIASAETVRDAQALKRALRTHRPTLMQATPATWTLLFQSGW-----ENAERVRIL
P45745_A1_gly                            SGAQIVIAKKETIREPQALAQMIENFDINIMQATPTLWHALVTSEP-----EKLRGLRVL
P45745_A1_2__gly                         SGAQIVIAKKETIREPQALAQMIENFDINIMQATPTLWHALVTSEP-----EKLRGLRVL
Q50E73_A5_10__gly                        SGATVVLADGETVRDPALARQTCEDHGVTMVQATPSWWHGMLADAG-----DSLRGVHAV
Q5DIS9_A3_7__gly                         RGACIVMPKAQQSKDPQALLALIEEAEVSIIQATPATWRMLLDGA--PERAEVLRGRKAL
Q50858_A1_gly                            QGARVVMATREQAADGRALSGVLARHGVTVMQATPATWRMLADAGGAPG-----TGFTVL
Q45R83_A3_10__gly                        TGGTVVLADATTQRDPAAVRSLCAREGVTVIQATPSWWHAMAVDGG-----LDLTALRVL
Q9L8H4_A2_5__gly                         SGAAVVLAPKEAVPQPSAVLDLIARHGVTTVQGTPSLWQLLVGHDA-----EALRGLRML
Q5DIP4_A1_4__gly                         RGASVLLASREQAQDPEALLDLVERQGVTVLQATPATWRMLCDS----ERVDLLRGCTLL
                                          *. :::.      :             . :*.**: *  :                  :

Q9Z5F4_A1_gly                            CGGEALPESLKAHFVRTASD-----VWNMFGPTETTIWSTMAKVS---------ASRPVT
P45745_A1_gly                            VGGEALPSGLLQELQDLHCS-----VTNLYGPTETTIWSAAAF------LEEGLKGVPP-
P45745_A1_2__gly                         VGGEALPSGLLQELQDLHCS-----VTNLYGPTETTIWSAAAF------LEEGLKGVPP-
Q50E73_A5_10__gly                        VGGEALSPGLRDALTRGARS-----VTNMYGPTETTIWSTSAG------QAAG-DSAPPS
Q5DIS9_A3_7__gly                         CGGEALGGGLARRLLAHVDS-----LWNVYGPTETTIWSSCQRVL---------DSEVIH
Q50858_A1_gly                            CGGEALPQDLADALTANGAR-----VWNLYGPTETTVWSCRKRLG---------AGDRVS
Q45R83_A3_10__gly                        VGGEALPPALARTLLEPGRAPLGDYLLNLYGPTETTVWSTVARITADSLEAHG-GAVP--
Q9L8H4_A2_5__gly                         VGGEALPLSLAEALRALTDD-----LVNLYGPTETTIWSTAAE------LAGG-TGAAP-
Q5DIP4_A1_4__gly                         CGGEALAEDLAVRMRGLSAS-----TWNLYGPTETTIWSARFRLG---------EEARPF
                                          *****   *   :             *::******:**                     

Q9Z5F4_A1_gly                            IGKPIDNTQVYVLDDRMQPVPIGVPGELWIAGAGVACGYLNRPALTAERFVSNPF--TPG
P45745_A1_gly                            IGKPIWNTQVYVLDNGLQPVPPGVVGELYIAGTGLARGYFHRPDLTAERFVADPYG-PPG
P45745_A1_2__gly                         IGKPIWNTQVYVLDNGLQPVPPGVVGELYIAGTGLARGYFHRPDLTAERFVADPYG-PPG
Q50E73_A5_10__gly                        IGTPILNTRVYVLDAALCVVPPGVAGELYIAGDGLARGYLGRAGLTAERFVACPFG-APG
Q5DIS9_A3_7__gly                         LGGPIGNTALHVLDDELEPMPAGGGGELLIGGGGLARGYFARPALTAERFVPNPFDAH-G
Q50858_A1_gly                            LGGALGNTSVHVLDPDLRPVPVGLAGELFIGGSGVARGYWGRPSITAERFVPDPFSARPG
Q45R83_A3_10__gly                        TGTPIARTAAYVLDAALRPVPDGVPGELYLAGAGLARGYLGRPGMTAERFVACPFG-EPG
Q9L8H4_A2_5__gly                         IGRPIANTRVYVLDDGLQPVAPGVVGELYIAGAGLARGYLDRPALTAERFPADPYGLEPG
Q5DIP4_A1_4__gly                         LGGPLENTALYILDSEMNPCPPGVAGELLIGGDGLARGYHRRPGLTAERFLPDPFAAD-G
                                          * .: .*  ::**  :   . *  *** :.* *:* **  *. :***** . *:    *

Q9Z5F4_A1_gly                            TTLYRTGDLARWRADGEVEYLGRLDHQVKVRGFRIEMGEIEAQLAGHPSVKNCAVVAKE-
P45745_A1_gly                            TRMYRTGDQARWRADGSLDYIGRADHQIKIRGFRIELGEIDAVLANHPHIEQAAVVVRED
P45745_A1_2__gly                         TRMYRTGDQARWRADGSLDYIGRADHQIKIRGFRIELGEIDAVLANHPHIEQAAVVVRED
Q50E73_A5_10__gly                        ERMYRTGDLVRWRVDGALEFVGRADDQVKVRGFRVELGEVEGAVAAHPDVVRAVVVVRED
Q5DIS9_A3_7__gly                         GRLYRTGDLASRRSDGVIDYLGRVDHQVKIRGFRIELGEIEARLQEQGLVREAVVLAQEG
Q50858_A1_gly                            ARLYRTGDLVRRRVDGELEFIGRADHQVKLRGYRIELAEIELTLRRHEAVRDVVALVWGS
Q45R83_A3_10__gly                        ERMYRTGDLARWRADGNLEHLGRTDDQVKVRGFRIELGEVERALTQAHGVGRAAAAVHPD
Q9L8H4_A2_5__gly                         GRMYRTGDLVRWNPDGELEFVGRADHQVKVRGFRIEPGEIEKVLTDHPDIAQAAVVVRED
Q5DIP4_A1_4__gly                         SRLYRTGDLARYRADGVIEYLGRIDHQVKIRGFRIELGEIETRLLEQDSVREAVVVAQPG
                                           :***** .  . ** ::.:** *.*:*:**:*:* .*::  :     :   .. .   

Q9Z5F4_A1_gly                            LNGTSQLVAYCQP
P45745_A1_gly                            QPGDKRLAAYVVA
P45745_A1_2__gly                         QPGDKRLAAYVVA
Q50E73_A5_10__gly                        RPGDHRLVAYVTG
Q5DIS9_A3_7__gly                         AHGA-QLVGYVVP
Q50858_A1_gly                            SEAERRLIAYVVP
Q45R83_A3_10__gly                        AAGSARLVGYLVP
Q9L8H4_A2_5__gly                         QPGDARLVAYVVT
Q5DIP4_A1_4__gly                         VAGP-TLVAYLVP
                                           .   * .*   
