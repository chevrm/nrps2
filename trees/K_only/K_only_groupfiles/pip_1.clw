CLUSTAL W multiple sequence alignment


Q54298_A1_pip                            AQETYRTRFPDVRDIILPDDPGLENQP-ASPPDVTTDRNSLAYAIYTSGSTGRPKAVLMP
Q9KID8v1_A1_pip                          AHEVYRSRFPDVPHVVALDDPELDRQPDDTAPDVELDRDSLAYAIYTSGSTGRPKAVLMP
Q9ZGA6_A1_11__pip                        AHETYRHRFSGVPHVVTPDDPDLGRQP-DTAPHLTLGRDSLAYAIYTSGSTGKPKAVLMP
Q9KID8v2_A1_pip                          AHEVYRSRFPDVPHVVALDDPELDRQPDDTAPDVELDRDSLAYAIYTSGSTGRPKAVLMP
                                         *:*.** **..* .::  *** * .**  :.*.:  .*:*************:*******

Q54298_A1_pip                            GICVVNLLLWQERTMGREPASRTAQFITATFDYSVQEIFSALLGGTLVIPPDDIRLDPAR
Q9KID8v1_A1_pip                          GVSAVNLLLWQERTMGREPASRTVQFVTPTFDYSVQEIFSALLGGTLVIPPDEVRFDPPG
Q9ZGA6_A1_11__pip                        GVSAVNLLLWQERTMGREPASRTVQFVTATFDYSVQEIFSALLGGTLVIPPDEARFDPPG
Q9KID8v2_A1_pip                          GVSAVNLLLWQERTMGREPASRTVQFVTPTFDYSVQEIFSALLGGTLVIPPDEVRFDPPG
                                         *:..*******************.**:*.***********************: *:**. 

Q54298_A1_pip                            LAQWIDDSRITRIYAPTTVLRALVEHVDPHGHGLVDIASPLPGGESLVLDGKLRECALHR
Q9KID8v1_A1_pip                          LARWMDEQAITRIYAPTAVLRALIEHVDPHSDQLAALRHLCQGGEALILDARLRELCRHR
Q9ZGA6_A1_11__pip                        LARWMDEQAITRIYAPTAVLRALVEHVDPHSDQLSALRHLCQGGEALALDARLRELCRHR
Q9KID8v2_A1_pip                          LARWMDEQAITRIYAPTAVLRALIEHVDPHSDQLAALRHLCQGGEALILDARLRELCRHR
                                         **:*:*:. ********:*****:******.. *  :     ***:* **.:*** . **

Q54298_A1_pip                            PHLRVHNHYGPAESQLVTGYTLPEDVSAWPSTTPIGKPIDNTRIHLLDDALRPYPTAYAA
Q9KID8v1_A1_pip                          PHLRVHNHYGPAESQLITGYTLPADPDAWPATAPIGPPIDNTRIHLLDEAMRPVPDGMPG
Q9ZGA6_A1_11__pip                        PHLRVHNHYGPAESQLITGYTLPADPDTWPAAAPIGRPIDNTRIHLLDDALRPVPDGMPG
Q9KID8v2_A1_pip                          PHLRVHNHYGPAESQLITGYTLPADPDAWPATAPIGPPIDNTRIHLLDEAMRPVPDGMPG
                                         ****************:****** * .:**:::*** ***********:*:** * . ..

Q54298_A1_pip                            QVCISGIGLARGYLARPELTRQRFITEGTGSEPRMYLSGDLARRLPDGNLEFLGRIDDQV
Q9KID8v1_A1_pip                          QLCVAGVGLARGYLARPELTAERWVPGDAVGEERMYLTGDLARRAPDGDLEFLGRIDDQV
Q9ZGA6_A1_11__pip                        QLCVAGIGLARGYLARPELTAERWVSGGATGEERMYLTGDLARRAPGGDLDFLGRIDDQV
Q9KID8v2_A1_pip                          QLCVAGVGLARGYLARPELTAERWVPGDAVGEERMYLTGDLARRAPDGDLEFLGRIDDQV
                                         *:*::*:************* :*::. .: .* ****:****** *.*:*:*********

Q54298_A1_pip                            KIRGIRIELGEIETALSEHAAITQAAVTVREDDRGDKRLVAYVVP
Q9KID8v1_A1_pip                          KIRGIRVEPGEIESLLAEDARVTQAAVSVREDRRGEKFLAAYVVP
Q9ZGA6_A1_11__pip                        KIRGIRIEPGEVENVLAEDARVAHAAVSVREDPRGEKFLAAYVVP
Q9KID8v2_A1_pip                          KIRGIRVEPGEIESLLAEDARVTQAAVSVREDRRGEKFLAAYVVP
                                         ******:* **:*. *:*.* :::***:**** **:* *.*****
