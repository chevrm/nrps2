CLUSTAL W multiple sequence alignment


Q9FB23_A2_asn                            VLPAGLDTPLRACGLPVVAPDDLGAPIAP--VSVHPEQLAAVMATSGSTGTPKTIGVPQR
Q9FB27_A1_asn                            VTPVALDADRRRIA---------AHPAGPTGIATTPDAPAYVVYTSGTTGKPNGVRVPHR
                                         * *..**:  *  .         . * .*  ::. *:  * *: ***:**.*: : **:*

Q9FB23_A2_asn                            ALAGYLRWAIGHYRLDEETVSPVHSSLGFDLTVTALLAPLAAGGQ-ARLTDSGDPGALGA
Q9FB27_A1_asn                            GLTNYLTWCTGAYGLDGGTGTLVHTSISFDLTLTTLFGPLLAGGQVVMLSETAGVTGLIA
                                         .*:.** *. * * **  * : **:*:.****:*:*:.** **** . *:::..  .* *

Q9FB23_A2_asn                            ALAAGHH-TLLKITPAHLAALAHQLGAP----TALRTVVAGGEPLHAGHVRALRAFAPGA
Q9FB27_A1_asn                            ALRSRRDLTLVKLTPTHL-DVVNQLLTPDELRGAVRTLVVGGEAVRAESLEPFR--ASGT
                                         ** : :. **:*:**:**  :.:** :*     *:**:*.***.::*  :..:*  *.*:

Q9FB23_A2_asn                            RLVNEYGPTETTVGCCAHDV-APDPGEAPIPVGTPIAGLSACVVDD-ALPAPPGVRGELY
Q9FB27_A1_asn                            RVVNEYGPSETVVGSVAHVVDAATPRTGPVPIGRPIANTTVHLLDQRRRPVPDGVVGELW
                                         *:******:**.**. ** * *. *  .*:*:* ***. :. ::*:   *.* ** ***:

Q9FB23_A2_asn                            IGGTGVTRGYLGRPAATAAAYVPDPAAP-GARRYRTGDLARRLPDGTLLLAGRADRQVKI
Q9FB27_A1_asn                            IGGAGVADGYLGRPELTGERFLPSDYPPDGGRVYRTGDLARRRADGTLEYLGRTDAQVKI
                                         ***:**: ******  *.  ::*.  .* *.* ********* .****   **:* ****

Q9FB23_A2_asn                            RGHRVEPGEVEQVLGGHPGVREAAVVAHPAPGGGRR--------LVAYWVP
Q9FB27_A1_asn                            RGVRVEPAETEAVLASHPGVGQAVVVARLDEDPGRSSPLAGELTLTGYVVP
                                         ** ****.*.* **..**** :*.***:   . **         *..* **
