CLUSTAL W multiple sequence alignment


Q83VS1_A3_val                            LTLSQMSLTTSTQRVDLDRLMLNGLDNDDLAL--AQSSESVAYIMYTSGSTGTPKGVLVP
Q83VS0_A1_11__val                        LTLSRHDLPDGIQRIDLDLL---ELQSDAPNPVHSASAESVAYIMYTSGSTGMPKGVLVP
Q83VS1_A2_val                            LTHSQVSLTTSAQRIDLDGLTLERLKGTDLAL-PPQSSESVAYIMYTSGSTGTPKGVLVP
Q83VS1_A2_7__val                         LTHSQVSLTTSAQRIDLDGLTLERLKGTDLAL-PPQSSESVAYIMYTSGSTGTPKGVLVP
Q83VS1_A3_8__val                         LTLSQMSLTTSTQRVDLDRLMLNGLDNDDLAL--AQSSESVAYIMYTSGSTGTPKGVLVP
Q9FDB3_A4_4__val                         LTYSTETLDPGTQRLDLDTRPASTVPADNPNP--EACADSVAYIMYTSGSTGTPKGVLVP
Q9FDB3_A3_3__val                         LTLSRMSLTASTQRIDLDGLTLDGLKDTDLTL--PQSSESVAYIMYTSGSTGVPKGVLVP
                                         ** *   *  . **:***      :           .::************* *******

Q83VS1_A3_val                            HRAISRLVINNGYADFNAQDRVAFASNPAFDASTLDVWAPLLNGGCVVVIGQHDLLSPLN
Q83VS0_A1_11__val                        HRAVSRLVLNNGYADFNAGDRVAFASNPAFDASTLDVWAPLLNGGCVVVVEQSVLLSLDE
Q83VS1_A2_val                            HRAISRLAINNGYADFNAQDRVAFASNPAFDASTLDVWAPLLNGGCVVVIGQHDLLSPLN
Q83VS1_A2_7__val                         HRAISRLAINNGYADFNAQDRVAFASNPAFDASTLDVWAPLLNGGCVVVIGQHDLLSPLN
Q83VS1_A3_8__val                         HRAISRLVINNGYADFNAQDRVAFASNPAFDASTLDVWAPLLNGGCVVVIGQHDLLSPLN
Q9FDB3_A4_4__val                         HRAISRLVIDNGYADFNGQDRVAFASNPAFDASTLDVWAPLLNGGCVIVVEQSVLLSQEA
Q9FDB3_A3_3__val                         HRAISRLVINNGYADFNAQDRVAFASNPAFDASTLDVWAPLLNGGCVVVIGQHDLLSPLN
                                         ***:***.::*******. ****************************:*: *  ***   

Q83VS1_A3_val                            FQRLLLEQSVTVLWMTAGLFHQYASGLGEAFSRLRYLIVGGDVLDPAVIARVLANNAPQH
Q83VS0_A1_11__val                        FRALLLSQSVSVLWMTAGLFHQYASGLMEALARLRYLIVGGDVLDPAVIARVLAEGAPQH
Q83VS1_A2_val                            FQRLLLEQSVSVLWMTAGLFHQYASGLGEAFSRLRYLIVGGDVLDPAVIGRVLANNPPQH
Q83VS1_A2_7__val                         FQRLLLEQSVSVLWMTAGLFHQYASGLGEAFSRLRYLIVGGDVLDPAVIGRVLANNPPQH
Q83VS1_A3_8__val                         FQRLLLEQSVTVLWMTAGLFHQYASGLGEAFSRLRYLIVGGDVLDPAVIARVLANNAPQH
Q9FDB3_A4_4__val                         FRALLLAQSISVLWLTAGLFHQYADGLMEVFAGLRYLIVGGDVLDPAVIARVLAQGAPQH
Q9FDB3_A3_3__val                         FQRLLLEQSVSVLWMTAGLFHQYATGLGEAFSRLRYLIVGGDVLDPAVIARVLANNAPQH
                                         *: *** **::***:********* ** *.:: ****************.****:..***

Q83VS1_A3_val                            LLNGYGPTEATTFSATYEIVSVDNGSIPIGKPVGNTRLYVLDSQGQPVPLGVPGELYIGG
Q83VS0_A1_11__val                        LLNGYGPTEATTFSTTHEITSVGSGGIPIGRPIGNSQVYVLDTLRQPVAVGVAGELYIGG
Q83VS1_A2_val                            LLNGYGPTEATTFSATYEIVSVGNGSIPIGKPVGNSRLYVLDNQGQPVPLGVPGELYIGG
Q83VS1_A2_7__val                         LLNGYGPTEATTFSATYEIVSVGNGSIPIGKPVGNSRLYVLDNQGQPVPLGVPGELYIGG
Q83VS1_A3_8__val                         LLNGYGPTEATTFSATYEIVSVDNGSIPIGKPVGNTRLYVLDSQGQPVPLGVPGELYIGG
Q9FDB3_A4_4__val                         LLNGYGPTEATTFSTTYEITAADHGGIPIGRPIANSQVYVLDALRQPVAVGVPGELYIGG
Q9FDB3_A3_3__val                         LLNGYGPTEATTFSATYEITSVDNGSIPIGKPVGNTRLYVLDSQGQPAPLGVAGELYIGG
                                         **************:*:**.:.. *.****:*:.*:::****   **..:**.*******

Q83VS1_A3_val                            QGVAKGYLNRDELSTTQFVVDPFSEQIDALMYRTGDLVRWRADGNLEYLGRNDDQVKIRG
Q83VS0_A1_11__val                        QGVAKGYLNRPELNATQFVANPFSDDAGALLYRTGDLGRWNADGIVEYLGRNDDQVKIRG
Q83VS1_A2_val                            QGVARGYLHRDELTLERFVADPFDSDPQARLYRTGDLVRWRADGNLEYLGRNDEQVKIRG
Q83VS1_A2_7__val                         QGVARGYLHRDELTLERFVADPFDSDPQARLYRTGDLVRWRADGNLEYLGRNDEQVKIRG
Q83VS1_A3_8__val                         QGVAKGYLNRDELSTTQFVVDPFSEQIDALMYRTGDLVRWRADGNLEYLGRNDDQVKIRG
Q9FDB3_A4_4__val                         QGVAKGYLNRDELSTTQFVADPFSGHDGALMYRTGDLGRWRADGNLEYLGRNDGQVKIRG
Q9FDB3_A3_3__val                         QGVARGYLHRDELTLEKFLADPFDSDPQARLYRTGDLVRWRADGNLEYLGRNDDQVKIRG
                                         ****:***:* **.  :*:.:**. .  * :****** **.*** :******* ******

Q83VS1_A3_val                            FRVELGEIEARLAEHADVREAVVLCRQDVPGDKRLVAYVTAQQSENALD
Q83VS0_A1_11__val                        FRIELGEIEARLVECPGVREAVVLARQDESAHKRLVAY-VVGEENSALS
Q83VS1_A2_val                            FRVELGEIEARLAEHAEVREAVVLCRQDAPGDKRLVAYVTAQQPETALD
Q83VS1_A2_7__val                         FRVELGEIEARLAEHAEVREAVVLCRQDAPGDKRLVAYVTAQQPETALD
Q83VS1_A3_8__val                         FRVELGEIEARLAEHADVREAVVLCRQDVPGDKRLVAYVTAQQSENALD
Q9FDB3_A4_4__val                         FRIELSEIEAALATHPAVHEAVLLARQD-TGEKRLVAYFTLREPQQTLE
Q9FDB3_A3_3__val                         FRVELGEIEARLAEHVDVREAVVLCRQDVPGDKRLVAYVTAQQAETALD
                                         **:**.**** *.    *:***:*.*** ...****** .  : : :*.
