CLUSTAL W multiple sequence alignment


P07702_A1_aad                            LDQLVEDYINDELEIVSRINSIAIQENGTIEGGKLDNG-EDVLAPYDHYKDTRTGVVVGP
P40976_A1_aad                            LSPTVVEYVEKSLELKTYVPALKLAKDGSLTGGSVSKGADDILQHVLHLKSEQTGVVVGP
                                         *.  * :*::..**: : : :: : ::*:: **.:.:* :*:*    * *. :*******

P07702_A1_aad                            DSNPTLSFTSGSEGIPKGVLGRHFSLAYYFNWMSKRFNLTENDKFTMLSGIAHDPIQRDM
P40976_A1_aad                            DSTPTLSFTSGSEGIPKGVKGRHFSLAYYFDWMAQEFNLSESDRFTMLSGIAHDPIQRDI
                                         **.**************** **********:**::.***:*.*:***************:

P07702_A1_aad                            FTPLFLGAQLYVPTQDDIGTPGRLAEWMSKYGCTVTHLTPAMGQLLTAQATTPFPKLHHA
P40976_A1_aad                            FTPLFLGASLIVPTAEDIGTPGQLAQWANKYKVTVTHLTPAMGQLLAAQADEPIPSLHHA
                                         ********.* *** :******:**:* .**  *************:***  *:*.****

P07702_A1_aad                            FFVGDILTKRDCLRLQTLAENCRIVNMYGTTETQRAVSYFEVKSKNDDPNFLKKLKDVMP
P40976_A1_aad                            FFVGDILTKRDCLRLQVLANNVNVVNMYGTTETQRSVSYFVVPARSQDQTFLESQKDVIP
                                         ****************.**:* .:***********:**** * ::.:* .**:. ***:*

P07702_A1_aad                            AGKGMLNVQLLVVNRNDRTQICGIGEIGEIYVRAGGLAEGYRGLPELNKEKFVNNWFVEK
P40976_A1_aad                            AGRGMKNVQLLVINRFDTNKICGIGEVGEIYLRAGGLAEGYLGNDELTSKKFLKSWFA--
                                         **:** ******:** * .:******:****:********* *  **..:**::.**.  

P07702_A1_aad                            DHWNYLDK-DNGEPWRQFWLGPRDRLYRTGDLGRYLPNGDCECCGRADDQVKIRGFRIEL
P40976_A1_aad                            DPSKFVDRTPENAPWKPYWFGIRDRMYRSGDLGRYLPTGNVECSGRADDQIKIRGFRIEL
                                         *  :::*:  :. **: :*:* ***:**:********.*: **.******:*********

P07702_A1_aad                            GEIDTHISQHPLVRENITLVRKNADNEPTLITFMVPRFDKPDDLSKFQSDVP
P40976_A1_aad                            GEINTHLSRHPNVRENITLVRRDKDEEPTLVAYIVPQGLNKDD---FDSAT-
                                         ***:**:*:** *********:: *:****::::**:  : **   *:* . 
