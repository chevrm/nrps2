CLUSTAL W multiple sequence alignment


O87314_A2_thr                            LTDT--AERFT---------GVPHVIL----AEAAQ-------NPARPQAPTVS-PDHAA
AEF33078.1_463_mod1_thr                  LTDTDGADRVR---------GAVHPLLLDDPALAAELTGLPGTDPTAAERPAAAGPDSLA
Q45R85_A4_thr                            LSTT--AVRETLHGTVGEAVGEVPWLLLDEPATGGATAGHSAAPVTDADRRSPLLPDHPA
                                         *: *  * *           *    :*    * ..          : .:  :   **  *

O87314_A2_thr                            YVIYTSGSTGVPKGVEVTHRNVAALFAGTTSGLYDFGPDDVWTMFHSAAFDFSVWELWGP
AEF33078.1_463_mod1_thr                  YVIYTSGSTGRPKGVAVTHHNVLRLFR-QTDHWFGFGADDVWTLFHSYAFDFSVWEIWGP
Q45R85_A4_thr                            YTIYTSGSTGRPKGVVVSHANVSRLLT-ACRAAVDFGPDDVWTLFHSSAFDFSVWEMWGP
                                         *.******** **** *:* **  *:        .**.*****:*** ********:***

O87314_A2_thr                            LLHGGRLVVVEHDVARDPERFVDLLARERVTVLNQTPSAFYPLLEADAR-----LRRQLA
AEF33078.1_463_mod1_thr                  LLHGGRLVVVPFDTSRSPADFLRLLVRERVTVLNQTPSAFYQLIAAERER--PDLAGRLT
Q45R85_A4_thr                            LAHGGRLVVVPHDVARSPGDLLDLLGRERVTVLSQTPSAFLQLLRAESDLGVP-PRTTAA
                                         * ******** .*.:*.*  :: ** *******.******  *: *:            :

O87314_A2_thr                            LRYVIFGGEALDVRRLAPWYANHESHSP-RLVNMYGITETCVHVSHRALDTADTGAA-GS
AEF33078.1_463_mod1_thr                  LRHVVFGGEALETHRLEDWYARHGDDAP-RLVNMYGITETTVHVTYAALDRETVRAARGS
Q45R85_A4_thr                            LRYVVFGGEALDTAQLAPWRGR-----PVRLVNMYGITETTVHVTHLELDDAAVDRG-GS
                                         **:*:******:. :*  * ..     * *********** ***::  **   .  . **

O87314_A2_thr                            VIGGPLPGLRIHLLDNNLQPVPAGVVGEMYIAGGQVARGYTGRPGLTATRFVANPFDGAG
AEF33078.1_463_mod1_thr                  VIGEPIPDLDVYVLDGELRPVPEGATGELYVAGPGLARGYLGRPGLTAERFVADPFGAPG
Q45R85_A4_thr                            PIGTPLNDLRAHVLDQGLLPVPVGVVGELYVAGPGLARGYRRRPGLSATRFVADPFD-TG
                                          ** *: .*  ::**  * *** *..**:*:**  :****  ****:* ****:**. .*

O87314_A2_thr                            ERLYRSGDLAMWTDAGELVYLGRSDAQVKVRGYRIELGEVEAALVTLPGVTNAAADVRHD
AEF33078.1_463_mod1_thr                  GRMYRTGDLACVLDDGSLGYLGRADSQVKIRGFRIELGEIEAALERHPGVERAAVVVRED
Q45R85_A4_thr                            GRMYRTGDLVRRTQDGGLHYVGRSDSQVKLRGYRIEPGEIEAAARRHPDVAQAATAVHGE
                                          *:**:***.   : * * *:**:*:***:**:*** **:***    *.* .**. *: :

O87314_A2_thr                            DTGRARLIGYVVGDAL------
AEF33078.1_463_mod1_thr                  RPGDQRLVAYAVPAAGTAPDTT
Q45R85_A4_thr                            GPQDRYLVCYVVPAADTDPDPH
                                          .    *: *.*  *       
