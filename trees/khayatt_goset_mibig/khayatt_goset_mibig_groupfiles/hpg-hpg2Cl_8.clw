CLUSTAL W multiple sequence alignment


Q70AZ7_A1_hpg|hpg2Cl                     VCTAAFRAAVLDGGLEAIVVDDPGTWPAVAPCPPVPTGPDDLAYVMYTSGSTGTPKGVAV
Q70AZ7_A1_4__hpg|hpg2Cl                  VCTAAFRAAVLDGGLEAIVVDDPGTWPAVAPCPPVPTGPDDLAYVMYTSGSTGTPKGVAV
                                         ************************************************************

Q70AZ7_A1_hpg|hpg2Cl                     SHGDVAALVGDPGWRTGPGDTVLMHASHAFDISLFEIWVPLLSGARVMIAGPGAVDGAAL
Q70AZ7_A1_4__hpg|hpg2Cl                  SHGDVAALVGDPGWRTGPGDTVLMHASHAFDISLFEIWVPLLSGARVMIAGPGAVDGAAL
                                         ************************************************************

Q70AZ7_A1_hpg|hpg2Cl                     AAQVAAGVTAAHLTAGAFRVLAEESPESVAGLREVLTGGDAVPLAAVERVRRACPDVRVR
Q70AZ7_A1_4__hpg|hpg2Cl                  AAQVAAGVTAAHLTAGAFRVLAEESPESVAGLREVLTGGDAVPLAAVERVRRACPDVRVR
                                         ************************************************************

Q70AZ7_A1_hpg|hpg2Cl                     HLYGPTETTLCATWWLLEPGDETGPVLPIGRPLAGRRVYVLDAFLRPLPPGTTGELYVAG
Q70AZ7_A1_4__hpg|hpg2Cl                  HLYGPTETTLCATWWLLEPGDETGPVLPIGRPLAGRRVYVLDAFLRPLPPGTTGELYVAG
                                         ************************************************************

Q70AZ7_A1_hpg|hpg2Cl                     AGVAQGYLGRPALTAERFVADPFAPGGRMYRTGDLAYWTEQGTLAFAGRADDQVKIRGYR
Q70AZ7_A1_4__hpg|hpg2Cl                  AGVAQGYLGRPALTAERFVADPFAPGGRMYRTGDLAYWTEQGTLAFAGRADDQVKIRGYR
                                         ************************************************************

Q70AZ7_A1_hpg|hpg2Cl                     VEPGEVEAVLGGLPGVAQAVVCVRGEHLIGYVVAEAGRDLDPER
Q70AZ7_A1_4__hpg|hpg2Cl                  VEPGEVEAVLGGLPGVAQAVVCVRGEHLIGYVVAEAGRDLDPER
                                         ********************************************
