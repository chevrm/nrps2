CLUSTAL W multiple sequence alignment


O05647_A1_pro                            ----------------LTEDD--VDEDLSGIPDGNLTDAERTAPLTPAHPAYVIYTSGST
Q9L8H4_A1_4__pro                         LTTTETEAKLPDRHPGLLLDDPAVLADLSGRPAHDP-----VVELHPDHPAYVIYTSGST
AEA30273.1_1527_mod2_pro                 LTTTDVAGRLP-AGPMLALDDPQVRAELAGLTPTDPRDDERTSPLRPQHPAYLIYTSGST
                                                         *  **  *  :*:* .  :      .  * * ****:*******

O05647_A1_pro                            GRPKAVVMPGAAVVNLLAWHRREIPAGAGTTVAQFASLSFDVAAQEILSTLLYGATLAVP
Q9L8H4_A1_4__pro                         GVPKGVVMPAGGLLNLLQWHHRAVGDEPGTRTAQFTAISFDVSAQEVLSSVAFGKTLVIP
AEA30273.1_1527_mod2_pro                 GRPKGVVLPSGALINLMAWHAATLPGEPGARVAQFTAISFDVSAQEILSALLDGKTLVMP
                                         * **.**:*...::**: **   :   .*: .***:::****:***:**::  * **.:*

O05647_A1_pro                            TDAVRRDADAFAAWLEEYRVNELYAPNLVVEALAEAAAEQGRTLPDLRHIAQAGEALTAG
Q9L8H4_A1_4__pro                         DEEVRRDAARFAGWLDDRQVDELFAPNLVLEALAEAAVETGRTLPQLRTVAQAGEALTLS
AEA30273.1_1527_mod2_pro                 EEDTRRDPARFAAWLAERDITELYAPTMMLDALGEAVAEHGTELPALRHVAQAGEALTLG
                                          : .***.  **.** :  : **:**.::::**.**..* *  ** ** :******** .

O05647_A1_pro                            PRVRDFCAALPGRRLHNHYGPAETHVMTGIELPVDPGG-WPERVPIGGPVDNARLYVLDG
Q9L8H4_A1_4__pro                         RTVRAFHRSAPGRRLHNHYGPTETHVVTAHALGDDPED-WRLPAPIGRPIDNTHAYVRTR
AEA30273.1_1527_mod2_pro                 GRVRDLC-DAPGRTLHNHYGPSETHVVTAHTLSDGPSDRARNAAPIGRPVWNTQVYVLDA
                                           ** :    *** *******:****:*.  *  .* .     .*** *: *:: **   

O05647_A1_pro                            FLRPVPPGVVGELYLAGAGVARGYLNRPGLTAERFVADPFG-GPGTRMYRTGDLARWAGS
Q9L8H4_A1_4__pro                         AVRLVEPGVVGELYIAGAGLARGYLGRPALTAERFVADPYGLEPGGRMYRTGDLVRRNPD
AEA30273.1_1527_mod2_pro                 ALRPVPDGVTGELYIAGAQLARGYHHRPGLTAERFTADPFG-APGGRMYRTGDLVRWNSD
                                          :* *  **.****:*** :****  **.******.***:*  ** ********.*   .

O05647_A1_pro                            GVLEFAGRADHQVKVRGFRIEPGEVESVLAAQPGVARAVVLAREDRPGERRLVAYLVAVP
Q9L8H4_A1_4__pro                         GELEFCGRADHQVKVRGFRIEPGEIEKVLTDHPDIAQAAVVTRPHRPGDTRLVAYVV-GR
AEA30273.1_1527_mod2_pro                 GELDYAGRADHQVKVRGFRIELGEIEAVLRTHPGVAQAAVVAQEDPRGGRRLVGYAVPAA
                                         * *::.*************** **:* **  :*.:*:*.*::: .  *  ***.* *   

O05647_A1_pro                            GSV-PDPGV
Q9L8H4_A1_4__pro                         EALRPEQ--
AEA30273.1_1527_mod2_pro                 GAVRPAE--
                                          :: *    
