CLUSTAL W multiple sequence alignment


Q8KLL4v2_A1_dpg|dhpg                     VCGAGTAAVVPSGHVLVTMDDAVDGETSLVPVGADDLAYVMYTSGSTGTPKGVAVAHGGV
Q70AZ8_A1_3__dpg|dhpg                    LCRESTAGAVPAGHELIDMDRVGEGVAAPVAIGADDLAYVMYTSGSTGKPKGVAVAHGGV
Q8KLL4v1_A1_dpg|dhpg                     VCGAGTAAVVPSGHVLVTMDDAVDGETSLVPVGADDLAYVMYTSGSTGTPKGVAVAHGGV
                                         :*  .**..**:** *: ** . :* :: *.:****************.***********

Q8KLL4v2_A1_dpg|dhpg                     AALAGDPGWGVGAGDTVLMHAPHTFDASLYDLWVPLVSGARVLLAEPGVVDAERLAAHVA
Q70AZ8_A1_3__dpg|dhpg                    AALAGDPVWEVGAGDAVLMHAPHTFDASLYDVWVPLVSGARVMLAEPGVVDAQRLAAHVA
Q8KLL4v1_A1_dpg|dhpg                     AALAGDPGWGVGAGDTVLMHAPHTFDASLYDLWVPLVSGARVLLAEPGVVDAERLAAHVA
                                         ******* * *****:***************:**********:*********:*******

Q8KLL4v2_A1_dpg|dhpg                     EGLTSVNFTAGQFRALVQESPESFSGLRDVLAGGDVVPLGEVERVRRACPELVVWHTYGP
Q70AZ8_A1_3__dpg|dhpg                    DGLTAVNFTAGQFRALAQESPESFAGLRDVLAGGDVVPLGAVERVRQACPGLRVWHTYGP
Q8KLL4v1_A1_dpg|dhpg                     EGLTSVNFTAGQFRALVQESPESFSGLRDVLAGGDVVPLGEVERVRRACPELVVWHTYGP
                                         :***:***********.*******:*************** *****:*** * *******

Q8KLL4v2_A1_dpg|dhpg                     TETTLCATWKAIEPSDRLGSVLPIGRPLPGRRLYVLDVFLRPLPPGVEGDLYIAGAGVAH
Q70AZ8_A1_3__dpg|dhpg                    TETTLCATWKKIEPGDRLGSTLPIGRPLPGRRLYVLDVFLRPLPPGVAGDLYIAGAGVAQ
Q8KLL4v1_A1_dpg|dhpg                     TETTLCATWKAIEPSDRLGSVLPIGRPLPGRRLYVLDVFLRPLPPGVEGDLYIAGAGVAH
                                         ********** ***.*****.************************** ***********:

Q8KLL4v2_A1_dpg|dhpg                     GYLDRPGLTAERFVADPFASGERMYRTGDVAYWTDEGELVYAGRADNQVKIRGYRVEPGE
Q70AZ8_A1_3__dpg|dhpg                    GYLGRPALTAERFVADPFAAGERMYRTGDVAYWTGDGELVFAGRADNQVKIRGYRVEPGE
Q8KLL4v1_A1_dpg|dhpg                     GYLDRPGLTAERFVADPFASGERMYRTGDVAYWTDEGELVYAGRADNQVKIRGYRVEPGE
                                         ***.**.************:**************.:****:*******************

Q8KLL4v2_A1_dpg|dhpg                     IEAVLAERPGVDQAVVVAREGRLIGYVVSGGDVDPEG
Q70AZ8_A1_3__dpg|dhpg                    IETVLAEQPGVDQAVVLARDGRLIGYVVPGGEVDPVR
Q8KLL4v1_A1_dpg|dhpg                     IEAVLAERPGVDQAVVVAREGRLIGYVVSGGDVDPEG
                                         **:****:********:**:********.**:***  
