CLUSTAL W multiple sequence alignment


Q50E73_A1_6__orn                         TTTTATPLP-HPGRTLVLDSPTTARALAAAPAHNLTDADRRTPLNARNAAYIIHTSGSTG
AHH53507.1_475_mod1_orn                  LTTTANHLPDHPAPTLALDDAPARAELLAAPTRTPRDVDRVVPAHPAHPAYVIYTSGSTG
                                          ****. ** **. **.**...:   * ***::.  *.** .* :. :.**:*:******

Q50E73_A1_6__orn                         RPKGVVIEHRSLANLFHDHRRALIEPHAAGGSRLKAGLTASLSFDTSWEGLICLAAGHEL
AHH53507.1_475_mod1_orn                  RPKGVVLTHEGLRNLYHDHATGLIEPHAPRTRRLRAGLSAALSFDTSWECLLSLAAGHEL
                                         ******: *..* **:***  .******.   **:***:*:******** *:.*******

Q50E73_A1_6__orn                         HLIDDDTRRDAERVAELIDRQRIDVIDVTPSFAQQLVETGILDEGRHHPAAFMLGGEGVD
AHH53507.1_475_mod1_orn                  HLVDDDTRRDAELLVRYVEHHRLDLLDLTPSHAQQLVETGLLAEDAHHPTVLMLGGEAVP
                                         **:********* :.. ::::*:*::*:***.********:* *. ***:.:*****.* 

Q50E73_A1_6__orn                         AKLWTRLSDVPGVTSYNYYGPTEFTVDALACTVGIAPRPVIGHPLDNTAAYILDGFLRPV
AHH53507.1_475_mod1_orn                  PALWSRLRAAPHTRCYNYYGPTEFTVDAVACALDESEHPIVGRPLDNTRAYVLNAALLPV
                                         . **:**  .* . .*************:**::. : :*::*:***** **:*:. * **

Q50E73_A1_6__orn                         PEGVAGELYLAGTQLARGYAGRPGLTAERFVACPFGAPGERMYRTGDLVRRSPGGVVEYL
AHH53507.1_475_mod1_orn                  APGVPGELYLAGPQLARGYLGRAALTADRFVACPFGTPGERMYRTGDLAAWTEHGTLRFL
                                         . **.*******.****** **..***:********:***********.  :  *.:.:*

Q50E73_A1_6__orn                         GRVDDQIKLRGFRIEPAEIELALAGHPAVAQNVVLLHRSATGEARLVAYVVPGT----PV
AHH53507.1_475_mod1_orn                  GRADGQVKIRGFRVELGEVEHALLTHPSVGQCAVVAARTEPAGTRLVAYLVPRASHPGAV
                                         **.*.*:*:****:* .*:* **  **:*.* .*:  *: .. :*****:** :    .*

Q50E73_A1_6__orn                         DPR
AHH53507.1_475_mod1_orn                  DTG
                                         *. 
