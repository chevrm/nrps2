CLUSTAL W multiple sequence alignment


BGC0000425_AFH75321.1_5900_mod6_val      LSHAGLGKALQVQRLDLDQAHLGQLPDDQPAVVVSSEAAAYIMYTSGSTGTPKGVRVPHR
AFZ61525.1_1627_mod2_val                 -AHRVLN---NLADLNLD----AQSPRN-PELPQSAESVAYILYTSGSTGAPKGVQVPHR
                                          :*  *.   ::  *:**    .* * : * :  *:*:.***:*******:****:****

BGC0000425_AFH75321.1_5900_mod6_val      AITRLVINNGYADFNQHDRVAFASNPAFDASTLEVWAPLLNGGAVVVIDQDTLLTQHAFA
AFZ61525.1_1627_mod2_val                 AISRLVLNNGYAEFNARDRVAFASNPAFDASTLDVWAPLLNGGCVVVVEHGVLLSQAAFA
                                         **:***:*****:** :****************:*********.***:::..**:* ***

BGC0000425_AFH75321.1_5900_mod6_val      ALLQQQDVSVVWMTAGLFHQYAEGLLEAFARLRYLIVGGDVLDPSVIARVLKGGAPRHLL
AFZ61525.1_1627_mod2_val                 SLLQEQSVSVLWMTAGLFHQYADALMPVFRQLRYLIVGGDVLDPQVIGRVLEHGKPQHLL
                                         :***:*.***:***********:.*: .* :*************.**.***: * *:***

BGC0000425_AFH75321.1_5900_mod6_val      NGYGPTEATTFSTTHEITRADEGSIPIGKPVGNSRAYVLDAHQQPVPVGVTGELYIGGQG
AFZ61525.1_1627_mod2_val                 NGYGPTEATTFSTTFEITSVGEGGIPIGRPIANAQAYVLDPRQQPVPLGVVGELYIGGAG
                                         **************.*** ..**.****:*:.*::*****.:*****:**.******* *

BGC0000425_AFH75321.1_5900_mod6_val      VALGYLNRPELTAEKFLVDPFSDGGLMYRTGDLVAWRADGTLQYQGRNDQQVKIRGFRIE
AFZ61525.1_1627_mod2_val                 VAKGYLNQPQLTAEKFIPNPFGE-GVLYRTGDLACWQVDGTLVYQGRNDLQVKIRGFRIE
                                         ** ****:*:******: :**.: *::******..*:.**** ****** **********

BGC0000425_AFH75321.1_5900_mod6_val      LGEIETRLAGCPGVKDAVVLARQDEPGHKRLVAYFTERESLDI
AFZ61525.1_1627_mod2_val                 PGEIETCLASFTGVKDAVVLAREDEPGDKRLVAYYTASEPLD-
                                          ***** **. .**********:****.******:*  *.** 
