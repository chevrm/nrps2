CLUSTAL W multiple sequence alignment


Q70LM5_A2_8__val                         -----LTQKHLGKQWKGRKRRTVYLDRDAKKWTAESPLAPAVDATKDSLAYVIYTSGSTG
AEC14349.1_24_mod1_val                   IEPIPFEINELGKEGH----------------------IPVNRSSENGLAYVIYTSGTTG
                                              :  :.***: :                       *.  ::::.*********:**

Q70LM5_A2_8__val                         TPKGVLVAHRGIVRLVKNTNYVTITEEDVFLQASTVSFDAATFEIWGALLNGAKLVLMPP
AEC14349.1_24_mod1_val                   KPKGVMIEQKSVVNLVLNSEILEINENNRVAQFSNPCFDAATFEIWGALLNGATLFLMSN
                                         .****:: ::.:*.** *:: : *.*:: . * *. .****************.*.**. 

Q70LM5_A2_8__val                         DLPSLDELGEAIVQHKVTTLWLTAGLFSIMVDHNADYLRGVRQLLVGGDVVSVPHVRKVL
AEC14349.1_24_mod1_val                   EFTSFDDWKEAISVGKVDVAFFTTGLFNAMVDEDPFIFEGLKKIVVGGDKISVSHVKRLK
                                         ::.*:*:  ***   ** . ::*:***. ***.:.  :.*:::::**** :**.**::: 

Q70LM5_A2_8__val                         ALDG-VTVINGYGPTENTTFTCCYPVTELAEEITSFPIGRPISNTTVYVLDKNRQPVPLG
AEC14349.1_24_mod1_val                   QTIGHFSLINGYGPTECTTFAICHEVED--KDVDVIPIGKPLSNVEIKILSEEQVEVEDG
                                            * .::******** ***: *: * :  :::  :***:*:**. : :*.:::  *  *

Q70LM5_A2_8__val                         VAGELYIGGDGLASGYLNNPELTAERFVDNPFDPQKASRLYRTGDLVRYLPDGAIEFIGR
AEC14349.1_24_mod1_val                   EAGEIYIGGQGVMRGYLNQPQLTEQALI---MDDLNQAKWYKTGDHGVRLHNGEILYKGR
                                          ***:****:*:  ****:*:** : ::   :*  : :: *:***    * :* * : **

Q70LM5_A2_8__val                         IDNQVKIRGFRIELGEIETALLRHPAVQEAFLMVREDAPGDKRLAAYLVFAGGQTVEP
AEC14349.1_24_mod1_val                   KDSQVKIRGFRIELNEIQEKLDKYMKIESSVVAVK-NINGENYVIAYYKHKDEKA---
                                          *.***********.**:  * ::  ::.:.: *: :  *:: : **  . . ::   
