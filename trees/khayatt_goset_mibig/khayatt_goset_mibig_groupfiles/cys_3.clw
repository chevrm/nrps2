CLUSTAL W multiple sequence alignment


Q79JZ1_A1_cys                            INESDSKNSPSNDLFFFLDWQTAIKSEPMRSPQDVAPSQPAYIIYTSGSTGTPKGVVISH
P48633_A1_cys                            ICQHDA--SAGSDDIPVLAWQQAIEAEPIVNPVVRAPTQPAYIIYTSGSTGTPKGVVISH
O85739_A1_cys                            ITEEDD----PQALPPRLDVQRLLRGPALAAPVPLAPQASAYVIYTSGSTGVPKGVEVSH
                                         * : *      .     *  *  :.. .:  *   **  .**:********.**** :**

Q79JZ1_A1_cys                            QGALNTCIAINRRYQIGKNDRVLALSALHFDLSVYDIFGLLSAGGTIVLVSELERRDPIA
P48633_A1_cys                            RGALNTCCDINTRYQVGPHDRVLALSALHFDLSVYDIFGVLRAGGALVMVMENQRRDPHA
O85739_A1_cys                            AAAINTIDALLDLLRVNASDRLLAVSALDFDLSVFDLFGGLGAGASLVLPAQEQARDAAA
                                          .*:**   :    ::.  **:**:***.*****:*:** * **.::*:  : : **. *

Q79JZ1_A1_cys                            WCQAIEEHNVTMWNSVPALFDMLLTYATCFNSIAPSKLRLTMLSGDWIGLDLPQRYRNYR
P48633_A1_cys                            WCELIQRHQVTLWNSVPALFDMLLTWCEGFADATPENLRAVMLSGDWIGLDLPARYRAFR
O85739_A1_cys                            WAEAIQRHAVSLWNSAPALLEMALSLPASQADY--RSLRAVLLSGDWVALDLPGRLRPRC
                                         *.: *:.* *::***.***::* *:      .    .** .:*****:.**** * *   

Q79JZ1_A1_cys                            VDG-QFIAMGGATEASIWSNVFDVEKVPMEWRSIPYGYPLPRQQYRVVDDLGRDCPDWVA
P48633_A1_cys                            PQG-QFIAMGGATEASIWSNACEIHDVPAHWRSIPYGFPLTNQRYRVVDERGRDCPDWVS
O85739_A1_cys                            AEGCRLHVLGGATEAGIWSNLQSVDTVPPHWRSIPYGRPLPGQAYRVVDTHGRDVPDLVV
                                          :* :: .:******.****  .:. ** .******* **. * *****  *** ** * 

Q79JZ1_A1_cys                            GELWIGGDGIALGYFDDELKTQAQFLHIDGHAWYRTGDMGCYWPDGTLEFLGRRDKQVKV
P48633_A1_cys                            GELWIGGIGVAEGYFNDSLRSEQQFLTLPDERWYRTGDLGCYWPDGTIEFLGRRDKQVKV
O85739_A1_cys                            GELWIGGASLARGYRNDPELSARRFVHDAQGRWYRTGDRGRYWGDGTLEFLGRVDQQVKV
                                         ******* .:* ** :*   :  :*:      ****** * ** ***:***** *:****

Q79JZ1_A1_cys                            GGYRIELGEIEVALNNIPGVQRAVAIAVGNKDKTLAAFIVMDSEQA
P48633_A1_cys                            GGYRIELGEIESALSQLAGVKQATVLAIGEKEKTLAAYVVPQSE--
O85739_A1_cys                            RGQRIELGEVEAALCAQAGVESACAAVLGGGVASLGAVLVPR----
                                          * ******:* **   .**: * . .:*    :*.* :*      
