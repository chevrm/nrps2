CLUSTAL W multiple sequence alignment


Q9R9J0_A2_asn                            LMPRDVRQQITYEGVVILLDEESSYHEEAFNLEPLSNANHLAYVIYTSGSTGKPKGVLIE
O33743_A1_5__asn                         LTQSHLQPNIRFAGSVLYLDDRSLYEGGSTSFAPESKPDDLAYMIYTSGSTGNPKGAMIT
Q9R9I9_A2_asn                            LTQSHLQQRLAHEGTIVLLDDENSYHKERSNLERISNIKDLAYVIYTSGSTGKPKGVLIE
O68008_A5_12__asn                        LADHKQDLGTLHQEAVELTGDFSSYPAD--NLEPAGNADSLAYIIYTSGSTGKPKGVMIR
Q70JZ9_A2_asn                            LMQRDVRKQLAYEGVTVLLDDEGSYHQDGSDLAPINDASHLAYVIYTSGSTGRPKGVLIE
Q93I55_A2_asn                            LMQQDVRKQLAYEGVTVLLDDESSYHQDGSDLAPINDVSHLAYVIYTSGSTGRPKGVLIE
                                         *   .      .       .: . *     .:   .. . ***:********.***.:* 

Q9R9J0_A2_asn                            HRGLSNYIWWAKEVYVKNEKTNFPLYSSISFDLTVTSIFTPLVTGNTIIVYD-GEDKTAL
O33743_A1_5__asn                         HQGLVNYIWWANKVYVQGEAVDFPLYSSISFDLTVTSIFTPLLSGNTIHVYR-GADKVQV
Q9R9I9_A2_asn                            HQGLTNYIWWADRVYVKGEKTTFPLYSSIAFDLTVTSIFTPLISGNAIIVYG-DKDRTTL
O68008_A5_12__asn                        QRGLVNYITWADRVYVQGEQLDFALYSSIAFDLTVTSIFTPLISGNRVIVYRHSEDGEPL
Q70JZ9_A2_asn                            HRGLTNYIWWAKEVYVKGEKANFPLYSSISFDLTVTSIFTPLVTGNAIIVYD-GEDKTAL
Q93I55_A2_asn                            HGGLTNYIWWAKEVYVKGEKANFPLYSSISFDLTVTSIFTPLVTGNAIIVYD-GEDKTAL
                                         : ** *** **..***:.*   *.*****:************::** : **  . *   :

Q9R9J0_A2_asn                            LSSIVQDQRVDIIKLTPAHLHVLKAMNIANKIAIRKMIVGGENLSTQLAQSIHEQFDGQI
O33743_A1_5__asn                         ILDIIKDNKVGIIKLTPTHLKLIEHID-GKASSIRRFIVGGENLPTKLAKQIYDHFGENV
Q9R9I9_A2_asn                            LSSIIEDSRVDIIKLTPAHLQLLKEMNISPECTIRKMIVGGDNLSTRLAQNISEQFQDQI
O68008_A5_12__asn                        IRKVFRDQKAGIVKLTPSHLSLVKDMD-ASGSSIKRLIVGGEDLKTELAKEITERFHHNI
Q70JZ9_A2_asn                            LESIVRDPRVDIIKLTPAHLQVLKEMNIADQTAVRRMIVGGENLSTRLARSIHEQFEGRI
Q93I55_A2_asn                            LESIVRDPRVDIIKLTPAHLQVLKEMNIADQMAVRRMIVGGENLSTRLARSIHEQFEGRI
                                         : .:..* :..*:****:** ::: :: .   :::::****::* *.**:.* ::*  .:

Q9R9J0_A2_asn                            EICNEYGPTETVVGCMLYRYDAVKDRRESVPIGTAAANTSIYVLDEDMKPVPIGVPGEMY
O33743_A1_5__asn                         QIFNEYGPTETVVGCMIYLYDPQTTTQESVPIGVPADNVQLYLLDASMQPVPVGSLGEMY
Q9R9I9_A2_asn                            EIFNEYGPTETVVGCMIYLYDPKKDRQESVPIGTAAANMNIYLLDTGMKPVPIGVPGEMY
O68008_A5_12__asn                        EIYNEYGPTETVVGCMIYQYDAGWDRQVSVPIGKPASNVQLYILDERQEVQPVGIAGELY
Q70JZ9_A2_asn                            EICNEYGPTETVVGCMIYRYDAAKDRRESVPIGTAAANTSIYVLDENMKPAPIGVPGEIY
Q93I55_A2_asn                            EICNEYGPTETVVGCMIYRYDPAKDRRESVPIGTAAANTSIYVLDENMKPAPIGVPGEIY
                                         :* *************:* **.    : ***** .* * .:*:**   :  *:*  **:*

Q9R9J0_A2_asn                            ISGAGVARGYLNRPELTAEKFVENPFVTGERMYKTGDLAKWLPDGNIEYLGRMDEQVKIR
O33743_A1_5__asn                         IAGDGVAKGYFNRPELTKEKFIDNPFRPGTKMYRTGDLAKWLPDGNMEYAGRMDYQVKIR
Q9R9I9_A2_asn                            ISGAGVARGYLNRPDLTAEKFVEHPFAAGERMYKTGDAARWMPDGHMEYLGRIDHQVKVR
O68008_A5_12__asn                        ISGDGVAKGYLNKPELTSEKFLPNPFLPGERMYRTGDLAKMRPDGHIEYLGRIDHQVKIR
Q70JZ9_A2_asn                            ISGAGVARGYLNRPELTAEKFVDDPFEPGAKMYKTGDLAKWLADGNIEYAGRIDEQVKIR
Q93I55_A2_asn                            ISGAGVARGYLNRPELTAEKFVDDLFEPGAKMYKTGDLAKWLADGNIEYAGRIDEQVKIR
                                         *:* ***:**:*:*:** ***: . * .* :**:*** *:  .**::** **:* ***:*

Q9R9J0_A2_asn                            GFRIELGEIETAMLQAEEIKEAVVTAREDVHGLKQLCGYYVSSQPITVSQ
O33743_A1_5__asn                         GHRIEMGEIETRLTQHEAVKEAVVIVEKDESGQNVLYAYLVSERELTVAE
Q9R9I9_A2_asn                            GYRIELGEVEAALLLVESVKEAVVIAVEE-EGSNQLCAYVTGDESLKTLQ
O68008_A5_12__asn                        GYRIELGEIEHQLLRHSDIKEAAVAAKTDQNNDQVLCAYVVSERDITQKD
Q70JZ9_A2_asn                            GYRIELGEIEAALLQEEAIKEAVVTAREDVHGFKQLCAYYVSGGQTTAAR
Q93I55_A2_asn                            GYRIELGEIEAALLQEEAIKEAVVTAREDVHGFKQLCAYYVSGGQTTAAR
                                         *.***:**:*  :   . :***.* .  :  . : * .* ..    .   
