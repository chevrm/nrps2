CLUSTAL W multiple sequence alignment


Q8NJX1_A1_iva|abu                        LASISNVDMCSKLIGNVVEVSHTLDEALAQTEMSSHGPVSNVSPRNAAYVLSTSGSTGTP
Q8NJX1_A12_iva|abu                       LSSPTNAALCNKLVHNVIEVSPSLIDELSKFCDGFNSPAINVPSSNAAYVLFTSGSTGTP
Q8NJX1_A7_iva|abu                        LVSPSNISTCIDLVEHVVEVSPVTDEILSKTESSHRGPDREISPSNAAYVLFTSGSTGTP
                                         * * :*   * .*: :*:***    : *::   . ..*  ::.. ****** ********

Q8NJX1_A1_iva|abu                        KGLVMQHQAVCTSQTAITKRLRMTSDVKMLQFASFVFDLSIGEIFGPWVVGGCICVPSEE
Q8NJX1_A12_iva|abu                       KGLVMQHGAVCTSQTAIAKRLSLTPDVRILQFAAYVFDLSIGEIVAPLIHGACVCVPSEE
Q8NJX1_A7_iva|abu                        KGLVMEHRSVCTSLTAITKRLKIRPSARTLQFAAHVFDCAVGEIISSLFTGGCLFVPSDH
                                         *****:* :**** ***:*** : ...: ****:.*** ::***... . *.*: ***:.

Q8NJX1_A1_iva|abu                        TRMNDLVNFINTMKINWAYLTPSFARTLNPVDVPGLELLLFAGEAVRRDVFEAWFGRLRL
Q8NJX1_A12_iva|abu                       TRMNGLKEFIRDARINWAYLTPSFVRTLRPEDVPSLQLLLLAGEAVGRDILDTWFGKVRL
Q8NJX1_A7_iva|abu                        DRMNALPEFIRQNKINYMWSTPSFIRTLSPMDVPSLELVVLVGEAVTRDIMDTWFGKVRL
                                          *** * :**.  :**: : **** *** * ***.*:*:::.**** **::::***::**

Q8NJX1_A1_iva|abu                        INGWGPAETCVFSTLHEWKSLDESPLTIGRPVGGHCWIVDPQDPQRLAPIGTFGEVVIQG
Q8NJX1_A12_iva|abu                       INGWGPAETCVFSTLHEWSSIDESPLTIGRPVGGYCWIVEAEDSNKLTPIGCLGEVVLQG
Q8NJX1_A7_iva|abu                        INGWGPAETCVVSTIHEWQSIDESPLTIGRSVGGFCWIVDPEDPQKIAPVGALGEIVIQG
                                         ***********.**:***.*:*********.***.****:.:*.::::*:* :**:*:**

Q8NJX1_A1_iva|abu                        PTILREYLADFTKTESSMVRSLPEWVPNRTS-TH-WDRFYKSGDLCRYNADGTMEFGSRK
Q8NJX1_A12_iva|abu                       PTLLREYLADPQRSKETIITELPPWAPKQVSDAHLWSRFYKSGDLCFYNPNGTLEFYSRK
Q8NJX1_A7_iva|abu                        PTILREYLADPVRTSGSTVYNLPNWAPNRRD-KY-WNRFYKSGDLGLYNADGTIQFASRK
                                         **:*******  ::. : : .** *.*:: .  : *.********  **.:**::* ***

Q8NJX1_A1_iva|abu                        DGQVKIRGLRVELGEIEHHIREALE--------------------------GVKQVAVDV
Q8NJX1_A12_iva|abu                       DTQVKIRGLRVELGEVEHHIRELLE--------------------------GVRQVAVDV
Q8NJX1_A7_iva|abu                        DTRIKIRGLRVELSEVEHHVKTALSRRPSGCCRCCIRSTRWCQVWPHTFALAMKQGLLVL
                                         * ::*********.*:***::  *.                          .::*  : :

Q8NJX1_A1_iva|abu                        ANGDGGAIIVSYFCFTDETRTAGKNYE
Q8NJX1_A12_iva|abu                       LTSETGTQLVSYICFNDDSQPSSPELK
Q8NJX1_A7_iva|abu                        AEG-----------FDE----------
                                           .           * :          
