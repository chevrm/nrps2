CLUSTAL W multiple sequence alignment


AEC14346.1_19_mod1_thr                   -----IGDKGRINGIDCNYVTLDE----VIENSNDITGENFHSLIDPKDPAYIIFTSGST
AGM16412.1_3015_mod3_thr                 LLPHDLRDK---VGFDGTVVMLDDEQSYVEDSSNPATASK------PSDLAYVIYTSGTT
                                              : **    *:* . * **:    * :.**  *..:      *.* **:*:***:*

AEC14346.1_19_mod1_thr                   GKPKGVCVSHENVMNLFLGCDGLFEFNNKDIWTFFHSYAFDFSVWEMWGAWIYGGEVVVV
AGM16412.1_3015_mod3_thr                 GKPKGTLIEHKNVVRLLFNSKNLFDFNSADTWTLFHSFCFDFSVWEMYGALLYGGRLVVV
                                         *****. :.*:**:.*::....**:**. * **:***:.********:** :***.:***

AEC14346.1_19_mod1_thr                   SFEESRNPISFVDLLISTQTTVFNCTPSAFFEVIGE-LINRQNELSLKYIIFGGEALNFK
AGM16412.1_3015_mod3_thr                 PQLTAKNPAQFLELLHEQQVTILNQTPTYFYQLLREALAEPGQELKVRKVIFGGEALNPQ
                                         .   ::** .*::** . *.*::* **: *:::: * * :  :**.:: :******** :

AEC14346.1_19_mod1_thr                   KLTQWFSPENDRLP--KLINMYGITETTVHVTYKQITKSDVEKGITNIGKVLPHLGFKVM
AGM16412.1_3015_mod3_thr                 LLKDW----KTKYPHTQLINMYGITETTVHVTYKEITQVEIEQAKSNIGRPIPTLKVYVL
                                          *.:*    : : *  :*****************:**: ::*:. :***: :* * . *:

AEC14346.1_19_mod1_thr                   NQNEEEVTGDEEGELYVSGKGVSLGYYKNTMMNKMKFTDI---DNVRYYKSGDVVKVLDD
AGM16412.1_3015_mod3_thr                 DANRQCVPVGVAGEMYVAGDGLARGYLHRPELTADKFVDSPFESGGRMYRTGDLARWLPD
                                         : *.: *. .  **:**:*.*:: ** :.. :.  **.*    .. * *::**:.: * *

AEC14346.1_19_mod1_thr                   GDLQYIARNDEQVQLRGFRIELGEIESSYNDLKNIKNAVVTMDKAFNDDMHLFLYYVADE
AGM16412.1_3015_mod3_thr                 GNIEYLGRIDHQVKIRGYRIELGEVEAQLTKVDPVREAIVIAREDGHGEKQLCAYFVAAR
                                         *:::*:.* *.**::**:******:*:. ..:. :::*:*   :  :.: :*  *:** .

AEC14346.1_19_mod1_thr                   E-----
AGM16412.1_3015_mod3_thr                 ELTVGE
                                         *     
