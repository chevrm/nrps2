CLUSTAL W multiple sequence alignment


O87314_A3_orn                            TDRARTSPRCRCHRARRRGNRCHTRRYARAPADRRRTRGFRVRQHAPEHPAYLIYTSGST
O87314_A1_orn                            -----------------------------------------------EHPAYLIYTSGST
                                                                                        *************

O87314_A3_orn                            GRPKGVLTGYAGLTNMYFNHREAIFAPTVARAGSAEQLRIAHTVSFSFDMSWEELFWLVE
O87314_A1_orn                            GRPKGVLTGYAGLTNMYFNHREAIFAPTVARAGSAEQLRIAHTVSFSFDMSWEELFWLVE
                                         ************************************************************

O87314_A3_orn                            GHQVHVCDEELRRDAPALVAYCHRHRIDVINVTPTYAHHLFDAGLLDDGAHTPPLVLLGG
O87314_A1_orn                            GHQVHVCDEELRRDAPALVAYCHRHRIDVINVTPTYAHHLFDAGLLDDGAHTPPLVLLGG
                                         ************************************************************

O87314_A3_orn                            EAVGDGVWSALRDHPDSAGYNLYGPTEYTINTLGGGTDDSDTPTVGQPIWNTRGYILDAA
O87314_A1_orn                            EAVGDGVWSALRDHPDSAGYNLYGPTEYTINTLGGGTDDSDTPTVGQPIWNTRGYILDAA
                                         ************************************************************

O87314_A3_orn                            LRPVPDGAVGELYIAGTGLALGYHRRAGLTAATMVADPYVPGGRMYRTGDLVRRRPGSAE
O87314_A1_orn                            LRPVPDGAVGELYIAGTGLALGYHRRAGLTAATMVADPYVPGGRMYRTGDLVRRRPGSAE
                                         ************************************************************

O87314_A3_orn                            LLDYLGRVDDQVKIRGYRVELGEIESVLTRADGVARCAVVARATGANPPVKTLAAYVIPD
O87314_A1_orn                            LLDYLGRVDDQVKIRGYRVELGEIESVLTRADGVARCAVVARATGANPPVKTLAAYVIPD
                                         ************************************************************

O87314_A3_orn                            RWPAEDAA
O87314_A1_orn                            RWPAEDAA
                                         ********
