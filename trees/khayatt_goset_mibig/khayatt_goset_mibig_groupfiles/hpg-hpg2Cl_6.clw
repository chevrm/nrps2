CLUSTAL W multiple sequence alignment


Q70AZ7_A2_hpg|hpg2Cl                     VCVQATSGAVPDGLAPVVMDSPAIAAAPSEAPPITVGAHDLAYVMYTSGSTGVPKGVAVP
Q70AZ7_A2_5__hpg|hpg2Cl                  VCVQATSGAVPDGLAPVVMDSPAIAAAPSEAPPITVGAHDLAYVMYTSGSTGVPKGVAVP
Q8KLL5_A2_hpg|hpg2Cl                     VCAEGTRNAVPDGLEPVPVDAPWAGETRHETP--TVTARDAAYVMYTSGSTGEPKGIVVP
Q8KLL5_A2_5__hpg|hpg2Cl                  VCAEGTRNAVPDGLEPVPVDAPWAGETRHETP--TVTARDAAYVMYTSGSTGEPKGIVVP
                                         **.:.* .****** ** :*:*  . :  *:*  ** *:* *********** ***:.**

Q70AZ7_A2_hpg|hpg2Cl                     HGSVAALAGDPGWSVGPGDGVLMHAPHAFDASLLEIWVPLLSGAHVVVADPGAVDAQRLR
Q70AZ7_A2_5__hpg|hpg2Cl                  HGSVAALAGDPGWSVGPGDGVLMHAPHAFDASLLEIWVPLLSGAHVVVADPGAVDAQRLR
Q8KLL5_A2_hpg|hpg2Cl                     HGSVAALAGDPGWALDADDCVLMHASHAFDASLFEIWAPLVRGARVMVAEPGAVDTQRLR
Q8KLL5_A2_5__hpg|hpg2Cl                  HGSVAALAGDPGWALDADDCVLMHASHAFDASLFEIWAPLVRGARVMVAEPGAVDTQRLR
                                         *************::...* *****.*******:***.**: **:*:**:*****:****

Q70AZ7_A2_hpg|hpg2Cl                     EAIDRGVTTVHLTAGSFRVLAEESPDAFRGLREVLTGGDAVPLASVVRLRETCPEIRVRH
Q70AZ7_A2_5__hpg|hpg2Cl                  EAIDRGVTTVHLTAGSFRVLAEESPDAFRGLREVLTGGDAVPLASVVRLRETCPEIRVRH
Q8KLL5_A2_hpg|hpg2Cl                     EAVARGVTTVHLTAGSFRVLAEESPGSFDGLREILTGGDVVPLASVAQLRRACPDVRVRH
Q8KLL5_A2_5__hpg|hpg2Cl                  EAVARGVTTVHLTAGSFRVLAEESPGSFDGLREILTGGDVVPLASVAQLRRACPDVRVRH
                                         **: *********************.:* ****:*****.******.:**.:**::****

Q70AZ7_A2_hpg|hpg2Cl                     LYGPTETTLCATWHLIEPGVATGDTLPIGRPLAGRRAYVLDAFLQPVAPNVTGELYLAGA
Q70AZ7_A2_5__hpg|hpg2Cl                  LYGPTETTLCATWHLIEPGVATGDTLPIGRPLAGRRAYVLDAFLQPVAPNVTGELYLAGA
Q8KLL5_A2_hpg|hpg2Cl                     LYGPTETTLCGTWHLLEPGDEPGDVLPIGRPLAGRRAYVLDAFLQPVAPNVTGELYLAGV
Q8KLL5_A2_5__hpg|hpg2Cl                  LYGPTETTLCGTWHLLEPGDEPGDVLPIGRPLAGRRAYVLDAFLQPVAPNVTGELYLAGV
                                         **********.****:***  .**.**********************************.

Q70AZ7_A2_hpg|hpg2Cl                     GLARGYLGAAAATAERFVADPFAAGERMYRTGDLARWTEQGELLFAGRADAQVKIRGYRV
Q70AZ7_A2_5__hpg|hpg2Cl                  GLARGYLGAAAATAERFVADPFAAGERMYRTGDLARWTEQGELLFAGRADAQVKIRGYRV
Q8KLL5_A2_hpg|hpg2Cl                     GLALGYLGARGATSERFVADPFVPGERMYRTGDLARRNDRGELLFAGRADAQVKIRGYRV
Q8KLL5_A2_5__hpg|hpg2Cl                  GLALGYLGARGATSERFVADPFVPGERMYRTGDLARRNDRGELLFAGRADAQVKIRGYRV
                                         *** ***** .**:********..************ .::********************

Q70AZ7_A2_hpg|hpg2Cl                     EPAEIEAALTAIPEVAQAVVVAREDGPGEKRLIAYVTAAGQPGPDPAA
Q70AZ7_A2_5__hpg|hpg2Cl                  EPAEIEAALTAIPEVAQAVVVAREDGPGEKRLIAYVTAAGQPGPDPAA
Q8KLL5_A2_hpg|hpg2Cl                     EPTEIETVLAEAPQVAQTVVVAREDGPGEKRLIAYAIAEPDQVLDPEA
Q8KLL5_A2_5__hpg|hpg2Cl                  EPTEIETVLAEAPQVAQTVVVAREDGPGEKRLIAYAIAEPDQVLDPEA
                                         **:***:.*:  *:***:*****************. *  :   ** *
