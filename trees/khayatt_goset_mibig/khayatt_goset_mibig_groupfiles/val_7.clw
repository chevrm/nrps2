CLUSTAL W multiple sequence alignment


Q9L8H4_A3_6__val                         TDHTTDLDTTTTQFNPADTPHDGEDPGNPNHTTHPDDAAYIMYTSGSTGRPKGVIATHRN
removed_A2_3__val                        TDHTTDLDTTTTQFNPADTPHDGEDPGNPNHTTHPDDAAYIMYTSGSTGRPKGVIATHRN
                                         ************************************************************

Q9L8H4_A3_6__val                         ITALALDPRFDPTAHRRVLLHSPTAFDASTYEIWVPLLNGNTVVLAPTGDLDVHTYHRVI
removed_A2_3__val                        ITALALDPRFDPTAHRRVLLHSPTAFDASTYEIWVPLLNGNTVVLAPTGDLDVHTYHRVI
                                         ************************************************************

Q9L8H4_A3_6__val                         TDQQITALWLTSWVFNLLTEQSPETFTRVRQIWTGGEAVSGATVTRLQQACPDTTVVDGY
removed_A2_3__val                        TDQQITAVFLTTALFNLLTEHDPACLAGVREVWTGGEAVSAFSVRRVQEACPSVVVVDVY
                                         *******::**: :******:.*  :: **::********. :* *:*:***...*** *

Q9L8H4_A3_6__val                         GPTETTTFATHHPVPTPYTGSAVVPIGRPMATMHTYVLDDSLQPVAPGVTGELYLAGSGL
removed_A2_3__val                        GPTETTTFATHNPVPTPYTGPAVVAIGRPMATMHAYVLDDALQPVAPGVVGELYLGGAGL
                                         ***********:********.***.*********:*****:********.*****.*:**

Q9L8H4_A3_6__val                         ARGYLDRPALTAERFVANPYAAPGERMYRTGDLARWNPDDHLEYAGRADHQVKVRGFRIE
removed_A2_3__val                        ARGYLDRPALTAERFVANPH-RPGERMYRTGDLARWSADAQLEFVGRADQQVKVRGFRIE
                                         *******************:  **************..* :**:.****:**********

Q9L8H4_A3_6__val                         PGEIENVLTDHPAVAQAAVHLNRDQPGNPRLVAYVVAD-TSAPSSDV
removed_A2_3__val                        PGEIENVLTGHPAVAQAAILVREDQPGRPRLVAYVVADGGTAPDG--
                                         *********.********: :..****.**********  :**..  
