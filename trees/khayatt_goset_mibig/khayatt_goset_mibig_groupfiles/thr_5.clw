CLUSTAL W multiple sequence alignment


AEI70245.1_1510_mod2_thr                 VTSEAGAQGLPDTGSLPRLVLDAPDTAERLRAYSPDNPGDGDRRGPLSPLSPAYMIYTSG
AEA30272.1_474_mod1_thr                  LTDSASAAGLPDPGTAAPLLLDDPGIVAELARQADGDLTEAERTAALDPRHAAYMIYTSG
                                         :*..*.* ****.*: . *:** *. . .*   : .:  :.:* ..*.*  .********

AEI70245.1_1510_mod2_thr                 STGKPKGVVIPHQNVIRLFASTQHWFGFGSEDVWTLFHSYAFDFSVWEIWGPLLHGSRLV
AEA30272.1_474_mod1_thr                  STGRPKGVVVPHRNVVRLFTATEPWFGFDADDVWTLFHSYAFDFSVWEIWGPLLHGGTLV
                                         ***:*****:**:**:***::*: ****.::*************************. **

AEI70245.1_1510_mod2_thr                 VVSHTVSRSPAEFLQLLVRERVTVLNQTPSAFYQLMQADREHPSLGGELALRYVVFGGEA
AEA30272.1_474_mod1_thr                  VVPHEVNRSPEEFLRLLARERVTVLNQTPSAFYQLMRADRETSDAGALPALRRVVFGGEA
                                         **.* *.*** ***:**.******************:**** .. *.  *** *******

AEI70245.1_1510_mod2_thr                 LELGRLEEWYARHPDDAPRLINMYGITETTVHVSYKELDRSSAASGAGSLIGEAIPDLRV
AEA30272.1_474_mod1_thr                  LDLWRLEPWYDSHPDDAPVLVNMYGITETTVHVSHVALDATSVGAERGSVIGTPIPDLGV
                                         *:* *** **  ****** *:*************:  ** :*..:  **:** .**** *

AEI70245.1_1510_mod2_thr                 YVLDEALQPVPPGVAGEMYVAGAGLARGYWGRPGLTAERFVADPFGAPGSRMYRTGDLAR
AEA30272.1_474_mod1_thr                  HLLDDALRPVPSGVAAEMYVSGEGLARGYHGRPDLTSHRFVADPFGAPGSRMYRTGDVAR
                                         ::**:**:***.***.****:* ****** ***.**:.*******************:**

AEI70245.1_1510_mod2_thr                 RFADGSLDYLGRADHQVKIRGFRIEPGEIEAVLAKHPGVAQVAVVVREDQPGDKRLVAYV
AEA30272.1_474_mod1_thr                  RTADGELEFVGRADHQVKIRGFRIELGEIEAVVEGLAEVAQAAVVVREDQPGDKRLVAYV
                                         * ***.*:::*************** ******:   . ***.******************

AEI70245.1_1510_mod2_thr                 VPAPDSLAEPA
AEA30272.1_474_mod1_thr                  TPVGGREGVDL
                                         .*. .  .   
