CLUSTAL W multiple sequence alignment


Q50E73_A2_7__asp                         LSHVVGDFPGGEVFEFSRVVRESCLVELVAADGVEVRNVTDGERASRLLPGHPLYVVYTS
Q50E73_A4_9__asp                         LSHVVGDFPGGEVFEFSRVVRESCLVELVAADGVEVRNVTDGERASRLLPGHPLYVVYTS
                                         ************************************************************

Q50E73_A2_7__asp                         GSTGRPKGVVVTHASVGGYLARGRDVYAGAVGGVGFVHSSLAFDLTVTVLFTPLVSGGCV
Q50E73_A4_9__asp                         GSTGRPKGVVVTHASVGGYLARGRDVYAGAVGGVGFVHSSLAFDLTVTVLFTPLVSGGCV
                                         ************************************************************

Q50E73_A2_7__asp                         VLGELDESAQGVGASFVKVTPSHLGLLGELEGVVAGNGMLLVGGEALSGGALREWRERNP
Q50E73_A4_9__asp                         VLGELDESAQGVGASFVKVTPSHLGLLGELEGVVAGNGMLLVGGEALSGGALREWRERNP
                                         ************************************************************

Q50E73_A2_7__asp                         GVVVVNAYGPTELTVNCAEFLIAPGEEVPDGPVPIGRPFAGQRMFVLDAALRVVPVGVVG
Q50E73_A4_9__asp                         GVVVVNAYGPTELTVNCAEFLIAPGEEVPDGPVPIGRPFAGQRMFVLDAALRVVPVGVVG
                                         ************************************************************

Q50E73_A2_7__asp                         ELYVAGVGLARGYLGRAGLTAERFVACPFGAPGERMYRTGDLVRWRVDGALEFVGRADDQ
Q50E73_A4_9__asp                         ELYVAGVGLARGYLGRVGLTAERFVACPFGVPGERMYRTGDLVRWRVDGALEFVGRADDQ
                                         ****************.*************.*****************************

Q50E73_A2_7__asp                         VKVRGFRVELGEVEGAVAAHPDVVRAVVVVREDRPGDHRLVAYVTGVDTGG--
Q50E73_A4_9__asp                         VKVRGFRVELGEVEGAVAAHPDVVRAVVVVREDRPGDHRLVAYVTAGGVGGDG
                                         *********************************************. ..**  
