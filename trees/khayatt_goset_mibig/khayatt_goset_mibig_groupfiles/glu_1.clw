CLUSTAL W multiple sequence alignment


P94459_A2_glu                            LQSAGLHVPEFTGEIVYLNQTNSGLAHRLSNPNVDVLPQSLAYVIYTSGSTGMPKGVEIE
O30980_A2_glu                            IQSDNLDIPAFDGEIVHLSQLNSGLKRRLSNPNVEVYPDSLAYMIYTSGSTGRPKGVQVE
                                         :** .*.:* * ****:*.* **** :*******:* *:****:******** ****::*

P94459_A2_glu                            HRSAVNFLNSLQSRYQLKHSDMIMHKTSYSFDASIWELFWWPYAGASVYLLPQGGEKEPE
O30980_A2_glu                            HQSAVNFLNSLQFRYPLNQSDVILHKTSYSFDASIWELFWWPYGGASVYLLPQGGEKEPD
                                         *:********** ** *::**:*:*******************.***************:

P94459_A2_glu                            VIAKAIEEQKITAMHFVPSMLHAFLEQIKYRSVPIKTNRLKRVFSGGEQLGTHLVSRFYE
O30980_A2_glu                            MILKVIEEQQITAMHFVPSMLHAFLEYLKNGPVPIKTNRLKRVFSGGEQLGAHLVSRFCE
                                         :* *.****:**************** :*  .*******************:****** *

P94459_A2_glu                            LLPNVSITNSYGPTEATVEAAFFDCPPHEKLERIPIGKPVHHVRLYLLNQNQRMLPVGCI
O30980_A2_glu                            LLPDVTLTNSYGPTEATVEAAFFDCPLDEKLDRIPIGKPIHHVRLYILNQKQKMLPAGCI
                                         ***:*::******************* .***:*******:******:***:*:***.***

P94459_A2_glu                            GELYIAGAGVARGYLNRPALTEERFLEDPFYPGERMYKTGDVARWLPDGNVEFLGRTDDQ
O30980_A2_glu                            GELYIAGAGVARGYLNRPELTEERFLDDPFYPGERMYKTGDLARWLPDGQVEFLGRLDDQ
                                         ****************** *******:**************:*******:****** ***

P94459_A2_glu                            VKIRGYRIEPGEIEAALRSIEGVREAAVTVRTDSGEPELCAYVEGLQRNE
O30980_A2_glu                            VKIRGYRIEPGEIEAALRSIEGVREAAVTVRTESGEAELCAYAEGLGRNE
                                         ********************************:***.*****.*** ***
