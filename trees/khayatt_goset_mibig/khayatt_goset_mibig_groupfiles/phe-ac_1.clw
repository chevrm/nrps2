CLUSTAL W multiple sequence alignment


Q7WRJ0/31616733_ALod_phe-ac              LTTQSHLKELRVVSIDKLRSNQSLSNQL------SAHELPHLTPLDPALLLFTSGSTGLP
Q9FDT8_ALod_phe-ac                       LGTYSHLEGWQVISVNELR-----------KAPSKIEQLPILDPQDAALLLFTSGSTGMP
Q8G985/24744795_ALod_phe-ac              LETQSHLKGLQVVSIDYLRSIESQSNSQPDKTSKKCYSLPDLTPENQALLLFTSGSTGMP
                                         * * ***:  :*:*:: **               .  .** * * : ***********:*

Q7WRJ0/31616733_ALod_phe-ac              KGVMLHHRNLLSMSVGTVRMNNFTRQEVTLNWMPLDHVGAIVFLGIMAVDLACHQIHVPI
Q9FDT8_ALod_phe-ac                       KGVILTHHNILSMTAGTVVMNHFTQQEVTLNWMPLDHVGAIVFLGIMAVDLACDQIHVPM
Q8G985/24744795_ALod_phe-ac              KGVMLTHNNLLSMSAGTVAMNNFSQQEVTLNWMPLDHAGAIVFLGIMAVDLACDQIHVPM
                                         ***:* *.*:***:.*** **:*::************.***************.*****:

Q7WRJ0/31616733_ALod_phe-ac              ELILRQPLKWLGLIQKYQATISWSPNFAFSLINQHTEELNQSCYDLSSIKFLVNAGEQVS
Q9FDT8_ALod_phe-ac                       ELVLRQPLQWLELIQKHQVSISWSPNFAFSLINQQAEELKHVSYNLSSMKFLVNAGEQVS
Q8G985/24744795_ALod_phe-ac              ELILRQPLKWLELIQNHQATISWAPNFAFSLINQQSEQLNQSSYNLSSMKFLVNAGEQVS
                                         **:*****:** ***::*.:***:**********::*:*:: .*:***:***********

Q7WRJ0/31616733_ALod_phe-ac              VKTIRLFLEILEKHKLRDRVIKPAFGMTESCSGITWSAGLSKDELIEENSFVSLGRPIPG
Q9FDT8_ALod_phe-ac                       VKTIRLFLEILEKHQLQERAIKPAFGMTESCSGITWSAGLSKNELTEENSFVSLGKPIPG
Q8G985/24744795_ALod_phe-ac              IKTIRLFLEILEKHQLPDRAIKPAFGMTESCSGITWSVGLSQDKLTEENIIISLGKPIPG
                                         :*************:* :*.*****************.***:::* *** ::***:****

Q7WRJ0/31616733_ALod_phe-ac              ATIRIIDQENNILPENEVGRLQIKGPSVTEGYYNNPDLNQEVFQDGGWFTTGDLGYLHNG
Q9FDT8_ALod_phe-ac                       ATIRIVDQENNPLPEREIGRLQIQGNSVTKGYYNNNELNQEVFQ-EGWFTTGDLGYLSKG
Q8G985/24744795_ALod_phe-ac              ATIRIVDQENNVLPEAEIGRLQIKGKSVTKEYYHNPELNQEVFQDEGWFTTGDLAYLLDG
                                         *****:***** *** *:*****:* ***: **:* :*******  ********.** .*

Q7WRJ0/31616733_ALod_phe-ac              ELVITGREKQEIIINGINYFAHEIETAIEELDGVAISYTAAFAVFDQNRETDLLVITFNP
Q9FDT8_ALod_phe-ac                       ELFITGREKQEIIINGVNYFAHELETTIEELEGVKVSYTAAFAVFDQSRETDLLIITFSP
Q8G985/24744795_ALod_phe-ac              ELFITGREKQEIIINGINYFAHEIETVIEELEGVNVSYTAAFALFDQIQETDLLIIIFSP
                                         **.*************:******:**.****:** :*******:*** :*****:* *.*

Q7WRJ0/31616733_ALod_phe-ac              ESNN
Q9FDT8_ALod_phe-ac                       ESEQ
Q8G985/24744795_ALod_phe-ac              ESEH
                                         **::
