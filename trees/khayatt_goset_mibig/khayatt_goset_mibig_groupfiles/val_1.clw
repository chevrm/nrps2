CLUSTAL W multiple sequence alignment


Q09164_A4_val                            LGANVTPPKLQEAAIDFVPIRDTFTTLTDGTLQDG--PTIE---RPSAQSLAYAMFTSGS
Q09164_A9_val                            LGSDTQAVKLHANSV-------RFTRISDALVESGSPPTEELSTRPTAQSLAYVMFTSGS
                                         **::. . **:  ::        ** ::*. ::.*  ** *   **:******.******

Q09164_A4_val                            TGRPKGVMVQHRNIVRLVKNSNVVAKQPAAARIAHISNLAFDASSWEIYAPLLNGGAIVC
Q09164_A9_val                            TGVPKGVMVEHRGITRLVKNSNVVAKQPAAAAIAHLSNIAFDASSWEIYAPLLNGGTVVC
                                         ** ******:**.*.**************** ***:**:*****************::**

Q09164_A4_val                            ADYFTTIDPQALQETFQEHEIRGAMLPPSLLKQCLVQAPDMISRLDILFAAGDRFSSVDA
Q09164_A9_val                            IDYYTTIDIKALEAVFKQHHIRGAMLPPALLKQCLVSAPTMISSLEILFAAGDRLSSQDA
                                          **:**** :**: .*::*.********:*******.** *** *:********:** **

Q09164_A4_val                            LQAQRLVGSGVFNAYGPTENTILSTIYNVAENDSFVNGVPIGSAVSNSGAYIMDKNQQLV
Q09164_A9_val                            ILARRAVGSGVYNAYGPTENTVLSTIHNIGENEAFSNGVPIGNAVSNSGAFVMDQNQQLV
                                         : *:* *****:*********:****:*:.**::* ******.*******::**:*****

Q09164_A4_val                            PAGVMGELVVTGDGLARGYMDPKLDADRFIQLTVNGSEQVRAYRTGDRVRYRPKDFQIEF
Q09164_A9_val                            SAGVIGELVVTGDGLARGYTDSKLRVDRFIYITLDGN-RVRAYRTGDRVRHRPKDGQIEF
                                         .***:************** *.** .**** :*::*. :***********:**** ****

Q09164_A4_val                            FGRMDQQIKIRGHRIEPAEVEQAFLNDGFVEDVAIVIRTPENQEPEMVAFVTAKGDNSAR
Q09164_A9_val                            FGRMDQQIKIRGHRIEPAEVEQALARDPAISDSAVITQLTDEEEPELVAFFSLKGNANGT
                                         ***********************: .*  :.* *:: : .:::***:***.: **: .. 

Q09164_A4_val                            E
Q09164_A9_val                            N
                                         :
