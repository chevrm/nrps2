CLUSTAL W multiple sequence alignment


Q2XNF8_A2_3__ala                         ---RVGRASLGADAAGARDLLDLDAADPAWSDRSAADPEPAGLSARNLAYVIYTSGSTGT
Q2XNF8_A1_2__ala                         LSDAAGRRALGENALLAHAVVDLDERRPVWAGASTADPRPAGLSPRHLACAIYPAGATAA
                                             .** :** :*  *: ::***   *.*:. *:***.*****.*:** .**.:*:*.:

Q2XNF8_A2_3__ala                         PKGVQNEHRGVVNRLAWMPEEYRLGAGDTVLQKTSFGFDVSVWEFFWPLLHGATLALAPP
Q2XNF8_A1_2__ala                         PAGAQNEHRALVDRLRWMQDAYRLGAGETVVQQTSLAADAALWELFWTWSNGATVVLAPA
                                         * *.*****.:*:** ** : ******:**:*:**:. *.::**:**.  :***:.***.

Q2XNF8_A2_3__ala                         DAHKDPAALIELIVRHRVTTVHFVPSMLAVFLQADGVERCAGLRRVICSGEALPGASVRL
Q2XNF8_A1_2__ala                         GAQSTPAALVELFVRHDIATAHFAPAALAAFLRADGVERCVGLRRLICSGEALSGASLRL
                                         .*:. ****:**:*** ::*.**.*: **.**:*******.****:*******.***:**

Q2XNF8_A2_3__ala                         LHKRLPQTAIHNLYGPTEAAIEATAWTCPRDFAGDTVPIGRPIANARIYLLDPRRQPVPL
Q2XNF8_A1_2__ala                         AQQRLPWAAIHRLYGPAETAMDATAWTCPPDFAGDRAPIGRPIANTRVYLLDRHRRPLPP
                                          ::*** :***.****:*:*::******* ***** .********:*:**** :*:*:* 

Q2XNF8_A2_3__ala                         GAVGELYIGGVGVARGYLNRPELTDERFLPDPFAADPDARMYRSGDLARHLAGGDIEFLG
Q2XNF8_A1_2__ala                         GAVGELHIGGAGIGRGYLNRPELTAERFVPDPFAADPAARMYRSGERARYRPDGELECLG
                                         ******:***.*:.********** ***:******** *******: **: ..*::* **

Q2XNF8_A2_3__ala                         RNDHQVKLRGFRIELGEIETRLAAHAEVRETVVLALGEGSLMRLVAYVVA--AQRGDE--
Q2XNF8_A1_2__ala                         RSDRQLRLRGLRIEPGDIEARLAEHPAVQRAVVLAPGDGERKRLVAYLVVRPGHAGDLSA
                                         *.*:*::***:*** *:**:*** *. *:.:**** *:*.  *****:*.  .: **   
