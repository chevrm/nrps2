CLUSTAL W multiple sequence alignment


O07944_A2_phe                            ---------------ALVVDAIP-DDTTLAAYADSRLTDADRSAPLLPAHPAYVIYTSGS
O05647_A2_phe                            LTHTSVAAGLPGGVPQLLVDQVGLDDV-----PGHDLTDAERTTPLHPLHPAYVIYTSGS
                                                         *:** :  **.     ..  ****:*::** * ***********

O07944_A2_phe                            TGAPKGVVVAHRSLAATVPAQAAAFGLGTHSRVLNFASISFDAAVWELTSALFTGAGLVL
O05647_A2_phe                            TGLPKGVPVPHRSVASVLVPLIEEFGLGPGSRVLQFASISFDAALWEITLALLSGATLVV
                                         ** **** *.***:*:.: .    ****. ****:*********:**:* **::** **:

O07944_A2_phe                            ADADDLLPGPSLARLVHDRHITLIALPPSALPALPDGALPPGTDLIVAGDATAPDQAARF
O05647_A2_phe                            APAEQLQPGPALAELVARTGTTFLTLPPTALAVLADDALPAGVDLVVAGEATSPDQVGRW
                                         * *::* ***:**.**     *:::***:**..*.*.***.*.**:***:**:***..*:

O07944_A2_phe                            APGRRMVNAYGLTETTVCATMSEPATGDGAPPIGRPVAHARVYVLDERLRPVPPGVTGEM
O05647_A2_phe                            STGRRMTNAYGPTEAAVCTTISAPLTGAVVPPIGRPVPNARAYVLDALLQPVPPGVVGEL
                                         :.****.**** **::**:*:* * **  .*******.:**.****  *:******.**:

O07944_A2_phe                            YVSGAGVARGYLHRPALTAQRFVPDPYALLFGETGTRMYRTGDLARLDADGRLHFAGRAD
O05647_A2_phe                            YLAGGGLARGYRNRPGLTAERFVADP----FGTPGARMYRTGDLARWRPDGELEFAGRTD
                                         *::*.*:**** :**.***:***.**    ** .*:**********  .**.*.****:*

O07944_A2_phe                            QQVKIRGFRIEPGEIETVLTAHPAVAAGAVIA--REDTPGDKQLVAYL-----
O05647_A2_phe                            HQVKIRGFRIEPGEVEAALATHPAVERAAVIAARHED---DRRLVAYLVPAGA
                                         :*************:*:.*::****  .****  :**   *::*****     
