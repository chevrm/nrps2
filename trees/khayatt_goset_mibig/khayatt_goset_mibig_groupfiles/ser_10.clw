CLUSTAL W multiple sequence alignment


EFE73312.1_2615_mod3_ser                 VTSRATEGAVPEGADIPARVQVDGADVLAQLKALSGEPVSS---VEVLPSHVAYVIYTSG
BGC0000431_EFE73313.1_484_mod1_ser       LTSESVRSVVP-GVEGLERVVVDAAQTVSALADLSGEPVSS---VEVLPSHVAYVIYTSG
Q9Z4X6_A1_ser                            LTTRDTAERLP-G-DGTPRLLLDEPAAAGTTAAGAPAPPGTLPRALPAPGHPAYVIYTSG
Q50E73_A6_11__ser                        LTRGDV--ELP-G--SVPRIGLDDTEIRATLATA----PGTNPGTPVTEAHPAYMIYTSG
AGC65516.1_2153_mod2_ser                 LCDAAGATAFP----ADRSVWVD---------AADGTAAATPPPLPSDPDALAYVLFTSG
EFE73313.1_484_mod1_ser                  LTSESVRSVVP-GVEGLERVVVDAAQTVSALADLSGEPVSS---VEVLPSHVAYVIYTSG
AGC65514.1_1579_mod2_ser                 ITTADLADLFD----ANNVLII------------DGSEDADIPDAINAPDDLAYVLFTSG
                                         :        .         : :                 .            **:::***

EFE73312.1_2615_mod3_ser                 STGRPKGVAVPHQGVVNRLLWMQDTFPLDGSDRVVQKTPFGFDVSVWEFFWPLITGAGLV
BGC0000431_EFE73313.1_484_mod1_ser       STGRPKGVAVPHQGVVNRLLWMQDTFPLDGSDRVVQKTPFGFDVSVWEFFWPLITGAGLV
Q9Z4X6_A1_ser                            STGRPKGVVISHRAIVNRLAWMQDTYGLEPSDRVLQKTPSGFDVSVWEFFWPLVQGATLV
Q50E73_A6_11__ser                        STGRPKGVVVSHGAIVNRLAWMQAEYRLDATDRVLQKTPAGFDVSVWEFFWPLLEGAVLV
AGC65516.1_2153_mod2_ser                 STGRPKGVEIAHHSVLNRILWMQATFPIGPGDVILQKTPAGFDVSVWELLWWSWTGAAVA
EFE73313.1_484_mod1_ser                  STGRPKGVAVPHQGVVNRLLWMQDTFPLDGSDRVVQKTPFGFDVSVWEFFWPLITGAGLV
AGC65514.1_1579_mod2_ser                 STGRPKGVEIAHRGVLNRILWMQDAFPIGPDDVILQKTPITFDVSVWELFWWSWTGARVV
                                         ******** :.* .::**: ***  : :   * ::****  *******::*    ** :.

EFE73312.1_2615_mod3_ser                 VARPGGHRDAGYLTELIRHEKVTVAHFVPSMLRVFLRE-----PDAAACTG---LRWVVC
BGC0000431_EFE73313.1_484_mod1_ser       VARPGGHRDAGYLTELIRHEKVTVAHFVPSMLRVFLRE-----PDAAACTG---LRWVVC
Q9Z4X6_A1_ser                            VARPGGHTDPAYLAGTVRREGVTTLHFVPSMLDVFLRE-----PAAAALGGATPVRRVFC
Q50E73_A6_11__ser                        FARPGGHRDAAYLAGLIERERITTAHFVPSMLRVFLEE-----PGAALCTG---LRRVIC
AGC65516.1_2153_mod2_ser                 MPPPGVERDPQALADAIAGHGVTVIHFVPAMLRAFLNAIEDGIVDAGRLAG---LRLVFA
EFE73313.1_484_mod1_ser                  VARPGGHRDAGYLTELIRHEKVTVAHFVPSMLRVFLRE-----PDAAACTG---LRWVVC
AGC65514.1_1579_mod2_ser                 LPDPGVERDPQQLAQAIRDHSVTTMHFVPSMLASFLFSVEHGLVDIEHLGS---LKRVFA
                                         .. ** . *.  *:  :  . :*. ****:**  **              .   :: *..

EFE73312.1_2615_mod3_ser                 SGEALPPELRDQFFDVLE---DVELHNLYGPTEASVDVTAWPCTP-DEGPA-VPIGHPVW
BGC0000431_EFE73313.1_484_mod1_ser       SGEALPPELRDQFFDVLE---GVELHNLYGPTEASVDVTAWPCTP-DEGPA-VPIGRPVW
Q9Z4X6_A1_ser                            SGEALPAELRARFRAVS----DVPLHNLYGPTEAAVDVTYWPCAE-DTGDGPVPIGRPVW
Q50E73_A6_11__ser                        SGEALGTDLAVDFRAKL----PVPLHNLYGPTEAAVDVTHHAYEP-ATGTATVPIGRPIW
AGC65516.1_2153_mod2_ser                 SGEALDAATVRRFNHLLHARFGTMLHNLYGPTEATVDVTWQPCTPWAADSQIVPIGRPVA
EFE73313.1_484_mod1_ser                  SGEALPPELRDQFFDVLE---GVELHNLYGPTEASVDVTAWPCTP-DEGPA-VPIGRPVW
AGC65514.1_1579_mod2_ser                 SGEALDPALVKRFNDLLFDRFGTELHNLYGPTEATVDVTWHACSP-LENPDIVPIGKPIA
                                         ***** .     *         . **********:****  .      .   ****:*: 

EFE73312.1_2615_mod3_ser                 NTRTYVLDTGLHPVPVGVAGDLYLAGDQLARGYLGRPGLTAERFVANP-FGTTGERMYRT
BGC0000431_EFE73313.1_484_mod1_ser       NTRTYVLDTGLRPVPVGVAGDLYLAGDQLARGYLGRPGLTAERFVANP-FGTAGERMYRT
Q9Z4X6_A1_ser                            NTRMYVLDAALRPVPAGVPGELYIAGVQLARGYLGRPALSAERFTADP-HGAPGSRMYRT
Q50E73_A6_11__ser                        NIRTYVLDAALRPVPPGVPGELYLAGAGLARGYHGRPALTAERFVACP-FGVPGERMYRT
AGC65516.1_2153_mod2_ser                 NTRVLILDDAGRPVPPGGVGEIVLAGPQVARGYRGRPGLTAERFRPDPDADHPGARVYHT
EFE73313.1_484_mod1_ser                  NTRTYVLDTGLRPVPVGVAGDLYLAGDQLARGYLGRPGLTAERFVANP-FGTAGERMYRT
AGC65514.1_1579_mod2_ser                 NTTIRILDDRLGDMPIGIAGEIVLGGPQIACGYRNRPELTAEKFPDDP--RKPGHKLYRT
                                         *    :**     :* *  *:: :.*  :* ** .** *:**:*   *    .* ::*:*

EFE73312.1_2615_mod3_ser                 GDVVRWRTDGNLEFLGRADDQVKVRGFRIELGEIEAALMSHAAVAQAAVLVREDVPGDKR
BGC0000431_EFE73313.1_484_mod1_ser       GDVVRWRTDGNLEFLGRADDQVKVRGFRIELGEIEAALTSHATVAQTAVLVREDVPGDKR
Q9Z4X6_A1_ser                            GDLARWNHDGSLDYLGRADHQVKLRGFRIELGEIEAALVRQPEIAQAAVVLREDRPGDQR
Q50E73_A6_11__ser                        GDLVRWRVDGTLEFVGRADDQVKVRGFRVELGEVEGAVAAHPDVVRAVVVVREDRPGDHR
AGC65516.1_2153_mod2_ser                 GDLGRWTADGAVAYLGRADDQVKIGGVRLELAEVETALDACPGVVRGLARVAT-RDGLTE
EFE73313.1_484_mod1_ser                  GDVVRWRTDGNLEFLGRADDQVKVRGFRIELGEIEAALTSHATVAQTAVLVREDVPGDKR
AGC65514.1_1579_mod2_ser                 GDLGRFLAGGSVEYLGRIDHQVKIRGFRIECGEVETTIESHDLVERALVKAVR-VGDLDE
                                         **: *:  .* : ::** *.***: *.*:* .*:* ::     : :  .       .  .

EFE73312.1_2615_mod3_ser                 LVAYVVPAVPGGTVDTTT-----------------
BGC0000431_EFE73313.1_484_mod1_ser       LVAYVIPTRSTGTVDTAA-----------------
Q9Z4X6_A1_ser                            LVAYTVPARDADTLTGPPAEAGTHPGPGAAPDTDP
Q50E73_A6_11__ser                        LVAYV-------TVGG-------------------
AGC65516.1_2153_mod2_ser                 LHAYVLGADDL-TP---------------------
EFE73313.1_484_mod1_ser                  LVAYVIPTRSTGTVDTAA-----------------
AGC65514.1_1579_mod2_ser                 LHAFVLGEGDL-TI---------------------
                                         * *:.       *                      
