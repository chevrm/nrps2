CLUSTAL W multiple sequence alignment


Q9RAH4_A3_pro-mpro                       LTQQSILDRLPQ------------------------------------HQANQVCLDTDA
Q84BC7_A3_6__pro-mpro                    LTQRSLLDRLPQCEKAGGQGAGSRGESPSTRDRASTKGKEEVLSLPASYQTQLVCLDTDA
                                         ***:*:******                                    :*:: *******

Q9RAH4_A3_pro-mpro                       QLISQCSQDNLISDVQANNLAYIIYTSGSTGQPKGIAMNQLALSNLILWHRENLKIPRGA
Q84BC7_A3_6__pro-mpro                    ELISQCSQDNLITGVQANNLGYIIYTSGSTGQPKGIAMNQLALCNLILWHPDNLKIARGA
                                         :***********:.******.**********************.****** :****.***

Q9RAH4_A3_pro-mpro                       KTLQFASINFDVSFQEIFTTWCSGGTLFLIGEELRRDTSALLGFLQQKAIERMFLPFVAL
Q84BC7_A3_6__pro-mpro                    KTLQFASINFDVSFQEIFTTWCSGGTLFLITKELRHDTSNLLRVIQEKAIQRMFLPVVGL
                                         ****************************** :***:*** ** .:*:***:*****.*.*

Q9RAH4_A3_pro-mpro                       QQLAEVAIGGELVNSHLREIITAGEQLQITPAISQWLSKLTDCTLHNHYGPSESHLATSF
Q84BC7_A3_6__pro-mpro                    QQLAEFAVGSELVNTHLREIITAGEQLQITPAISKWLSQLSDCTLHNHYGPSESHVATSF
                                         *****.*:*.****:*******************:***:*:**************:****

Q9RAH4_A3_pro-mpro                       TLTNSVETWPLLPPVGRPIANAQIYILDRFLQPVPVGVPGELYIAGVLLSQGYFNRPELT
Q84BC7_A3_6__pro-mpro                    TLPNLVNTWPLLPPIGRPISNTQIYILDKYLQPVPIGVPGEVYIAGVLLARGYLNRPELT
                                         **.* *:*******:****:*:******::*****:*****:*******::**:******

Q9RAH4_A3_pro-mpro                       LEKFIPNPFKRSRGAGEQGSRG---ETF-----------NCDRLYKTGDLARYLSDGNIE
Q84BC7_A3_6__pro-mpro                    QEKFIQNPFGGSRGAGEQGSRGAEEQSFPSAPHSLCPSASSERLYKTGDLARYLPDGNIE
                                          **** ***  ***********   ::*           ..:************.*****

Q9RAH4_A3_pro-mpro                       YLGRIDNQVKIRGFRIELGEIEAVLSQ-LDVQASCAMATPAAGIAREDIPGNKRLVAYIV
Q84BC7_A3_6__pro-mpro                    YLGRIDNQVKIRGFRIELGEIEAVLSQHINVQASCA-------VVREDTPGDKRLVAYIV
                                         *************************** ::******       :.*** **:********

Q9RAH4_A3_pro-mpro                       PQKEQKLTVSF
Q84BC7_A3_6__pro-mpro                    PQPEQRVSVNV
                                         ** **:::*..
