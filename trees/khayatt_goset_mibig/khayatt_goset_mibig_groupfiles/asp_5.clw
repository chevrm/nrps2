CLUSTAL W multiple sequence alignment


AHH53506.1_1527_mod2_asp                 ITDSASLRVLPEGLDPLVLDAPALA-RDVTARPAGELTDADRTTALRGGHPAYVIYTSGS
AHH53507.1_1547_mod2_asp                 LTD-------PDLLGRLPADVPAVAVHDLAAEPPGTTLPA----ATLPGQLAYVVYTSGS
                                         :**       *: *. *  *.**:* :*::*.*.*    *    *   *: ***:*****

AHH53506.1_1527_mod2_asp                 TGLPKGVVVEHRSLLEFLRRSTRNHPDLGGVSLLHSPVSFDLTVTTLFGPLLAGGRVYVA
AHH53507.1_1547_mod2_asp                 TGRPKGVAVSHDALAGYVRRSARAYPDAAGVSLAHSSIGFDLTVTALFTPLVTGGRVV--
                                         ** ****.*.* :*  ::***:* :** .**** **.:.******:** **::****   

AHH53506.1_1527_mod2_asp                 DLAEGRPAGAPRPTFVKATPSHLRLLGGVAEWASPTGTLMLGGEQLTGAELASWRAAHPG
AHH53507.1_1547_mod2_asp                 -LAESVEDGA-DATFTKVTPSHLALLADLPEIAP--STLVLGGEALTGQALFDWRAAHPG
                                          ***.   **  .**.*.***** **..:.* *.  .**:**** ***  * .*******

AHH53506.1_1527_mod2_asp                 VTVVNDFGPTETTVHCTVHTVYPDDEIGPGPVPIGRPVSGMRAYVLDERLALAPAGVPGE
AHH53507.1_1547_mod2_asp                 TTVVNAYGPTELTVNCTDFPVPPAAELDAGPVPIGRPFPGVAVFVLDQGLHPVPVGTVGE
                                         .**** :**** **:** ..* *  *:..********..*: .:***: *  .*.*. **

AHH53506.1_1527_mod2_asp                 LYISGIGLARGYLGRRGLTAERFVACPF-EPGERMYRTGDVVRRTADGDLVFVGRRDDQV
AHH53507.1_1547_mod2_asp                 LYVSGAGLARGYLARPGLSAERFVACPFGEPGTRMYRTGDLVRWLPDGNLVYVGRADDQV
                                         **:** *******.* **:********* *** *******:**  .**:**:*** ****

AHH53506.1_1527_mod2_asp                 KVHGFRIEPGEVEAALAAHAGVARCVVVVRADPGGTDRLVGYVVAEDGA
AHH53507.1_1547_mod2_asp                 KVRGHRIELGEVESAVADTPGVLRAVAVLREDTPAEPRLVCYVLAATGS
                                         **:*.*** ****:*:*  .** *.*.*:* *. .  *** **:*  *:
