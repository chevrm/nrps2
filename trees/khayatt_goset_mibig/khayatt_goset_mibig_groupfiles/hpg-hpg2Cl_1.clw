CLUSTAL W multiple sequence alignment


Q70AZ9a_A1_hpg|hpg2Cl                    VCSAATRDGVPEGIEAIVVTDEEAFEASAAGARPGDLAYVMYTSGSTGIPKGVAVPHRSV
Q70AZ9b_A1_hpg|hpg2Cl                    VCSAATRDGVPEGIEAIVVTDEEAFEASAAGARPGDLAYVMYTSGSTGIPKGVAVPHRSV
                                         ************************************************************

Q70AZ9a_A1_hpg|hpg2Cl                    AELAGNPGWAVEPGDAVLMHAPYAFDASLFEIWVPLVSGGRVVIAEPGPVDARRLREAIS
Q70AZ9b_A1_hpg|hpg2Cl                    AELAGNPGWAVEPGDAVLMHAPYAFDASLFEIWVPLVSGGRVVIAEPGPVDARRLREAIS
                                         ************************************************************

Q70AZ9a_A1_hpg|hpg2Cl                    SGVTRAHLTAGSFRAVAEESPESFAGLREVLTGGDVVPAHAVARVRSACPRVRIRHLYGP
Q70AZ9b_A1_hpg|hpg2Cl                    SGVTRAHLTAGSFRAVAEESPESFAGLREVLTGGDVVPAHAVARVRSACPRVRIRHLYGP
                                         ************************************************************

Q70AZ9a_A1_hpg|hpg2Cl                    TETTLCATWHLLEPGDEIGPVLPIGRPLPGRRAQVLDASLRAVAPGVIGDLYLSGAGLAD
Q70AZ9b_A1_hpg|hpg2Cl                    TETTLCATWHLLEPGDEIGPVLPIGRPLPGRRAQVLDASLRAVAPGVIGDLYLSGAGLAD
                                         ************************************************************

Q70AZ9a_A1_hpg|hpg2Cl                    GYLRRAGLTAERFVADPSAPGARMYRTGDLAQWTADGALLFAGRADDQVKVRGFRIEPAE
Q70AZ9b_A1_hpg|hpg2Cl                    GYLRRAGLTAERFVADPSAPGARMYRTGDLAQWTADGALLFAGRADDQVKVRGFRIEPAE
                                         ************************************************************

Q70AZ9a_A1_hpg|hpg2Cl                    VEAALTAQPGVHEAVVRAVDGRLVGYVVAEGDAEPAV
Q70AZ9b_A1_hpg|hpg2Cl                    VEAALTAQPGVHEAVVRAVDGRLVGYVVAEGDAEPAV
                                         *************************************
