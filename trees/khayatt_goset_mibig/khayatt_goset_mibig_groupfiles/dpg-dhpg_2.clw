CLUSTAL W multiple sequence alignment


Q70AZ6_A1_dpg|dhpg                       ICTQATEAAVPQNVPQRATIVLDAPPTRAAVDACAGTAPTFRPSAADLAYVMYTSGSTGV
Q70AZ6_A1_7__dpg|dhpg                    ICTQATEAAVPQNVPQRATIVLDAPPTRAAVDACAGTAPTFRPSAADLAYVMYTSGSTGV
                                         ************************************************************

Q70AZ6_A1_dpg|dhpg                       PKGVAVPHGAVAGLAGDAGWRIGPGDGVLMHATHVFDPSLYEMWVPLATGGRVLVAAPGV
Q70AZ6_A1_7__dpg|dhpg                    PKGVAVPHGAVAGLAGDAGWRIGPGDGVLMHATHVFDPSLYEMWVPLATGGRVLVAAPGV
                                         ************************************************************

Q70AZ6_A1_dpg|dhpg                       VDAGGIRQAVARGATAVHLTAGTFRALAEASPDCFAGLREVGTGGDVVPAHTVAHLRRAQ
Q70AZ6_A1_7__dpg|dhpg                    VDAGGIRQAVARGATAVHLTAGTFRALAEASPDCFAGLREVGTGGDVVPAHTVAHLRRAQ
                                         ************************************************************

Q70AZ6_A1_dpg|dhpg                       PQLRVRNTYGPTETTLCATWKPIEPGAQLGPELPIGRPMTNRRIYILDAFLRPVAPGVAG
Q70AZ6_A1_7__dpg|dhpg                    PQLRVRNTYGPTETTLCATWKPIEPGAQLGPELPIGRPMTNRRIYILDAFLRPVAPGVAG
                                         ************************************************************

Q70AZ6_A1_dpg|dhpg                       ELYIAGTGLARGYLSRPDLTAERFVACPFLAGERMYRTGDLARWNRDGEVVFLGRADDQV
Q70AZ6_A1_7__dpg|dhpg                    ELYIAGTGLARGYLSRPDLTAERFVACPFLAGERMYRTGDLARWNRDGEVVFLGRADDQV
                                         ************************************************************

Q70AZ6_A1_dpg|dhpg                       KIRGYRVELAEVEAVLAAQPGVREAVVVAREDRPGERRLVGYVVSDAGELDTEQ
Q70AZ6_A1_7__dpg|dhpg                    KIRGYRVELAEVEAVLAAQPGVREAVVVAREDRPGERRLVGYVVSDAGELDTEQ
                                         ******************************************************
