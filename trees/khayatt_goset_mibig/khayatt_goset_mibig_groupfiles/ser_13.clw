CLUSTAL W multiple sequence alignment


Q84BC7_A2_5__ser                         LSQHHLVEKLPEHHARVVCLDTDWQIIPQSNQQNPIAGVQASNLAYVIYTSGSTGKPKGA
Q9RAH4_A2_ser                            LTQHQLKEKLPQHQGQVVCLDTDWQFISQSSQENLITTVQASNLAYVIYTSGSTGKPKGA
                                         *:**:* ****:*:.:*********:*.**.*:* *: **********************

Q84BC7_A2_5__ser                         INTHLGICNRLLWMQQAYQLTEIDCVLQKTPFSFDVSVWEFFWPLLTGARLVVAKPDGHK
Q9RAH4_A2_ser                            MNTHLGICNRLLWMQQAYQLTALDCILQKTPFSFDVSVWEFFWPLITGARLVVAKPGGHK
                                         :******************** :**:*******************:**********.***

Q84BC7_A2_5__ser                         DSGYLVNLILEQQVTTLHFVPSMLQIFLEEQGLKDCSSLKRVICSGEALPKELQERFFAC
Q9RAH4_A2_ser                            DSAYLVNLILEQQVTHVHFVPSMLQVFLEEQNLENCRSLKRVICSGEALPVELQERFFAR
                                         **.************ :********:*****.*::* ************* ******** 

Q84BC7_A2_5__ser                         LGCQLHNLYGPTEAAIDVTFWECQPESNLKTVPIGRPISNTQIYILDQNLQPVPVGVPGE
Q9RAH4_A2_ser                            LECELHNLYGPTEAAIDVTYWQCFPNGHLRTVPIGRAIANTQIYILDEHLQPVPVGVAGE
                                         * *:***************:*:* *:.:*:******.*:********::********.**

Q84BC7_A2_5__ser                         LHIGGAGLAKGYLNRPELTQEKFIPNPFSGSRRAGEQRSRGAGERRRKIV---QSPVASP
Q9RAH4_A2_ser                            LHIAGVGLAKGYLNRPDLTTDKFIPNPF--SREVGEQGSKGA-----KILPNSQSLVPNP
                                         ***.*.**********:** :*******  **..*** *:**     **:   ** *..*

Q84BC7_A2_5__ser                         RLYKTGDLARYLPDGNIEYLGRIDNQVKIRGFRIELGEIEAALSQHKDVQTSVVIVREDI
Q9RAH4_A2_ser                            QLYKTGDLARYLPDGTIEYIGRIDNQVKIRGFRIELGEIEVLLNQCEDVQASCVIAREGT
                                         :**************.***:********************. *.* :***:* **.**. 

Q84BC7_A2_5__ser                         PGDKRLVSYIVLQPEQTTTVKE
Q9RAH4_A2_ser                            TGDKCLVAYVVPHQHSKPTTNE
                                         .*** **:*:* : ....*.:*
