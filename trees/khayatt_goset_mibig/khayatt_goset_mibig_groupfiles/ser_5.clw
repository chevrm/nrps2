CLUSTAL W multiple sequence alignment


EFE73312.1_2615_mod3_ser                 VTSRATE--GAVPEGADIPARVQVDGADVLAQLKAL----SGEPVSSVEVLPSHVAYVIY
BGC0000431_EFE73313.1_484_mod1_ser       LTSESVR--SVVP-GVEGLERVVVDAAQTVSALADL----SGEPVSSVEVLPSHVAYVIY
Q50E73_A6_11__ser                        LTRGDVELPGSVP-------RIGLDDTEIRATLATAPGTNPGTPVTE-----AHPAYMIY
EFE73313.1_484_mod1_ser                  LTSESVR--SVVP-GVEGLERVVVDAAQTVSALADL----SGEPVSSVEVLPSHVAYVIY
                                         :*   ..  . **       *: :* ::  : *       .* **:.     :* **:**

EFE73312.1_2615_mod3_ser                 TSGSTGRPKGVAVPHQGVVNRLLWMQDTFPLDGSDRVVQKTPFGFDVSVWEFFWPLITGA
BGC0000431_EFE73313.1_484_mod1_ser       TSGSTGRPKGVAVPHQGVVNRLLWMQDTFPLDGSDRVVQKTPFGFDVSVWEFFWPLITGA
Q50E73_A6_11__ser                        TSGSTGRPKGVVVSHGAIVNRLAWMQAEYRLDATDRVLQKTPAGFDVSVWEFFWPLLEGA
EFE73313.1_484_mod1_ser                  TSGSTGRPKGVAVPHQGVVNRLLWMQDTFPLDGSDRVVQKTPFGFDVSVWEFFWPLITGA
                                         ***********.*.* .:**** ***  : **.:***:**** *************: **

EFE73312.1_2615_mod3_ser                 GLVVARPGGHRDAGYLTELIRHEKVTVAHFVPSMLRVFLREPDAAACTGLRWVVCSGEAL
BGC0000431_EFE73313.1_484_mod1_ser       GLVVARPGGHRDAGYLTELIRHEKVTVAHFVPSMLRVFLREPDAAACTGLRWVVCSGEAL
Q50E73_A6_11__ser                        VLVFARPGGHRDAAYLAGLIERERITTAHFVPSMLRVFLEEPGAALCTGLRRVICSGEAL
EFE73313.1_484_mod1_ser                  GLVVARPGGHRDAGYLTELIRHEKVTVAHFVPSMLRVFLREPDAAACTGLRWVVCSGEAL
                                          **.*********.**: **.:*::*.************.**.** ***** *:******

EFE73312.1_2615_mod3_ser                 PPELRDQFFDVLEDVELHNLYGPTEASVDVTAWPCTPDEGPA-VPIGHPVWNTRTYVLDT
BGC0000431_EFE73313.1_484_mod1_ser       PPELRDQFFDVLEGVELHNLYGPTEASVDVTAWPCTPDEGPA-VPIGRPVWNTRTYVLDT
Q50E73_A6_11__ser                        GTDLAVDFRAKLP-VPLHNLYGPTEAAVDVTHHAYEPATGTATVPIGRPIWNIRTYVLDA
EFE73313.1_484_mod1_ser                  PPELRDQFFDVLEGVELHNLYGPTEASVDVTAWPCTPDEGPA-VPIGRPVWNTRTYVLDT
                                          .:*  :*   *  * **********:****  .  *  *.* ****:*:** ******:

EFE73312.1_2615_mod3_ser                 GLHPVPVGVAGDLYLAGDQLARGYLGRPGLTAERFVANPFGTTGERMYRTGDVVRWRTDG
BGC0000431_EFE73313.1_484_mod1_ser       GLRPVPVGVAGDLYLAGDQLARGYLGRPGLTAERFVANPFGTAGERMYRTGDVVRWRTDG
Q50E73_A6_11__ser                        ALRPVPPGVPGELYLAGAGLARGYHGRPALTAERFVACPFGVPGERMYRTGDLVRWRVDG
EFE73313.1_484_mod1_ser                  GLRPVPVGVAGDLYLAGDQLARGYLGRPGLTAERFVANPFGTAGERMYRTGDVVRWRTDG
                                         .*:*** **.*:*****  ***** ***.******** ***..*********:****.**

EFE73312.1_2615_mod3_ser                 NLEFLGRADDQVKVRGFRIELGEIEAALMSHAAVAQAAVLVREDVPGDKRLVAYVVPAVP
BGC0000431_EFE73313.1_484_mod1_ser       NLEFLGRADDQVKVRGFRIELGEIEAALTSHATVAQTAVLVREDVPGDKRLVAYVIPTRS
Q50E73_A6_11__ser                        TLEFVGRADDQVKVRGFRVELGEVEGAVAAHPDVVRAVVVVREDRPGDHRLVAYV-----
EFE73313.1_484_mod1_ser                  NLEFLGRADDQVKVRGFRIELGEIEAALTSHATVAQTAVLVREDVPGDKRLVAYVIPTRS
                                         .***:*************:****:*.*: :*. *.::.*:**** ***:******     

EFE73312.1_2615_mod3_ser                 GGTVDTTT
BGC0000431_EFE73313.1_484_mod1_ser       TGTVDTAA
Q50E73_A6_11__ser                        --TVGG--
EFE73313.1_484_mod1_ser                  TGTVDTAA
                                           **.   
