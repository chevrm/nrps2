CLUSTAL W multiple sequence alignment


AGM16412.1_3015_mod3_thr                 LLPHDLRDKVGFDGTVVMLDDEQSYVEDSSNPA-------TASKPSDLAYVIYTSGTTGK
P39846_A2_thr                            LTERGLNKPADYTGHILYIDE----CENNSIPADVNIEEIVTDQP---AYVIYTSGTTGQ
O87704_A2_4__thr                         LTEHGHNKPADYHGQILYLND----AENELISPDLKAQETLADQP---AYVIYTSGTTGQ
                                         *  :. .. ..: * :: :::     *:.  ..        :.:*   ***********:

AGM16412.1_3015_mod3_thr                 PKGTLIEHKNVVRLLFNSKNLFDFNSADTWTLFHSFCFDFSVWEMYGALLYGGRLVVVPQ
P39846_A2_thr                            PKGVIVEHRNVISLLKHQNLPFEFNHEDVWTLFHSYCFDFSVWEMFGALLNGSTLVVVSK
O87704_A2_4__thr                         PKGVVVEHRNVISLLKHQDLPFDFGSEDVWTLFHSYCFDFSVWEMFGALLNGSTLVVVSR
                                         ***.::**:**: ** :..  *:*.  *.******:*********:**** *. ****.:

AGM16412.1_3015_mod3_thr                 LTAKNPAQFLELLHEQQVTILNQTPTYFYQLLREALAEPGQELKVRKVIFGGEALNPQLL
P39846_A2_thr                            ETARDPQAFRLLLKKERVTVLNQTPTAFYGLMLED-QNHTDHLNIRYVIFGGEALQPGLL
O87704_A2_4__thr                         ETARDPNAFRLLLKNEGVTVLNQTPTAFYGLIHEE-ENHTDRLHVRYVIFGGEALQPGML
                                          **::*  *  **::: **:****** ** *: *   :  :.*::* ********:* :*

AGM16412.1_3015_mod3_thr                 KDWKTKYPHTQLINMYGITETTVHVTYKEITQVEIEQAKSNIGRPIPTLKVYVLDANRQC
P39846_A2_thr                            QSWNEKYPHTDLINMYGITETTVHVTFKKLSAADIAKNKSNIGRPLSTLQAHVMDAHMNL
O87704_A2_4__thr                         VTWNEKYPDTDLINMYGITETTVHVTYKKLSSADIEKNKSNIGKPLATLQAYVMDAHMNL
                                           *: ***.*:***************:*::: .:* : *****:*:.**:.:*:**: : 

AGM16412.1_3015_mod3_thr                 VPVGVAGEMYVAGDGLARGYLHRPELTADKFVDSPFESGGRMYRTGDLARWLPDGNIEYL
P39846_A2_thr                            QPTGVPGELYIGGEGVARGYLNRDELTADRFVSNPYLPGDRLYRTGDLAKRLSNGELEYL
O87704_A2_4__thr                         QPTGVPGELYIGGEGVARGYLNRDDLTAARFVPNPYLPGDRLYRTGDLAKRLASGDLEYM
                                          *.**.**:*:.*:*:*****:* :*** :** .*: .*.*:*******: *..*::**:

AGM16412.1_3015_mod3_thr                 GRIDHQVKIRGYRIELGEVEAQLTKVDPVREAIVIAREDGHGEKQLCAYFVAARELTVGE
P39846_A2_thr                            GRIDEQVKVRGHRIELGEIQAALLQYPMIKEAAVITRADEQGQTAIYAYMVIKDQQAANI
O87704_A2_4__thr                         GRIDDQVKVRGHRIELGEIQASLLQLPIIKEAAVITRDDEQGQSAVYAYLVAEDGQVVNE
                                         ****.***:**:******::* * :   ::** **:* * :*:. : **:*     ... 

AGM16412.1_3015_mod3_thr                 -
P39846_A2_thr                            S
O87704_A2_4__thr                         A
                                          
