CLUSTAL W multiple sequence alignment


AHZ34233.1_2589_mod3_val                 ---------LAQR--------AASIDSATPRIDLDQLVLDDQPAHD-------PALVQNS
AGC65514.1_530_mod1_val                  NTEFDRLSGISGRVRIAYLPDANATSNA-------------SFAKRTGDDL---------
AGC65514.1_4147_mod3_val                 DAGHDALDALGA-----------LSESGHPSPQRIN-IGGIAPA---PTDFKLPELT-NA
AGC65516.1_1081_mod1_val                 DAGLAHWAALAPDITLVPLTAAMPVASAHDRPDR-------APAQRGPEDL---------
                                                  :.               ..               *                

AHZ34233.1_2589_mod3_val                 NSV-AYVMYTSGSTGAPKGVCVLHRGIARLVLNNGFADFNEQDRVAFASNPAFDASTMEV
AGC65514.1_530_mod1_val                  ----AYVMFTSGSTGAPKGVLIPNRAVARLVVNNRALAFDDTDVMAQAASLGFDAATLEI
AGC65514.1_4147_mod3_val                 DSLPAYIMYTSGSTGTPKGVLIEQGAVRRLASGADYAKIVASDVVAQAGPLAFDASTFEI
AGC65516.1_1081_mod1_val                 ----ACILYTSGSTGEPKGVAIPQRAIARLAHGGLCRP---GQTMAQAAPIAFDAICLEL
                                             * :::****** **** : : .: **. .         : :* *.  .***  :*:

AHZ34233.1_2589_mod3_val                 WGALLNGGQLLIIDHQTLIDPARLSDALRT--VSVLFLTTALFNQYVQLIPEALKGLRYL
AGC65514.1_530_mod1_val                  WAPLLNGGKLVLIDNETLFDPAALRNALEDGCVTTMWLTASLFNRIADDAPGCFAPLKRL
AGC65514.1_4147_mod3_val                 WATLLRGVEIAVIDRNDLLDPAKFGDALTQFGVTKMFMSVGLFNRQVDHDPQTLAGLDIL
AGC65516.1_1081_mod1_val                 WAPLLNHGRVRIIPAMTMFDPPLLERCLRDARVDVAWVTVSLLNRIIDDRPQTFAGLSLL
                                         *..**.  .: :*    ::**. :  .*    *   :::..*:*:  :  *  :  *  *

AHZ34233.1_2589_mod3_val                 MSGGERADPASFRAMLEHGPGPRLVNGYGPTETTTFATTNEVREVAEGAES--VSIGRPI
AGC65514.1_530_mod1_val                  LSGGEALSPTHLQKVMTACPGLALINGYGPTENTTFTCIHPITPQDVKSAN--IPIGRPI
AGC65514.1_4147_mod3_val                 MIGGDAISKQHVRNFINACPHVTVLNGYGPTESTTFAVVGPITENDLSGDTDATIIGKSI
AGC65516.1_1081_mod1_val                 ITGGEVVSPGHIARLIDACPGITVLNGYGPTENGTFTTTHIVTSADLDGGP--VPIGRPV
                                         : **:  .   .  .:   *   ::*******. **:    :      .      **:.:

AHZ34233.1_2589_mod3_val                 GNTSIYVLDAHQRLVPQGVIGELYIGGDGVAQGYLNRPDLTAEKFLTDPFSDEPGATMYR
AGC65514.1_530_mod1_val                  GNTRTYILDAGGQPVPTGVWGELYAAGDGLALGYSGAPERTAKAFVTFD--HLPETRLYK
AGC65514.1_4147_mod3_val                 AHTKTLILDQNGQRAPVGVWGELMIGGSRLAREYWQRSDLTGERFIPDP--DQPDQRLYR
AGC65516.1_1081_mod1_val                 ANTRIHVVDDRGRPVPPGVWGNLLAAGDGLAIGYVGRPDLTARSFVTLP--DIDEPLLYR
                                         .:*   ::*   : .* ** *:*  .*. :*  *   .: *.. *:.    .     :*:

AHZ34233.1_2589_mod3_val                 TGDLGRWLEDGQLECLGRNDDQVKIRGFRIELGEIVSRLHELPSVRDAVVL--AREDEPG
AGC65514.1_530_mod1_val                  TGDRARWRADGVIEFGGRRDGQVKIRGHRIETAAIEKRLSQIDGIRNAC----VMSVGSG
AGC65514.1_4147_mod3_val                 TGDLARWTRDGRIEFGGRHDNQIKLRGFRIELDEIEQQLQSAPGIKNAVALFDVNAPDGG
AGC65516.1_1081_mod1_val                 TGDRVRLRADGVLEFGGRRDGQLKIRGQRIETAAIEAALTARPGVRDAM----VVALGAG
                                         ***  *   ** :*  **.*.*:*:** ***   *   *    .:::*     .     *

AHZ34233.1_2589_mod3_val                 RVRLVAYFTQHQDAE------------
AGC65514.1_530_mod1_val                  A-----------DAFLGAAIAADQDNS
AGC65514.1_4147_mod3_val                 AIIGCIQTTQDQDIDIPALM-------
AGC65516.1_1081_mod1_val                 A-----------DQTLAALVAADTADP
                                                     *              
