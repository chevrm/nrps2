CLUSTAL W multiple sequence alignment


AGM16412.1_4054_mod4_orn                 LAQRHMQASVAFAGTWVILDEEAFYHEDGTNLEPLNEPMHLSYVIYTSGTTGNPKGVMIE
AGM16412.1_456_mod1_orn                  LAQRHLQARIAFTGTWVILDENAFYDEDGTNLESNNDPSNLSYVIYTSGTTGKPKGVMIE
                                         *****:** :**:********:***.*******. *:* :************:*******

AGM16412.1_4054_mod4_orn                 HRQLVAIADAWKREYRLEEEGIRWLQWASFSFDVFSGDMVRTLLYGGELILCPEQARANP
AGM16412.1_456_mod1_orn                  HRQLVAMAHAWKSRYHLHEAGIRWLQWASFSFDVFSGDMVRTLLNGGELILCPGHARANP
                                         ******:*.*** .*:*.* ************************ ******** :*****

AGM16412.1_4054_mod4_orn                 AAISELIRKHQIQMFESTPALVIPFMDYVYDNNLDISSLKMLIVGSDHCPTAEFDKLTER
AGM16412.1_456_mod1_orn                  EAICELIRKHRIQMFESTPALVVPLMEYIYDNKMDIHSLQLLIIGSDYCPAEEFQKLMER
                                          **.******:***********:*:*:*:***::** **::**:***:**: **:** **

AGM16412.1_4054_mod4_orn                 CGSHMRILNSYGVTEACVDACYYERTTPDALRTLPIGKPLPAVTMYILDDNRSLQPIGHT
AGM16412.1_456_mod1_orn                  FGSQMRILNSYGVTEACVDASYFEQTDSDALRTLPIGKPLPAVSMMVLDDNRSLQPIGIT
                                          **:****************.*:*:* .***************:* :*********** *

AGM16412.1_4054_mod4_orn                 GELYIGGAGVGRGYLNRPDLTVEKFVDNPFMPGARMYRTGDLARWLPDGNIEYAGRIDHQ
AGM16412.1_456_mod1_orn                  GELYIGGACVGRGYLNRPDLTAEKFVDNPYAPSEMMYRTGDLARWLPDGNIEYLGRIDHQ
                                         ******** ************.*******: *.  ****************** ******

AGM16412.1_4054_mod4_orn                 VKIRGYRIEIGEVESQLLAAAGVREAAVVAREDGSGQKVLCAYFVADSALTVGE
AGM16412.1_456_mod1_orn                  VKIRGYRIEIGEIESQLLKAESVRESVVVAREDGSGQKVLCAYYVADRELTVNE
                                         ************:***** * .***:.****************:***  ***.*
