CLUSTAL W multiple sequence alignment


O52820_A1_4__hpg|hpg2Cl                  VCAEAYRDAVLDVGLDPISLDDPQTRQAVAAAARISVGTKANDLAYVMYTSGSTGTPKGV
Q939Z0_A1_4__hpg|hpg2Cl                  VCAEAYRAAVPDTCPEPIVLDDPRTRQAVAESPRLSAGTSADDLAYVMYTSGSTGTPKGV
                                         ******* ** *.  :** ****:****** :.*:*.**.*:******************

O52820_A1_4__hpg|hpg2Cl                  AVSHGNVAALVGEPGWGVGPDDAVLMHASHAFDISLFEMWVPLLSGARLVLAGSGAVDGE
Q939Z0_A1_4__hpg|hpg2Cl                  AVSHGNVAALAGEPGWRVGPGDAVLLHASHAFDISLFEMWVPLLSGARVVLAGPGAVDGA
                                         **********.***** ***.****:**********************:****.***** 

O52820_A1_4__hpg|hpg2Cl                  ALAGYVAGGVTAAHLTAGAFRVLADESPESVVGLREVLTGGDAVPLAAVERVRRTCPDVR
Q939Z0_A1_4__hpg|hpg2Cl                  ALAAYVAGGVTAAHLTAGAFRVLADESPEAVAGLREVLTGGDAVPLAAVERVRGRVRNVR
                                         ***.*************************:*.*********************    :**

O52820_A1_4__hpg|hpg2Cl                  VRHLYGPTEATLCATWWLLEPGDETGSVLPIGRPLAGRRVYVLDAFLRPVPPGVAGELYI
Q939Z0_A1_4__hpg|hpg2Cl                  VRHLYGPTEATLCATWWLLEPGDETGSVLPIGRPLAGRRVHVLDAFLRPVPPGVAGELYV
                                         ****************************************:******************:

O52820_A1_4__hpg|hpg2Cl                  AGAGVAQGYLGRSALTAERFVADPFVAAGRMYRTGDLAYWTHQGALAFAGRADDQVKIRG
Q939Z0_A1_4__hpg|hpg2Cl                  AGAGVAQGYSSRPALTAERFVADPSGSGARMYRTGDLAYWTEQGALAFAGRADDQVKIRG
                                         ********* .*.***********  :..************.******************

O52820_A1_4__hpg|hpg2Cl                  YRVEPGEIEVVLAGLPGVGQAVVSVRDEHLIGYVVAEAGQAIDPAR
Q939Z0_A1_4__hpg|hpg2Cl                  YRVEPGEIEVVLAGLPGVGQAVVTPRGEHLIGYVVAEAGHDADPVR
                                         ***********************: *.************:  **.*
