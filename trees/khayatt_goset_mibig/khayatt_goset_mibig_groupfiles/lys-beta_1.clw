CLUSTAL W multiple sequence alignment


Q6WZ98_A1_lys-beta                       LGRAESLGELPGLGIPVIP---AEPPGDLAGGAPPATRADAEPPLPDDLAYVMLTSGTTG
O33743_A_lys-beta                        ----------------IRPRWTQTPVTTVLQGTLPESGAR-----PEDPAYMLFTSGSTG
                                                         : *     *   :  *: * : *      *:* **:::***:**

Q6WZ98_A1_lys-beta                       TPKAVLVPHRAVTRAARSLVPLFGVTSTDRVLHWTSLIWDTSGEEIYPALLGGAALVVDG
O33743_A_lys-beta                        EPKGVVVPHRAPAAVVPLLRDLYGIGPEETVLHFHGAGGDTSLEEILPALTGGATLVID-
                                          **.*:***** : ..  *  *:*: . : ***: .   *** *** *** ***:**:* 

Q6WZ98_A1_lys-beta                       RVETRSVPALLAAV-REHRVTVVDLPTAMWNELAHYLALGGEELPPALRLVVIGGEAAHA
O33743_A_lys-beta                        ----DAAPEGFARVAEEQQVSVAILPTGFWSSLTGDLLHQGARLPESLRTVVIGGEAVRA
                                              :.*  :* * .*::*:*. ***.:*..*:  *   * .** :** *******.:*

Q6WZ98_A1_lys-beta                       RTVRLWNERVP--DRVRLLNTYGQTETVMVTHAAELGGPAGRAL-RDGDPVPIGRPLPHI
O33743_A_lys-beta                        DMLERW-RRLPGTDGVRLLNTYGSTETALVTHAAQLAGPGAPALPGTGLDLPIGHPLPHV
                                           :. * .*:*  * ********.***.:*****:*.**.. **   *  :***:****:

Q6WZ98_A1_lys-beta                       RQVLVPSDDGPDELWTGGPGLAWGYADRPALTAAAFGPAPGAGGRFYRTGDLVRTLPDGS
O33743_A_lys-beta                        RQRV----DDRGELSIAGPGLALGYHGNPGATSARFRERDGT--RWYRTGDLVTEAPGGI
                                         ** :    *. .**  .***** ** ..*. *:* *    *:  *:*******   *.* 

Q6WZ98_A1_lys-beta                       LVHAGRADRRLKVRGVRVEPAEVERAMTTCPGVVAAAVFPVG----------------DD
O33743_A_lys-beta                        LVFRGRSDHQVKIRGFRVDLLDVEALIGRCEGVAAVAAARVDRTEHGVLAAFFVALPHRE
                                         **. **:*:::*:**.**:  :**  :  * **.*.*.  *.                 :

Q6WZ98_A1_lys-beta                       PE--
O33743_A_lys-beta                        PDEI
                                         *:  
