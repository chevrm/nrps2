#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

## List of temp files created by NRPSPredictor2 for cleanup
my @tmp = (
	'nrpspredictor2_codes.txt',
	'input.sig',
	'muscle.fasta',
	'infile.fasta',
	'query.tmp'
);
## Set up environment
my $mdldir = './NRPSPredictor2/data/models';
my $nrps2 = './NRPSPredictor2/NRPSpredictor2.sh';
my $codepred = './NRPSPredictor2/nrpscodepred.py';
system("rm -r $mdldir/N*SINGLE*") if(-d "$mdldir/N*SINGLE*");

print join("\t", 'shuffle', 'jackknife', 'query', 'method', 'spec') . "\n";
foreach my $faa ( glob("../benchmarks/secondjk/jk*/*.faa") ){
    next if($faa =~ m/stach/);
    ## Parse filename
    my @p = split(/\//, $faa);
    my ($jk, $k) = ($p[3], $p[4]);
    $jk =~ s/jk//;
    $k =~ s/fullset_smiles_knife//;
    $k =~ s/\.faa//;
    ## Copy svm to NRPSPredictor2 model directory
    my $svmdir = 'jackknife_smile_training_folders/jk'.$jk.'_fullset_smiles_knife'.$k.'_train.faa/NRPS2_SINGLE_CLUSTER';
    system("cp -r $svmdir $mdldir");
    ## Get signatures
    system("python $codepred $faa");
    ## Run SVMs
    system("$nrps2 -i input.sig -r query.rep -s 1 1>/dev/null");
    ## Get results
    open my $rep, '<', 'query.rep' or die $!;
    while(<$rep>){
	next if($_ =~ m/^#/);
	chomp;
	my @res = split(/\t/, $_);
	my ($seqid, $svmpred) = ($res[0], $res[6]);
	print join("\t", 'jk' . $jk, 'k' . $k . '_as_query', $res[0], 'svm', $res[6]) . "\n";
    }
    close $rep;
    system("rm -r $mdldir/N*SINGLE*");
}
## Cleanup
foreach my $t (@tmp){
    system("rm $t") if(-e $t);
}
