#!/bin/env perl

use strict;
use warnings;

my %s = ();
while(<>){
	chomp;
	if($_=~ m/^>/){
		my @h = split(/_+/, $_);
		$s{$h[-1]} += 1;
	}
}
foreach my $sp ( sort {$s{$b} <=> $s{$a}} keys %s){
	print join("\t", $s{$sp}, $sp) . "\n";
}
