#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

my ($bl, $db) = (shift, shift);
my %seen = ();
my $blfa = new Bio::SeqIO(-file=>$bl, -format=>'fasta');
while(my $seq=$blfa->next_seq){
	$seen{$seq->id} = 1;
}
my $dbfa = new Bio::SeqIO(-file=>$db, -format=>'fasta');
while(my $seq=$dbfa->next_seq){
	print '>' . $seq->id . "\n" . $seq->seq . "\n" unless(exists $seen{$seq->id});
}
