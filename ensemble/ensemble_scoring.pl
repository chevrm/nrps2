#!/bin/env perl

use strict;
use warnings;

my $dfile = './alldata.tsv';
my %da = ();
open my $dfh, '<', $dfile or die $!;
while(<$dfh>){
	chomp;
	next if ($_ =~ m/^row/);
	my ($row, $shuf, $jk, $query, $pid, $truespec, $called_spec, $method, $covered, $correct, $methshuf, $uname, $bin) = split(/\t/, $_);
	unless(exists $da{$uname}){
		$da{$uname}{'true'} = $truespec;
		$da{$uname}{'pid'} = $pid;
		$da{$uname}{'shuf'} = $shuf;
		$da{$uname}{'jk'} = $jk;
		$da{$uname}{'query'} = $query;
		$da{$uname}{'bin'} = $bin;
	}
	$called_spec = 'no_call' if($covered eq 'N');
	$da{$uname}{'method'}{$method} = $called_spec;
}
close $dfh;

my @m = ('prediCAT', 'forced_prediCAT_snn00', 'svm', 'stach', 'forced_prediCAT', 'pHMM');

print join("\t", 'shuffle', 'jackknife', 'query', 'pid', 'spec', 'called_spec',
	'method', 'call_made', 'call', 'methshuf', 'uname', 'bin'
#	, 'submeth'
) . "\n";

foreach my $uname (keys %da){
	foreach(@m){
		$da{$uname}{'method'}{$_} = 'no_call' unless(exists $da{$uname}{'method'}{$_});
	}
	my ($spec, $submeth) = algdecide(
				$da{$uname}{'pid'}, 
				$da{$uname}{'method'}{'prediCAT'},
				$da{$uname}{'method'}{'forced_prediCAT_snn00'},
				$da{$uname}{'method'}{'svm'},
				$da{$uname}{'method'}{'stach'}
	);
	my $cov = 'Y';
	$cov = 'N' if($spec eq 'no_call');
	my @c = split(/\|/, $spec);
	my @t = split(/\|/, $da{$uname}{'true'});
	my $call = 'N';
	foreach my $cq (@c){
		foreach my $tq (@t){
			$call = 'Y' if($cq eq $tq);
		}
	}
	print join("\t",
		$da{$uname}{'shuf'},
		$da{$uname}{'jk'},
		$da{$uname}{'query'},
		$da{$uname}{'pid'},
		$da{$uname}{'true'},
		$spec,
		'ensemble50',
		$cov,
		$call,
		join('_', 'ensemble50', $da{$uname}{'shuf'}),
		$uname,
		$da{$uname}{'bin'},
#		$submeth
	) . "\n";
	my ($fspec, $fsubmeth) = algdecideforce(
					$da{$uname}{'pid'},
					$spec,
					$submeth,
					$da{$uname}{'method'}{'forced_prediCAT'},
					$da{$uname}{'method'}{'pHMM'}
	);
	my $fcov = 'Y';
	$fcov = 'N' if($fspec eq 'no_call');
	my @fc = split(/\|/, $fspec);
	my $fcall = 'N';
	foreach my $fcq (@fc){
		foreach my $tq (@t){
			$fcall = 'Y' if($fcq eq $tq);
		}
	}
	print join("\t",
		$da{$uname}{'shuf'},
		$da{$uname}{'jk'},
		$da{$uname}{'query'},
		$da{$uname}{'pid'},
		$da{$uname}{'true'},
		$fspec,
		'forced_ensemble50',
		$fcov,
		$fcall,
		join('_', 'forced_ensemble50', $da{$uname}{'shuf'}),
		$uname,
		$da{$uname}{'bin'},
#		$fsubmeth
	) . "\n";
}

sub algdecide{
	my ($pid, $predicat, $snn, $svm, $asm) = @_;
	if($pid > 80){
		if($asm ne 'no_call'){
			my @c1 = ('lys');
			if( algexception($snn, @c1) ){
				return ($snn, 'SNN50');
			}else{
				my @c2 = ('val', 'pip', 'bht', 'lys', 'dhpg', 'ile', 'tyr', 'phe', 'thr');
				if( algexception($svm, @c2) ){
					return ($svm, 'SVM');
				}else{
					return ($asm, 'ASM');
				}
			}
		}elsif($snn ne 'no_call'){
			return ($snn, 'SNN50');
		}else{
			return ('no_call', 'NO CONFIDENCE');
		}
	}elsif($pid > 60 && $pid <= 80){
		if($asm ne 'no_call'){
			my @c1 = ('abu', 'iva', 'trp');
			if( algexception($svm, @c1) ){
				return ($svm, 'SVM');
			}else{
				return ($asm, 'ASM');
			}
		}elsif($svm ne 'no_call'){
			my @c1 = ('me-asp', 'phe-ac');
			if( algexception($snn, @c1) ){
				return ($snn, 'SNN50');
			}else{
				return ($svm, 'SVM');
			}
		}elsif($snn ne 'no_call'){
			return ($snn, 'SNN50');
		}else{
			return ('no_call', 'NO CONFIDENCE');
		}
	}elsif($pid <= 60){
		if($svm ne 'no_call'){
			my @c1 = ('b-ala');
			if( algexception($asm, @c1) ){
				return ($asm, 'ASM');
			}else{
				return ($svm, 'SVM');
			}
		}elsif($asm ne 'no_call'){
			return ($asm, 'ASM');
		}else{
			return ('no_call', 'NO CONFIDENCE');
		}
	}else{
		die "PID error: $pid\n";
	}
}

sub algdecideforce{
        my ($pid, $adcall, $admeth, $fp, $phmm) = @_;
	if($adcall eq 'no_call'){
	        if($pid > 60){
			return ($fp, 'Forced_prediCAT');
		}elsif($pid <= 60){
			return ($phmm, 'pHMM');
		}else{
			die "PID error: $pid\n";
		}
	}else{
		return ($adcall, $admeth);
	}
}

sub algexception{
	my ($query, @check) = @_;
	my @q = split(/\|/, $query);
	foreach my $a (@q){
		foreach my $b (@check){
			return 1 if($a eq $b);
		}
	}
	return 0;
}
