#!/bin/env perl

use strict;
use warnings;
use LWP::Simple;


my (%uniprot, %genbank, %ac) = ((),(),());
my $faa = '../consolseqs/K_only_cl.faa';
open my $ffh, '<', $faa or die $!;
while(<$ffh>){
	chomp;
	if($_ =~ m/^>(\S+)/){
		my $head = $1;
		my @header = split(/_+/, $head);
		#$header[0] =~ s/ADL64235/ADL64235\.1/;
		#$header[0] =~ s/CBG75492/CBG75492\.1/;
		if($header[0] =~ m/^BGC/){
			if(length($header[1]) == 2){
				$genbank{join('_', $header[1], $header[2])} = 1;
				push @{$ac{join('_', $header[1], $header[2])}}, $head;
			}else{
				$header[1] =~ s/xnc1/FN667742\.1/;
				$header[1] =~ s/XBJ1/FN667741\.1/;
				$header[1] =~ s/509516289/CM001889\.1/;
				$genbank{$header[1]} = 1;
				push @{$ac{$header[1]}}, $head;
			}
		}elsif(length($header[0]) == 2){
			$genbank{join('_', $header[0], $header[1])} = 1;
			push @{$ac{join('_', $header[0], $header[1])}}, $head;
		}elsif($header[0] =~ m/\./){
			$genbank{$header[0]} = 1;
			push @{$ac{$header[0]}}, $head;
		}else{
			unless($header[0] eq 'removed' || $header[0] eq 'simA'){
				$header[0] =~ s/v\d+$//;
				$header[0] =~ s/\D$//;
				$header[0] =~ s/\/.+//;
				if($header[0] =~ m/^\w\d/){
					$header[0] =~ s/P71717/P9WQ62/;
					$uniprot{$header[0]} = 1;
				}else{
					$header[0] .= '.1';
					$genbank{$header[0]} = 1;
				}
				push @{$ac{$header[0]}}, $head;
			}
		}
	}
}
close $ffh;
my ($u, $g) = (scalar(keys %uniprot), scalar(keys %genbank));
foreach (sort keys %genbank){
	my $xml = `esearch -db protein -query \"$_\" | efetch -format gpc`;
	my $taxstring = 'no-taxon-fetched';
	if($xml =~ m/<INSDSeq_taxonomy>(.+)</){
		$taxstring = $1;
		$taxstring =~ s/;\s+/_/g;
	}else{
		$xml = `esearch -db nucleotide -query \"$_\" | efetch -format gpc`;
		if($xml =~ m/<INSDSeq_taxonomy>(.+)</){
			$taxstring = $1;
			$taxstring =~ s/;\s+/_/g;
		}
	}
	$genbank{$_} = $taxstring;
}
foreach(sort keys %genbank){
	foreach my $h (@{$ac{$_}}){
		print join("\t", $h, $genbank{$_}) . "\n";
	}
}
foreach (sort keys %uniprot){
	my $url = "http://www.uniprot.org/uniprot/$_";
	my $page = get($url) or die "$url not found\n";
	#print $page;
	my @pass = ();
	if($page =~ m/Taxonomic lineage(.+)/){
		my @spl = split(/<a href=..taxonomy.\d+.>/, $1);
		foreach my $l (@spl){
			$l =~ s/^(\w+)<.+/$1/;
			unless($l =~ m/\s/){
				push @pass, $l if($l =~ m/^\w/);
			}
		}
	}
	if($_ eq 'Q9L8H4'){
		@pass = ('Bacteria', 'Actinobacteria', 'Actinobacteridae', 'Actinomycetales', 'Streptomycineae', 'Streptomycetaceae', 'Streptomyces');
	}elsif($_ eq 'O86329'){
		@pass = ('Bacteria', 'Actinobacteria', 'Actinobacteridae', 'Actinomycetales', 'Corynebacterineae', 'Mycobacteriaceae', 'Mycobacterium');
	}
	$uniprot{$_} = join('_', @pass);
}
foreach(sort keys %uniprot){
	foreach my $h (@{$ac{$_}}){
		print join("\t", $h, $uniprot{$_}) . "\n";
	}
}
