#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

my $kf = new Bio::SeqIO(-file=>shift, -format=>'fasta');
my %k = ();
while(my $seq=$kf->next_seq){
	$k{$seq->seq} += 1;
}
print scalar(keys %k) . " codes in known\n";
my %f = ();
my $ff = new Bio::SeqIO(-file=>shift, -format=>'fasta');
while(my $seq=$ff->next_seq){
	$f{$seq->seq} += 1;
}
print scalar(keys %f) . " codes in full\n";
my $c = 0;
foreach my $known (keys %k){
	unless(exists $f{$known}){
		#print "$known not found\n";
		$c += 1;
	}
}
print "$c codes in known not in full\n";
