#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

my ($trainstach, $querystach) = (shift, shift);
my %code2spec = ();
my $tfa = new Bio::SeqIO(-file=>$trainstach, -format=>'fasta');
while(my $seq = $tfa->next_seq){
	next if($seq->seq =~ m/-/);
	my @id = split(/_+/, $seq->id);
	$code2spec{$seq->seq}{$id[-1]} += 1;
}
#foreach my $c (sort keys %code2spec){
#	print "$c\t" . join('|', sort keys %{$code2spec{$c}}) . "\n";
#}
my $qfa = new Bio::SeqIO(-file=>$querystach, -format=>'fasta');
print join("\t", 'Query', 'Call') . "\n";
while(my $seq = $qfa->next_seq){
	my $call = undef;
	if(exists $code2spec{$seq->seq}){
		$call = join('|', sort keys %{$code2spec{$seq->seq}});
	}else{
		$call = 'no_call';
	}
	print join("\t", $seq->id, $call) . "\n";
}
