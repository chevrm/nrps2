#!/bin/env perl

use strict;
use warnings;

my %ft = ();
while(<>){
	chomp;
	my ($from, $to) = split(/\t/, $_);
	$ft{$from} = $to;
}
system("cp mibig_adom.faa tmp");
open my $t, '<', 'tmp' or die $!;
open my $o, '>', 'mibig_adom.faa' or die $!;
while(<$t>){
	chomp;
	if($_ =~ m/^>/){
		my @header = split(/_/, $_);
		my $lead = join '_', @header[0..$#header-1];
		$_ = $lead . '_' . $ft{$header[-1]};
	}
	print $o "$_\n";
}
close $t;
close $o;
system("rm tmp");
