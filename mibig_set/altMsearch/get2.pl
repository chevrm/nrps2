#!/bin/env perl

use strict;
use warnings;

my %seen = ();
while(<>){
	chomp;
	my ($query, $hit, $pctid, $alen, $mismatch, $gapopen, $qstart, $qend, $sstart, $send, $evalue, $bitscore) = split(/\t/, $_);
	$seen{$hit}{$query} += 1;
}
foreach my $h (keys %seen){
	print "$h";
	#foreach my $q (sort keys %{$seen{$h}}){
	#	print "\t$q" . '= ' . $seen{$h}{$q};
	#}
	print "\n";
}
