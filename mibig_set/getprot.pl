#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

my %g2b = (); 
open my $lfh, '<', 'mibig_set.tsv' or die $!; 
while(<$lfh>){
	next if ($_ =~ m/^Cluster/);
	chomp;
	my ($bgc, $mod, $evid, $spec, $gene) = split(/\t/, $_);
	#unless($mod =~ m/\w?(\d+)/){
	#	print STDERR "Issue parsing $bgc:\n\tbgc:\t$bgc\n\tmod:\t$mod\n";
	#	next;
	#}
	$g2b{$gene} = $bgc;
}
close $lfh;
open my $bo, '>', 'mibig_nrps.faa' or die $!;
foreach my $g (keys %g2b){
	system("esearch -db protein -query \"$g\" | efetch -format fasta |grep . > tmp.faa");
	my $t = new Bio::SeqIO(-file=>'tmp.faa', -format=>'fasta');
	my $found = 0;
	while(my $seq = $t->next_seq){
		die "ERROR:\t$found sequences found for $g\n" if($found > 1);
		$found+=1;
		print $bo '>' . join('_', $g2b{$g}, $g) . "\n" . $seq->seq . "\n";
	}
}
close $bo;
system("rm tmp.faa");
